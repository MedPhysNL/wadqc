#imports
from flask import Flask, jsonify, request, Response, send_file, make_response
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity
from wad_qc.connection import dbio
from wad_qc.connection.dbio_models import DBDataSources
from wad_qc.connection.exchange import export_wadqc, import_wadqc

import os
import datetime
import json
import jsmin
import codecs
from functools import wraps
from peewee import IntegrityError
from io import BytesIO

# Use wad_admin login
from werkzeug.security import check_password_hash, generate_password_hash
try:
    from app.mod_wadconfig.models import WAUsers, role_names, role_numbers
except ImportError:
    from wad_admin.app.mod_wadconfig.models import WAUsers, role_names, role_numbers

"""
roles:

admin: not allowed in wadapi until wad_admin is superceded
rest_full: closest to admin, just not possible to create/delete admin users;
           needed for admin-routes (services,users,etc)
rest_major: needed to delete stuff, create stuff (POST, DELETE)
rest_minor: GET requests only (Exception: rest_minor can POST wadwriter)
"""

#connection info to WAD DB
INIFILE = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')

# Define the WSGI application object
flask_app = Flask(__name__)

# Make a list of true/false-values we can use for different routes since true/false values in a request might look differently
truelist=['true','True','TRUE',1,'1','yes','Yes','YES','OK','ok','Ok']
falselist=['false','False','FALSE','0',0,'no','No','NO','error','Error']

# Generate a random secret key for signing cookies
import uuid
flask_app.config['JWT_SECRET_KEY'] = generate_password_hash(str(uuid.uuid4()))
jwt = JWTManager(flask_app)

flask_app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=5)
# flask_app.config['JWT_ACCESS_TOKEN_EXPIRES'] = 10
flask_app.config['JWT_HEADER_TYPE'] = 'JWT'


@flask_app.before_request
def before_request():
    if dbio.db.is_closed():
        try:
            dbio.db_connect(INIFILE)
        except Exception as e:
            raise RuntimeError("Cannot connect to WADQC")

@flask_app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin','*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response

@flask_app.teardown_request
def teardown_request(exception):
    if not dbio.db.is_closed:
        dbio.db.close()


# helper functions
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]


def get_current_user_role(username):
    if username is None:
        # get the actual user
        if request.headers['Authorization'].startswith("JWT"):
            username = get_jwt_identity()
        elif request.headers['Authorization'].startswith("Basic"):
            username = (request.authorization["username"])
        else:
            return False
    try:
        user = WAUsers.get(WAUsers.username==username)
        if user:
            return role_names.get(user.role)
    except WAUsers.DoesNotExist:
        return False

def get_current_user_id(username):
    if username is None:
        # get the actual user
        if request.headers['Authorization'].startswith("JWT"):
            username = get_jwt_identity()
        elif request.headers['Authorization'].startswith("Basic"):
            username = (request.authorization["username"])
        else:
            return False
    try:
        user = WAUsers.get(WAUsers.username==username)
        if user:
            return user.id
    except WAUsers.DoesNotExist:
        return False

# route functions
def _login(username, password):
    """
    Use WAUsers to authenticate.
    First check password, then check if REST access is granted
    """
    try:
        user = WAUsers.get(WAUsers.username==username)

        if user:
            if check_password_hash(user.password, password):
                if user.status == 0:
                    return {'success':False, 'msg':'User not activated!'}
                if role_names.get(user.role, None) in [None, 'admin']:
                    return {'success':False, 'msg':'User has no REST access! {} {}'.format(user.role, role_names.get(user.role, None))}
               
                return {'success':True, 'msg':'Welcome!', 'user':user.serialize}
            else:
                return {'success':False, 'msg':'Wrong password!'}
        else:
            return {'success':False, 'msg':'Wrong username!'}

    except WAUsers.DoesNotExist:
        return {'success':False, 'msg':'Wrong username!'}

# Decorators
def required_roles(*roles):
    def wrapper(f):
        @wraps(f)
        @jwt_required(optional=True)
        def wrapped(*args,**kwargs):
            if 'Authorization' in request.headers:
                if request.headers['Authorization'].startswith("JWT"):
                    username = get_jwt_identity()
                    if get_current_user_role(username) not in roles:
                        return jsonify({'success':False, 'msg':"User not authorized!"}),403
                    else:
                        return f(*args,**kwargs)
                elif request.headers['Authorization'].startswith("Basic"):
                    username = (request.authorization["username"])
                    password = (request.authorization["password"])
                    valid_login = _login(username,password)
                    if valid_login:
                        if get_current_user_role(username) not in roles:
                            return Response('Please authenticate...', 401, {'WWW-Authenticate':'Basic realm="User not authorized!"'})
                        else:
                            return f(*args,**kwargs)
            else:
                return Response('Please authenticate...', 401, {'WWW-Authenticate':'Basic realm="Authentication Required"'})
        return wrapped    
    return wrapper   

# Routes
# catch all undefined routes
@flask_app.route('/', defaults={'path': ''}, methods=['POST', 'GET', 'DELETE', 'HEAD', 'PUT', 'OPTIONS'])
@flask_app.route('/<path:path>', methods=['POST', 'GET', 'DELETE', 'HEAD', 'PUT', 'OPTIONS'])
def catch_all(path):
    return jsonify({'success':False, 'msg':"'{}' is not a valid endpoint".format(path)}),405

@flask_app.route('/api/authenticate', methods=['POST'])
def authenticate_post():
    try:
        if request.is_json:
            username=request.json.get('username', None)
            password=request.json.get('password', None)
        else:
            username=request.form.get('username', None)
            password=request.form.get('password', None)
    except:
        return jsonify({"success":False, "msg":"No logindata given"}),401
    if not username:
        return jsonify({"success":False, "msg":"No user given"}),401
    if not password:
        return jsonify({"success":False, "msg":"No password given"}),401

    loginresponse = _login(username, password)
    if loginresponse['success']:
        access_token = create_access_token(identity=username)
        return jsonify({'token':access_token, 'success':True, 'user':loginresponse['user']}),200
    else:
        return jsonify({"success":False,'msg':loginresponse}),401
        
@flask_app.route('/api/verifytoken', methods=['POST'])
@jwt_required()
def verifytoken_post():
    user = get_jwt_identity()
    if get_jwt_identity():
        return jsonify({"success":True, "msg":"Welcome back", "rest_full":get_current_user_role(user)=='rest_full'}),200
    else:
        return jsonify({"success":False, "msg":"Invalid or no token"}),401

@flask_app.route('/api/selectors', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selectors_get():
    if request.method=='GET':
        selectors = dbio.DBSelectors.select()       
        return jsonify({'success':True, 'selectors':[s.serialize for s in selectors]}),200
            
@flask_app.route('/api/selectors', methods=['POST'])
@required_roles('rest_full','rest_major')
def selectors_post():
    if request.method=='POST':
        new_selector={}
        existingnames = [v.name for v in dbio.DBSelectors.select()]
        if request.form.get('name') in existingnames:
            return jsonify({'success':False,'msg':'Selector name already in use.'}),400
            
        new_selector['name'] = (request.form.get('name') or 'New selector')
        new_selector['description'] = (request.form.get('description') or 'New description')
        new_selector['isactive'] = False
        
        if request.form.get('id_config'):
            new_selector['module_config'] = request.form.get('id_config')
        else:
            return jsonify({'success':False,'msg':'No config given'}),400
            
        try:
            id_new_selector = dbio.DBSelectors.create(**new_selector)
            return jsonify({'success':True,'msg':'New selector created','id':str(id_new_selector)}),201
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403
            
@flask_app.route('/api/selectors/<int:id_selector>', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selector_get(id_selector):
    try:
        selector = dbio.DBSelectors.get(dbio.DBSelectors.id==id_selector)
    except dbio.DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)}),404
    if not selector:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)}),404

    if request.args.get('download') in truelist:
        temp_dir = dbio.DBVariables.get_by_name('temp_dir').val
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)      
    
        with tempfile.SpooledTemporaryFile(dir=temp_dir) as tmpf:
            error,msg = export_wadqc(dbio, tmpf, [], [id_selector], logger=None)
            
            if error:
                return jsonify({'success':False,'msg':msg}),400
            else:
                tmpf.seek(0)
                return send_file(BytesIO(tmpf.read()), as_attachment=True, download_name='selector_'+str(id_selector)+'.zip')#mimetype=None

    else:
        return jsonify({'success':True, 'selector':selector.serialize}),200
                      
@flask_app.route('/api/selectors/<int:id_selector>', methods=['PUT','DELETE'])
@required_roles('rest_full','rest_major')
def selector_putdel(id_selector):
    try:
        selector = dbio.DBSelectors.get(dbio.DBSelectors.id==id_selector)
    except dbio.DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)}),404
    if not selector:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)}),404

    if request.method=='PUT':
        other_existingnames = [v.name for v in dbio.DBSelectors.select().where(dbio.DBSelectors.id != id_selector)]
    
        try:
            if request.form.get('name') in other_existingnames:
                return jsonify({'success':False,'msg':'Selector name already in use.'}),400
            selector.name = (request.form.get('name') or selector.name)
            selector.description = (request.form.get('description') or selector.description)
            if (request.form.get('isactive') in truelist):
                selector.isactive = True
            else:
                selector.isactive = False
            if (request.form.get('manual_input') in truelist):
                selector.manual_input = True
            else:
                selector.manual_input = False
            selector.module_config = (request.form.get('id_config') or selector.module_config)
            selector.save()
            return jsonify({'success':True, 'msg':'Selector '+str(selector.id)+' updated','id':selector.id}),201
        except Exception as e:
            return jsonify({'success':False, 'msg':str(e)}),400
            
    if request.method == 'DELETE':
        with dbio.db.atomic() as trx:
            try:
                selector.delete_instance(recursive=True)
            except IntegrityError as e:
                trx.rollback()
                if 'violates foreign key constraint' in str(e):
                    return jsonify({'success':False,'msg':'This selector is referenced from a table from an external service (e.g. WAD Dashboard). Remove the reference and try again.'}),400
        return jsonify({'success':True,'msg':'Selector deleted.'}),200 
    
@flask_app.route('/api/selectors/<int:id_selector>/rules', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selector_rules_get(id_selector):
    try:
        selector = dbio.DBSelectors.get(dbio.DBSelectors.id==id_selector)
    except dbio.DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
    if not selector:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
        
    rules=[]
    for row in selector.rules:
        rule={}
        rule['id']=row.id
        rule['dicomtag']=row.dicomtag
        rule['logic']=row.logic.name
        rule['values'] = []
        for item in row.values:
            rule['values'].append(item.val)
        rules.append(rule)
    return jsonify({'success':True,'rules':rules,'id_selector':id_selector}),200
        
@flask_app.route('/api/selectors/<int:id_selector>/rules', methods=['POST'])
@required_roles('rest_full', 'rest_major')
def selector_rules_post(id_selector):
    try:
        selector = dbio.DBSelectors.get(dbio.DBSelectors.id==id_selector)
    except dbio.DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
    if not selector:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404

    values = request.form.get('values')
    dicomtag = request.form.get('dicomtag')                                 # we should check if the input is a valid dicomtag (xxxx,xxxx)
    logic = request.form.get('logic')
    if values is None:
        return jsonify({'success':False,'msg':'Value(s) missing.'}),400
    if ';' in values:
        values = values.split(';')                                          # multiple values should be separated by ;
    else:
        values = [values]
    if dicomtag is None:
        return jsonify({'success':False,'msg':'Dicomtag is missing.'}),400
    if logic is None:
        return jsonify({'success':False,'msg':'Logic is missing.'}),400
    try:
        id_rule = selector.addRule(dicomtag,logic,values)
        return jsonify({'success':True,'msg':'Rule(s) added.','id_rule':str(id_rule)}),201
    except Exception as e:
        return jsonify({'success':False,'msg':str(e)}),403

@flask_app.route('/api/selectors/<int:id_selector>/rules/<int:id_rule>', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selector_rule_get(id_selector,id_rule):
    try:
        rule = dbio.DBSelectorRules.get_by_id(id_rule)
    except dbio.DBSelectorRules.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no rule with id '+str(id_rule)+'.'}),404
    if not rule:
        return jsonify({'success':False,'msg':'There is no rule with id '+str(id_rule)+'.'}),404
        
    resp={}
    resp['id']=rule.id
    resp['dicomtag']=rule.dicomtag
    resp['logic']=rule.logic.name
    resp['values'] = []
    for item in rule.values:
        resp['values'].append(item.val)
    selector={'id':rule.selector.id,'name':rule.selector.name}
    return jsonify({'success':True,'rule':resp,'selector':selector}),200

@flask_app.route('/api/selectors/<int:id_selector>/rules/<int:id_rule>', methods=['PUT','DELETE'])
@required_roles('rest_full','rest_major')
def selector_rule_putdel(id_selector,id_rule):
    try:
        rule = dbio.DBSelectorRules.get_by_id(id_rule)
    except dbio.DBSelectorRules.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no rule with id '+str(id_rule)+'.'}),404
    if not rule:
        return jsonify({'success':False,'msg':'There is no rule with id '+str(id_rule)+'.'}),404
        
    if request.method == 'DELETE':
        try:
            rule.delete_instance(recursive=True)
            return jsonify({'success':True,'msg':'Rule deleted.'}),200
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403
            
    if request.method == 'PUT':
        rule.dicomtag = (request.form.get('dicomtag') or rule.dicomtag)
        rule.logic = (request.form.get('logic') or rule.logic)
        rule.save()
        
        values = request.form.get('values')
        if values is None:
            return jsonify({'success':False,'msg':'Value(s) missing.'}),400
        else:
            if ';' in values:
                values = values.split(';') 
   
            try:
                d = dbio.DBRuleValues.delete().where(dbio.DBRuleValues.rule == rule.id)
                d.execute()
                for value in values:
                    dbio.DBRuleValues.create(rule=rule, val=value)
                return jsonify({'success':True,'msg':'Rule(s) updated.'}),201
                
            except Exception as e:
                return jsonify({'success':False,'msg':str(e)}),403
 
@flask_app.route('/api/selectors/<int:id_selector>/results', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selector_results_get(id_selector):
    try:
        selector = dbio.DBSelectors.get(dbio.DBSelectors.id==id_selector)
    except dbio.DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
    if not selector:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404

    results = [{'id':result.id,'date':result.getDate()} for result in selector.results]     
    
    return jsonify({'success':True, 'results':results, 'selector':selector.serialize}),200

@flask_app.route('/api/selectors/<int:id_selector>/datasets', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selector_datasets_get(id_selector):
    try:
        selector = dbio.DBSelectors.get(dbio.DBSelectors.id==id_selector)
    except dbio.DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
    if not selector:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404

    results = [{'source':result.data_set.data_source.name,
                'type': result.data_set.data_type.name,
                'uid': result.data_set.data_id,
                'references': len(result.data_set.results)+len(result.data_set.processes),
                'status': 'result'} for result in selector.results]     
    results.extend(
        [{'source':result.data_set.data_source.name,
          'type': result.data_set.data_type.name,
          'uid': result.data_set.data_id,
          'references': len(result.data_set.results)+len(result.data_set.processes),
          'status': 'process'} for result in selector.processes])

    return jsonify({'success':True, 'results':results, 'selector':selector.serialize}),200

@flask_app.route('/api/selectors/<int:id_selector>/processes', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selector_processes_get(id_selector):
    try:
        selector = dbio.DBSelectors.get(dbio.DBSelectors.id==id_selector)
    except dbio.DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
    if not selector:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
    
    processes = {}
    for name in [status.name for status in dbio.DBProcessStatus.select()]:
        processes[name] = [process.serialize for process in selector.processes if process.process_status.name == name]
    
    return jsonify({'success':True, 'processes':processes, 'selector':selector.serialize}),200
    
@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selector_result_get(id_selector, id_result):
    try:
        result = dbio.DBResults.get_by_id(id_result)
    except dbio.DBResults.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no result with id '+str(id_result)+'.'}),404
    if not result:
        return jsonify({'success':False,'msg':'There is no result with id '+str(id_result)+'.'}),404
    
    tests=result.getResults()
    meta = json.loads(jsmin.jsmin(bytes_as_string(result.module_config.meta.val)))['results']
    return jsonify({
        'success':True,
        'result':result.serialize,
        'tests':[t.meta_serialize(meta) for t in tests],
        'selector':result.selector.serialize
    }),200 

@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>', methods=['DELETE'])
@required_roles('rest_full','rest_major')
def selector_result_del(id_selector, id_result):
    try:
        result = dbio.DBResults.get_by_id(id_result)
    except dbio.DBResults.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no result with id '+str(id_result)+'.'}),404
    if not result:
        return jsonify({'success':False,'msg':'There is no result with id '+str(id_result)+'.'}),404

    try:
        result.delete_instance(recursive=True)
        return jsonify({'success':True,'msg':'Result deleted.'}),200
    except Exception as e:
        return jsonify({'success':False,'msg':str(e)}),403

@flask_app.route('/api/selectors/<int:id_selector>/results/last', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selector_resultlast_get(id_selector):
    try:
        selector = dbio.DBSelectors.get_by_id(id_selector)
    except dbio.DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
    if not selector:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
        
    if len(selector.results)==0:
        return jsonify({'success':False,'msg':'No results for this selector (yet).'}),404
       
    results = [{'id':result.id,'date':result.getDate()} for result in selector.results]
    results.sort(key=lambda r: r['date'],reverse=True)
    id_result = results[0]['id']
    result = dbio.DBResults.get_by_id(id_result)

    tests=result.getResults()
    meta = json.loads(jsmin.jsmin(bytes_as_string(result.module_config.meta.val)))['results']
    return jsonify({
        'success':True,
        'result':result.serialize,
        'tests':[t.meta_serialize(meta) for t in tests],
        'selector':result.selector.serialize
    }),200         

@flask_app.route('/api/selectors/<int:id_selector>/results/last', methods=['DELETE'])
@required_roles('rest_full', 'rest_major')
def selector_resultlast_del(id_selector):
    try:
        selector = dbio.DBSelectors.get_by_id(id_selector)
    except dbio.DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
    if not selector:
        return jsonify({'success':False,'msg':'There is no selector with id '+str(id_selector)+'.'}),404
        
    results = [{'id':result.id,'date':result.getDate()} for result in selector.results]
    results.sort(key=lambda r: r['date'],reverse=True)
    id_result = results[0]['id']
    result = dbio.DBResults.get_by_id(id_result)

    try:
        result.delete_instance(recursive=True)
        return jsonify({'success':True,'msg':'Result deleted.'}),200
    except Exception as e:
        return jsonify({'success':False,'msg':str(e)}),403

@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/tests/<int:id_test>/<path:rtype>', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selector_result_test_type_get(id_selector, id_result, id_test, rtype):
    if rtype=='float':
        test = dbio.DBResultFloats.get_by_id(id_test)
    elif rtype=='bool':
        test = dbio.DBResultBools.get_by_id(id_test)
    elif rtype=='datetime':
        test = dbio.DBResultDateTimes.get_by_id(id_test)
    elif rtype=='string':
        test = dbio.DBResultStrings.get_by_id(id_test)
    elif rtype=='object':
        test = dbio.DBResultObjects.get_by_id(id_test)
    else:
        return jsonify({'success':False,'msg':'Unknown type: it should be float, bool, datetime, string or object.'}),400
    if not test:
        return jsonify({'success':False,'msg':'No test with id '+str(id_test)+' of type '+str(rtype)+'.'}),404
    
    meta = json.loads(jsmin.jsmin(bytes_as_string(test.result.module_config.meta.val)))['results']
    return jsonify({'success':True,
                    'selector':{'id':test.result.selector.id},
                    'result':test.result.serialize,
                    'test':test.meta_serialize(meta)}),200
                        
@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/tests/<int:id_test>/<path:rtype>', methods=['DELETE','PUT'])
@required_roles('rest_full','rest_major')
def selector_result_test_type_putdel(id_selector, id_result, id_test, rtype):
    if rtype=='float':
        test = dbio.DBResultFloats.get_by_id(id_test)
    elif rtype=='bool':
        test = dbio.DBResultBools.get_by_id(id_test)
    elif rtype=='datetime':
        test = dbio.DBResultDateTimes.get_by_id(id_test)
    elif rtype=='string':
        test = dbio.DBResultStrings.get_by_id(id_test)
    elif rtype=='object':
        test = dbio.DBResultObjects.get_by_id(id_test)
    else:
        return jsonify({'success':False,'msg':'Unknown type: it should be float, bool, datetime, string or object.'}),400
    if not test:
        return jsonify({'success':False,'msg':'No test with id '+str(id_test)+' of type '+str(rtype)+'.'}),404

    if request.method=='DELETE':
        try:
            test.delete_instance(recursive=True)
            return jsonify({'success':True,'msg':'Test deleted.'}),200
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403
            
    if request.method=='PUT':
        try:
            test.val = (request.form.get('value') or test.val)
            test.save()
            return jsonify({'success':True,'msg':'Test with id '+str(id_test)+' updated.'}),201
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403

     
@flask_app.route('/api/selectors/<int:id_selector>/results/<int:id_result>/tests/<int:id_test>/<path:rtype>/history', methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def selector_result_test_type_history_get(id_selector, id_result, id_test, rtype):
    historic_data=[]  
    if rtype=='float':
        test = dbio.DBResultFloats.get(dbio.DBResultFloats.id==id_test)
        model = dbio.DBResultFloats
    elif rtype=='string':
        test = dbio.DBResultStrings.get(dbio.DBResultStrings.id==id_test)
        model = dbio.DBResultStrings
    elif rtype=='bool':
        test = dbio.DBResultBools.get(dbio.DBResultBools.id==id_test)
        model = dbio.DBResultBools
    elif rtype=='object':
        test = dbio.DBResultObjects.get(dbio.DBResultObjects.id==id_test)
        model = dbio.DBResultObjects
    elif rtype=='datetime':
        test = dbio.DBResultDateTimes.get(dbio.DBResultDateTimes.id==id_test)
        model = dbio.DBResultDateTimes

    items = model.select().join(dbio.DBResults).join(dbio.DBSelectors).where((dbio.DBSelectors.id == id_selector) & (model.name == test.name))
    meta = json.loads(jsmin.jsmin(bytes_as_string(items[0].result.module_config.meta.val)))['results']
       
    for item in items:
        historic_data.append({'result':item.result.serialize,'test':item.meta_serialize(meta)})

    sortorder=request.args.get('sort','desc')
    historic_data.sort(key=lambda r: r['result']['date'],reverse=(sortorder=='desc'))
    limit=request.args.get('limit',False)
    if limit:
        historic_data=historic_data[0:int(limit)]
    return jsonify({'success':True,'history':historic_data}),200                 

@flask_app.route('/api/wadselector', methods=['GET'])
# @auth_required(,'rest_full')
def wadselector_get():
    import subprocess
    
    studyid = request.args.get('studyid')
    source = request.args.get('source')
        
    if studyid is None:
        return jsonify({'success':False, 'msg':'Missing studyid'}),400
        
    if source is None:
        return jsonify({'success':False, 'msg':'Missing source'}),400

    try:
        src = DBDataSources.get(DBDataSources.name==source)

    except DBDataSources.DoesNotExist:
        return jsonify({'success':False, 'msg':"Unregistered Source '{}'".format(source)}),404

    selector_wrapper = os.path.join(os.environ['WADROOT'], 'orthanc', 'lua', 'wadselector.py')
    cmd = [selector_wrapper, '--source', source, '--studyid', studyid, '--inifile', INIFILE, '--logfile_only']

    subprocess.Popen(cmd)

    return jsonify({'success':True, 'msg':'Study send to wadselector'}),200

from wad_admin.app.libs.shared import upload_file

@flask_app.route('/api/wadwriter',methods=['GET','POST'])
@required_roles('rest_full','rest_major','rest_minor')    # for other systems we should create a user
def wadwriter_getpost():
    from wad_api.app.wadwriter import makeds,sendds
    
    data={}
    objects={}
       
    if request.method=='GET':
        data = request.args.to_dict()
        ds=makeds(data,objects)
        response=sendds(ds,'127.0.0.1',11112)    #assuming orthanc on localhost and port 11112

        if response['success']:
            return jsonify({'success':True,'msg':'DICOM report build and send to WADQC'}),200
        else:
            return jsonify({'success':False,'msg':response['msg']}),400

    if request.method=='POST':

        for item in request.form:
            data[item]=request.form[item]
        for item in request.files:
            outname = upload_file(request.files[item])
            objects[request.files[item].filename]=outname

        ds=makeds(data,objects)

        for item in objects:
            os.remove(objects[item])

        response=sendds(ds,'127.0.0.1',11112)    #assuming orthanc on localhost and port 11112

        if response['success']:
            return jsonify({'success':True,'msg':'DICOM report build and send to WADQC'}),200
        else:
            return jsonify({'success':False,'msg':response['msg']}),400


### WADadministrator routes ###
@flask_app.route('/api/users', methods=['GET','POST'])
@required_roles('rest_full')
def users_getpost():
    if request.method=='GET':
        users = WAUsers.select()
        return jsonify({'success':True, 'users':[user.serialize for user in users]}),200
    
    if request.method=='POST':
        if not request.form.get('username'):
            return jsonify({'success':False, "msg":"No username given."}),400
        username=request.form['username']
        usercheck = WAUsers.select().where(WAUsers.username==username)
        if usercheck.exists():
            return jsonify({'success':False, "msg":"User already exists"}),400
        if not request.form.get('password'):
            return jsonify({'success':False, "msg":"Password is missing."}),400
        password=generate_password_hash(request.form['password'])           
        email=(request.form.get('email') or 'email@email.com')
        role=int(request.form.get('role') or 700)    #we expect numbers, not the descriptive string        
        if role_numbers[get_current_user_role(None)]>role:
            # prevent creating roles with more privileges than current role
            return jsonify({'success':False, 'msg':'Not allowed to create user with higher access level.'}),400
            
        try:
            id_user = WAUsers.create(username=username, password=password, email=email, status=0, role=role, refresh=30)
            user = WAUsers.get_by_id(id_user)
            return jsonify({'success':True, 'msg':'User created','user':user.serialize}),201   #or should we return id_users only?
        except Exception as e:
            return jsonify({'success':False, "msg":str(e)}),400

@flask_app.route('/api/users/<int:id_user>', methods=['DELETE','PUT','GET'])
@required_roles('rest_full')
def user_getdelput(id_user):
    try:
        user=WAUsers.get_by_id(id_user)
    except WAUsers.DoesNotExist:
        return jsonify({'success':False,'msg':'No user with id '+str(id_user)}),404
    if not user:
        return jsonify({'success':False,'msg':'No user with id '+str(id_user)}),404
    
    if request.method == 'GET':
        return jsonify({'success':True, 'user':user.serialize}),200
    
    if request.method == 'PUT':
        user.username = (request.form.get('username') or user.username)
        if 'password' in request.form:
            user.password = generate_password_hash(request.form.get('password'))
        user.email = (request.form.get('email') or user.email)
        user.role = int(request.form.get('role') or user.role)    #we expect numbers, not the descriptive string
        if role_numbers[get_current_user_role(None)]>user.role:
            # prevent creating roles with more privileges than current role
            return jsonify({'success':False, 'msg':'Not allowed to give user a higher access level than current user.'}),403
            
        user.status = (request.form.get('status') or user.status)        
        try:
            user.save()
            return jsonify({'success':True, 'msg':'Account '+str(user.id)+' updated','id':str(user.id)}),201
        except Exception as e:
            print(e)
            return jsonify({'success':False, 'msg':str(e)}),400

    if request.method=='DELETE':
        # should prevent delete of current user or admins
        if role_numbers[get_current_user_role(None)]>user.role:
            return jsonify({'success':False,'msg':'Not allowed to delete user with higher access level than current user.'}),400
            
        if user.id == get_current_user_id(None):
            return jsonify({'success':False,'msg':'Not allowed to delete current user'}),403
            
        try:
            user.delete_instance()
            return jsonify({'success':True, 'msg':'User deleted.'}),200
        except Exception as e:
            return jsonify({'success':False, 'msg':str(e)}),400

from wad_admin.app.libs.modulerepos import *
@flask_app.route('/api/modules',methods=['GET','POST'])
@required_roles('rest_full')
def modules_getpost():
    if request.method=='GET':
        modules = dbio.DBModules.select()
        return jsonify({'success':True,'modules':[m.serialize for m in modules]}),200
        
    if request.method=='POST':
    
        # expected release url example: "https://api.github.com/repos/MedPhysQC/CR_Flatfield/releases/17044074"
    
        if 'url' in request.form:
            url = request.form['url']
        
            # let WADQC get the release info from github
            import requests
            resp = requests.get(url)
            resp_json = resp.json()
            repo_version=resp_json['tag_name']
            zipball_url = resp_json['zipball_url']

            error, msg = install_from_url(dbio,zipball_url, {'repo_url': url, 'repo_version': repo_version},logger=None)
        
            if(error):
                return jsonify({'success':False, 'msg':msg}),400
            else:
                return jsonify({'success':True, 'msg':msg}),201
                
        else:
            return jsonify({'success':False, 'msg':'No valid url given'}),400
                
        
@flask_app.route('/api/modules/<int:id_module>',methods=['DELETE'])
@required_roles('rest_full')
def module_del(id_module):
    try:
        mod = dbio.DBModules.get_by_id(id_module)
    except dbio.DBModules.DoesNotExist:
        return jsonify({'success':False,'msg':'No module with that id'}),404
    if not mod:
        return jsonify({'success':False,'msg':'No module with that id'}),404    
    
    sels = sum([1 if len(cfg.selectors) else 0 for cfg in mod.module_configs ])
    if not sels == 0:
        return jsonify({'success':False,'msg':'Module is coupled with '+sels+' selectors, please delete these selectors first.'}),400

    modname = mod.name

    del_meta = [ c.meta for c in mod.module_configs ]
    mod.delete_instance(recursive=True)

    for mt in del_meta:
        try:
            mt.delete_instance(recursive=False)
        except:
            pass
    return jsonify({'success':True,'msg':'Module '+modname+' removed.'}),200
                

@flask_app.route('/api/services', methods=['GET'])
@required_roles('rest_full')
def services_get():
    import wad_admin.wadservices_communicate as wad_com

    services=[]
    rows=['wadprocessor','postgresql','Orthanc']

    for row in rows:
        if row == 'wadprocessor':
            stat = wad_com.wadcontrol('status', INIFILE)
            if stat == 'ERROR':
                stat = 'stopped'
        elif row == 'postgresql':
            stat = wad_com.postgresql('status')
        else:
            stat = wad_com.status(row)
        services.append({
            'name':row,
            'status':stat
        })
    return jsonify({'success':True, 'services':services}),200

@flask_app.route('/api/selectors/import', methods=['POST'])
@required_roles('rest_full')
def selectors_import_post():
    skip_selectors = request.form.get('skip_selectors',None)
    if not skip_selectors:
        return jsonify({'success':False, 'msg':'Missing skip_selectors in the request.'}),400
    if skip_selectors in truelist:
        skip_selectors = True
    else:
        skip_selectors = False
        
    skip_configs = request.form.get('skip_configs',None)
    if not skip_configs:
        return jsonify({'success':False, 'msg':'Missing skip_configs in the request.'}),400
    if skip_configs in truelist:
        skip_configs = True
    else:
        skip_configs = False
    
    skip_modules = request.form.get('skip_modules',None)
    if not skip_modules:
        return jsonify({'success':False, 'msg':'Missing skip_modules in the request.'}),400
    if skip_modules in truelist:
        skip_modules = True
    else:
        skip_modules = False
        
    zipfile = request.files.get('zipfile',None)
    if not zipfile:
        return jsonify({'success':False, 'msg':'Missing zipfile in the request.'}),400
    
    inname = upload_file(request.files['zipfile'])
    valid,msg = import_wadqc(dbio, inname, skip_selectors, skip_configs, skip_modules)
    os.remove(inname)    
    return jsonify({'success':valid, 'msg':msg}),201


@flask_app.route('/api/selectors/<int:id_selector>', methods=['DELETE'])
@required_roles('rest_full')
def selector_del(id_selector):
    try:
        selector = dbio.DBSelectors.get_by_id(id_selector)
    except dbio.DBSelectors.DoesNotExist:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)}),404
    if not selector:
        return jsonify({'success':False,'msg':'No selector with id '+str(id_selector)}),404
        
    with dbio.db.atomic() as trx:
        try:
            dbio.DBSelectors.get_by_id(id_selector).delete_instance(recursive=True)
        except IntegrityError as e:
            trx.rollback()
            msg = str(e)
            if "violates foreign key constraint" in str(e):
                return jsonify({'success':False,'msg':'This Selector is referenced from a table from an external service (e.g. WAD Dashboard). Use that service to remove the reference and try again.'}),400
    return jsonify({'success':True,'msg':'Selector deleted'}),200
        
@flask_app.route('/api/metas',methods=['GET'])
@required_roles('rest_full')
def metas_get():
    metas = dbio.DBMetaConfigs.select().order_by(dbio.DBMetaConfigs.id)
    return jsonify({'success':True,'metas':[m.serialize for m in metas]}),200

from wad_admin.app.libs import configmaintenance
@flask_app.route('/api/metas/<int:id_meta>',methods=['GET'])
@required_roles('rest_full')
def meta_get(id_meta):
    try:
        meta = dbio.DBMetaConfigs.get_by_id(id_meta)
    except dbio.DBMetaConfigs.DoesNotExist:
        return jsonify({'success':False,'msg':'No meta with id '+id_meta}),404
    if not meta:
        return jsonify({'success':False,'msg':'No meta with id '+id_meta}),404

    if  request.args.get('download') in truelist:
        filename = 'meta.json'
        blob = meta.val
        blob = json.loads(bytes_as_string(blob))
        blob = string_as_bytes(json.dumps(blob, sort_keys=True, indent=4))
        return send_file(BytesIO(blob), as_attachment=True, download_name=filename)
    else:         
        m = meta.serialize
        m['json']=json.loads(bytes_as_string(meta.val))
        m['success']=True
        return jsonify({'success':True,'meta':m})
            
@flask_app.route('/api/metas/<int:id_meta>',methods=['DELETE','PUT'])
@required_roles('rest_full')
def meta_putdel(id_meta):
    try:
        meta = dbio.DBMetaConfigs.get_by_id(id_meta)
    except dbio.DBMetaConfigs.DoesNotExist:
        return jsonify({'success':False,'msg':'No meta with id '+id_meta}),404
    if not meta:
        return jsonify({'success':False,'msg':'No meta with id '+id_meta}),404
            
    if request.method=='PUT':
        blob = request.form.get('json')
        valid, msg = configmaintenance.validate_json('metafile', blob)
        if not valid:
            return jsonify({'success':False,'msg':msg}),403
            
        else:
            meta.val=blob
            meta.save()
            return jsonify({'success':True,'msg':'Meta '+str(meta.id)+' updated.'}),201
            
    if request.method=='DELETE':
        if (len(meta.module_configs)>0 and len(meta.module_configs[0].selectors)>0):
            return jsonify({'success':False,'msg':'Meta used in '+str(len(meta.module_configs[0].selectors))+' selectors, please change this first.'}),400
        if len(meta.module_configs)>0:
            return jsonify({'success':False,'msg':'Meta used in '+str(len(meta.module_configs))+' configs, please change this first.'}),400
        
        try:
            meta.delete_instance(recursive=True)
            return jsonify({'success':True,'msg':'Meta deleted.'}),200
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403

@flask_app.route('/api/configs',methods=['GET'])
@required_roles('rest_full')
def configs_get():
    configs = dbio.DBModuleConfigs.select()
    return jsonify({'success':True,'configs':[c.serialize for c in configs]}),200

@flask_app.route('/api/configs/<int:id_config>',methods=['GET'])
@required_roles('rest_full')
def config_get(id_config):
    try:
        config = dbio.DBModuleConfigs.get_by_id(id_config)
    except dbio.DBModuleConfigs.DoesNotExist:
        return jsonify({'success':False,'msg':'No config with id '+id_config}),404
    if not config:
        return jsonify({'success':False,'msg':'No config with id '+id_config}),404

    if  request.args.get('download') in truelist:
        filename = 'config.json'
        blob = config.val
        blob = json.loads(bytes_as_string(blob))
        blob = string_as_bytes(json.dumps(blob, sort_keys=True, indent=4))
        return send_file(BytesIO(blob), as_attachment=True, download_name=filename)
    else:         
        c = config.serialize
        c['json']=json.loads(bytes_as_string(config.val))
        return jsonify({'success':True,'config':c}),200

@flask_app.route('/api/configs/<int:id_config>',methods=['DELETE','PUT'])
@required_roles('rest_full')
def config_putdel(id_config):
    try:
        config = dbio.DBModuleConfigs.get_by_id(id_config)
    except dbio.DBModuleConfigs.DoesNotExist:
        return jsonify({'success':False,'msg':'No config with id '+id_config}),404
    if not config:
        return jsonify({'success':False,'msg':'No config with id '+id_config}),404
            
    if request.method=='PUT':
        blob = (request.form.get('json') or config.val)
        meta = (request.form.get('meta') or config.meta)
        valid, msg = configmaintenance.validate_json('configfile', blob)
        if not valid:
            return jsonify({'success':False,'msg':msg}),400
            
        else:
            config.data_type = (request.form.get('data_type') or config.data_type)
            config.description = (request.form.get('description') or config.description)       
            config.val=blob
            config.meta = meta
            config.save()
            
            return jsonify({'success':True,'msg':'Config '+str(config.id)+' updated.'}),201
            
    if request.method=='DELETE':
        if len(config.selectors)>0:
            return jsonify({'success':False,'msg':'Config is used with '+str(len(config.selectors))+' selectors, please change this first.'}),400
        
        try:
            config.delete_instance(recursive=True)
            return jsonify({'success':True,'msg':'Config deleted.'}),200
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403
        
            
##
@flask_app.route('/api/processes',methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def processes_get():
    processes = dbio.DBProcesses.select()
    return jsonify({'success':True,'processes':[p.serialize for p in processes]}),200

from wad_core.selector import Selector
@flask_app.route('/api/processes/<int:id_process>',methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def process_get(id_process):
    try:
        process = dbio.DBProcesses.get_by_id(id_process)
    except dbio.DBProcesses.DoesNotExist:
        return jsonify({'success':False,'msg':'No process with id '+str(id_process)+'.'}),404
    if not process:
        return jsonify({'success':False,'msg':'No process with id '+str(id_process)+'.'}),404

    return jsonify({'success':True,'process':process.serialize}),200

@flask_app.route('/api/processes/<int:id_process>',methods=['DELETE','PUT'])
@required_roles('rest_full')
def process_putdel(id_process):
    try:
        process = dbio.DBProcesses.get_by_id(id_process)
    except dbio.DBProcesses.DoesNotExist:
        return jsonify({'success':False,'msg':'No process with id '+str(id_process)+'.'}),404
    if not process:
        return jsonify({'success':False,'msg':'No process with id '+str(id_process)+'.'}),404
        
    if request.method=='DELETE':
        try:
            process.delete_instance(recursive=True)
            return jsonify({'success':True,'msg':'Process deleted.'}),200
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403
            
    if request.method=='PUT':     #we change a process by deleting it and resending the data to the wadselector
        try:
            from wad_core.selector import Selector
            sel_name = process.selector.name
            source_name = process.data_set.data_source.name
            data_id = process.data_set.data_id
            data_type_name = process.data_set.data_type.name
            data_set = process.data_set
            
            process.delete_instance(recursive=True)
                        
            wsel = Selector(INIFILE, log_file_only=True)
            wsel.run(source_name, data_id, datalevel=data_type_name, selectornames=[sel_name])
            
            return jsonify({'success':True,'msg':'Process '+str(id_process)+' send to the WADselector again.'}),201
            
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403

from wad_qc.connection.pacsio import PACSIO              
@flask_app.route('/api/processes/<int:id_process>/input',methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def process_input_get(id_process):
    try:
        process = dbio.DBProcesses.get_by_id(id_process)
    except dbio.DBProcesses.DoesNotExist:
        return jsonify({'success':False,'msg':'No process with id '+str(id_process)+'.'}),404
    if not process:
        return jsonify({'success':False,'msg':'No process with id '+str(id_process)+'.'}),404
    
    
    data_id = process.data_set.data_id
    data_type = process.data_set.data_type.name
    id_datasource = process.data_set.data_source.id
    pacsconfig = dbio.DBDataSources.get_by_id(id_datasource).as_dict()
    pacsio = PACSIO(pacsconfig)

    if data_type == 'dcm_study':
        uid = pacsio.getStudyTags(data_id)['StudyInstanceUID']
    elif data_type == 'dcm_series':
        uid = pacsio.getSeriesTags(data_id)['SeriesInstanceUID']
    elif data_type == '	dcm_instance':
        uid = pacsio.getInstanceTags(data_id)['InstanceUID']
    else:
        return jsonify({'success':False,'msg':'Cannot get associated dicom UID'}),400
     
    wanted = dbio.DBProcessInput.select().where(dbio.DBProcessInput.uid == uid)
    return jsonify({'success':True,'inputs':[w.serialize for w in wanted]}),200
    
@flask_app.route('/api/processes/<int:id_process>/input',methods=['POST'])
@required_roles('rest_full')
def process_input_post(id_process):
    try:
        process = dbio.DBProcesses.get_by_id(id_process)
    except dbio.DBProcesses.DoesNotExist:
        return jsonify({'success':False,'msg':'No process with id '+str(id_process)+'.'}),404
    if not process:
        return jsonify({'success':False,'msg':'No process with id '+str(id_process)+'.'}),404
    
    data_id = process.data_set.data_id
    data_type = process.data_set.data_type.name
    id_datasource = process.data_set.data_source.id
    pacsconfig = dbio.DBDataSources.get_by_id(id_datasource).as_dict()
    pacsio = PACSIO(pacsconfig)

    if data_type == 'dcm_study':
        uid = pacsio.getStudyTags(data_id)['StudyInstanceUID']
    elif data_type == 'dcm_series':
        uid = pacsio.getSeriesTags(data_id)['SeriesInstanceUID']
    elif data_type == '	dcm_instance':
        uid = pacsio.getInstanceTags(data_id)['InstanceUID']
    else:
        return jsonify({'success':False,'msg':'Cannot get associated dicom UID'}),400

    id_selector = process.selector.id

    for name in request.form.to_dict():
        record = dbio.DBProcessInput.get(dbio.DBProcessInput.uid==uid,
                                         dbio.DBProcessInput.name==name,
                                         dbio.DBProcessInput.selector_id==id_selector)
        val = request.form.get(name)
        if val == 'null' or len(val)==0:
            val = None
        record.val = val
        record.save()
    
    missing_required = dbio.DBProcessInput.select().where(
        (dbio.DBProcessInput.uid == uid) &
        (dbio.DBProcessInput.required == True) &
        (dbio.DBProcessInput.val.is_null(True))
        )
    
    missing_required = len(missing_required)
    
    if missing_required == 0:
        status_new = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == "new")
        process.process_status = status_new
        process.save()
        return jsonify({'success':True,'msg':'Manual input complete, starting proces.'}),201
    else:
        return jsonify({'success':True,'msg':'Manual input incomplete, awaiting '+str(missing_required)+' more items.'}),200
        

@flask_app.route('/api/results',methods=['GET'])
@required_roles('rest_full','rest_major','rest_minor')
def results_get():
    page = (request.args.get('page') or 'all')
    per_page = 25
    if page == 'all':
        results = dbio.DBResults.select().order_by(dbio.DBResults.created_time.desc())
    else:
        results = dbio.DBResults.select().order_by(dbio.DBResults.created_time.desc()).paginate(int(page), per_page)

    return jsonify({'success':True,'results':[r.serialize for r in results]}),200

@flask_app.route('/api/results/<int:id_result>',methods=['GET'])     
@required_roles('rest_full','rest_major','rest_minor')
def result_get(id_result):
    try:
        result = dbio.DBResults.get_by_id(id_result)
    except dbio.DBResults.DoesNotExist:
        return jsonify({'success':False,'msg':'No result with id '+str(id_result)+'.'}),404
    if not result:
        return jsonify({'success':False,'msg':'No result with id '+str(id_result)+'.'}),404

    return jsonify({'success':True,'result':result.serialize}),200
        
@flask_app.route('/api/results/<int:id_result>',methods=['DELETE','PUT'])     
@required_roles('rest_full')
def result_putdel(id_result):
    try:
        result = dbio.DBResults.get_by_id(id_result)
    except dbio.DBResults.DoesNotExist:
        return jsonify({'success':False,'msg':'No result with id '+str(id_result)+'.'}),404
    if not result:
        return jsonify({'success':False,'msg':'No result with id '+str(id_result)+'.'}),404

    if request.method == 'DELETE':
        try:
            result.delete_instance(recursive=True)
            return jsonify({'success':True,'msg':'Result deleted.'}),200
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403

    if request.method=='PUT':     #we redo a result by deleting it and resending the data to the wadselector
        try:
            sel_name    = result.selector.name
            source_name = result.data_set.data_source.name
            data_id     = result.data_set.data_id
            datatype_name = result.data_set.data_type.name
            data_set = result.data_set
            result.delete_instance(recursive=True)
            wsel = Selector(INIFILE, logfile_only=True)
            wsel.run(source_name, data_id, datalevel=datatype_name, selectornames=[sel_name])
            
            return jsonify({'success':True,'msg':'Result '+str(id_result)+' send to the WADselector again.'}),201
            
        except Exception as e:
            print(e)
            return jsonify({'success':False,'msg':str(e)}),403  

@flask_app.route('/api/datasources',methods=['GET'])     
@required_roles('rest_full')
def datasources_get():
    datasources = dbio.DBDataSources.select()
    return jsonify({'success':True,'datasources':[d.serialize for d in datasources]}),200

@flask_app.route('/api/datasources/<int:id_datasource>',methods=['GET'])
@required_roles('rest_full')        
def datasource_get(id_datasource):
    try:
        datasource = dbio.DBDataSources.get_by_id(id_datasource)
    except dbio.DBDataSets.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no datasource with id '+str(id_datasource)+'.'}),404
    if not datasource:
        return jsonify({'success':False,'msg':'There is no datasource with id '+str(id_datasource)+'.'}),404
        
    return jsonify({'success':True,'datasource':datasource.serialize}),200
    
    
@flask_app.route('/api/datasets',methods=['GET'])
@required_roles('rest_full')
def datasets_get():
    page = (request.args.get('page') or 'all')
    per_page = 25
    if page == 'all':
        datasets = dbio.DBDataSets.select()
    else:
        datasets = dbio.DBDataSets.select().paginate(int(page), per_page)    
    
    return jsonify({'success':True,'datasets':[d.serialize for d in datasets]}),200
    
@flask_app.route('/api/datasets/<int:id_dataset>',methods=['GET'])
@required_roles('rest_full')
def dataset_get(id_dataset):
    try:
        dataset = dbio.DBDataSets.get_by_id(id_dataset)
    except dbio.DBDataSets.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no dataset with id '+str(id_dataset)+'.'}),404
    if not dataset:
        return jsonify({'success':False,'msg':'There is no dataset with id '+str(id_dataset)+'.'}),404
        
    return jsonify({'success':True,'dataset':dataset.serialize}),200

@flask_app.route('/api/datasets/<int:id_dataset>/tags',methods=['GET'])
@required_roles('rest_full')
def dataset_tags_get(id_dataset):
    try:
        dataset = dbio.DBDataSets.get_by_id(id_dataset)
    except dbio.DBDataSets.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no dataset with id '+str(id_dataset)+'.'}),404
    if not dataset:
        return jsonify({'success':False,'msg':'There is no dataset with id '+str(id_dataset)+'.'}),404
    
    pacsconfig = dbio.DBDataSources.get_by_id(dataset.data_source.id).as_dict()
    pacsio = PACSIO(pacsconfig)
    
    try:
        if dataset.data_type.name == 'dcm_study':
            tags = pacsio.getSharedStudyHeaders(dataset.data_id)
        elif dataset.data_type.name == 'dcm_series':
            tags = pacsio.getSharedSeriesHeaders(dataset.data_id)
        elif dataset.data_type.name == 'dcm_instance':
            tags = pacsio.getInstanceHeaders(dataset.data_id)
        else:
            return jsonify({'success':False,'msg':'Datatype of the dataset should be study, serie or instance.'}),400
        return jsonify({'success':True,'tags':tags}),200
    except:
        return jsonify({'success':False,'msg':'Could not get dicom information from source'}),404
            
@flask_app.route('/api/datasets/<int:id_dataset>/notes',methods=['POST'])
@required_roles('rest_full')
def dataset_notes_post(id_dataset):
    try:
        dataset = dbio.DBDataSets.get_by_id(id_dataset)
    except dbio.DBDataSets.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no dataset with id '+str(id_dataset)+'.'}),404
    if not dataset:
        return jsonify({'success':False,'msg':'There is no dataset with id '+str(id_dataset)+'.'}),404
        
    description = (request.form.get('description') or 'Enter description')
    data_tag = (dbio.DBDataTags.get_by_id(request.form.get('data_tag')) or None)
    
    try:
        id_new_note = dbio.DBNotes.create(data_set = dataset, data_tag = data_tag, description = description)
        return jsonify({'success':True,'msg':'New note created','id':str(id_new_note)}),201
    except Exception as e:
        return jsonify({'success':False,'msg':str(e)}),403
            
@flask_app.route('/api/datasets/<int:id_dataset>/notes/<int:id_note>',methods=['PUT','DELETE'])
@required_roles('rest_full')
def dataset_note_putdel(id_dataset,id_note):
    try:
        note = dbio.DBNotes.get_by_id(id_note)
    except dbio.DBNotes.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no note with id '+str(id_note)+'.'}),404
    if not note:
        return jsonify({'success':False,'msg':'There is no note with id '+str(id_note)+'.'}),404
        
    if request.method == 'DELETE':
        try:
            note.delete_instance(recursive=True)
            return jsonify({'success':True,'msg':'Note deleted.'}),200
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403
        
    if request.method == 'PUT':
        if 'data_tag' in request.form:
            note.data_tag = request.form.get('data_tag')
        if 'description' in request.form:
            note.description = request.form.get('description')
        try:
            note.save()
            return jsonify({'success':True,'msg':'Note updated.'}),201
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403

@flask_app.route('/api/datasets/<int:id_dataset>/statistics',methods=['GET'])
@required_roles('rest_full','rest_major')
def dataset_statistics_get(id_dataset):
    try:
        dataset = dbio.DBDataSets.get_by_id(id_dataset)
    except dbio.DBDataSets.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no dataset with id '+str(id_dataset)+'.'}),404
    if not dataset:
        return jsonify({'success':False,'msg':'There is no dataset with id '+str(id_dataset)+'.'}),404
    
    pacsconfig = dbio.DBDataSources.get_by_id(dataset.data_source.id).as_dict()
    pacsio = PACSIO(pacsconfig)
    
    try:
        if dataset.data_type.name == 'dcm_study':
            stats = pacsio.getStudyStatistics(dataset.data_id)
        elif dataset.data_type.name == 'dcm_series':
            stats = pacsio.getSeriesStatistics(dataset.data_id)
        elif dataset.data_type.name == 'dcm_instance':
            stats = pacsio.getInstanceStatistics(dataset.data_id)
        else:
            return jsonify({'success':False, 'msg':'Datatype of the dataset should be study, serie or instance.'}),400
        return jsonify({'success':True,'statistics':stats}),200
    except:
        return jsonify({'success':False, 'msg':'Could not get dicom information from source'}),404
            

@flask_app.route('/api/datatags',methods=['GET'])
@required_roles('rest_full')
def datatags_get():
    datatags = dbio.DBDataTags.select()
    return jsonify({'success':True,'datatags':[d.serialize for d in datatags]}),200
        
@flask_app.route('/api/datatags',methods=['POST'])
@required_roles('rest_full')
def datatags_post():
    try:
        id_new_tag = dbio.DBDataTags.create(name = request.form.get('name'))
        return jsonify({'success':True,'msg':'New data_tag created','id':str(id_new_tag)}),201
    except Exception as e:
        return jsonify({'success':False,'msg':str(e)}),403
    
@flask_app.route('/api/datatags/<int:id_data_tag>',methods=['PUT','DELETE'])
@required_roles('rest_full')
def datatag_putdel(id_data_tag):
    try:
        data_tag = dbio.DBDataTags.get_by_id(id_data_tag)
    except dbio.DBDataTags.DoesNotExist:
        return jsonify({'success':False,'msg':'There is no data_tag with id '+str(id_data_tag)+'.'}),404
        
    if request.method == 'PUT':
        try:
            data_tag.name = request.form.get('name')
            data_tag.save()
            return jsonify({'success':True,'msg':'Data_tag updated.'}),201
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403
            
    if request.method == 'DELETE':
        try:
            data_tag.delete_instance(recursive=True)
            return jsonify({'success':True,'msg':'Data_tag deleted.'}),200
        except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403
            
from wad_qc.connection import dbupgrade as libdbupgrade            
import pkg_resources
@flask_app.route('/api/systems',methods=['GET'])
@required_roles('rest_full')
def systems_get():
    try:
        version_whl = pkg_resources.get_distribution("wad_qc").version
        running_version = libdbupgrade.get_running_version(dbio)[0]
        latest_version  = libdbupgrade.get_latest_version(dbio)[0]
        return jsonify({'success':True,'systems':{'wadqc':version_whl,'wadqc_database':running_version,'wadqc.dbio':latest_version}}),200
    except Exception as e:
            return jsonify({'success':False,'msg':str(e)}),403

       
from wad_qc.connection.pacsio import PACSIO    
@flask_app.route('/api/datasources/<int:id_datasource>/data',methods=['GET'])
@required_roles('rest_full') 
def datasource_data_get(id_datasource):
    pacsconfig = dbio.DBDataSources.get_by_id(id_datasource).as_dict()
    pacsio = PACSIO(pacsconfig)

    level = request.args.get('level')
    if level is None:
        return jsonify({'success':False,'msg':'Level is not given, possiblities are patient, study and serie.'}),404
        
    residual = request.args.get('residual')
    if residual in truelist:
        id_results = [r.serialize['id_data'] for r in dbio.DBResults.select()]      
        
    if level == 'patients':
        return jsonify({'success':True,'id_patients':pacsio.getPatientIds()}),200
    elif level == 'studies':
        id_studies = pacsio.getStudyIds()
        if residual in truelist:
            id_studies_without_result = [el for el in id_studies if el not in id_results]
            id_studies = id_studies_without_result
        return jsonify({'success':True,'id_studies':id_studies}),200
    elif level == 'series':
        id_series = pacsio.getSeriesIds(None)
        if residual in truelist:
            id_series_without_result = [el for el in id_series if el not in id_results]
            id_series = id_series_without_result
        return jsonify({'success':True,'id_series':id_series}),200
    else:
        return jsonify({'success':False,'msg':'Level is not correct, possiblities are patients, studies and series.'}),404

@flask_app.route('/api/datasources/<int:id_datasource>/data/<path:id_data>',methods=['GET'])
@required_roles('rest_full') 
def datasource_data_tags_get(id_datasource,id_data):
    pacsconfig = dbio.DBDataSources.get_by_id(id_datasource).as_dict()
    pacsio = PACSIO(pacsconfig)

    if id_data is None:
        return jsonify({'success':False,'msg':'No id given'}),400
        
    try:
        tags = pacsio.getStudyTags(id_data)
    except:
        try: #then the serie level
            tags = pacsio.getSeriesTags(id_data)
        except:
            try:  # then the patient level
                tags = pacsio.getPatientTags(id_data)
            except:
                return jsonify({'success':False,'msg':'Given id is not a study, serie or patient.'}),404
    
    return jsonify({'success':True,'data':tags}),200
    
@flask_app.route('/api/datasources/<int:id_datasource>/check-for-result',methods=['GET'])
@required_roles('rest_full') 
def datasource_resultcheck_get(id_datasource):
    id_data = request.args.get('id')
    if id_data is None:
        return jsonify({'success':False,'msg':'No id given'}),404
        
    id_results = [r.serialize[id_data] for r in dbio.DBResults.select()]
    if id_data in id_results:
        id_results=[]
        results = dbio.DBResults.select().where(dbio.DBResults.data_id == id_data)
        for result in results:
            id_results.append(result.id)
        return jsonify({'success':True,'id_results':id_results}),200
        
    else:
        return jsonify({'success':True,'msg':'No result for this id.','id_results':None}),200

           
    
if __name__ == "__main__":
    flask_app.run(host='0.0.0.0', port=3001, use_reloader=True)