"""
Routines for upgrading the WAD-QC database.

This should be a collection of functional stuff for interfacing with WAD-QC through the web interface,
and also contain routines needed for running database upgrades from the command line.

Changelog:
  20230322: upgrade for wad-qc 2.1.0: dashboard/maingroups no longer unique
  20200713: added DBDataTrash
  20200113: added progress indicator 
  20200106: added DBDataSets, DBDataTags, DBNotes
  20180316: added  update for factory modules
  20180209: Initial version
"""
from __future__ import print_function
from playhouse.migrate import *
from progress.bar import Bar
from progress import SHOW_CURSOR

class DummyLogger:
    def _output(self, prefix, msg):
        print("{}: {}".format(prefix, msg))

    def warning(self, msg):
        self._output("[warning] ", msg)

    def info(self, msg):
        self._output("[info] ", msg)

    def error(self, msg):
        self._output("[error] ", msg)

    def debug(self, msg):
        self._output("[debug] ", msg)


def version_smaller(v1, v2):
    """
    compare a two (major, minor) tuples
    """
    if v1[0]<v2[0]:
        return True
    if v1[0] == v2[0] and v1[1]<v2[1]:
        return True
    return False

def version_equal(v1, v2):
    """
    compare a two (major, minor) tuples
    """
    if v1[0] == v2[0] and v1[1] == v2[1]:
        return True
    return False

def get_running_version(dbio, logger=None):
    """
    Read the version of the database in use. Allow point releases
    """
    if logger is None:
        logger = DummyLogger()

    version = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version').val.split('.')
    major = int(version[0])
    try:
        minor = int(version[1])
    except:
        minor = 0

    return major, minor

def get_latest_version(dbio, logger=None):
    """
    Read the version of the database implemented in dbio. Allow point releases
    """
    if logger is None:
        logger = DummyLogger()

    version = str(dbio.__version__).split('.')
    major = int(version[0])
    try:
        minor = int(version[1])
    except:
        minor = 0

    return major, minor

def _upgrade20171020(dbio, logger):
    """
    Changes:
    DBModuleConfig.origin now takes 3 values: factory, user and result.
     factory: shipped by the wadqc developers (active)
     user: manually imported or user modified (active)
     result: coupled to a selector (active) or a process or result (inactive)
     
    We distinguish 'active' from 'inactive' Configs. Configs that are only coupled to a Process or Result are 'inactive', and are 
    retained for reproducing the coupled Result only. 'active' Configs are 'factory' and 'user' Configs and 'result' Configs
    that are still coupled to a Selector. For creating and modifying Selectors, we are only interested in 'active' Configs.
    
    Implementation:
      1. List all 'user' Configs.
      2. For each 'user' Config, change to 'result' if coupled to a Selector or Process or Result
      
    Caveats:
    Configs that were once coupled to a Selector, but did not Produce any Results or Processes will stay 'user'.
    """
    if logger is None:
        logger = DummyLogger()

    msg = ""
    error = True
    nw_version = "20171020"

    # get all configs
    stuff = dbio.DBModuleConfigs.select().order_by(dbio.DBModuleConfigs.id).where(dbio.DBModuleConfigs.origin == 'user')

    # change origin to 'result' if coupled to a Selector or Process or Result
    for cfg in stuff:
        if cfg.origin == 'user':
            if len(cfg.selectors)>0 or len(cfg.processes)>0 or len(cfg.results)> 0:
                cfg.origin = 'result'
                cfg.save()
    
    version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version')
    version_cur.val = nw_version
    version_cur.save()

    msg = "upgraded to {}".format(version_cur.val)
    error = False
    return error, msg
    

def _upgrade20180209(dbio, logger):
    """
    Add a field "val_ref" to the DBResultFloats table to implement relative constraints
    """
    if logger is None:
        logger = DummyLogger()

    msg = ""
    error = True
    nw_version = "20180209"

    if isinstance(dbio.db, PostgresqlDatabase):
        try:
            migrator = PostgresqlMigrator(dbio.db)
            val_ref  = DoubleField(null=True)
    
            migrate(
                migrator.add_column('dbresultfloats', 'val_ref', val_ref),
            )
    
            version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version')
            version_cur.val = nw_version
            version_cur.save()
        
            msg = "upgraded to {}".format(version_cur.val)
            error = False
        except Exception as e:
            msg = "ERROR! Cannot apply upgrade {}: {}".format(nw_version, str(e))
            error = True
            
    else:
        msg = "ERROR! upgrade {} is only implemented for PostgresSQL!".format(nw_version)
        error = True
        
    return error, msg
"""
    repo_version = CharField(max_length=32 )  # if installed from repo
    repo_url     = CharField(max_length=250 ) # if installed from repo

"""
def _upgrade20180316(dbio, logger):
    """
    Add repository fields to DBModules (repo_version and repo_url)
    repo_version = CharField(max_length=32 )  # if installed from repo
    repo_url     = CharField(max_length=250 ) # if installed from repo

    """
    if logger is None:
        logger = DummyLogger()

    msg = ""
    error = True
    nw_version = "20180316"

    if isinstance(dbio.db, PostgresqlDatabase):
        try:
            migrator = PostgresqlMigrator(dbio.db)
            repo_version = CharField(max_length=32, default=""  )
            repo_url     = CharField(max_length=250, default=""  )
            migrate(
                migrator.add_column('dbmodules', 'repo_version', repo_version),
                migrator.add_column('dbmodules', 'repo_url', repo_url),
            )
    
            version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version')
            version_cur.val = nw_version
            version_cur.save()
        
            msg = "upgraded to {}".format(version_cur.val)
            error = False
        except Exception as e:
            msg = "ERROR! Cannot apply upgrade {}: {}".format(nw_version, str(e))
            error = True
            
    else:
        msg = "ERROR! upgrade {} is only implemented for PostgresSQL!".format(nw_version)
        error = True
        
    return error, msg

def _upgrade20200106(dbio, showprogress, logger):
    """
    create new table DBDataSets
    migrate DBProcesses to point to DBDataSets
    migrate DBResults to point to DBDataSets
    """
    if logger is None:
        logger = DummyLogger()

    msg = ""
    error = True
    nw_version = "20200106"

    if isinstance(dbio.db, PostgresqlDatabase):
        ## https://github.com/coleifer/peewee/blob/7e61d86bf6c3f256d09b2a3e1897693dfd68b48d/tests/migrations.py#L665-L715
        ## ForeignKeyField needs field=db.table.id and null=True
        ## a foreign key data_set to DBDataSets is named 'data_set_id' in migrator (also see in psql directly)
        """
        Workflow:
        1. Create table DBDataSets if not exists
        2. Add new fields 'data_set' as ForeignKey to DBDataSets to DBProcesses and DBResults
        3. For each item in DBProcesses and DBResults create a new DBDataSets item (if not exist), 
           and update the field 'data_set' to point to this item.
        4. Drop the no longer used fields 'data_source' and 'data_id'
        5. Update database version
        """
        with dbio.db.atomic() as trx:
            try: # use one big try block so we can rollback if any error occurs
                # create table DBDataSets if not exists
                dbio.db.create_tables([dbio.DBDataSets, dbio.DBDataTags, dbio.DBNotes], safe=True)
                
                # fill new DBDataTags with default values
                for i, rel in dbio.default_content[dbio.DBDataTags]:
                    dbio.DBDataTags.create(name = rel, id = i)
                
                # add columns to processes and results
                migrator = PostgresqlMigrator(dbio.db)
                migrate(
                    migrator.add_column('dbprocesses', 'data_set_id', ForeignKeyField(dbio.DBDataSets, backref='processes', field=dbio.DBDataSets.id, null=True)),
                    migrator.add_column('dbresults', 'data_set_id', ForeignKeyField(dbio.DBDataSets, backref='results', field=dbio.DBDataSets.id, null=True)),
                )
        
                # add dataset FK to processes
                # for each element in processes/results, make dataset item (if not exist) and set in processes/results
                # drop columns data_id and data_source of processes/results
                # Here use extended old def of DBProcesses/DBResults, else data_source and data_id are not accessible, 
                #   while at this point they still exist
                class DBProcesses(dbio.DBModel):
                    # define fields
                    selector          = ForeignKeyField(dbio.DBSelectors, backref='processes')
                    module_config     = ForeignKeyField(dbio.DBModuleConfigs, backref='processes')
                    data_source       = ForeignKeyField(dbio.DBDataSources, backref='processes')
                    data_id           = CharField(max_length=64) # study_id or series_id or instance_id
                    process_status    = ForeignKeyField(dbio.DBProcessStatus, backref='processes')
                    created_time      = DateTimeField()
                    process_log       = BlobField(null=True)
                    data_set          = ForeignKeyField(dbio.DBDataSets, backref='processes')
    
                    class Meta:
                        database = dbio.db
                        table_name = 'dbprocesses'
    
                class DBResults(dbio.DBModel):
                    # define fields
                    selector          = ForeignKeyField(dbio.DBSelectors, backref='results')
                    module_config     = ForeignKeyField(dbio.DBModuleConfigs, backref='results')
                    data_source       = ForeignKeyField(dbio.DBDataSources, backref='results')
                    data_id           = CharField(max_length=64) # study_id or series_id or instance_id
                    created_time      = DateTimeField()
                    process_log       = BlobField(null=True)
                    data_set          = ForeignKeyField(dbio.DBDataSets, backref='results')
    
                    class Meta:
                        database = dbio.db
                        table_name = 'dbresults'
    
                print("Need to upgrade {} entries. This might take a while...".format(DBProcesses.select().count()+DBResults.select().count()))
                
                if DBProcesses.select().count()>0:
                    with Bar("Upgrading DBProcesses", max=DBProcesses.select().count()) as pbar: 
                        # create entries in datasets based on info in processes
                        for item in DBProcesses.select():
                            nw_id, created = dbio.DBDataSets.get_or_create(
                                data_source = item.data_source,
                                data_type = item.module_config.data_type,
                                data_id = item.data_id)
                            item.data_set = nw_id.id
                            item.save()
                            pbar.next()
                    print(SHOW_CURSOR, end='')

                if DBResults.select().count()>0:
                    # create entries in datasets based on info in results
                    with Bar("Upgrading DBResults", max=DBResults.select().count()) as pbar: 
                        for item in DBResults.select():
                            nw_id, created = dbio.DBDataSets.get_or_create(
                                data_source = item.data_source,
                                data_type = item.module_config.data_type,
                                data_id = item.data_id)
                            item.data_set = nw_id.id
                            item.save()
                            pbar.next()
                    print(SHOW_CURSOR, end='')
    
                # drop columns data_id and data_source
                migrate(
                    migrator.drop_column('dbprocesses', 'data_source_id', cascade=False), # it is a ForeignKeyField, so add '_id'
                    migrator.drop_column('dbprocesses', 'data_id', cascade=False),
                    migrator.drop_column('dbresults', 'data_source_id', cascade=False), # it is a ForeignKeyField, so add '_id'
                    migrator.drop_column('dbresults', 'data_id', cascade=False),
                )
    
                # now update version information
                version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version')
                version_cur.val = nw_version
                version_cur.save()
            
                msg = "upgraded to {}".format(version_cur.val)
                error = False
                
            except Exception as e:
                error = True
                msg = "ERROR! Cannot apply upgrade {}; changes will not be committed: {}".format(nw_version, str(e))
                trx.rollback()
                    
    else:
        msg = "ERROR! upgrade {} is only implemented for PostgresSQL!".format(nw_version)
        error = True
        
    return error, msg

def _upgrade20200207(dbio, logger):
    from wad_qc.connection.dbio_models import DBProcessInput

    if logger is None:
        logger = DummyLogger()

    msg = ""
    error = True
    nw_version = "20200207"

    if isinstance(dbio.db, PostgresqlDatabase):
        with dbio.db.atomic() as trx:
            try: # use one big try block so we can rollback if any error occurs
                migrator = PostgresqlMigrator(dbio.db)
                manual_input = BooleanField(default=False)
                migrate(
                    migrator.add_column('dbselectors', 'manual_input', manual_input),
                )
    
                dbio.db.create_tables([DBProcessInput])
    
                version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version')
                version_cur.val = nw_version
                version_cur.save()
    
                msg = "upgraded to {}".format(version_cur.val)
                error = False
            except Exception as e:
                error = True
                msg = "ERROR! Cannot apply upgrade {}; changes will not be committed: {}".format(nw_version, str(e))
                trx.rollback()
            

    else:
        msg = "ERROR! upgrade {} is only implemented for PostgresSQL!".format(nw_version)
        error = True

    return error, msg
    

def _upgrade20200713(dbio, logger):
    """
    create new table DBDataTrash
    """
    if logger is None:
        logger = DummyLogger()

    msg = ""
    error = True
    nw_version = "20200713"

    if isinstance(dbio.db, PostgresqlDatabase):
        ## https://github.com/coleifer/peewee/blob/7e61d86bf6c3f256d09b2a3e1897693dfd68b48d/tests/migrations.py#L665-L715
        ## ForeignKeyField needs field=db.table.id and null=True
        ## a foreign key data_set to DBDataSets is named 'data_set_id' in migrator (also see in psql directly)
        """
        Workflow:
        1. Create table DBDataTrash if not exists
        5. Update database version
        """
        with dbio.db.atomic() as trx:
            try: # use one big try block so we can rollback if any error occurs
                # create table DBDataSets if not exists
                dbio.db.create_tables([dbio.DBDataTrash], safe=True)
                
                # create virtual trash module with configs
                dbio._create_virtual_trashmodule()
                
                # now update version information
                version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version')
                version_cur.val = nw_version
                version_cur.save()
            
                msg = "upgraded to {}".format(version_cur.val)
                error = False
                
            except Exception as e:
                error = True
                msg = "ERROR! Cannot apply upgrade {}; changes will not be committed: {}".format(nw_version, str(e))
                trx.rollback()
                    
    else:
        msg = "ERROR! upgrade {} is only implemented for PostgresSQL!".format(nw_version)
        error = True
        
    return error, msg


def _upgrade20230322(dbio, logger):
    """
    Change name column of wdmaingroups table to non-unique
    """
    if logger is None:
        logger = DummyLogger()

    msg = ""
    error = True
    nw_version = "20230322"

    if isinstance(dbio.db, PostgresqlDatabase):
        """
        Workflow:
        1. Remove index wdmaingroups_name, that requires wdmaingroups.name to be unique
        2. Update database version
        """
        with dbio.db.atomic() as trx:
            try: # use one big try block so we can rollback if any error occurs
                # now update version information

                migrator = PostgresqlMigrator(dbio.db)
                migrate(
                    migrator.drop_index('wdmaingroups', 'wdmaingroups_name')
                )

                version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'iqc_db_version')
                version_cur.val = nw_version
                version_cur.save()

                msg = "upgraded to {}".format(version_cur.val)
                error = False

            except Exception as e:
                error = True
                msg = "ERROR! Cannot apply upgrade {}; changes will not be committed: {}".format(nw_version, str(e))
                trx.rollback()

    else:
        msg = "ERROR! upgrade {} is only implemented for PostgresSQL!".format(nw_version)
        error = True

    return error, msg



def upgrade(dbio, showprogress, logger):
    """
    Apply all available upgrades in version order, and return all upgrade messages. Stop when an error occurs.
    """
    if logger is None:
        logger = DummyLogger()

    latest_version  = get_latest_version(dbio, logger)
    running_version = get_running_version(dbio, logger)

    error = False
    msgs = []

    while version_smaller(running_version, latest_version):
        if version_smaller(running_version, (20171020,0)):
            error, msg = _upgrade20171020(dbio, logger)
        elif version_smaller(running_version, (20180209,0)):
            error, msg = _upgrade20180209(dbio, logger)
        elif version_smaller(running_version, (20180316,0)):
            error, msg = _upgrade20180316(dbio, logger)
        elif version_smaller(running_version, (20200106,0)):
            error, msg = _upgrade20200106(dbio, showprogress, logger)
        elif version_smaller(running_version, (20200207,0)):
            error, msg = _upgrade20200207(dbio, logger)
        elif version_smaller(running_version, (20200713,0)):
            error, msg = _upgrade20200713(dbio, logger)
        elif version_smaller(running_version, (20230322,0)):
            error, msg = _upgrade20230322(dbio, logger)

        msgs.append(msg.strip())
        if error:
            return error, msgs
        else:
            logger.info(msg)

        running_version  = get_running_version(dbio, logger)
            
    return error, msgs
