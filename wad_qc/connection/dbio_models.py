import os
from peewee import SqliteDatabase, PostgresqlDatabase, Model, CharField, IntegerField, BooleanField, ForeignKeyField, BlobField, DateTimeField, DoubleField, OperationalError, Check, AutoField
from datetime import datetime

import logging
import stat
import zipfile
import shutil
import tempfile

import base64
import json
import jsmin
import codecs

# helper functions
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

CFGFORMAT  = "20180910"
METAFORMAT = "20180910"

db = SqliteDatabase(None)

def retry(times, func, *args, **kwargs):
    for n in range(times):
        try:
            return func(*args, **kwargs)
        except OperationalError as e:
            logging.error('Database locked, will try again later.')
            
    return func(*args, **kwargs)


class DBModel(Model):
    class Meta:
        database = db

    def save(self, *args, **kwargs):
        return retry(10, super(DBModel, self).save, *args, **kwargs)
                
    @classmethod
    def get_by_name(cls, name):
        # convenience function
        return cls.get(cls.name == name) if hasattr(cls, 'name') else None

    @classmethod
    def get_by_id(cls, id):
        # convenience function
        return cls.get(cls.id == id)
    

class DBVariables(DBModel):
    # define fields
    name = CharField(unique=True, max_length=64) # identifier
    val  = CharField(max_length=512)

class DBSourceTypes(DBModel):
    # define fields
    name = CharField(unique=True, max_length=64) # identifier

class DBDataTypes(DBModel):
    # define fields
    name = CharField(unique=True, max_length=50) # identifier
    
    @property
    def serialize(self):
        data = {
            'id':self.id,
            'name':self.name
        }
        return data

class DBDataSources(DBModel):
    # define fields
    name       = CharField(unique=True, max_length=64) # identifier
    source_type = ForeignKeyField(DBSourceTypes, backref='datasources') 
    protocol   = CharField(max_length=64) # access protocol for credentials below 'http', 'https', 'samba', ...
    aetitle    = CharField(max_length=64, null=True) # for dicomqr
    host       = CharField(max_length=250) # 'localhost', '127.0.0.1', ...
    port       = IntegerField(null=True) # access port for protocol above: 8042, 11112, ...
    user       = CharField(max_length=250, null=True) 
    pswd       = CharField(max_length=250, null=True) 
    
    @classmethod
    def create(cls, *args, **kwargs):
        if 'typename' in kwargs and 'source_type' not in kwargs:
            kwargs['source_type'] = DBSourceTypes.get(DBSourceTypes.name == kwargs['typename'])
        return super(DBDataSources, cls).create(*args, **kwargs)

    def as_dict(self):
        fields = [f for f in DBDataSources._meta.sorted_field_names]
        field_dict = {field: getattr(self, field) for field in fields}
        field_dict['typename'] = self.source_type.name
        return field_dict
        
    @property
    def serialize(self):
        data = {
            'id':self.id,
            'name':self.name,
            'protocol':self.protocol,
            'host':self.host,
            'port':self.port
        }
        return data
    
class DBModules(DBModel):
    # define fields
    name        = CharField(unique=True, max_length=250) # identifier
    description = CharField(max_length=250) 
    filename    = CharField(max_length=250) # path to executable relative to root/Module/name
    foldername  = CharField(max_length=250, unique=True) # what is the length of the tempfolder dir name?
    origin      = CharField(max_length=32, default='user') # either factory or user
    repo_version = CharField(max_length=32, default="" )  # if installed from repo
    repo_url     = CharField(max_length=250, default=""  ) # if installed from repo
    
    def delete_instance(self, *args, **kwargs):
        # also delete uploaded folder
        module_dir = DBVariables.get(DBVariables.name=='modules_dir').val
        # remove the folder if modifying an existing module
        shutil.rmtree(os.path.join(module_dir, self.foldername), ignore_errors=True)
        return super(DBModules, self).delete_instance(*args, **kwargs)
    
    @classmethod
    def create(cls, *args, **kwargs):
        # this dstfolder should be unique, and can only exist if it is coupled to the current pk
        # however, since we are deleting folders, we need to check if there is not another reference to it.
        if len(DBModules.select().where(DBModules.name == kwargs['name']))>0:
            raise ValueError('Uniqueness constraint failed on module name %s'%kwargs['name'])
 
        # create unique foldername
        module_dir = DBVariables.get(DBVariables.name=='modules_dir').val
        if not os.path.exists(module_dir):
            os.makedirs(module_dir)
        kwargs['foldername'] = os.path.basename(tempfile.mkdtemp(dir=module_dir,prefix='mod'))
        
        module = super(DBModules, cls).create(*args, **kwargs)
        if 'uploadfilepath' in kwargs:
            module.upload_module(kwargs['uploadfilepath'])
        
        return module

    def save(self, *args, **kwargs):
        if 'uploadfilepath' in kwargs:
            self.upload_module(kwargs['uploadfilepath'])
            del kwargs['uploadfilepath']
        return super(DBModules, self).save(*args, **kwargs)

    def upload_module(self, uploadfilepath):
        """
        mechanism to upload new or revised module. After uploading, a call to dbio is needed for inclusion in the wad db
        """
        module_dir = DBVariables.get(DBVariables.name == 'modules_dir').val
        # a module is always placed in a subfolder with the name 'modulename'
        dstfolder = os.path.join(module_dir, self.foldername)
        
        # remove the folder if modifying an existing module
        if os.path.exists(dstfolder):
            shutil.rmtree(dstfolder, ignore_errors=True)
    
        # create the dstfolder (no error checking, since we just deleted it)
        os.makedirs(dstfolder)
    
        # if the uploaded file is a zip file, unpack it
        ext = os.path.splitext(os.path.basename(uploadfilepath))[1]
        if ext.lower() == '.zip':
            # unzip uploaded zip to destination folder and remove zip file
            with zipfile.ZipFile(uploadfilepath, 'r') as z:
                z.extractall(dstfolder)
            exename = os.path.join(dstfolder,self.filename)
        else: # not a zipfile, take upload as is
            # destination file
            exename = os.path.join(dstfolder, os.path.basename(self.filename))
            # copy uploaded file to proper location
            shutil.copyfile(uploadfilepath, exename)
    
        try: # make module executable
            os.chmod(exename, os.stat(exename).st_mode | stat.S_IEXEC)
        except Exception as e:
            logging.debug('cannot make %s executable'%exename)
            
    @property
    def serialize(self):
        data = {
            'id': self.id,
            'name': str(self.name).strip(),
            'description': str(self.description).strip(),
            'executable':self.filename,
            'origin': self.origin,
            'repo_version':self.repo_version,
            'repo_url':self.repo_url             
        }
        return data
        

class DBMetaConfigs(DBModel):
    # define fields
    val           = BlobField(default='{"results": {},'+' "metaformat": "{}"'.format(METAFORMAT)+'}')
    
    @property
    def serialize(self):
        data = {
            'id': self.id,
            'id_config':[],
            'id_selector':[]
        }
        for cfg in self.module_configs:
            data['id_config'].append(cfg.id)
            for selector in cfg.selectors:
                data['id_selector'].append(selector.id)
        return data

class DBModuleConfigs(DBModel):
    # define fields
    name          = CharField(max_length=250)
    description   = CharField(max_length=250) 
    module        = ForeignKeyField(DBModules, backref='module_configs') # backref is for reference from DBModules
    data_type     = ForeignKeyField(DBDataTypes, backref='module_configs') 
    val           = BlobField()
    meta          = ForeignKeyField(DBMetaConfigs, backref='module_configs') # display stuff, limits, ...
    origin        = CharField(max_length=32, default='user') # either factory or user or result
    
    @classmethod
    def getConfigsByName(cls, name):
        # NOTE: these names need not be unique
        return list(cls.select().where(cls.name == name).order_by(cls.name))

    @classmethod
    def get_by_name(cls, name):
        # Name is not unique, so do not use!
        raise NotImplemented("get_by_name not implemented for classes where name is not unique!")

    @classmethod
    def create(cls, *args, **kwargs): # for a new moduleconfig, make sure to supply a new meta as well!
        if 'datatypename' in kwargs and 'data_type' not in kwargs:
            kwargs['data_type'] = DBDataTypes.get(DBDataTypes.name == kwargs['datatypename'])
        if 'modulename' in kwargs and 'module' not in kwargs:
            kwargs['module'] = DBModules.get(DBModules.name == kwargs['modulename'])
        return super(DBModuleConfigs, cls).create(*args, **kwargs)
    
    def clone(self):
        # clone also creates a new meta
        fields = [f for f in DBModuleConfigs._meta.sorted_field_names if f != 'id']
        field_dict = {field: getattr(self, field) for field in fields}
        nw_meta = DBMetaConfigs.create(val=self.meta.val)
        field_dict['meta'] = nw_meta.id
        return DBModuleConfigs.create(**field_dict)

    def delete_instance(self, *args, **kwargs):
        # also delete connected meta
        mta = self.meta
        res = super(DBModuleConfigs, self).delete_instance(*args, **kwargs)
        if len(mta.module_configs) == 0: # only if no references left
            mta.delete_instance(recursive=False)
        return res
        
    @property
    def serialize(self):
        data = {
            'id': self.id,
            'name': str(self.name).strip(),
            'description': str(self.description).strip(),
            'data_type':self.data_type.serialize,
            'id_selector':[],
            'origin':self.origin,
            'meta':self.meta.id
        }
        for selector in self.selectors:
            data['id_selector'].append(selector.id)
        return data


class DBSelectors(DBModel):
    # define fields
    name              = CharField(unique=True, max_length=250) # identifier
    description       = CharField(max_length=250) 
    isactive          = BooleanField(default=False)
    module_config     = ForeignKeyField(DBModuleConfigs, backref='selectors') 
    manual_input      = BooleanField(default=False)
    
    @classmethod
    def create(cls, *args, **kwargs):
        if 'configname' in kwargs and 'module_config' not in kwargs:
            kwargs['module_config'] = DBModuleConfigs.get(DBModuleConfigs.name == kwargs['configname'])
        #copy base config, but supply a new meta
        cfg = DBModuleConfigs.get(DBModuleConfigs.id == kwargs['module_config'])
        fields = [f for f in DBModuleConfigs._meta.sorted_field_names if not f == 'id']
        field_dict = {field: getattr(cfg, field) for field in fields}
        nw_meta = DBMetaConfigs.create(val=cfg.meta.val)
        field_dict['meta'] = nw_meta.id
        nw_cfg = DBModuleConfigs.create(**field_dict)
        nw_cfg.origin = 'result' # flag it, so it is possible to distinguish between user/factory and old coupled configs
        nw_cfg.save()
        
        kwargs['module_config'] = nw_cfg
        return super(DBSelectors, cls).create(*args, **kwargs)
    
    def addRule(self, dicomtag, logicname, values):
        rule = DBSelectorRules.create(selector=self, dicomtag=dicomtag, logic=DBSelectorLogics.get(DBSelectorLogics.name == logicname))
        for val in values:
            DBRuleValues.create(rule=rule, val=val)
        return rule

    def clone(self):
        #copy fields
        fields = [f for f in DBSelectors._meta.sorted_field_names if f != 'id']
        field_dict = {field: getattr(self, field) for field in fields}

        # make selector name unique
        existing_names = [m.name for m in DBSelectors.select()]
        i = 0
        while 'clone_%d of %s'%(i,self.name) in existing_names:
            i += 1
        field_dict['name'] = 'clone_%d of %s'%(i,self.name)

        # make selector
        nw_sel = DBSelectors.create(**field_dict)
        
        # copy rules
        for rule in self.rules:
            values = [v.val for v in rule.values]
            nw_sel.addRule(rule.dicomtag, rule.logic.name, values)

        return nw_sel
        
    @property
    def serialize(self):
        data = {
            'id': self.id,
            'name': str(self.name).strip(),
            'isactive': self.isactive,
            'manual_input': self.manual_input,
            'description': str(self.description).strip(),
            'id_config':self.module_config.id,
            'id_meta':self.module_config.meta.id
        }
        return data


class DBSelectorLogics(DBModel):
    # define fields
    name = CharField(unique=True, max_length=50) # identifier


class DBSelectorRules(DBModel):
    # define fields
    dicomtag          = CharField(max_length=20) 
    selector          = ForeignKeyField(DBSelectors, backref='rules') 
    logic             = ForeignKeyField(DBSelectorLogics, backref='rules') 


class DBRuleValues(DBModel):
    # define fields
    val               = CharField(max_length=250) 
    rule              = ForeignKeyField(DBSelectorRules, backref='values') 


class DBProcessInput(DBModel):
    # define fields
    uid  = CharField(max_length=64) # study_uid or series_uid or instance_uid
    data_type = ForeignKeyField(DBDataTypes, backref='process_input')
    selector = ForeignKeyField(DBSelectors, backref='process_input')
    name = CharField(max_length=64) # identifier
    result_type = CharField(max_length=10)
    val  = CharField(max_length=512, null=True)
    desc = CharField(max_length=64)
    required = BooleanField(default=False)
    
    @property
    def serialize(self):
        data = {
            'id':self.id,
            'uid': self.uid,
            'selector':self.selector.serialize,
            'name':self.name,
            'result_type':self.result_type,
            'required':self.required,
            'val':self.val,
            'desc':self.desc
        }
        return data


class DBProcessStatus(DBModel):
    # define fields
    name = CharField(unique=True, max_length=250) # identifier


class DBDataSets(DBModel):
    # define fields
    data_source   = ForeignKeyField(DBDataSources, backref='datasets')
    data_type     = ForeignKeyField(DBDataTypes, backref='datasets')
    data_id       = CharField(max_length=64) # study_id or series_id or instance_id or orthanc_id

    class Meta:
        indexes = (
            (("data_source_id", "data_type_id", "data_id"), True), # make sure the combination of source/type/data_id is unique
        )
        
    @property
    def serialize(self):
        results = []
        rows = DBResults.select().where(DBResults.data_set == self)
        for row in rows:
            results.append(row.id)

        notes = []
        for note in self.notes:
            notes.append(note.serialize)
        data = {
            'id':self.id,
            'data_source':self.data_source.serialize,
            'data_type':self.data_type.serialize,
            'data_id':self.data_id,
            'notes':notes,
            'results':results
        }
        return data
    
class DBDataTrash(DBModel):
    """
    Indicate datasets that are in the trash. These will not show up in residuals.
    """
    data_set = ForeignKeyField(DBDataSets, backref='datathrash')

class DBDataTags(DBModel):
    # define fields
    name = CharField(unique=True, max_length=250) # identifier
    
    @property
    def serialize(self):
        data = {
            'id': self.id,
            'name':self.name
        }
        return data

class DBNotes(DBModel):
    # define fields
    data_set       = ForeignKeyField(DBDataSets, backref='notes')
    data_tag       = ForeignKeyField(DBDataTags, backref='notes')
    description    = CharField(max_length=250)
    
    @property
    def serialize(self):
        data = {
            'id': self.id,
            'data_tag': self.data_tag.serialize,
            'description': str(self.description).strip()
        }
        return data
    


class DBProcesses(DBModel):
    # define fields
    selector          = ForeignKeyField(DBSelectors, backref='processes')
    module_config     = ForeignKeyField(DBModuleConfigs, backref='processes')
    data_set          = ForeignKeyField(DBDataSets, backref='processes')
    process_status    = ForeignKeyField(DBProcessStatus, backref='processes')
    created_time      = DateTimeField()
    process_log       = BlobField(null=True)

    @classmethod
    def getProcessesByStatus(cls, status):
        if isinstance(status, str):
            status = [status]
        return cls.select().join(DBProcessStatus).where(DBProcessStatus.name.in_(status))
        
    @classmethod
    def create(cls, *args, **kwargs):
        if 'selectorname' in kwargs and 'selector' not in kwargs:
            kwargs['selector'] = DBSelectors.get(DBSelectors.name == kwargs['selectorname'])
        if 'processstatusname' in kwargs and 'process_status' not in kwargs:
            kwargs['process_status'] = DBProcessStatus.get(DBProcessStatus.name == kwargs['processstatusname'])
        if 'created_time' not in kwargs:
            kwargs['created_time'] = datetime.now()
        #if 'datasourcename' in kwargs and 'data_source' not in kwargs:
        #    kwargs['data_source'] = DBDataSources.get(DBDataSources.name == kwargs['datasourcename'])
            
        kwargs['module_config'] = kwargs['selector'].module_config
        
        return super(DBProcesses, cls).create(*args, **kwargs)
        
    @property
    def serialize(self):
        if not self.process_log:
            log = 'No log for this process'
        else:
            log = bytes_as_string(self.process_log)
        data = {
            'id':self.id,
            'selector':{'id':self.selector.id,'name':self.selector.name},
            'data_set':self.data_set.serialize,
            'status':self.process_status.name,
            'created':self.created_time,
            'log':log
        }
        return data        

class DBResults(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    selector          = ForeignKeyField(DBSelectors, backref='results')
    module_config     = ForeignKeyField(DBModuleConfigs, backref='results') 
    data_set          = ForeignKeyField(DBDataSets, backref='results')
    created_time      = DateTimeField()
    process_log       = BlobField(null=True)
    
    @classmethod
    def finishedProcess(cls, process):
        fields = [f for f in DBResults._meta.sorted_field_names if f != 'id']
        field_dict = {field: getattr(process, field) for field in fields}
        return super(DBResults, cls).create(**field_dict)
    
    
    def getResults(self):
        result_fields = ['strings', 'floats', 'bools', 'objects', 'datetimes']
        all_results = [x for field in result_fields for x in getattr(self, field)]
        return all_results

    def getDate(self):
        dts = self.datetimes
        if len(dts)>0:
            dt = max([p.val for p in dts])
        else:
            dt = self.created_time
        return dt  

    def getStatus(self,meta):
        status = {}
        status_list = []
        for test in self.getResults():
            t = test.metafy(meta)
            if t['type']=='datetime':
                status['datetime'] = t['status']
            elif t['type']!='object':
                status_list.append(t['status'])
        try:
            status['tests'] = max(status_list)
        except:
            status['tests'] = 0

        return status
        
    @property
    def serialize(self):
        meta = json.loads(jsmin.jsmin(bytes_as_string(self.module_config.meta.val)))['results']
        if not self.process_log:
            log = 'No log for this result'
        else:
            log = bytes_as_string(self.process_log)
        data = {
            'id':self.id,                                #we could only return the ids, but getting everything is probably more usefull (maybe resending)
            'selector':{'id':self.selector.id,'name':self.selector.name},
            'id_config':{'id':self.module_config.id},
            'data_set':self.data_set.serialize,
            'created':self.created_time,
            'date':self.getDate(),
            'status':self.getStatus(meta),
            'log':log
        }
        return data


class DBResultDateTimes(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    result          = ForeignKeyField(DBResults, backref='datetimes') # should this link always remain valid? never cleanup?
    name            = CharField(max_length=100) 
    val             = DateTimeField()
    val_equal       = DateTimeField(null=True)
    val_period      = IntegerField(null=True) # days between results (intended as acq interval)

    def metafy(self,meta):
        if self.name in meta:
            if 'constraint_period' in meta[self.name]:
                limit = meta[self.name]['constraint_period']
                t = (datetime.now()-self.val)
                t = t.days+t.seconds/60/60/24
                if t>limit:
                    status = 3
                else:
                    status = 1
            else:
                limit = None
                status = 0
            if 'display_name' in meta[self.name]:
                display_name = meta[self.name]['display_name']
            else:
                display_name = self.name
            if 'display_level' in meta[self.name]:
                display_level = meta[self.name]['display_level']
            else:
                display_level = 2
        else:
            limit = None
            status = 0
            display_name = self.name
            display_level = 1
        return {'status':status,'limit':limit,'display_name':display_name,'type':'datetime','display_level':display_level}
    
    def meta_serialize(self,meta):
        data = self.metafy(meta)
        data['id'] = self.id
        data['name'] = self.name
        data['value'] = self.val
        return data

    @classmethod
    def get_by_name(cls, name):
        # Name is not unique, so do not use!
        raise NotImplemented("get_by_name not implemented for classes where name is not unique!")


class DBResultStrings(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    result          = ForeignKeyField(DBResults, backref='strings') # should this link always remain valid? never cleanup?
    name            = CharField(max_length=100) 
    val             = CharField(max_length=100)
    val_equal       = CharField(max_length=100, null=True)

    def metafy(self,meta):
        if self.name in meta:
            if 'constraint_equals' in meta[self.name]:
                limit = meta[self.name]['constraint_equals']
                if limit==self.val:
                    status = 1
                else:
                    status = 3
            else:
                limit = None
                status = 0
                
            if 'display_name' in meta[self.name]:
                display_name = meta[self.name]['display_name']
            else:
                display_name = self.name
            if 'display_level' in meta[self.name]:
                display_level = meta[self.name]['display_level']
            else:
                display_level = 9
        else:
            limit = None
            status = 0
            display_name = self.name
            display_level = 9
        
        return {'status':status,'limit':limit,'display_name':display_name,'type':'string','display_level':display_level}
        
    def meta_serialize(self,meta):
        data = self.metafy(meta)
        data['id'] = self.id
        data['name'] = self.name
        data['value'] = self.val
        return data

    @classmethod
    def get_by_name(cls, name):
        # Name is not unique, so do not use!
        raise NotImplemented("get_by_name not implemented for classes where name is not unique!")


class DBResultFloats(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    result          = ForeignKeyField(DBResults, backref='floats') # should this link always remain valid? never cleanup?
    name            = CharField(max_length=100) 
    val             = DoubleField()
    val_equal       = DoubleField(null=True) 
    val_min         = DoubleField(null=True) 
    val_max         = DoubleField(null=True) 
    val_low         = DoubleField(null=True) 
    val_high        = DoubleField(null=True)
    val_ref         = DoubleField(null=True)

    def metafy(self,meta):     #we expect meta['results'] as input
        if self.name in meta:
            if 'constraint_minlowhighmax' in meta[self.name]:
                limits = meta[self.name]['constraint_minlowhighmax']
                limit = limits
                if not limits[3] is None and self.val>limits[3]:
                    status = 3
                elif not limits[0] is None and self.val<limits[0]:
                    status = 3
                elif not limits[2] is None and self.val>limits[2]:
                    status = 2
                elif not limits[1] is None and self.val<limits[1]:
                    status = 2
                else:
                    status = 1

            elif 'constraint_refminlowhighmax' in meta[self.name]:
                limits = meta[self.name]['constraint_refminlowhighmax']
                if limits[0] is None:
                    status = 0
                    limit = None
                else:
                    limit = [(limits[0]+limits[0]*(float(limits[1])/100)) if not limits[1] is None else None, 
                             (limits[0]+limits[0]*(float(limits[2])/100)) if not limits[2] is None else None, 
                             (limits[0]+limits[0]*(float(limits[3])/100)) if not limits[3] is None else None, 
                             (limits[0]+limits[0]*(float(limits[4])/100)) if not limits[4] is None else None]
                
                    if not limit[3] is None and self.val>limit[3]:
                        status = 3
                    elif not limit[0] is None and self.val<limit[0]:
                        status = 3
                    elif not limit[2] is None and self.val>limit[2]:
                        status = 2
                    elif not limit[1] is None and self.val<limit[1]:
                        status = 2
                    else:
                        status = 1

            else:
                status = 0
                limit = None

            if 'units' in meta[self.name]:
                units = meta[self.name]['units']
            else:
                units = ''
            if 'display_name' in meta[self.name]:
                display_name = meta[self.name]['display_name']
            else:
                display_name = self.name
            if 'display_level' in meta[self.name]:
                display_level = meta[self.name]['display_level']
            else:
                display_level = 9
        else:
            status = 0
            limit = None
            display_name = self.name
            units = ''
            display_level = 9
            
        return {'status':status,'limit':limit,'display_name':display_name,'units':units,'type':'float','display_level':display_level}
        
    def meta_serialize(self,meta):
        data = self.metafy(meta)
        data['id'] = self.id
        data['name'] = self.name
        data['value'] = self.val
        return data

    @classmethod
    def get_by_name(cls, name):
        # Name is not unique, so do not use!
        raise NotImplemented("get_by_name not implemented for classes where name is not unique!")


class DBResultBools(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    result          = ForeignKeyField(DBResults, backref='bools') # should this link always remain valid? never cleanup?
    name            = CharField(max_length=100) 
    val             = BooleanField()
    val_equal       = BooleanField(null=True)
    
    def metafy(self,meta):
        if self.name in meta:
            if 'constraint_equals' in meta[self.name]:
                limit = meta[self.name]['constraint_equals']
                if limit==self.val:
                    status = 1
                else:
                    status = 3
            else:
                limit = None
                status = 0
                
            if 'display_name' in meta[self.name]:
                display_name = meta[self.name]['display_name']
            else:
                display_name = self.name
            if 'display_level' in meta[self.name]:
                display_level = meta[self.name]['display_level']
            else:
                display_level = 9
                
        else:
            limit = None
            status = 0
            display_name = self.name
            display_level = 9
        
        return {'status':status,'limit':limit,'display_name':display_name,'type':'bool','display_level':display_level}
        
    def meta_serialize(self,meta):
        data = self.metafy(meta)
        data['id'] = self.id
        data['name'] = self.name
        data['value'] = self.val
        return data

    @classmethod
    def get_by_name(cls, name):
        # Name is not unique, so do not use!
        raise NotImplemented("get_by_name not implemented for classes where name is not unique!")

class DBResultObjects(DBModel): # a copy of the processes table, only filled when processes are finished
    # define fields
    result          = ForeignKeyField(DBResults, backref='objects') # should this link always remain valid? never cleanup?
    name            = CharField(max_length=100) 
    filetype        = CharField(max_length=10) 
    val             = BlobField()

    def metafy(self,meta):
        if self.name in meta:
            if 'display_name' in meta[self.name]:
                display_name = meta[self.name]['display_name']
            else:
                display_name = self.name
            if 'display_level' in meta[self.name]:
                display_level = meta[self.name]['display_level']
            else:
                display_level = 9
        else:
            display_name = self.name
            display_level = 9
        return {'display_name':display_name,'type':'object','status':0,'display_level':display_level}
        
    def meta_serialize(self,meta):
        data = self.metafy(meta)
        data['id'] = self.id
        data['name'] = self.name
        data['value'] = base64.b64encode(self.val)
        return data

    @classmethod
    def get_by_name(cls, name):
        # Name is not unique, so do not use!
        raise NotImplemented("get_by_name not implemented for classes where name is not unique!")

DBTables = [
    DBVariables,
    DBProcessInput,
    DBSourceTypes,
    DBDataSources,
    DBModules,
    DBMetaConfigs,
    DBModuleConfigs, 
    DBDataTypes,
    DBSelectors,
    DBSelectorLogics,
    DBSelectorRules, 
    DBRuleValues, 
    DBProcessStatus,
    DBDataSets,
    DBDataTrash,
    DBDataTags,
    DBNotes,
    DBProcesses,
    DBResults, 
    DBResultDateTimes, 
    DBResultStrings, 
    DBResultFloats,
    DBResultBools, 
    DBResultObjects
]
