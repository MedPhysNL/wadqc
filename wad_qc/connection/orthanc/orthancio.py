import os
from ..pacsiobase import PACSIOBASE
from . import rest_toolbox as RestToolbox
import zipfile
import shutil
import tempfile
from socket import error as SocketError
import logging

#----string/bytes conversion support for python2 and python3
import codecs
def string_as_bytes(x):
    return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    return codecs.latin_1_decode(x)[0]

__version__ = '20210326'

"""
Changelog:
  20210326: added endpoints for disk statistics
  20170301: added getSeriesId
  20170227: fixed RemoteAET showing as bytes and non_lower case
  20161110: option to obtain all seriesIDs, all studyIDs, all instanceIDs
  20160907: typo in getInstanceHeaders
  20160631: input all access parameters as dict, not json
  20160526: split from pacsio class as this should be specific for orthanc
  20160513: simplified logger and input
  20160425: prefer init with a dict instead of an inifile to prevent problems due to runtime changes in inifile;
    add RemoteAET as special entry to instanceheaders
  20160422: using python logging; read config only once
  20160421: wad_qc path;check for inifile before parsing;
  20160420: added verbosity
  20160419: uploadDicom follows symlinks
  20160406: read config from inifile (supplied testfile, or predefined); moved testcode to pacsio__test.py
  20160314: wad2.0 redesign

"""

class PACSIO(PACSIOBASE):
    """
    Class with all lower level functions to access the local PACS
    Must provide:  
         getSharedStudyHeaders(studyid)
         getSharedSeriesHeaders(seriesid)
         getInstanceHeaders(instanceid)
         getSeriesIds(studyid)
         getInstancesIds(seriesid)
         getStudyData(studyid)
    """
    def __init__(self, config_dict):
        #set the user and password to access Orthanc
        self.pacstype = config_dict['typename']
        self.URL, self.credentials = self._generate_pacs_url(config_dict)
        self._isAccessible()
        
    def _generate_pacs_url(self, config_dict):
        url = '%s://%s:%d'%(config_dict['protocol'], config_dict['host'], int(config_dict['port']))
        
        credentials = (config_dict['user'], config_dict['pswd'])
        return url, credentials

    def _isAccessible(self):
        try:
            res = RestToolbox.DoGet('%s/statistics' % (self.URL), self.credentials)
        except SocketError as e:
            logging.error('Orthanc not running at %s. Please start it and run this script again'%(self.URL))
            raise
        except Exception as e:
            if e.args[0] == 401:
                logging.error('Supplied credentials not valid for accessing Orthanc at %s.\n'\
                      'Please edit credentials PACS configuration file.'%(self.URL))
            else: 
                logging.error(str(e))
            raise        
        
    def getSharedStudyHeaders(self, studyid):
        """ 
        input: 'studyid' is the orthanc id of this study.
        output: a dictionary of the headers shared by all instances in a study.
        
        each dicom tag 1234,abcd is an item, containing a dictionary {'Name','Type','Value'}
           "0008,0005" : {
             "Name" : "SpecificCharacterSet",
             "Type" : "String",
             "Value" : "ISO_IR 100"
             },
        where 'Value' can be a list containing a new dictionary of tags for nested tags
        """
        # Retrieve the DICOM tags of this study

        return RestToolbox.DoGet('%s/studies/%s/shared-tags' % (self.URL, studyid), self.credentials)
    
    def getSharedSeriesHeaders(self, seriesid):
        """ 
        input: 'seriesid' is the orthanc id of this series.
        output: a dictionary of the headers shared by all instances in a series.
        """
        # Retrieve the DICOM tags of this series

        return RestToolbox.DoGet('%s/series/%s/shared-tags' % (self.URL, seriesid), self.credentials)

    def getInstanceHeaders(self, instanceid):
        """ 
        input: 'instanceid' is the orthanc id of this instance.
        output: a dictionary of all headers of this instance.
        """
        # Retrieve the DICOM tags of this instance
        # maybe we should add the SrcAET here, which is part of the metadata, not the header
        #http://localhost:8042/instances/d1e4edd3-de32cb54-ae1d7ae4-4260ace8-553fdaf8/metadata/RemoteAET
        headers = RestToolbox.DoGet('%s/instances/%s/tags' % (self.URL, instanceid), self.credentials)
        aet = RestToolbox.DoGet('%s/instances/%s/metadata/RemoteAET' % (self.URL, instanceid), self.credentials)
        headers['RemoteAET'.lower()] = { 
            'Name': 'RemoteAET', 
            'Type': 'String',
            'Value': bytes_as_string(aet)
            }
        return headers

    def getSeriesIds(self,studyid):
        """
        Return a list of all seriesids connected to that studyid
        """
        if studyid is None: # return all series
            return RestToolbox.DoGet('%s/series' % (self.URL), self.credentials)
        return RestToolbox.DoGet('%s/studies/%s' % (self.URL, studyid), self.credentials)['Series']

    def getStudyIds(self,patientid=None):
        """
        Return a list of all studyids connected to that patientid
        """
        if patientid is None: # return all studies
            return RestToolbox.DoGet('%s/studies' % (self.URL), self.credentials)
        return RestToolbox.DoGet('%s/patients/%s' % (self.URL, patientid), self.credentials)['Studies']

    def getPatientIds(self):
        """
        Return a list of all patientids
        """
        return RestToolbox.DoGet('%s/patients' % (self.URL), self.credentials)

    def getInstancesIds(self, seriesid):
        """
        Return a list of all instanceids connected to that seriesid
        """
        if seriesid is None: # return all instances
            return RestToolbox.DoGet('%s/instances' % (self.URL), self.credentials)
        return RestToolbox.DoGet('%s/series/%s' % (self.URL, seriesid), self.credentials)['Instances']
    
    def getStudyId(self, seriesid=None, instanceid=None):
        # return the studyid of a series or instance
        if not seriesid is None:
            return RestToolbox.DoGet('%s/series/%s/study' % (self.URL, seriesid), self.credentials)['ID']
        if not instanceid is None:
            return RestToolbox.DoGet('%s/instances/%s/study' % (self.URL, instanceid), self.credentials)['ID']
        return None
        
    def getSeriesId(self, instanceid):
        # return the seriesid of an instance
        return RestToolbox.DoGet('%s/instances/%s/series' % (self.URL, instanceid), self.credentials)['ID']
        
    def getPatientTags(self, id_patient):
        # return selection of patient tags
        tags = RestToolbox.DoGet('%s/patients/%s' % (self.URL, id_patient), self.credentials)
        data = {
            'Type':'Patient',
            'PatientName': tags['MainDicomTags']['PatientName'],
            'PatientID': tags['MainDicomTags']['PatientID'],
            'Studies':tags['Studies']
        }
        return data      
    
        
    def getStudyTags(self, id_study):
        # return selection of study tags
        tags = RestToolbox.DoGet('%s/studies/%s' % (self.URL, id_study), self.credentials)
        data = {
            'Type':'Study',
            'StudyDescription': tags['MainDicomTags']['StudyDescription'],
            'StudyDate': tags['MainDicomTags']['StudyDate'],
            'Series':tags['Series'],
            'PatientID':tags['ParentPatient'],
            'StudyInstanceUID':tags['MainDicomTags']['StudyInstanceUID']
        }
        return data        
        
    def getSeriesTags(self, id_series):
        # return selection of series tags
        tags = RestToolbox.DoGet('%s/series/%s' % (self.URL, id_series), self.credentials)
        data = {
            'Type':'Series',
            'SeriesDescription': tags['MainDicomTags']['SeriesDescription'],
            'SeriesDate': tags['MainDicomTags']['SeriesDate'],
            'SeriesNumber': tags['MainDicomTags']['SeriesNumber'],
            'StudyID':tags['ParentStudy'],
            'SeriesInstanceUID':tags['MainDicomTags']['SeriesInstanceUID']
        }
        return data
        
    def getInstanceTags(self, id_instance):
        # return selection of instance tags
        tags = RestToolbox.DoGet('%s/instances/%s' % (self.URL, id_instance), self.credentials)
        data = {
            'Type':'Instances',
            'InstanceDate': tags['MainDicomTags']['InstanceCreationDate'],
            'Index': tags['IndexInSeries'],
            'SeriesID':tags['ParentSeries'],
            'InstanceUID':tags['MainDicomTags']['SOPInstanceUID']
        }
        return data

    def getSeriesStatistics(self, dataid):
        """
        return statistics
        """
        return RestToolbox.DoGet('%s/series/%s/statistics' % (self.URL, dataid), self.credentials)
        
    def getStudyStatistics(self, dataid):
        """
        return statistics
        """
        return RestToolbox.DoGet('%s/studies/%s/statistics' % (self.URL, dataid), self.credentials)

    def getInstanceStatistics(self, dataid):
        """
        return statistics
        """
        return RestToolbox.DoGet('%s/instances/%s/statistics' % (self.URL, dataid), self.credentials)

    def getLevelId(self, level, studyid=None, seriesid=None, instanceid=None):
        # return the patientid/studyid/seriesid of an item with given studyid/seriesid/instanceid
        if not level in ['patient','study','series']:
            raise ValueError('[_getLevelData] unknown leveltype %s'%level)

        if level == 'patient':
            if not studyid is None:
                return RestToolbox.DoGet('%s/studies/%s/patient' % (self.URL, studyid), self.credentials)['ID']
            if not seriesid is None:
                return RestToolbox.DoGet('%s/series/%s/patient' % (self.URL, seriesid), self.credentials)['ID']
            if not instanceid is None:
                return RestToolbox.DoGet('%s/instances/%s/patient' % (self.URL, instanceid), self.credentials)['ID']
        elif level == 'study':
            if not seriesid is None:
                return RestToolbox.DoGet('%s/series/%s/study' % (self.URL, seriesid), self.credentials)['ID']
            if not instanceid is None:
                return RestToolbox.DoGet('%s/instances/%s/study' % (self.URL, instanceid), self.credentials)['ID']
        elif level == 'series':
            if not instanceid is None:
                return RestToolbox.DoGet('%s/instances/%s/series' % (self.URL, instanceid), self.credentials)['ID']

        return None

    def getData(self, data_id, data_type, dcmfolder):
        """
        Retrieve data of patient from Orthanc, and put in the given, previously created folder dcmfolder
        hierarchy: patient/study/series/instances
        """
        if data_type == 'dcm_series': 
            return self._getLevelData('series',data_id,dcmfolder)
        if data_type == 'dcm_study':
            return self._getLevelData('studies',data_id,dcmfolder)
        if data_type == 'dcm_instance':
            return self._getLevelData('instances',data_id,dcmfolder)
        if data_type == 'dcm_patient':
            return self._getLevelData('patients',data_id,dcmfolder)
        return None
        
    def _getLevelData(self,level,itemid,dcmfolder):
        """
        Retrieve data from Orthanc, and put in the given, previously created folder dcmfolder
        hierarchy: patient/study/series/instances
        """
        if not level in ['patients','studies','series','instances']:
            raise ValueError('[_getLevelData] unknown leveltype %s'%level)
        
        if dcmfolder is None or not os.path.isdir(dcmfolder):
            raise ValueError('[_getLevelData] offered outputfolder %s does not exist!'%dcmfolder)

        if level == 'instances':
            """
            Retrieve data of instance from Orthanc, and put in the given, previously created folder dcmfolder
            """
            # restructure: dcmfolder/series001/...dcm
            dstfolder = os.path.join(dcmfolder,'series001')
            os.makedirs(dstfolder)
            filename = os.path.join(dstfolder,'%s.dcm'%str(itemid))
            with open(filename,'wb') as f:
                f.write( RestToolbox.DoGet('%s/instances/%s/file' % (self.URL, itemid), self.credentials) )
            return dcmfolder
        
        ## series or study
        tmpfolder = tempfile.mkdtemp()  # create dir
        # first write the incoming archive to disk
        filename = os.path.join(tmpfolder,'%s.zip'%str(itemid))
        with open(filename,'wb') as f:
            f.write( RestToolbox.DoGet('%s/%s/%s/archive' % (self.URL, level, itemid), self.credentials) )# gives a archive.zip

        # unpack zip
        with zipfile.ZipFile(filename,'r') as zip_file:
            zip_file.extractall(tmpfolder)
            
        ## restructure
        # restructure: dcmfolder/series001/...dcm dcmfolder/series002/...dcm
        seriesfolders =[]
        for root, dirs, files in os.walk(tmpfolder):
            for fname in files:
                if fname.endswith('.dcm'): 
                    if not root in seriesfolders:
                        seriesfolders.append(root)
                        break
        
        for i,s in enumerate(seriesfolders):
            shutil.move(s, os.path.join(dcmfolder,'series%s'%str(i).zfill(3)))
        
        # clean up temporary stuff
        shutil.rmtree(tmpfolder)
        return dcmfolder

    def uploadDicomFile(self, filename):
        """
        send a single dicom to the PACS
        Returns a log dict
        """
        log = {'Filename': filename}
        
        with open(filename, 'rb') as f:
            d = f.read()
        
        try:
            log.update( RestToolbox.DoPost('%s/instances'%self.URL, self.credentials, d, 'application/dicom') )
        except Exception as e:
            if e.args[0] == 400:
                log['Status'] = 'invalid'
        
        return log

    def deleteData(self, data_id, data_type):
        """
        delete given data_type from PACS
        """
        if data_type == 'dcm_series': 
            return RestToolbox.DoDelete('%s/series/%s' % (self.URL, data_id), self.credentials)
        if data_type == 'dcm_study':
            return RestToolbox.DoDelete('%s/studies/%s' % (self.URL, data_id), self.credentials)
        if data_type == 'dcm_instance':
            return RestToolbox.DoDelete('%s/instances/%s' % (self.URL, data_id), self.credentials)
        if data_type == 'dcm_patient':
            return RestToolbox.DoDelete('%s/patients/%s' % (self.URL, data_id), self.credentials)

        raise ValueError('[deleteData] data_type %s not recognized for PACS type %s!'%(data_type, self.pacstype))

    
