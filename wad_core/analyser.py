#!/usr/bin/env python

from __future__ import print_function

import os
import shutil
import subprocess
import tempfile
import jsmin
import json
import traceback
import datetime
import time
import threading
    
from wad_qc.connection import bytes_as_string
from wad_qc.connection.pacsio import PACSIO

from logging import getLogger
LOGGERNAMEWORKERS = 'wad_control' # could define a separate logfile if this one gets too cluttered
logger = getLogger(LOGGERNAMEWORKERS)


def prepareTask(tempdir, config):
    datadir = os.path.join(tempdir, 'DICOM')
    config_json = os.path.join(tempdir, 'config.json')
    result_json = os.path.join(tempdir, 'results.json')
    
    os.makedirs(datadir)
    
    with open(config_json, 'w') as f:
        f.write(config)

    return datadir, config_json, result_json


def submitResults(dbio, result, result_json):
    """
    Only add constraints to results if they are supplied by analysis; constraints from meta are to be used at display time only.
    """
    with open(result_json) as json_data:
        data = json.load(json_data)

    for res in data:
        if res['category'] == 'string':
            model = dbio.DBResultStrings
        elif res['category'] == 'bool':
            model = dbio.DBResultBools
            res['val'] = (res['val'] == "1")
            if 'val_equal' in res:
                res['val_equal'] = (res['val_equal'] == "1")
        elif res['category'] == 'float':
            model = dbio.DBResultFloats
        elif res['category'] == 'datetime':
            model = dbio.DBResultDateTimes
            # convert string back to datetime
            res['val'] = datetime.datetime.strptime(res['val'], '%Y-%m-%d %H:%M:%S')
            if 'val_equal' in res:
                res['val_equal'] = datetime.datetime.strptime(res['val_equal'], '%Y-%m-%d %H:%M:%S')
                
        elif res['category'] == 'object':
            model = dbio.DBResultObjects
            
            with open(res['val'], 'rb') as f:
                value = bytes(f.read())
                
            res['filetype'] = res['val'].split('.')[-1].lower()
            res['val'] = bytes_as_string(value)

            
        else:
            logger.warning("Unknown category: %s"%res['category'])
            continue

        model.create(result=result, **res)


def runProcess(cmdline, tempdir, timeout):
    try:        
        kill_check = threading.Event()
        def _kill_after_timeout(p):
            p.terminate()
            kill_check.set()

        proc = subprocess.Popen(cmdline, cwd=tempdir, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)

        timer = threading.Timer(timeout, _kill_after_timeout, args=(proc, ))
        timer.start()
        out, err = proc.communicate()
        timer.cancel()

        if kill_check.isSet():
            message = "Process killed due to timeout (%s seconds)"%timeout
            status = "module error"
        elif proc.returncode < 0: # A negative value -N indicates that the child was terminated by signal N (Unix only).
            message = "Process was killed for unknown reason\n"
            if out:
                message += "Output of module before exception:\n%s\n"%out
            message += "Exception:\n%s"%err
            status = "analyser failed"
        elif proc.returncode and err: # check for returncode, else python warnings are treated as errors!
            message = "Error during execution of process\n"
            if out:
                message += "Output of module before exception:\n%s\n"%out
            message += "Exception:\n%s"%err
            status = "module error"
        else:
            message = out
            if err:
                message += "\n{}".format(err)
            status = "finished"

    except Exception as e:
        message = "Error invoking process"
        message += " ".join(cmdline) + "\n"
        message += "Exception:\n%s"%traceback.format_exc()
        status = "module error"
    
    return status, message


def analyse(dbio, pacsio, tempdir, module_path, process, timeout):
    ### Set process status to busy
    if not process.process_status.name == 'queued': return
    busy_status = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == 'busy')
    
    process.process_status = busy_status
    process.save()
    # convert blob to dict
    config=json.loads(jsmin.jsmin(bytes_as_string(process.module_config.val)))

    ### check for virtual modules
    if process.selector.module_config.module.origin == "virtual":
        # first the trash module
        if process.selector.module_config.module.name == dbio.THRASH_MODULE_NAME:
            logger.info("Moving data set to trash")
            dummy, created = dbio.DBDataTrash.get_or_create(data_set=process.data_set) # do nothing if it already exist (while it should not)
            if not created:
                logger.warning("{} already existed in Trash".format(process.data_set.id))
            
            process.delete_instance()        
            return

    ### check if data in trash: then skip this process
    try:
        dummy = dbio.DBDataTrash.get(data_set=process.data_set)
        logger.warning("Dataset to process {} found in Trash. Removing process.".format(process.data_set.id))
        process.delete_instance()        
        return
    except dbio.DBDataTrash.DoesNotExist:
        # it should not exist
        pass
    
    ### check if selector has manual_input=true
    if process.selector.manual_input:
        logger.info("Manual input process")
        # get uid from dataset
        datatype_name=process.module_config.data_type.name

        if datatype_name == 'dcm_study':
            uid=pacsio.getSharedStudyHeaders(process.data_set.data_id)["0020,000d"]["Value"]
        elif datatype_name == 'dcm_series':
            uid=pacsio.getSharedSeriesHeaders(process.data_set.data_id)["0020,000e"]["Value"]
        elif datatype_name == 'dcm_instance':
            uid=pacsio.getInstanceHeaders(process.data_set.data_id)["0008,0018"]["Value"]
        else:
            raise ValueError('[manual_input] Unimplemented datatype: %s'%(datatype_name))

        # insert value from dbprocessinput table
        inputs=dbio.DBProcessInput.select().where(dbio.DBProcessInput.uid==uid,
                                                  dbio.DBProcessInput.selector_id==process.selector.id)

        if len(inputs)==0: logger.warning("No inputs present in the dbprocessinput table!")

        for input in inputs:
            logger.debug("manual input: datatype_name = {} | uid = {}".format(datatype_name,uid))
            logger.debug("manual input: input = {} | {} | {}".format(input.name, input.result_type, input.val))
            config['manual_input'][input.name]['val'] = input.val
 
    # convert dict to string
    config = json.dumps(config)

    ### Prepare working directory
    logger.info("Preparing task")
    datadir, config_json, result_json = prepareTask(tempdir, config)
    
    ### Retrieve data from PACS
    logger.info("Querying PACS db")
    pacsio.getData(process.data_set.data_id, process.data_set.data_type.name, datadir)

    ### Run the module
    logger.info("Processing module")
    cmdline = [module_path, '-d', datadir, '-c', config_json, '-r', result_json]
    status, message = runProcess(cmdline, tempdir, timeout)
    
    ### Handle the results
    process.process_log = message
    process.process_status = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == status)

    if status != "finished":
        # Error encountered when running the process
        info = "\nProcess ID: {}\nCommand: {}".format(process.id, " ".join(cmdline))
        process.process_log += info
        process.save()
        logger.error(process.process_log)
    else:
        # Process ran successfully
        logger.info("Submitting results")                  
        result = dbio.DBResults.finishedProcess(process)
        submitResults(dbio, result, result_json)
        process.delete_instance()        


def run(dbio, processid):

    process = None
    tempdir = None
    try:
        ### Initialize variables
        process = dbio.DBProcesses.get(dbio.DBProcesses.id == processid)
        timeout = float(dbio.DBVariables.get(dbio.DBVariables.name == 'processor_timeout').val)
        pacsio = PACSIO(process.data_set.data_source.as_dict())
        module = process.module_config.module
        wadqcroot = dbio.DBVariables.get(dbio.DBVariables.name == 'wadqcroot').val
        module_dir = dbio.DBVariables.get(dbio.DBVariables.name == 'modules_dir').val
        module_path = os.path.join(module_dir, module.foldername, module.filename)
        temp_root = dbio.DBVariables.get(dbio.DBVariables.name == 'temp_dir').val
        if not os.path.exists(temp_root):
            os.makedirs(temp_root)
        tempdir = tempfile.mkdtemp(dir=temp_root)

        ### Get data from PACS and run the module
        analyse(dbio, pacsio, tempdir, module_path, process, timeout)

    except Exception as e:
        ### Encountered unexpected error, not while running the module
        message = traceback.format_exc()
        logger.critical('Analyser failed')
        logger.critical(message)

        if process is not None:
            process.process_log = message
            process_status = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == 'analyser failed')
            process.process_status = process_status
            process.save()

    if tempdir is not None:
        try:
            logger.info("Cleaning up")
            shutil.rmtree(tempdir)
        except Exception as e:
            logger.critical('Cleanup failed')
            logger.critical(traceback.format_exc())
