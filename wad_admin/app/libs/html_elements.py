from flask import render_template, Markup, url_for, redirect

def Button(label, name=None, type="button", href=None, _class=None):
    return Markup(render_template("elements/button.html", name=name, label=label, type=type, _class=_class, href=href))

def Table(headers=[], rows=[], _class=None, _id=None):
    return Markup(render_template("elements/table.html", headers=headers, rows=rows, _class=_class, _id=_id))

def Link(label, href, target=None):
    return Markup(render_template("elements/link.html", label=label, href=href, target=target))
    
def Image(label, src, width=None):
    return Markup(render_template("elements/image.html", src=src, label=label, width=width))

def Div(label, style=None):
    return Markup(render_template("elements/div.html", label=label, style=style))

def Heading(label, type):
    return Markup(render_template("elements/heading.html", label=label, type=type))

def Picker(name="selector", idname=[], sid=None, fun=None, label=None):
    return Markup(render_template("elements/picker.html", name=name, idname=idname, sid=sid, fun=fun, label=label))

def Spinner(name, val, label=None, min=None, max=None, fun=None):
    return Markup(render_template("elements/spinner.html", name=name, val=val, label=label, min=min, max=max, fun=fun))

def Pagination(label, pages, base_url, prevlab, nextlab, index, num):
    if num >= len(pages):
        show = list(range(len(pages)))
    else:
        show = [index]
        while len(show)<min(num, len(pages)):
            if show[0]>0:
                show.insert(0, show[0]-1)
            if show[-1]< len(pages)-1:
                show.append(show[-1]+1)
    navitems = [ {'lab': prevlab, 'href': base_url+pages[max(0,index-1)], 'active': index>0} ]
    navitems.extend( [ {'lab': pages[i], 'href': base_url+pages[i], 'active': not i==index} for i in show ] )
    navitems.append({'lab': nextlab, 'href': base_url+pages[min(len(pages)-1, index+1)], 'active': index<len(pages)-1})
    return Markup(render_template("elements/pagination.html", label=label, items=navitems,
                                  info="Showing page {} of {}".format(pages[index], len(pages))))
