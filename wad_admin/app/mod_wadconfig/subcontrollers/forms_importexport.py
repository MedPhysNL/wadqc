# Import Form 
from flask_wtf import FlaskForm 
from flask import Markup
# Import Form elements such as StringField 
from wtforms import HiddenField, BooleanField, FieldList, FormField, FileField

# Import Form validators
from wtforms.validators import InputRequired, NoneOf, NumberRange

class ConfigEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    cfg_name = HiddenField()
    cfg_description = HiddenField()
    cfg_origin = HiddenField()
    cfg_selector = HiddenField()
    cfg_hasmeta = HiddenField()
    cid = HiddenField()
    cfg_selected = BooleanField('include in export')

class ExportModulesForm(FlaskForm):
    gid = HiddenField('gid', [])
    configs = FieldList(FormField(ConfigEntryForm))

class ImportForm(FlaskForm):
    gid = HiddenField('gid', [])
    file = FileField('File', [])  
    skip_selectors = BooleanField('do not import selectors')
    skip_configs   = BooleanField('do not import configs without selectors')
    skip_modules   = BooleanField('do not import modules (link configs to existing modules with same name)')

class SelectorEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    sel_name = HiddenField()
    sel_description = HiddenField()
    sel_isactive = HiddenField()
    sel_manual_input = HiddenField()
    cid = HiddenField()
    sel_selected = BooleanField('include in backup')

class ExportSelectorsForm(FlaskForm):
    gid = HiddenField('gid', [])
    selectors = FieldList(FormField(SelectorEntryForm))
