from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, session
try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements, dbmaintenance
    from app.libs.shared import dbio_connect, INIFILE, getLocalIpAddress, bytes_as_string, string_as_bytes
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import html_elements, dbmaintenance
    from wad_admin.app.libs.shared import dbio_connect, INIFILE, getLocalIpAddress, bytes_as_string, string_as_bytes

from wad_qc.connection.pacsio import PACSIO

dbio = dbio_connect()
from wad_core.selector import Selector

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# forms
from .forms_consistency import ConsistencyForm
from .forms_confirm import ConfirmForm
import json

mod_blueprint = Blueprint('wadconfig_trash', __name__, url_prefix='/wadadmin/trash')

@mod_blueprint.route('/', methods=['GET', 'POST'])
@login_required
def default():
    # display and allow handling of datasets marked in DBDataTrash table
    msg = "There are no DataSets in the trash."
    subtitle=[]

    stuff = dbio.DBDataTrash.select().join(dbio.DBDataSets).order_by(dbio.DBDataTrash.id.desc())

    # aet is only not empty for instances, so do not show
    header_row=[
            'Source', 'data_type', 'uid',
            'patient', 'modality', 'study desc', 'series desc', 
            'protocol', 'station', 'aet', 'study date', 'series date']

    table_rows = []

    missing_data = []
    for data in stuff:
        # 1. make sure a local pacs is running and that it has the required demo data
        try:
            pacsio = PACSIO(dbio.DBDataSources.get_by_name(data.data_set.data_source.name).as_dict())

        except Exception as e:
            msg = ' '.join(['Cannot access PACS {} with provided credentials'.format(data.data_set.data_source.name),str(e)])
            logger.error(msg)
            error = True
            return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg='',
                                   inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

        srcname = data.data_set.data_source.name
        dtypename = data.data_set.data_type.name
        datauid = data.data_set.data_id
        do_include, hdrs, _missing_data = dbmaintenance.source_select_data(pacsio, 
                                                            (srcname,  
                                                             dtypename, 
                                                             datauid), 
                                                            filters={}, return_headers=True, logger=logger)

        if len(_missing_data) > 0:
            missing_data.extend(_missing_data)


        # http://localhost:8042/app/explorer.html#series?uuid=3144fcf6-833a17ba-5454f959-d37e6d60-c2ac12ca
        if do_include:
            btnDel    = html_elements.Button(label='Delete from PACS', href=url_for('.delete_pacs', dataid=data.id), _class='btn btn-danger')
            btnResend = html_elements.Button(label='Resend', href=url_for('.resend', dataid=data.id), _class='btn btn-primary')

            table_rows.append([
                srcname,
                dtypename,
                html_elements.Link(data.data_set.data_id, url_for('wadconfig_dbtool.showtags', 
                                                                  gid=datauid,
                                                                  level=dtypename,
                                                                  source=srcname)),
            
                hdrs['pat_name'], hdrs['modality'], hdrs['study_desc'], hdrs['series_desc'],
                hdrs['prot_name'], hdrs['stat_name'], hdrs['aet'], hdrs['study_date'], hdrs['series_date'],
                btnResend, btnDel
            ])
            

    if len(missing_data) > 0:
        logger.warning("Could not find {} sets in Sources. Will attempt to delete.".format(len(missing_data)))
        _delete_missing_data(missing_data)

    table = html_elements.Table(headers=header_row, rows=table_rows,
                             _class='tablesorter-wadred', _id='sortTable')


    if len(table_rows) >0:
        msg = [
            "These {} datasets are in the Trash.".format(len(table_rows)),
            "Pressing \"Resend\" will treat the data as new incoming data for the WAD Selector.",
            "Pressing \"Delete\" will delete the data from the Source PACS.",
            "Use the filterboxes in the table and the buttons below the table to resend/delete a selection of DataSets.",
            'Clicking delete will ask for confirmation.'
        ]

    form = ConsistencyForm(None if request.method=="GET" else request.form)

    return render_template("wadconfig/consistency.html", title="Trash", subtitle='', msg='', html=Markup(table),
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg},
                           form=form, 
                           #add_refresh=add_refresh,
                           action_send=url_for('.multi_send'), action_delete=url_for('.multi_delete'),
                           label_send="Resend shown Data", label_delete="Delete shown Data")
        

@mod_blueprint.route('/resend')
@login_required
def resend():
    """
    delete from DBDataTrash. If not references left, delete from DBDataSets. Resend to WAD Selector.
    """
    gid = request.args.get('dataid', None)
    if gid is None:
        logger.error("Missing argument 'dataid'")
        return redirect(url_for('wadconfig.home'))

    try:
        data = dbio.DBDataTrash.get_by_id(gid)
    except dbio.DBDataTrash.DoesNotExist:
        logger.error("Invalid argument {} for 'dataid'".format(gid))
        return redirect(url_for('wadconfig.home'))        
    

    # delete from DBDataTrash
    dset = data.data_set
    srcname = dset.data_source.name
    dtypename = dset.data_type.name
    datauid = dset.data_id
    data.delete_instance(recursive=True)

    # if no references to the data_set exist, delete it
    if len(dset.results) == 0 and len(dset.notes) == 0 and len(dset.processes) == 0:
        dset.delete_instance(recursive=True)
    
    # resend function
    return redirect(url_for('wadconfig_dbtool.resend_selector', src=srcname, datatype=dtypename, dataid=datauid))
    


@mod_blueprint.route('/delete_pacs', methods=['GET', 'POST'])
@login_required
def delete_pacs():
    """
    delete from DBDataTrash. If not references left, delete from DBDataSets. Delete from PACS.
    """    
    gid = request.args.get('dataid', None)
    if gid is None:
        logger.error("Missing argument 'dataid'")
        return redirect(url_for('wadconfig.home'))

    try:
        data = dbio.DBDataTrash.get_by_id(gid)
    except dbio.DBDataTrash.DoesNotExist:
        logger.error("Invalid argument {} for 'dataid'".format(gid))
        return redirect(url_for('wadconfig.home'))        

    # valid
    dset = data.data_set
    srcname = dset.data_source.name
    dtypename = dset.data_type.name
    datauid = dset.data_id
   
    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete one {} from {}'.format(dtypename, srcname)
    msg = [
        'This will permanently delete the selected data set from the QC PACS.',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # delete from DBDataTrash
            data.delete_instance(recursive=True)
        
            # if no references to the data_set exist, delete it
            if len(dset.results) == 0 and len(dset.notes) == 0 and len(dset.processes) == 0:
                dset.delete_instance(recursive=True)

            # delete from PACS
            src_id = dbio.DBDataSources.get_by_name(srcname).id
            return _delete_datasets({src_id: {dtypename: [datauid]}})

    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.delete_pacs', dataid=gid),
                           title=formtitle, msg=msg)


@mod_blueprint.route('/multi_send', methods=['GET', 'POST'])
@login_required
def multi_send():
    """
    resend all data as shown in filtered consistency table 
    """
    
    wsel = Selector(INIFILE, logfile_only=True)

    num = 0
    datasets = json.loads(request.form.get('resendform-posttable'))

    for srcname, datatype, dataid in datasets:
        """
        delete from DBDataTrash. If not references left, delete from DBDataSets. Resend to WAD Selector.
        """
        srcname = srcname.strip()
        datatype = datatype.strip()
        dataid = dataid.strip()
        
        # delete DBDataTrash and DBDataSet
        dset = dbio.DBDataSets.get(dbio.DBDataSets.data_source==dbio.DBDataSources.get_by_name(srcname), 
                                   dbio.DBDataSets.data_type==dbio.DBDataTypes.get_by_name(datatype), 
                                   dbio.DBDataSets.data_id==dataid)
        for trashset in dset.datathrash:
            trashset.delete_instance(recursive=True)
        
        # if no references to the data_set exist, delete it
        if len(dset.results) == 0 and len(dset.notes) == 0 and len(dset.processes) == 0:
            dset.delete_instance(recursive=True)
        
        num += 1
        try:
            wsel.run(srcname, dataid, dryrun=False, datalevel=datatype, include_inactive=False)
        except ValueError as e:
            msg = "Error in selector: {}".format(e)
            logger.error(msg)
            return render_template("wadconfig/generic.html", title='Selector Error', subtitle='', msg='',
                                   inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

    msg = 'Did send {} datasets to WAD Selector'.format(num)
    return render_template("wadconfig/generic.html", title='Send from Trash', subtitle='', msg='',
                           inpanel={'type': "panel-success", 'title': "Success", 'content':msg})


@mod_blueprint.route('/multi_delete', methods=['GET', 'POST'])
@login_required
def multi_delete():
    """
    delete all data as shown in filtered consistency table 
    """
    skipvalidate = False
    if 'deleteform-posttable'in request.form:
        datasets = json.loads(request.form.get('deleteform-posttable'))
        skipvalidate = True # skip validate trigger on arrival
    else:
        extra = request.form.get('extradata')
        if extra is None or len(extra) == 0:
            return redirect(url_for('.default'))
        else:
            datasets = json.loads(extra)
            
    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete {} data sets from PACS'.format(len(datasets))
    msg = [
        'This will permanently delete the selected data sets from the QC PACS.',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)
    form.extradata.data = json.dumps(datasets)

    # Verify the sign in form
    valid = True
    if not skipvalidate and form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            # make a list per source, per datatype
            smsg = []
            stuff = {}

            for srcname, datatype, dataid in datasets:
                srcname = srcname.strip()
                if not srcname in stuff.keys():
                    stuff[srcname] = {}
                datatype = datatype.strip()
                if not datatype in stuff[srcname].keys():
                    stuff[srcname][datatype] = []
                dataid = dataid.strip()
                stuff[srcname][datatype].append(dataid)

                # delete DBDataTrash and DBDataSet
                dset = dbio.DBDataSets.get(dbio.DBDataSets.data_source==dbio.DBDataSources.get_by_name(srcname), 
                                           dbio.DBDataSets.data_type==dbio.DBDataTypes.get_by_name(datatype), 
                                           dbio.DBDataSets.data_id==dataid)
                for trashset in dset.datathrash:
                    trashset.delete_instance(recursive=True)
                
                # if no references to the data_set exist, delete it
                if len(dset.results) == 0 and len(dset.notes) == 0 and len(dset.processes) == 0:
                    dset.delete_instance(recursive=True)
                

            for srcname,val in stuff.items():
                src_id = dbio.DBDataSources.get_by_name(srcname).id
                num = 0
                for key,va in val.items():
                    num += len(va)
                    
                error, emsg = dbmaintenance.sources_delete_datasets(dbio, {src_id: val}, logger)
                if error:
                    return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg='',
                                           inpanel={'type': "panel-danger", 'title': "ERROR", 'content':emsg})
            
                if emsg=="":
                    smsg.append('Deleted {} datasets from source "{}"'.format(num, srcname))
                else:
                    smsg.append('{}\nDeleted {} datasets from source "{}"'.format(emsg, num, srcname))
                    
            if smsg == []:
                smsg.append('Deleted {} datasets'.format(0))

                    
            return render_template("wadconfig/generic.html", title='Delete from Sources', subtitle='', msg='', html="",
                                    inpanel={'type': "panel-success", 'title': "Success", 'content':smsg})


    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.multi_delete'),
                           title=formtitle, msg=msg)




def _delete_datasets(data):
    """
    data[source_id] = {dtype: [data_id]}
    Permanently delete list of files
    """
    error, msg = dbmaintenance.sources_delete_datasets(dbio, data, logger)
    if error:
        return render_template("wadconfig/generic.html", title='PACS Access', subtitle='', msg='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg})

    else:
        return render_template("wadconfig/generic.html", title='Delete from Sources', subtitle='', msg='', html="",
                                   inpanel={'type': "panel-success", 'title': "Success", 'content':msg})

def _delete_missing_data(datasets):
    """
    for whatever reason, sometimes datasets marked as trash are already deleted from PACS
    Remove them, if no links exist anymore
    """
    links = 0
    for srcname, datatype, dataid in datasets:
        srcname = srcname.strip()
        datatype = datatype.strip()
        dataid = dataid.strip()

        # delete DBDataTrash and DBDataSet
        dset = dbio.DBDataSets.get(dbio.DBDataSets.data_source==dbio.DBDataSources.get_by_name(srcname), 
                                   dbio.DBDataSets.data_type==dbio.DBDataTypes.get_by_name(datatype), 
                                   dbio.DBDataSets.data_id==dataid)

        for trashset in dset.datathrash:
            trashset.delete_instance(recursive=True)

        # if no references to the data_set exist, delete it
        if len(dset.results) == 0 and len(dset.notes) == 0 and len(dset.processes) == 0:
            dset.delete_instance(recursive=True)
        else:
            links +=1

    logger.warning("Attempted to delete {} datasets: {} skipped because of links.".format(len(datasets), links))