from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, send_file, session
try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs.shared import dbio_connect, INIFILE, getLocalIpAddress, bytes_as_string, string_as_bytes
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import html_elements
    from wad_admin.app.libs.shared import dbio_connect, INIFILE, getLocalIpAddress, bytes_as_string, string_as_bytes

import io

dbio = dbio_connect()
from wad_core.selector import Selector

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# forms
from .forms_consistency import ConsistencyForm
from .forms_confirm import ConfirmForm
import json
from math import ceil

mod_blueprint = Blueprint('wadconfig_results', __name__, url_prefix='/wadadmin/results')

@mod_blueprint.route('/', methods=['GET', 'POST'])
@login_required
def default():
    # display and allow handling of processes DBResults table
    add_refresh = int(request.args.get('refresh', session.get('refresh')))
    msg = "There are no Results in the WAD-QC database."

    numentries = [('page size {}'.format(n), n) for n in [10, 50, 100, 200, 500, 1000] ]
    numentries.append( ('page size (all)', 0) ) # display all
    try:
        pagesize = max(0, int(request.args['pagesize']))
    except:
        pagesize = numentries[1][1]

    try:
        pageno = int(request.args.get('pageno', 1)) 
    except:
        pageno = 1

    total_results = dbio.DBResults.select().count()
    stuff = dbio.DBResults.select().join(dbio.DBDataSets).order_by(dbio.DBResults.id.desc())

    # make a pager: num results per page; pagenum; prev next
    pmax = 1 if pagesize == 0  else max(1, ceil(1.*stuff.count()/pagesize))
    pageno = min(pmax, max(pageno, 1))
    # pagination
    navitems = [ str(i+1) for i in list(range(pmax))]
    psnav = html_elements.Pagination(label="navigate pages of inspector", 
                                     base_url = url_for('.default', pagesize=pagesize)+"&pageno=",
                                     prevlab='Previous', nextlab='Next',
                                     pages = navitems,
                                     index = pageno-1,
                                     num = 9)
    if pagesize>0: 
        stuff = stuff.paginate(pageno, pagesize)
            
    table_rows = []
    pacs_url = {}
    url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}

    for data in stuff:
        if not data.data_set.data_source.name in pacs_url:
            pacs_url[data.data_set.data_source.name] = None
            if data.data_set.data_source.source_type.name == 'orthanc': # we know how to construct that url
                # determine the outside address of this server
                ip = getLocalIpAddress(data.data_set.data_source.host, request.base_url)
                pacs_url[data.data_set.data_source.name] = '%s://%s:%s/app/explorer.html'%(data.data_set.data_source.protocol,
                                                                                  ip, 
                                                                                  data.data_set.data_source.port,
                                                                                  )
        if pacs_url[data.data_set.data_source.name] is None:
            data_id = data.data_set.data_id
        else:
            url = '%s#%s?uuid=%s'%(pacs_url[data.data_set.data_source.name], url_part[data.data_set.data_type.name], data.data_set.data_id)
            data_id = html_elements.Link(label=data.data_set.data_id, href=url)

        table_rows.append([data.id, data.selector.name,
                           data.module_config.module.name, data.module_config.name,
                           data.data_set.data_type.name, data.data_set.data_source.name, data_id,
                           html_elements.Link(label=len(data.data_set.notes), href=url_for('wadconfig_datatags.manage_note', gid=data.data_set.id)),
                           html_elements.Link(label=len(data.getResults()), href=url_for('.showresults', rid=data.id)),
                           data.created_time.strftime('%Y-%m-%d %H:%M:%S'),
                           html_elements.Button(label='show log', href=url_for('.showlog', gid=data.id)),
                           html_elements.Button(label='resend', href=url_for('.redo', gid=data.id)),
                           html_elements.Button(label='delete', href=url_for('.delete', gid=data.id)),
                           ])
    date_hdr = Markup('created_at'+20*'&nbsp;')
    table = html_elements.Table(headers=['id', 'selector', 'module', 'config', 'datatype', 'source', 'data_id (open in PACS)', '#tags (modify)',
                                         '#results (show)', date_hdr], rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')


    subtitle='Showing latest %d out of %d successful analyses in the database.'%(len(table_rows), total_results)
    
    # add a limit_number picker
    # make a dropdown for limit_number
    idname = [ [tn[1], tn[0]] for tn in numentries ]
    picker = html_elements.Picker(name="maxlimit", idname=idname, sid=pagesize,
                                  fun=Markup('location.href = "?pagesize="+jQuery(this).val();'))

    
    page = table
    if len(table_rows) >0:
        msg = [
            subtitle,
            "Pressing \"resend\" will delete the Result, "
            "and offer the same data again to the same Selector.",
            "Pressing \"delete\" only deletes the Result, not the DataSet, unless there are no other references (Notes, Processes) left. The PACS data will not be deleted.",
            "Use the filterboxes in the table and the buttons below the table to resend/delete a selection of Results."
        ]

        page = picker+psnav+table+psnav

    form = ConsistencyForm(None if request.method=="GET" else request.form)

    return render_template("wadconfig/consistency.html", title="Results", subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg},
                           form=form, add_refresh=add_refresh, uncheck_refresh=False,
                           action_send=url_for('.multi_send'), action_delete=url_for('.multi_delete'),
                           label_send="Resend shown Results", label_delete="Delete shown Results")

@mod_blueprint.route('/log')
@login_required
def showlog():
    """
    show log of process if available
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if _gid is None:
        return redirect(url_for('.default'))

    try:
        msg = bytes_as_string(dbio.DBResults.get_by_id(_gid).process_log)
    except: # maybe the log does not exist, or this process just finished
        msg = ''

    # go back to overview page
    return render_template("wadconfig/generic.html", title='Results log', msg=msg)

@mod_blueprint.route('/delete')
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    valid = True
    # invalid table request are to be ignored
    if _gid is None:
        valid = False
    if valid:
        try:
            result = dbio.DBResults.get_by_id(_gid)
            if result is None:
                valid = False
        except dbio.DBResults.DoesNotExist:
            valid = False

    if not valid:
        logger.error("Need a valid Result id")
        # go back to overview page
        return redirect(url_for('.default'))

    result = dbio.DBResults.get_by_id(_gid)
    data_set = result.data_set
    result.delete_instance(recursive=True)
    # if no references to the data_set exist, delete it
    if len(data_set.results) == 0 and len(data_set.notes) == 0 and len(data_set.processes) == 0:
        data_set.delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('.default'))

@mod_blueprint.route('/redo')
@login_required
def redo():
    """
    delete this result and offer same data to same selector. 
    changes to the module or the selector (config) might now result in different results.
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    valid = True
    # invalid table request are to be ignored
    if _gid is None:
        valid = False
    if valid:
        try:
            result = dbio.DBResults.get_by_id(_gid)
            if result is None:
                valid = False
        except dbio.DBResults.DoesNotExist:
            valid = False

    if not valid:
        logger.error("Need a valid Result id")
        # go back to overview page
        return redirect(url_for('.default'))

    # valid process. 
    #  get data params and selector. delete process. offer data as new to same selector.
    sel_name    = result.selector.name
    source_name = result.data_set.data_source.name
    data_id     = result.data_set.data_id
    datatype_name = result.data_set.data_type.name

    data_set = result.data_set
    result.delete_instance(recursive=True)
    # if no references to the data_set exist, delete it
    if len(data_set.results) == 0 and len(data_set.notes) == 0 and len(data_set.processes) == 0:
        data_set.delete_instance(recursive=True)

    wsel = Selector(INIFILE, logfile_only=True)
    wsel.run(source_name, data_id, datalevel=datatype_name, selectornames=[sel_name])

    # go back to overview page
    return redirect(url_for('.default'))

# Set the route and accepted methods
@mod_blueprint.route('/show', methods=['GET', 'POST'])
@login_required
def showresults():
    # show a table with all details of a result
    _rid = int(request.args['rid']) if 'rid' in request.args else None
    if _rid is None:
        return(redirect(url_for('wadconfig.home')))
    
    result   = dbio.DBResults.get_by_id(_rid)
    selector = dbio.DBSelectors.get_by_id(result.selector.id)

    msg =''
    subtitle = '%s: %s'%(selector.name, selector.description) # name + description of selector
    stuff = result.getResults()
    
    table_rows = []
    tableObj_rows = []

    constraint_labels = ['val_equal','val_period', 'val_ref', 'val_min', 'val_low','val_high', 'val_max']
    for data in sorted(stuff, key=lambda x: x.name):
        if isinstance(data, dbio.DBResultObjects):
            datatype='object'
            row = [data.name,
                   html_elements.Link(label=html_elements.Image(label=data.name, src=url_for('.getobject',did=data.id), width=120), 
                                   href=url_for('.showobject',did=data.id), target="_blank")
                   ]
            tableObj_rows.append(row)
            continue
        elif isinstance(data, dbio.DBResultStrings):
            datatype='string'
        elif isinstance(data, dbio.DBResultBools):
            datatype='bool'
        elif isinstance(data, dbio.DBResultDateTimes):
            datatype='datetime'
        elif isinstance(data, dbio.DBResultFloats):
            datatype='float'

        # make sure all displayed elements are (empty) strings
        constraints = {f: getattr(data, 'val_equal', None) for f in  constraint_labels }
        for k,v in constraints.items():
            if v is None:
                constraints[k] = ''
            else:
                constraints[k] = str(v)

        row = [data.name,
               str(getSingleStatus(data)), #data.val,
            ]
        row.extend([ constraints[f] for f in constraint_labels ])

        table_rows.append(row)
        
    table = html_elements.Table(headers=['name', 'value', 'stat_equals', 'stat_period', 'stat_ref', 'stat_min', 'stat_low', 'stat_high', 'stat_max'],
                                rows=table_rows, _class='tablesorter-wadred', _id='sortTable')
    page = table
    if len(tableObj_rows) > 0 :
        tableObj = html_elements.Table(headers=['name', 'thumbnail'], rows=tableObj_rows,
                                       _class='tablesorter-wadred')  

        page += tableObj
    
    return render_template('wadconfig/generic.html', title='WAD-QC Results', subtitle=subtitle, msg=msg, html=Markup(page))

@mod_blueprint.route('/getobject')
@login_required
def getobject():
    """Serves the image."""
    _did = int(request.args['did']) if 'did' in request.args else None
    if _did is None:
        return redirect(url_for('.showresults'))

    res = dbio.DBResultObjects.get_by_id(_did)
    
    return send_file(io.BytesIO(res.val),
                     download_name='%s.%s'%(res.name, res.filetype),
                     mimetype='image/%s'%res.filetype)

@mod_blueprint.route('/object')
@login_required
def showobject():
    _did = int(request.args['did']) if 'did' in request.args else None
    if _did is None:
        return redirect(url_for('.showresults'))

    msg =''

    res = dbio.DBResultObjects.get_by_id(_did)
    selector = dbio.DBSelectors.get_by_id(res.result.selector.id)
    img = html_elements.Image(label=res.name, src=url_for('.getobject',did=_did))
    
    subtitle = '%s: %s'%(selector.name, selector.description) # name + description of selector
    dt = res.result.created_time

    page = img
    
    return render_template('wadconfig/generic.html', title='%s: %s'%(res.name,dt), subtitle=subtitle, msg=msg, html=Markup(page))

def getSingleStatus(result):
    # determine if all results are within limits
    status = None
    
    """
    if status is None:
        if not getattr(result, 'val_period', None) is None:
            status = 'ok'
    """        
    # check equals
    if status is None:
        if not getattr(result, 'val_equal', None) is None:
            status = 'ok'
            if not result.val == result.val_equal:
                status = 'critical'

    # check floats min_low_high_max
    if status is None:
        for lim in ['val_min', 'val_low', 'val_high', 'val_max']:
            if not getattr(result, lim, None) is None:
                status = 'ok'
                break;
            
        if not getattr(result, 'val_low', None) is None:
            if result.val < result.val_low:
                status = 'warning'

        if not getattr(result, 'val_high', None) is None:
            if result.val > result.val_high:
                status = 'warning'


        if not getattr(result, 'val_min', None) is None:
            if result.val < result.val_min:
                status = 'critical'

        if not getattr(result, 'val_max', None) is None:
            if result.val > result.val_max:
                status = 'critical'

    if status == 'ok':
        status_color = 'yellowgreen'
    elif status == 'warning':
        status_color = 'yellow'
    elif status == 'critical':
        status_color = 'red'
    else:
        return result.val
    
    return {'value': result.val, 'style':{"bgcolor":status_color}}

@mod_blueprint.route('/multi_send', methods=['GET', 'POST'])
@login_required
def multi_send():
    """
    resend all results as shown in filtered results table 
    """
    wsel = Selector(INIFILE, logfile_only=True)

    num = 0
    num_error = 0
    datasets = json.loads(request.form.get('resendform-posttable'))

    for dat in datasets:
        valid = True
        try:
            result = dbio.DBResults.get_by_id(dat[0])
            if result is None:
                valid = False
        except dbio.DBResults.DoesNotExist:
            valid = False

        if not valid:
            logger.error("Result {} does not exist".format(dat[0]))
            num_error += 1       
            continue

        if valid:
            # valid result. 
            #  get data params and selector. delete result. offer data as new to same selector.
            try:
                sel_name    = result.selector.name
                source_name = result.data_set.data_source.name
                data_id     = result.data_set.data_id
                datatype_name = result.data_set.data_type.name

                data_set = result.data_set
                result.delete_instance(recursive=True)
                # if no references to the data_set exist, delete it
                if len(data_set.results) == 0 and len(data_set.notes) == 0 and len(data_set.processes) == 0:
                    data_set.delete_instance(recursive=True)
    
                wsel.run(source_name, data_id, datalevel=datatype_name, selectornames=[sel_name])
                num += 1
            except Exception as e:
                logger.error("Problem resending Result {}: {}".format(dat[0], str(e)))
                num_error += 1

    msg = 'Did resend {}/{} Results to linked Selector'.format(num, num+num_error)
    if num_error == 0:
        inpanel={'type': "panel-success", 'title': "Success", 'content':msg}
    elif num >0:
        inpanel={'type': "panel-warning", 'title': "WARNING", 'content':msg}
    else:
        inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg}

    return render_template("wadconfig/generic.html", title='Resend Results', subtitle='', msg='',
                           inpanel=inpanel)

@mod_blueprint.route('/multi_delete', methods=['GET', 'POST'])
@login_required
def multi_delete():
    """
    delete all results as shown in filtered results table 
    """
    skipvalidate = False
    if 'deleteform-posttable'in request.form:
        datasets = json.loads(request.form.get('deleteform-posttable'))
        skipvalidate = True # skip validate trigger on arrival
    else:
        extra = request.form.get('extradata')
        if extra is None or len(extra) == 0:
            return redirect(url_for('.default'))
        else:
            datasets = json.loads(extra)
            
    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete {} Results'.format(len(datasets))
    msg = [
        'This will delete the selected Results (data sets in the QC PACS are not affected).',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)
    form.extradata.data = json.dumps(datasets)

    # Verify the sign in form
    valid = True
    if not skipvalidate and form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            num = 0
            num_error = 0
            for dat in datasets:
                error = False
                try:
                    result = dbio.DBResults.get_by_id(dat[0])
                    if result is None:
                        error = True
                except dbio.DBResults.DoesNotExist:
                    error = True
        
                if error:
                    logger.error("Process {} does not exist".format(dat[0]))
                    num_error += 1       
                    continue
    
                if not error:
                    try:
                        data_set = result.data_set
                        result.delete_instance(recursive=True)
                        # if no references to the data_set exist, delete it
                        if len(data_set.results) == 0 and len(data_set.notes) == 0 and len(data_set.processes) == 0:
                            data_set.delete_instance(recursive=True)
                        num += 1
                    except Exception as e:
                        logger.error("Problem deleting Result {}: {}".format(dat[0], str(e)))
                        num_error += 1
                    
            msg = 'Did delete {}/{} Results'.format(num, num+num_error)
            if num_error == 0:
                inpanel={'type': "panel-success", 'title': "Success", 'content':msg}
            elif num >0:
                inpanel={'type': "panel-warning", 'title': "WARNING", 'content':msg}
            else:
                inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg}
                
            return render_template("wadconfig/generic.html", title='Delete Results', subtitle='', msg='', html="",
                                    inpanel=inpanel)


    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.multi_delete'),
                           title=formtitle, msg=msg)
