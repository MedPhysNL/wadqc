# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as StringField and BooleanField (optional)
from wtforms import HiddenField, StringField

# Import Form validators
from wtforms.validators import InputRequired, NoneOf

class ProxyForm(FlaskForm):
    proxyserver = StringField('proxy-server')
