from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash
import os
import peewee
try:
    from app.mod_auth.controllers import login_required
    from app.libs.shared import dbio_connect
    from app.libs import html_elements
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs.shared import dbio_connect
    from wad_admin.app.libs import html_elements

dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_datastatustags import ModifyForm

mod_blueprint = Blueprint('wadconfig_datastatustags', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/datastatustags/', methods=['GET', 'POST'])
@login_required
def default():
    # invalid table request are to be ignored
    formtitle = 'Tags to connect to DataSets'
    subtitle = 'The status tags defined below can be used to assign Tags to DataSets. Click Submit after making changes below.'
    stuff = dbio.DBDataStatus.select().order_by(dbio.DBDataStatus.id)
    table_rows = []
    for data in stuff:
        table_rows.append([data.id, data.name, 
                           len(data.notes),
                           html_elements.Button(label='delete', href=url_for('.delete', gid=data.id), _class='btn btn-danger'),
                           html_elements.Button(label='edit', href=url_for('.modify', gid=data.id))
                           ])
    
    table = html_elements.Table(headers=['id', 'name', '#datasets'],
                                rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')
    newbutton = html_elements.Button(label='New', href=url_for('.modify'))
    page = table+newbutton
    
    return render_template("wadconfig/generic.html", title='DataStatus Tags', msg='', subtitle='', 
                           html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':subtitle})



@mod_blueprint.route('/datastatustags/delete/')
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if not _gid is None:
        dbio.DBDataStatus.get_by_id(_gid).delete_instance(recursive=True)

    # go back to overview page
    return redirect(url_for('.default'))


# Set the route and accepted methods
@mod_blueprint.route('/datastatustags/modify', methods=['GET', 'POST'])
@login_required
def modify():
    """
    Show a form with all details of a status
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    subtitle = None
        
    # invalid table request are to be ignored
    formtitle = 'Modify DataStatus Tag'
    form = ModifyForm(None if request.method=="GET" else request.form)
    if not _gid is None:
        status = dbio.DBDataStatus.get_by_id(_gid)
        form.gid.data = _gid
        form.name.data = status.name

    newentry = False
    if form.gid is None or form.gid.data == '' or form.gid.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        newentry = True
        formtitle = 'New DataStatus Tag'

    existingnames = [ s.name for s in dbio.DBDataStatus.select() ]
    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if newentry:
            if form.name.data in existingnames:
                flash('Tag name already exists!', 'error')
                valid = False
        else:
            status = dbio.DBDataStatus.get_by_id(int(form.gid.data))
            if not status.name == form.name.data and form.name.data in existingnames:
                flash('Tag name already exists!', 'error')
                valid = False
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}

            if newentry:
                # need to manually add a proper id
                dbio.DBDataStatus.create(name=field_dict['name'], id=dbio.DBDataStatus.select(peewee.fn.MAX(dbio.DBDataStatus.id)).scalar()+1)
            else:
                status = dbio.DBDataStatus.get_by_id(field_dict['gid'])
                status.name = field_dict['name']
                status.save()

            return redirect(url_for('.default'))
            
    return render_template("wadconfig/datastatustags_modify.html", form=form, action='modify', 
                           title=formtitle, subtitle=subtitle, msg='Fill out the fields and click Submit')
    
