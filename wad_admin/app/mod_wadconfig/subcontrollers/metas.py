from flask import Blueprint, render_template, Markup, url_for, redirect, request, send_file, flash
import os
try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs.shared import dbio_connect, upload_file, bytes_as_string, string_as_bytes
    from app.libs import configmaintenance
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import html_elements
    from wad_admin.app.libs.shared import dbio_connect, upload_file, bytes_as_string, string_as_bytes
    from wad_admin.app.libs import configmaintenance
dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_metas import ModifyForm
from .forms_confirm import ConfirmForm
from .forms_consistency import ConsistencyForm

import json
from io import BytesIO

mod_blueprint = Blueprint('wadconfig_metas', __name__, url_prefix='/wadadmin/metas')

@mod_blueprint.route('/', methods=['GET', 'POST'])
@login_required
def default():
    # display and allow editing of modules table
    subtitle = [
        'Deleting a meta_config sets off a cascade: It also deletes the module_config '
        'that references that meta_config, and all other entries that reference that module_config, and so on. ',
        'A meta_config coupled to a Selector cannot be deleted.',
        'A meta_config connected to a virtual Module cannot be modified, and can only be deleted if it is not a factory Meta.',
        "Use the filterboxes in the table and the button below the table to delete a selection of Metas."
    ]

    """
    Loop over all module_configs, find the meta reference and check if config coupled to selector
    """

    table_rows = []
    stuff = dbio.DBMetaConfigs.select().order_by(dbio.DBMetaConfigs.id)
    for data in stuff:
        selname = ''
        modname = ''

        allow_modify = True
        allow_delete = True
        if len(data.module_configs) >0:
            # not allowed to delete factory meta of virtual module
            if data.module_configs[0].origin == "factory" and data.module_configs[0].module.origin == "virtual":
                allow_delete = False

            modname = data.module_configs[0].name
            # skip virtual module stuff
            #if data.module_configs[0].module.origin == "virtual":
            #    continue

            if len(data.module_configs[0].selectors) >0:
                selname = data.module_configs[0].selectors[0].name
                allow_delete = False

            # not allowed to modify virtual meta
            if  data.module_configs[0].module.origin == "virtual" == "virtual":
                allow_modify = False
        
        table_rows.append([data.id, html_elements.Link(label="meta.json", href=url_for('editor.default', mid=data.id)), 
                           modname, selname,
                           '' if not allow_delete else html_elements.Button(label='delete', href=url_for('.delete', metaid=data.id)),
                           '' if not allow_modify else html_elements.Button(label='replace',href=url_for('.modify', metaid=data.id)),
                           ])

    table = html_elements.Table(headers=['id', 'view/edit', 'coupled_module_config', 'coupled_selector'], 
                                rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')
    
    form = ConsistencyForm(None if request.method=="GET" else request.form)

    return render_template("wadconfig/consistency.html", title="Metas", subtitle='', msg='', html=Markup(table),
                           form=form, inpanel={'type': "panel-warning", 'title': "WARNING", 'content':subtitle},
                           action_delete=url_for('.multi_delete'), label_delete="Delete shown Metas")

@mod_blueprint.route('/delete', methods=['GET', 'POST'])
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _metaid = int(request.args['metaid']) if 'metaid' in request.args else None

    # invalid table request are to be ignored
    if _metaid is None:
        logger.error("Delete meta called with no valid meta id")
        return redirect(url_for('.default'))
    
    try:
        meta = dbio.DBMetaConfigs.get_by_id(_metaid)
        if not meta:
            logger.error("Delete meta called with no valid meta id")
            return redirect(url_for('.default'))
    except dbio.DBMetaConfigs.DoesNotExist:
        logger.error("Delete meta called with no valid meta id")
        return redirect(url_for('.default'))

        
    # refuse to delete meta coupled to Selector
    if len(meta.module_configs) >0 and len(meta.module_configs[0].selectors)>0:
        sel = meta.module_configs[0].selectors[0]
        page = 'Meta {} is coupled to Config which is coupled to Selector "{}". Deleting this Meta would also delete that Selector ' \
            'and all its Results. If you do want to do that, delete Selector "{}". Else, couple the Selector "{}" '\
            'to a different Config, and then delete this Meta.' \
            ''.format(meta.id, sel.name, sel.name, sel.name)
        return render_template("wadconfig/generic.html", title='Delete Meta',  subtitle='', msg='', html='',
                           inpanel={'type': "panel-danger", 'title': "ERROR", 'content':page})

    # refuse to delete factory meta coupled to Virtual Module
    if len(meta.module_configs) >0:
        if meta.module_configs[0].origin == "factory" and meta.module_configs[0].module.origin == "virtual":
            page = [
                'Meta {} is the factory Meta coupled to virtual Module "{}". Deleting this Meta would '
                'cripple that Module.'.format(meta.id, meta.module_configs[0].module.name),
            ]
            return render_template("wadconfig/generic.html", title='Delete Meta',  subtitle='', msg='', html='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':page})

    # ask for confirmation if coupled to a config
    if len(meta.module_configs)>0:
        inuse = 0
        for cfg in meta.module_configs:
            inuse += len(cfg.processes) + len(cfg.results)
        # invalid table request are to be ignored
        formtitle = 'Confirm action: delete meta with {} coupled configs'.format(len(meta.module_configs))
        msg = [
            'This will also delete those {} configs and the coupled {} processes and results.'.format(len(meta.module_configs), inuse),
            'Tick confirm and click Submit to proceed.'
        ]
        form = ConfirmForm(None if request.method=="GET" else request.form)
    
        # Verify the sign in form
        valid = True
        if form.validate_on_submit():
            # check if this is a new module
            if form.confirm.data is False:
                flash('Must tick confirm!', 'error')
                valid = False
    
            if valid:
                # do stuff
                meta.delete_instance(recursive=True)
                # go back to overview page
                return redirect(url_for('.default'))
    
        return render_template("wadconfig/confirm.html", form=form, 
                               action=url_for('.delete', metaid=_metaid),
                               title=formtitle, msg=msg)


    else:
        meta.delete_instance(recursive=True)
        # go back to overview page
        return redirect(url_for('.default'))

@mod_blueprint.route('/download')
@login_required
def download():
    """
    download given id of given table from iqc db
    """
    _metaid = int(request.args['metaid']) if 'metaid' in request.args else None
    
    # invalid table request are to be ignored
    if _metaid is None:
        return redirect(url_for('.default'))

    row = dbio.DBMetaConfigs.get_by_id(_metaid)
    # find the elements of the config.json to delete
    filename = 'meta.json'
    blob = row.val #str(base64.b64decode(row['val']))
    
    # make a bit more pretty
    blob = json.loads(bytes_as_string(blob))
    blob = string_as_bytes(json.dumps(blob, sort_keys=True, indent=4))

    return send_file(BytesIO(blob), as_attachment=True, download_name=filename)#mimetype=None

@mod_blueprint.route('/modify', methods=['GET', 'POST'])
@login_required
def modify():
    _metaid = int(request.args['metaid']) if 'metaid' in request.args else None

    # invalid request are to be ignored
    if _metaid is None:
        return redirect(url_for('.default'))

    try:
        meta = dbio.DBMetaConfigs.get_by_id(_metaid)
    except dbio.DBMetaConfigs.DoesNotExist:
        logger.error("Must supply a valid meta id")
        # go back to overview page
        return redirect(url_for('wadconfig_metas.default')) # catch wild call

    if meta is None:
        logger.error("Must supply a valid meta id")
        # go back to overview page
        return redirect(url_for('wadconfig_metas.default')) # catch wild call

    # refuse to modify meta coupled to Virtual Module
    meta = dbio.DBMetaConfigs.get_by_id(_metaid)
    if len(meta.module_configs) > 0:
        if meta.module_configs[0].module.origin == "virtual":
            page = [
                'Meta {} is coupled to virtual Module "{}". '
                'No modifications allowed.'.format(meta.id, meta.module_configs[0].module.name),
            ]
            return render_template("wadconfig/generic.html", title='Modify Meta',  subtitle='', msg='', html='',
                               inpanel={'type': "panel-danger", 'title': "ERROR", 'content':page})

    # now we have a valid id
    formtitle = 'Replace meta config'
    form = ModifyForm(None if request.method=="GET" else request.form)

    if len(meta.module_configs) > 0:
        formtitle += " \"{}\"".format(meta.module_configs[0].name)

    # Verify the form
    valid = True
    if form.validate_on_submit():
        blob = None
        if 'metafile' in request.files and len(request.files['metafile'].filename) > 0:
            outname = upload_file(request.files['metafile'])
            try:
                with open(outname,'r') as fcfg: blob = fcfg.read()
            except Exception as e:
                flash('{} is not a valid meta.json'.format(request.files['metafile'].filename), 'error')
                valid = False
                msg = ""
            os.remove(outname)
        else:
            flash('no meta.json selected', 'error')
            valid = False
            msg = ""
            
        if valid:
            valid, msg = configmaintenance.validate_json('metafile', blob)
            if not valid:
                flash('{} {}'.format(request.files['metafile'].filename, msg), 'error')

        if valid:
            meta = dbio.DBMetaConfigs.get_by_id(_metaid)
                
            if not meta is None:
                meta.val = blob
            meta.save()

            return redirect(url_for('.default'))
            
    return render_template("wadconfig/metas_modify.html", form=form, action=url_for('.modify', metaid=_metaid),
                           title=formtitle, msg='Fill out the fields and click Submit')
        

@mod_blueprint.route('/multi_delete', methods=['GET', 'POST'])
@login_required
def multi_delete():
    """
    delete all metas as shown in filtered metas table 
    """
    skipvalidate = False
    if 'deleteform-posttable'in request.form:
        datasets = json.loads(request.form.get('deleteform-posttable'))
        skipvalidate = True # skip validate trigger on arrival
    else:
        extra = request.form.get('extradata')
        if extra is None or len(extra) == 0:
            return redirect(url_for('.default'))
        else:
            datasets = json.loads(extra)
            
    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete {} Metas'.format(len(datasets))
    msg = [
        'This will delete the selected Metas and all coupled Configs, Processes and Results. ',
        'Metas coupled to a Selector or to factory Configs of virtual Modules will NOT be deleted.',
        'Tick confirm and click Submit to proceed.'
    ]

    form = ConfirmForm(None if request.method=="GET" else request.form)
    form.extradata.data = json.dumps(datasets)

    # Verify the sign in form
    valid = True
    if not skipvalidate and form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            num = 0
            num_error = 0
            num_skip = 0
            for dat in datasets:
                error = False
                skip = False
                try:
                    meta = dbio.DBMetaConfigs.get_by_id(dat[0])
                    if not meta:
                        error = True
                except dbio.DBMetaConfigs.DoesNotExist:
                    error = True
                    
                if error:
                    logger.error("Meta {} does not exist".format(dat[0]))
                    num_error += 1       
                    continue
        
                # refuse to delete meta coupled to Selector
                if len(meta.module_configs) >0 and len(meta.module_configs[0].selectors)>0:
                    num_skip += 1
                    skip = True
                    continue

                # refuse to delete meta coupled to factory virtual module
                if len(meta.module_configs) >0:
                    if meta.module_configs[0].origin == "factory" and meta.module_configs[0].module.origin == "virtual":
                        num_skip += 1
                        skip = True
                        continue

                if not error and not skip:
                    try:
                        meta.delete_instance(recursive=True)
                        num += 1
                    except Exception as e:
                        logger.error("Problem deleting Meta {}: {}".format(dat[0], str(e)))
                        num_error += 1
                    
            msg = ['Did delete {}/{} Metas.'.format(num, num+num_error+num_skip)]
            if num_skip>0:
                msg.append('Skipped {} Metas that were coupled to Selectors or to factory Configs of virtual Modules.'.format(num_skip))

            if num_error == 0:
                inpanel={'type': "panel-success", 'title': "Success", 'content':msg}
            elif num >0:
                inpanel={'type': "panel-warning", 'title': "WARNING", 'content':msg}
            else:
                inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg}
                
            return render_template("wadconfig/generic.html", title='Delete Metas', subtitle='', msg='', html="",
                                   inpanel=inpanel)


    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.multi_delete'),
                           title=formtitle, msg=msg)
