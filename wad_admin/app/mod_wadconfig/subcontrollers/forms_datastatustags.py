# Import Form 
from flask_wtf import FlaskForm 

# Import Form elements such as StringField 
from wtforms import HiddenField, StringField

# Import Form validators
from wtforms.validators import InputRequired, NoneOf


class ModifyForm(FlaskForm):
    gid = HiddenField('gid', [])
    name = StringField('name', [InputRequired(message='Name cannot be empty!'), NoneOf(['None'], message='Name cannot be None!')])
