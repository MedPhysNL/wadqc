from flask import Blueprint, render_template, url_for, redirect, request, send_file, send_from_directory, flash
import os
import tempfile
from io import BytesIO
try:
    from app.mod_auth.controllers import login_required
    from app.libs.shared import dbio_connect, upload_file
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs.shared import dbio_connect, upload_file

dbio = dbio_connect()
from wad_qc.connection.exchange import export_wadqc, import_wadqc

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_importexport import ExportModulesForm, ImportForm, ExportSelectorsForm

mod_blueprint = Blueprint('wadconfig_importexport', __name__, url_prefix='/wadadmin')

@mod_blueprint.route('/export/modules', methods=['GET', 'POST'])
@login_required
def export_modules():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Export modules and configurations'
    configs = getConfigs()
    form = ExportModulesForm(None if request.method=="GET" else request.form, configs=configs)
    if form.gid.data is None:
        if _gid is None:
            return redirect(url_for('wadconfig.home')) # catch wild call
        form.gid.data = 1

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            result = doExportModules(field_dict)
            if result is None:
                valid = False
            else:
                return result
            
    return render_template("wadconfig/export_modules.html", form=form, action=url_for('.export_modules'),
                           title=formtitle, msg='Select configs to export (modules will be added automatically) and click Export.')

def getConfigs(selectors_only=False):
    """
    Return a list of configs suitable for export
    """
    stuff = dbio.DBModuleConfigs.select().order_by(dbio.DBModuleConfigs.id)
    
    results = []
    for cfg in stuff:
        if selectors_only:
            if len(cfg.selectors) == 0:
                continue
        else:
            # skip virtual module stuff
            if cfg.module.origin == "virtual":
                continue
                
        res = {'cfg_name':cfg.name, 
                'cfg_description':cfg.description,
                'cfg_origin':cfg.origin,
                'cfg_selector': cfg.selectors[0].name if len(cfg.selectors)>0 else '',
                'cfg_hasmeta': not cfg.meta is None,
                'cid':cfg.id}

        # exclude configs coupled to results, but not to a current selector (meaning replaced)
        if res['cfg_origin'] == 'result' and res['cfg_selector'] == '':
            continue
        else:
            results.append(res)
    return results
    
def doExportModules(field_dict):
    """
    Pack all selected configs and connected modules with a manifest.ini file and send
    Export:
      manifest.json
      modules/module_1/
              module_2/
      configs/config_1.json
             /config_2.json
    """
    cidsuffix = '-cid'
    selsuffix = '-cfg_selected'
    cfg_ids = []
    for key, entry in field_dict.items():
        if key.endswith(selsuffix): # it's either marked and present or not present
            cidkey = key[:(len(key)-len(selsuffix))]+cidsuffix
            cfg_ids.append(int(field_dict[cidkey]))
    
    # make the Backup location
    modules_dir = dbio.DBVariables.get_by_name('modules_dir').val
    temp_dir = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)

    # make a temporary file; either stream it of write it to disk
    with tempfile.SpooledTemporaryFile(dir=temp_dir) as tmpf:
        error, msg = export_wadqc(dbio, tmpf, cfg_ids=cfg_ids, sel_ids=[], logger=logger)

        if not error:
            # Reset file pointer
            tmpf.seek(0)
    
            return send_file(BytesIO(tmpf.read()), as_attachment=True, download_name='export.zip')#mimetype=None

    logger.error(msg)
    return None


@mod_blueprint.route('/export/selectors', methods=['GET', 'POST'])
@login_required
def export_selectors():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Export selectors with module and configuration'
    selectors = getSelectors()
    form = ExportSelectorsForm(request.form, selectors=selectors)
    if form.gid.data is None:
        if _gid is None:
            return redirect(url_for('wadconfig.home')) # catch wild call
        form.gid.data = 1

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}
            result = doExportSelectors(field_dict)
            if result is None:
                valid = False
            else:
                return result
            
    return render_template("wadconfig/export_selectors.html", form=form, action=url_for('.export_selectors'),
                           title=formtitle, msg='Select selectors to export (modules and configs will be added automatically) and click Export.')

@mod_blueprint.route('/import', methods=['GET', 'POST'])
@login_required
def importing():
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    formtitle = 'Import selectors/modules/configs'
    form = ImportForm(None if request.method=="GET" else request.form)
    if form.gid.data is None:
        if _gid is None:
            return redirect(url_for('wadconfig.home')) # catch wild call
        form.gid.data = 1

    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if not 'file' in request.files or len(request.files['file'].filename) == 0:
            flash('No valid zip file chosen.', 'error')
            valid = False
        if valid:
            inname = upload_file(request.files['file'])
            skip_modules = form.skip_modules.data
            skip_configs = form.skip_configs.data
            skip_selectors = form.skip_selectors.data
            imp_valid, msg = doImport(inname, skip_selectors, skip_configs, skip_modules)
            logger.info(msg)
            if  imp_valid:
                msg = 'Imported {}: {} Note that all imported Selectors are set to not active.'.format(request.files['file'].filename, msg)
                logger.info(msg)
                return render_template("wadconfig/generic.html", title='Import', subtitle='', msg='',
                                       inpanel={'type': "panel-success", 'title': "Success", 'content':msg})
            else:
                flash(msg, 'error')

    msg = [
        'Importing selectors recreates selectors with configs and modules. It does not couple existing results or processes to imported selectors. ',
        'Select package with exported selectors and/or configs and modules to import and click Import.'
    ]
    return render_template("wadconfig/import.html", form=form, action=url_for('.importing'),
                           title=formtitle, msg=msg)


def doImport(inname, skip_selectors, skip_configs, skip_modules):
    """
    return success or not.
    check if zip
    unpack zip
      check if manifest.ini
      check consistency of manifest
      check consistency of manifest with folders modules and configs; check if exe file present in folder
      check uniqueness of new module names (if not, suggest user renames current modules, refuse restore)
      check uniqueness of new selector names (if not, suggest user renames current selector, refuse restore)
        create each module (zip for upload?!)
        create each config coupled to correct module
        create each selector, coupled to correct config, with rules
    """
    valid, msg = import_wadqc(dbio, inname, skip_selectors, skip_configs, skip_modules, logger)

    # remove zip
    os.remove(inname)
    
    return valid, msg

def getSelectors(active_only=False):
    stuff = dbio.DBSelectors.select().order_by(dbio.DBSelectors.id)
    
    results = []
    for sel in stuff:
        if active_only:
            if not sel.isactive:
                continue
        results.append(
            {'sel_name':sel.name, 
             'sel_description':sel.description,
             'sel_isactive': sel.isactive,
             'sel_manual_input': sel.manual_input,
             'cid':sel.id})
    return results
    
def doExportSelectors(field_dict):
    """
    Pack all selected selectors and connected configs and modulea with a manifest.ini file and send
    Export:
      manifest.json
      selectors/selector_1.json
               /selector_2.json
      modules/module_1/
              module_2/
      configs/config_1.json
             /config_2.json
    """
    cidsuffix = '-cid'
    selsuffix = '-sel_selected'
    
    # make a list of all selected selectors
    sel_ids = []
    for key, entry in field_dict.items():
        if key.endswith(selsuffix): # it's either marked and present or not present
            cidkey = key[:(len(key)-len(selsuffix))]+cidsuffix
            sel_ids.append(int(field_dict[cidkey]))
    
    # make the Backup location
    modules_dir = dbio.DBVariables.get_by_name('modules_dir').val
    temp_dir = dbio.DBVariables.get_by_name('temp_dir').val
    if not os.path.exists(temp_dir):
        os.makedirs(temp_dir)

    # make a temporary file; either stream it of write it to disk
    with tempfile.SpooledTemporaryFile(dir=temp_dir) as tmpf:
        error, msg = export_wadqc(dbio, tmpf, cfg_ids=[], sel_ids=sel_ids, logger=logger)
        
        if not error:
            # Reset file pointer
            tmpf.seek(0)
    
            return send_file(BytesIO(tmpf.read()), as_attachment=True, download_name='backup.zip')#mimetype=None

    logger.error(msg)
    return None
