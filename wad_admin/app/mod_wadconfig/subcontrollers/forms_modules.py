# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as StringField and BooleanField (optional)
from wtforms import StringField, FileField, HiddenField # BooleanField

# Import Form validators
from wtforms.validators import InputRequired, NoneOf

class ModifyForm(FlaskForm):
    name = StringField('name', [InputRequired(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    description = StringField('description', [])
    executable = StringField('executable', [InputRequired(message='Executable cannot be empty!')])
    file = FileField('File', [ ])  
    currentname = HiddenField('gid', [])
    gid = HiddenField('gid', [])
    
