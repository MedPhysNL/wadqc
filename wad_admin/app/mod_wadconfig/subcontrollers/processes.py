from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash, session
try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs.shared import dbio_connect, INIFILE, getLocalIpAddress, bytes_as_string, string_as_bytes
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs import html_elements
    from wad_admin.app.libs.shared import dbio_connect, INIFILE, getLocalIpAddress, bytes_as_string, string_as_bytes

from wad_qc.connection.pacsio import PACSIO

dbio = dbio_connect()
from wad_core.selector import Selector

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# forms
from .forms_consistency import ConsistencyForm
from .forms_confirm import ConfirmForm
import json

mod_blueprint = Blueprint('wadconfig_processes', __name__, url_prefix='/wadadmin/processes')

@mod_blueprint.route('/', methods=['GET', 'POST'])
@login_required
def default():
    # display and allow handling of processes DBProcesses table
    add_refresh = int(request.args.get('refresh', session.get('refresh')))
    msg = "There are no Processes in the queue."
    subtitle=[]
    stuff = dbio.DBProcesses.select().join(dbio.DBDataSets).order_by(dbio.DBProcesses.id.desc())

    table_rows = []
    pacs_url = {}
    url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}
    num_status = {}
    for data in stuff:
        if not data.process_status.name in num_status:
            num_status[data.process_status.name] = 0
        num_status[data.process_status.name] += 1

        if not data.data_set.data_source.name in pacs_url:
            pacs_url[data.data_set.data_source.name] = None
            if data.data_set.data_source.source_type.name == 'orthanc': # we know how to construct that url
                # determine the outside address of this server
                ip = getLocalIpAddress(data.data_set.data_source.host, request.base_url)
                pacs_url[data.data_set.data_source.name] = '%s://%s:%s/app/explorer.html'%(data.data_set.data_source.protocol,
                                                                                  ip, 
                                                                                  data.data_set.data_source.port,
                                                                                  )
        if pacs_url[data.data_set.data_source.name] is None:
            data_id = data.data_set.data_id
        else:
            url = '%s#%s?uuid=%s'%(pacs_url[data.data_set.data_source.name], url_part[data.module_config.data_type.name], data.data_set.data_id)
            data_id = html_elements.Link(label=data.data_set.data_id, href=url)

        # http://localhost:8042/app/explorer.html#series?uuid=3144fcf6-833a17ba-5454f959-d37e6d60-c2ac12ca
        table_rows.append([data.id, data.selector.name,
                           data.module_config.module.name, data.module_config.name,
                           data.data_set.data_type.name, data.data_set.data_source.name, data_id,
                           html_elements.Link(label=len(data.data_set.notes), href=url_for('wadconfig_datatags.manage_note', gid=data.data_set.id)),
                           data.process_status.name, data.created_time.strftime('%Y-%m-%d %H:%M:%S'),
                           html_elements.Button(label='show log', href=url_for('.showlog', gid=data.id)),
                           html_elements.Button(label='resend', href=url_for('.redo', gid=data.id)),
                           html_elements.Button(label='delete', href=url_for('.delete', gid=data.id)),
                           ])
    date_hdr = Markup('created_at'+20*'&nbsp;')
    table = html_elements.Table(headers=['id', 'selector', 'module', 'config', 'datatype', 'source', 'data_id', 
                                         '#tags (modify)', 'status', date_hdr], rows=table_rows,
                             _class='tablesorter-wadred', _id='sortTable')

    for key,val in num_status.items():
        subtitle.append('{}: {}'.format(key, val))

    if len(table_rows) >0:
        msg = [
            "Pressing \"resend\" will delete the Process, "
            "and offer the same data again to the same Selector.",
            "Pressing \"delete\" only deletes the Process, not the DataSet, unless there are no other references (Notes, Results) left. The PACS data will not be deleted.",
            "Use the filterboxes in the table and the buttons below the table to resend/delete a selection of Processes."
        ]

    form = ConsistencyForm(None if request.method=="GET" else request.form)

    return render_template("wadconfig/consistency.html", title="Processes", subtitle='', msg='', html=Markup(table),
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg},
                           form=form, add_refresh=add_refresh,
                           action_send=url_for('.multi_send'), action_delete=url_for('.multi_delete'),
                           label_send="Resend shown Processes", label_delete="Delete shown Processes")
        
@mod_blueprint.route('/processes/manualinput/')
@login_required
def manualinput():
    """
    load given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    valid = True
    # invalid table request are to be ignored
    if _gid is None:
        valid = False
    if valid:
        try:
            proc = dbio.DBProcesses.get_by_id(_gid)
            if proc is None:
                valid = False
        except dbio.DBProcesses.DoesNotExist:
            valid = False

    if not valid:
        logger.error("Need a valid Process id")
        # go back to overview page
        return redirect(url_for('.default'))

    # check if associated selector is manual_input selector
    if not proc.selector.manual_input:
        logger.error("Manual input disabled for associated selector")
        # go back to overview page
        return redirect(url_for('.default'))

    # init PACS connection
    pacsio = PACSIO(dbio.DBDataSources.get_by_id(proc.data_source).as_dict())

    # retrieve uid of dataset
    datatype_name = proc.module_config.data_type.name

    if datatype_name == 'dcm_study':
        uid=pacsio.getSharedStudyHeaders(proc.data_id)["0020,000d"]["Value"]
    elif datatype_name == 'dcm_series':
        uid=pacsio.getSharedSeriesHeaders(proc.data_id)["0020,000e"]["Value"]
    elif datatype_name == 'dcm_instance':
        uid=pacsio.getInstanceHeaders(proc.data_id)["0008,0018"]["Value"]
    else:
        raise ValueError('[processes.py] Unimplemented datatype: %s'%(datatype_name))

    # get list of inputs for selector/process from dbprocessinput table
    inputs=dbio.DBProcessInput.select().where(dbio.DBProcessInput.uid==uid,
                                              dbio.DBProcessInput.selector_id==proc.selector.id).order_by(dbio.DBProcessInput.id.desc())

    if len(inputs)==0: logger.warning("No inputs present in the dbprocessinput table!")

    for input in inputs:
        logger.debug("manual input: datatype_name = {} | uid = {}".format(datatype_name,uid))
        logger.debug("manual input: input = {} | {} | {}".format(input.name, input.result_type, input.val))

    # FIXME:
    #   - pass input data on to the template
    #   - modify template to generate the input form
    #   - on submit perform POST (current route or new route)
    #     * store values in dbprocessinput table
    #     * change status of process to "new"

    return render_template("wadconfig/manualinput.html", title='Provide manual input', subtitle='', msg='',
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg})

@mod_blueprint.route('/log')
@login_required
def showlog():
    """
    show log of process if available
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if _gid is None:
        return redirect(url_for('.default'))

    try:
        msg = bytes_as_string(dbio.DBProcesses.get_by_id(_gid).process_log)
    except: # maybe the log does not exist, or this process just finished
        msg = ''

    # go back to overview page
    return render_template("wadconfig/generic.html", title='Process log', msg=msg)

@mod_blueprint.route('/delete')
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    valid = True
    # invalid table request are to be ignored
    if _gid is None:
        valid = False
    if valid:
        try:
            proc = dbio.DBProcesses.get_by_id(_gid)
            if proc is None:
                valid = False
        except dbio.DBProcesses.DoesNotExist:
            valid = False

    if not valid:
        logger.error("Need a valid Process id")
        # go back to overview page
        return redirect(url_for('.default'))

    
    proc = dbio.DBProcesses.get_by_id(_gid)
    data_set = proc.data_set
    proc.delete_instance(recursive=True)
    # if no references to the data_set exist, delete it
    if len(data_set.results) == 0 and len(data_set.notes) == 0 and len(data_set.processes) == 0:
        data_set.delete_instance(recursive=True)
        
    # go back to overview page
    return redirect(url_for('.default'))

@mod_blueprint.route('/redo')
@login_required
def redo():
    """
    delete process and offer same data to same selector. 
    changes to the module or the selector (config) might now result in success.
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    valid = True
    # invalid table request are to be ignored
    if _gid is None:
        valid = False
    if valid:
        try:
            proc = dbio.DBProcesses.get_by_id(_gid)
            if proc is None:
                valid = False
        except dbio.DBProcesses.DoesNotExist:
            valid = False

    if not valid:
        logger.error("Need a valid Process id")
        # go back to overview page
        return redirect(url_for('.default'))
        
    # valid process. 
    #  get data params and selector. delete process. offer data as new to same selector.
    sel_name    = proc.selector.name
    source_name = proc.data_set.data_source.name
    data_id     = proc.data_set.data_id
    datatype_name = proc.data_set.data_type.name

    data_set = proc.data_set
    proc.delete_instance(recursive=True)
    # if no references to the data_set exist, delete it
    if len(data_set.results) == 0 and len(data_set.notes) == 0 and len(data_set.processes) == 0:
        data_set.delete_instance(recursive=True)

    wsel = Selector(INIFILE, logfile_only=True)
    try:
        wsel.run(source_name, data_id, datalevel=datatype_name, selectornames=[sel_name])
    except ValueError as e:
        logger.error("Error in selector: {}".format(e))
    
    # go back to overview page
    return redirect(url_for('.default'))


@mod_blueprint.route('/multi_send', methods=['GET', 'POST'])
@login_required
def multi_send():
    """
    resend all processes as shown in filtered processes table 
    """
    wsel = Selector(INIFILE, logfile_only=True)

    num = 0
    num_error = 0
    datasets = json.loads(request.form.get('resendform-posttable'))

    for dat in datasets:
        valid = True
        try:
            proc = dbio.DBProcesses.get_by_id(dat[0])
            if proc is None:
                valid = False
        except dbio.DBProcesses.DoesNotExist:
            valid = False

        if not valid:
            logger.error("Process {} does not exist".format(dat[0]))
            num_error += 1       
            continue

        if valid:
            # valid process. 
            #  get data params and selector. delete process. offer data as new to same selector.
            try:
                sel_name    = proc.selector.name
                source_name = proc.data_set.data_source.name
                data_id     = proc.data_set.data_id
                datatype_name = proc.data_set.data_type.name

                data_set = proc.data_set
                proc.delete_instance(recursive=True)
                # if no references to the data_set exist, delete it
                if len(data_set.results) == 0 and len(data_set.notes) == 0 and len(data_set.processes) == 0:
                    data_set.delete_instance(recursive=True)
    
                wsel.run(source_name, data_id, datalevel=datatype_name, selectornames=[sel_name])
                num += 1
            except Exception as e:
                logger.error("Problem resending Process {}: {}".format(dat[0], str(e)))
                num_error += 1

    msg = 'Did resend {}/{} Processes to linked Selector'.format(num, num+num_error)
    if num_error == 0:
        inpanel={'type': "panel-success", 'title': "Success", 'content':msg}
    elif num >0:
        inpanel={'type': "panel-warning", 'title': "WARNING", 'content':msg}
    else:
        inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg}

    return render_template("wadconfig/generic.html", title='Resend Processes', subtitle='', msg='',
                           inpanel=inpanel)

@mod_blueprint.route('/multi_delete', methods=['GET', 'POST'])
@login_required
def multi_delete():
    """
    delete all processes as shown in filtered processes table 
    """
    skipvalidate = False
    if 'deleteform-posttable'in request.form:
        datasets = json.loads(request.form.get('deleteform-posttable'))
        skipvalidate = True # skip validate trigger on arrival
    else:
        extra = request.form.get('extradata')
        if extra is None or len(extra) == 0:
            return redirect(url_for('.default'))
        else:
            datasets = json.loads(extra)
            
    # invalid table request are to be ignored
    formtitle = 'Confirm action: delete {} Processes'.format(len(datasets))
    msg = [
        'This will delete the selected Processes (data sets in the QC PACS are not affected).',
        'Tick confirm and click Submit to proceed.'
    ]
    form = ConfirmForm(None if request.method=="GET" else request.form)
    form.extradata.data = json.dumps(datasets)

    # Verify the sign in form
    valid = True
    if not skipvalidate and form.validate_on_submit():
        # check if this is a new module
        if form.confirm.data is False:
            flash('Must tick confirm!', 'error')
            valid = False

        if valid:
            # do stuff
            num = 0
            num_error = 0
            for dat in datasets:
                error = False
                try:
                    proc = dbio.DBProcesses.get_by_id(dat[0])
                    if proc is None:
                        error = True
                except dbio.DBProcesses.DoesNotExist:
                    error = True
        
                if error:
                    logger.error("Process {} does not exist".format(dat[0]))
                    num_error += 1       
                    continue
    
                if not error:
                    try:
                        data_set = proc.data_set
                        proc.delete_instance(recursive=True)
                        num += 1
                        # if no references to the data_set exist, delete it
                        if len(data_set.results) == 0 and len(data_set.notes) == 0 and len(data_set.processes) == 0:
                            data_set.delete_instance(recursive=True)
                    except Exception as e:
                        logger.error("Problem deleting Process {}: {}".format(dat[0], str(e)))
                        num_error += 1
                    
            msg = 'Did delete {}/{} Processes'.format(num, num+num_error)
            if num_error == 0:
                inpanel={'type': "panel-success", 'title': "Success", 'content':msg}
            elif num >0:
                inpanel={'type': "panel-warning", 'title': "WARNING", 'content':msg}
            else:
                inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg}
                
            return render_template("wadconfig/generic.html", title='Delete Processes', subtitle='', msg='', html="",
                                    inpanel=inpanel)


    return render_template("wadconfig/confirm.html", form=form, 
                           action=url_for('.multi_delete'),
                           title=formtitle, msg=msg)
