# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as StringField and BooleanField (optional)
from wtforms import StringField, SelectField, HiddenField

try:
    from app.libs.shared import dbio_connect
except ImportError:
    from wad_admin.app.libs.shared import dbio_connect
dbio = dbio_connect()

tag_choices = [(m.id, m.name) for m in dbio.DBDataTags.select().order_by(dbio.DBDataTags.id)]

class ModifyForm(FlaskForm):
    data_tag = SelectField("data_tag:", choices = tag_choices, coerce=int)
    description = StringField('description:')
    gid = HiddenField('gid', [])
    did = HiddenField('did', [])
    
