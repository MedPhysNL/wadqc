from flask import Blueprint, render_template, Markup, url_for, redirect, request, flash
import os
import peewee
try:
    from app.mod_auth.controllers import login_required
    from app.libs.shared import dbio_connect, getLocalIpAddress
    from app.libs import html_elements
except ImportError:
    from wad_admin.app.mod_auth.controllers import login_required
    from wad_admin.app.libs.shared import dbio_connect, getLocalIpAddress
    from wad_admin.app.libs import html_elements

dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_datatags import ModifyForm
from .forms_notes import ModifyForm as ModifyFormNote
from .forms_confirm import ConfirmForm

mod_blueprint = Blueprint('wadconfig_datatags', __name__, url_prefix='/wadadmin/datatags')

@mod_blueprint.route('/', methods=['GET', 'POST'])
@login_required
def default():
    # invalid table request are to be ignored
    msg = [
        'The tags defined below can be used to added to DataSets. Use the buttons to modify, '\
        'delete or create new tags. Deleting Tags that are connected to Notes will ask for confirmation, '\
        'because those Notes will be deleted too.',
        '#datasets: number of datasets that are tagged with this tag. Click to show the notes and datasets.'
    ]
    stuff = dbio.DBDataTags.select().order_by(dbio.DBDataTags.id)
    table_rows = []
    for data in stuff:
        table_rows.append([data.id, data.name, 
                           html_elements.Link(label=len(data.notes), href=url_for('.show', gid=data.id)),
                           html_elements.Button(label='delete', href=url_for('.delete', gid=data.id), _class='btn btn-danger'),
                           html_elements.Button(label='edit', href=url_for('.modify', gid=data.id))
                           ])
    
    table = html_elements.Table(headers=['id', 'name', '#datasets (show)'],
                                rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')
    newbutton = html_elements.Button(label='New', href=url_for('.modify'))
    page = table+newbutton
    
    return render_template("wadconfig/generic.html", title='DataTags', msg='', subtitle='', 
                           html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg})



@mod_blueprint.route('/delete', methods=['GET', 'POST'])
@login_required
def delete():
    """
    delete given id of given table from iqc db
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    if _gid is None:
        logger.error("Delete DataTag called with no valid tag id")
        return redirect(url_for('.default'))
    
    try:
        tag = dbio.DBDataTags.get_by_id(_gid)
        if not tag:
            logger.error("Delete DataTag called with no valid tag id")
            return redirect(url_for('.default'))
    except dbio.DBDataTags.DoesNotExist:
        logger.error("Delete DataTag called with no valid tag id")
        return redirect(url_for('.default'))


    # ask for confirmation if coupled to notes
    if len(tag.notes)>0:
        formtitle = 'Confirm action: delete DataTag with {} coupled Notes'.format(len(tag.notes))
        msg = [
            'This will also delete those Notes.',
            'Tick confirm and click Submit to proceed.'
        ]
        form = ConfirmForm(None if request.method=="GET" else request.form)
    
        # Verify the sign in form
        valid = True
        if form.validate_on_submit():
            # check if this is a new module
            if form.confirm.data is False:
                flash('Must tick confirm!', 'error')
                valid = False
    
            if valid:
                # do stuff
                tag.delete_instance(recursive=True)
                # go back to overview page
                return redirect(url_for('.default'))
    
        return render_template("wadconfig/confirm.html", form=form, 
                               action=url_for('.delete', gid=_gid),
                               title=formtitle, msg=msg)

    else:
        tag.delete_instance(recursive=True)
        # go back to overview page
        return redirect(url_for('.default'))


# Set the route and accepted methods
@mod_blueprint.route('/modify', methods=['GET', 'POST'])
@login_required
def modify():
    """
    Show a form with all details of a DataTag
    """
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    subtitle = None
        
    # invalid table request are to be ignored
    formtitle = 'Modify DataTag'
    form = ModifyForm(None if request.method=="GET" else request.form)
    if not _gid is None:
        tag = dbio.DBDataTags.get_by_id(_gid)
        form.gid.data = _gid
        form.name.data = tag.name

    newentry = False
    if form.gid is None or form.gid.data == '' or form.gid.data is None: #define here, to avoid wrong label on redisplaying a form with errors
        newentry = True
        formtitle = 'New DataTag'

    existingnames = [ s.name for s in dbio.DBDataTags.select() ]
    # Verify the sign in form
    valid = True
    if form.validate_on_submit():
        if newentry:
            if form.name.data in existingnames:
                flash('Tag name already exists!', 'error')
                valid = False
        else:
            tag = dbio.DBDataTags.get_by_id(int(form.gid.data))
            if not tag.name == form.name.data and form.name.data in existingnames:
                flash('Tag name already exists!', 'error')
                valid = False
        if valid:
            # the elements of the form
            field_dict = {k:v for k,v in request.form.items()}

            if newentry:
                # need to manually add a proper id
                dbio.DBDataTags.create(name=field_dict['name'], id=dbio.DBDataTags.select(peewee.fn.MAX(dbio.DBDataTags.id)).scalar()+1)
            else:
                tag = dbio.DBDataTags.get_by_id(field_dict['gid'])
                tag.name = field_dict['name']
                tag.save()

            return redirect(url_for('.default'))
            
    return render_template("wadconfig/datatags_modify.html", form=form, action='modify', 
                           title=formtitle, subtitle=subtitle, msg='Fill out the fields and click Submit')
    

@mod_blueprint.route('/show', methods=['GET', 'POST'])
@login_required
def show():
    # invalid table request are to be ignored
    _gid = int(request.args['gid']) if 'gid' in request.args else None

    # invalid table request are to be ignored
    error_tag_msg = "Show DataTag usage called with no valid tag id"
    if _gid is None:
        logger.error(error_tag_msg)
        return redirect(url_for('.default'))

    try:
        tag = dbio.DBDataTags.get_by_id(_gid)
        if not tag:
            logger.error(error_tag_msg)
            return redirect(url_for('.default'))
    except dbio.DBDataTags.DoesNotExist:
        logger.error(error_tag_msg)
        return redirect(url_for('.default'))

    msg = [
        'The DataSets below are tagged with this tag. ',
        'selectors: names of Selectors connected to that DataSet.',
        '#processes: number of Processes connected to that DataSet.',
        '#results: number of Results connected to that DataSet.',
    ]
    stuff = sorted(tag.notes, key=lambda x:x.id)
    if len(stuff) == 0:
        msg = "There are NO DataSets tagged with this tag."
        return render_template("wadconfig/generic.html", title='Usage of DataTag "{}"'.format(tag.name), msg='', subtitle='', 
                               inpanel={'type': "panel-info", 'title': "info", 'content':msg})
        
    table_rows = []
    pacs_url = {}
    url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}
    for note in stuff:
        # construct link to PACS view
        data_source = note.data_set.data_source
        if not data_source.name in pacs_url:
            pacs_url[data_source.name] = None
            if data_source.source_type.name == 'orthanc': # we know how to construct that url
                # determine the outside address of this server
                ip = getLocalIpAddress(data_source.host, request.base_url)
                pacs_url[data_source.name] = '%s://%s:%s/app/explorer.html'%(data_source.protocol,
                                                                             ip, 
                                                                             data_source.port,
                                                                             )
        if pacs_url[data_source.name] is None:
            data_id = note.data_set.data_id
        else:
            url = '%s#%s?uuid=%s'%(pacs_url[data_source.name], url_part[note.data_set.data_type.name], note.data_set.data_id)
            data_id = html_elements.Link(label=note.data_set.data_id, href=url)

        # build list of SelectorNames
        selectors = set()
        for proc in note.data_set.processes:
            selectors.add(proc.selector.name)
        for res in note.data_set.results:
            selectors.add(res.selector.name)
        selectors = "; ".join(sorted(list(selectors)))
        table_rows.append([note.id, note.description,
                           data_id,
                           selectors,
                           len(note.data_set.processes), len(note.data_set.results),
                           html_elements.Button('modify', href=url_for('wadconfig_datatags.manage_note', gid=note.data_set.id)),
                           ])
    
    table = html_elements.Table(headers=['id', 'note', 'data_id (show in PACS)', "selectors", '#processes', '#results'],
                                rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')
    page = table
    
    return render_template("wadconfig/generic.html", title='Usage of DataTag "{}"'.format(tag.name), msg='', subtitle='', 
                           html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg})



@mod_blueprint.route('/manage', methods=['GET', 'POST'])
@login_required
def manage_note():
    # display and allow handling of DataTags attached to data

    try:
        _gid = int(request.args['gid'])
        data = dbio.DBDataSets.get_by_id(_gid)
    except Exception as e:
        logger.error("Invalid DataSet id '{}' ({})".format(request.args.get('gid', None), e))
        
        # go back to overview page
        return(redirect(url_for('wadconfig.home')))

    title = "DataTags for {}".format(data.data_id)

    stuff = data.notes

    table_rows = []

    for note in stuff:
        table_rows.append([note.id, note.data_tag.name, note.description,
                           html_elements.Button(label='delete', href=url_for('.delete_note', gid=note.id)),
                           html_elements.Button(label='edit', href=url_for('.modify_note', gid=note.id)),
                           ])
    table_rows = sorted(table_rows, key=lambda x: x[0])
    table = html_elements.Table(headers=['id', 'tag', 'description'], 
                                rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')


    # add a direct link to open data in PACS
    url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}

    pacs_url = None
    if data.data_source.source_type.name == 'orthanc': # we know how to construct that url
        # determine the outside address of this server
        ip = getLocalIpAddress(data.data_source.host, request.base_url)
        pacs_url = '%s://%s:%s/app/explorer.html'%(data.data_source.protocol,
                                                   ip, 
                                                   data.data_source.port,
                                                   )
    if pacs_url is None:
        data_id = data.data_id
    else:
        url = '%s#%s?uuid=%s'%(pacs_url, url_part[data.data_type.name], data.data_id)
        data_id = html_elements.Link(label=data.data_id, href=url)
    
    # add tag
    newbutton = html_elements.Button(label='New', href=url_for('.modify_note', did=data.id))

    page = data_id + table + newbutton
    
    msg = "The following tags are attached to this dataset. Use the buttons to delete/edit/add tags."

    return render_template("wadconfig/generic.html", title=title, subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg})


@mod_blueprint.route('/delete_note', methods=['GET', 'POST'])
@login_required
def delete_note():
    """
    delete a note from the data
    """
    try:
        _gid = int(request.args['gid'])
        note = dbio.DBNotes.get_by_id(_gid)
    except Exception as e:
        logger.error("Invalid Note id '{}' ({})".format(request.args.get('gid', None), e))
        
        # go back to overview page
        return(redirect(url_for('wadconfig.home')))

    did = note.data_set.id
    note.delete_instance(recursive=True)

    return(redirect(url_for('.manage_note', gid=did)))


@mod_blueprint.route('/modify_note', methods=['GET', 'POST'])
@login_required
def modify_note():
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    _did = int(request.args['did']) if 'did' in request.args else None

    # invalid table request are to be ignored
    # for now only orthanc is supported
    formtitle = 'Modify Note'
    form = ModifyFormNote(None if request.method=="GET" else request.form)
    if not _gid is None:
        src = dbio.DBNotes.get_by_id(_gid)
        form.description.data = src.description
        form.data_tag.data = src.data_tag.id # dropdown
        form.gid.data = _gid
        form.did.data = _did
    elif not _did is None:
        form.did.data = _did
        
    if form.gid is None or form.gid.data in ['', None]: 
        #define here, to avoid wrong label on redisplaying a form with errors
        formtitle = 'New Note'

    # Verify the form
    error = True
    if form.validate_on_submit():
        # the elements of the form
        field_dict = {k:v for k,v in request.form.items()}
        igid = 0
        if 'gid' in field_dict:
            try:
                igid = int(field_dict['gid'])
            except:
                pass
            
        msg = []
        if igid>0: # update, not create
            src = dbio.DBNotes.get_by_id(igid)
            error = False
            src.description = field_dict['description']
            src.data_tag = field_dict['data_tag']
            src.save()
            msg.append('Changed Note {} of DataSet {}.'.format(src.id, src.data_set.id))
        else:
            error = False
            idid = 0
            if 'did' in field_dict:
                try:
                    idid = int(field_dict['did'])
                except:
                    pass
            data_set = dbio.DBDataSets.get_by_id(idid)
            field_dict['data_set'] = data_set
            dbio.DBNotes.create(**field_dict)
            msg.append('Added new Note to DataSet {}.'.format(data_set.id))

        if error:
            inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg}
        else:
            inpanel={'type': "panel-success", 'title': "Success", 'content':msg}
            
        return render_template("wadconfig/generic.html", title='Notes', subtitle='', msg='', html="",
                               inpanel=inpanel)


    msg = [
        'Fill out the fields and click Submit'
    ]
    return render_template("wadconfig/notes_modify.html", form=form,
                           action=url_for('.modify_note'),
                           title=formtitle, msg=msg,
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg})
        
