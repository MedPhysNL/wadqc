# Import Form 
from flask_wtf import FlaskForm

# Import Form elements such as StringField and BooleanField (optional)
from wtforms import StringField, PasswordField # BooleanField

# Import Form validators
from wtforms.validators import InputRequired, EqualTo


# Define the login form (WTForms)

class LoginForm(FlaskForm):
    username = StringField('Username', [
                InputRequired(message='Forgot your email address?')])
    password = PasswordField('Password', [
                InputRequired(message='Must provide a password. ;-)')])
    