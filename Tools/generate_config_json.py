#!/usr/bin/env python
from __future__ import print_function

"""
Changelog:
  20230907: adjusted OCR bboxes for generic ocr/Philips
  20230223: adjusted OCR bboxes for Mindray to new layout
  20220118: added DCoffset to MG_Hologic
  20220101: increased signal_thresh for MindRay
  20211117: added SR for DEXA Hologic Horizon
  20210614: added MindRay TE7
  20210608: changed linroi_avg2 value for CT7500 body
  20210420: added QuickIQ2
  20210321: change initial bbox for 17_5io
  20210202: added CiosAlpha
  20210127: added DXHough; added _setparam
  20201027: fixed init L15_7io; affirm border right 4
  20200728: updated boxes for L17_5, L15_7, eL18_4.
  20200722: added US AirReverberations2
  20200421: ocr_border for US/OCR
  20190611: Brilliance64 iPatient; added use_phantomrotation to Normi13
  20181105: removed bogus limits MR
  20180912: updated to cfgformat 20180910; bit more info for CT; changed ocr parameter definition
  20180616: errno no longer in os
  20180418: changed DRXSmall lowCNR to minimally 1.15; removed some warning limits for MR
  20180417: Changed skip top from 6 to 16 for Affirm
  20180329: OCR change channel "sum" to "avg"
  20180219: fixed XA siemens uniformity; changed basic name order: modality_phantom_machine
  20180209: Added extra ocr generic
  20180124: fixed DXU for XA; increased pixelsize for FD2020 Frontal to fix rotated box; increase border Frontal
  20180115: added eL18_4
  20171025: US philips hor_offset to 3, for kurtosis
  20170925: US param split between hdrs, ocr and qc; added units and images to MR
  20170905: added more OCR to US
  20170901: US philips ver_offset=2 (white line iu22); added rev_bbox to all philips L
  20170830: added action limits (meta) for CR, DR, DRX, RF; US philips hor_offset=2; added rev_bbox to US
  20170802: added parsCDCOM to CDMAM
  20170801: add DX and DXU for XA
  20170622: separate config for portable DR for DX
  20170621: added auto_suffix to RF/DDL; adjusted values for Force; adjusted values CT1
  20170616: added DRX-Revolution
  20170503: more params for US
  20170502: Split CT Force in 4 configs
  20170320: Added dummy config for yet to be implemented fBIRN ReceiveFieldTest
  20170310: Added Eleva Normi13 module
  20170227: Renamed MR stuff to FBIRN where appropriate
  20161221: Added params to US
  20161219: Added params for MG
  20161216: Added CT Siemens SOMATOM Force; CR Siemens Ysio
  20161110: meta results is now dict
  20161026: fBIRN
  20160901: OCR
  20160829: params for US
  20160822: new version and structure for DX Normi13; DX unif 5mm border
  20160622: split in config and meta
  20160620: add section results containing limits and display stuff
  20160610: add action 'acqdatetime' with limit period
  20160531: changed param back to key-value pair; moved param info to comments
  20160518: changed param value to {'value': value, 'info': info}
  20160415: added destination folder
  20160414: initial version based on generate_config_xml.py version 20160414
"""
__version__ = '20180912' # only change if really different; otherwise all configs are updated with only the date
__author__ = 'aschilham'

import json
import argparse
import os.path
import errno
import sys

DISPLAY_USER = 2
DISPLAY_KEYUSER = 1
DISPLAY_ADMIN = 0

CFGFORMAT  = "20180910"
METAFORMAT = "20180910"

def _addparaminfo(params, info, name, value, msg):
    """
    set param with info; if param already exists, ERROR
    """
    if name in params.keys():
        raise KeyError('[_addparaminfo] param key "{}" already exists'.format(name))

    params[name] = value

    if not info is None:
        if name in info.keys():
            raise KeyError('[_addparam] info key "{}" already exists'.format(name))

        if not msg is None:
            info[name] = msg

def _addparam(params, name, value):
    """
    set param; if param already exists, ERROR
    """
    _addparaminfo(params, None, name, value, None)

def _setparaminfo(params, info, name, value, msg):
    """
    set param with info; if param already exists, overwrite it
    """
    if not params is None and not value is None:
        params[name] = value

    if not info is None and not msg is None:
        info[name] = msg


def _setparam(params, name, value):
    """
    set param; if param not yet exists, create it
    """
    _addparaminfo(params, None, name, value, None)
    

def _addaction(config, name, params={}, filters={}, info={}):
    """
    add a new action to the config, and add the info block to the info
    """
    if not 'actions' in config.keys():
        config['actions'] = {}

    if not 'info' in config.keys():
        config['info'] = {}

    if name in config['actions'].keys():
        raise KeyError('[_addaction] action "{}" already exists'.format(name))

    config['actions'][name] = {
        'params': params,
        'filters': filters
    }

    config['info'][name] = {
        'params': info,
    }

def _addresult(meta, name, display_level=None, display_name=None, description=None, units=None, 
               equals=None, minlowhighmax=None, refminlowhighmax=None, valpaccpcrit=None, period=None):
    # all results names should be unique; multiple calls with the same name add to or overwrite the existing result

    if not "results" in meta.keys():
        meta["results"] = {}

    # find results entry to add to
    # (0, 'admin'), (1, 'key-user'), (2, 'user')
    if not name in meta["results"].keys():
        meta["results"][name] = {'display_level': 0}
    res = meta["results"][name]

    # display 
    if not display_level is None: res['display_level'] = display_level
    if not display_name is None: res['display_name'] = display_name
    if not description is None: res['description'] = description
    if not units is None: res['units'] = units
        
    # constraints
    if not equals is None: res['constraint_equals'] = equals
    if not minlowhighmax is None: res['constraint_minlowhighmax'] = list(minlowhighmax)
    if not refminlowhighmax is None: res['constraint_refminlowhighmax'] = list(refminlowhighmax)
    if not period is None: res['constraint_period'] = period
    if not valpaccpcrit is None: 
        cval, pacc, pcrit = valpaccpcrit
        res['constraint_minlowhighmax'] = [cval*(100.-pcrit)/100., cval*(100.-pacc)/100., cval*(100.+pacc)/100., cval*(100.+pcrit)/100.]

def _writeConfig(filename, config, meta):
    # internal function for writing
    folder, fname = os.path.split(filename)
    metafilename = os.path.join(folder,'meta',fname)

    with open(filename,'w') as f:
        json.dump(config,f,indent=4,sort_keys=True)
    print("written",filename)

    # copy general comments from config to meta
    meta["comments"] = {
        "author": config["comments"]["author"],
        "creator": config["comments"]["creator"],
        "description": config["comments"]["description"]
    }
    with open(metafilename,'w') as f:
        json.dump(meta,f,indent=4,sort_keys=True)
    print("written",metafilename)

def _addfilter(filters, filtdict):
    for name,value in filtdict.items():
        if name in filters:
            raise KeyError('[_addfilter] key "%s" already exists'%name)
        filters[name] = value

def writeXXConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "US/AirReverberations module for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}
    
    if m_id == 'epiq':
        comments['description'] += 'Philips Epiq'
        comments['version'] = '20160413'
        _addparam(hdr_params, 'dummy1', 'hdr dummy parameter 1')
        _addparam(hdr_params, 'dummy2', 'hdr dummy parameter 2')

        _addparam(qc_params, 'dummy1', 'qc dummy parameter 1')
        _addparam(qc_params, 'dummy2', 'qc dummy parameter 2')

        _addresult(meta, 'example string',
                   display_name='example string', display_level=2, description='example result string', 
                   units='', equals='WITHIN SPECS')
        _addresult(meta, 'example float',
                   display_name='example float', display_level=2, description='example result float', 
                   units='', minlowhighmax=[40., 50., 70., 80.])
        _addresult(meta, 'example float2',
                   display_name='example float2', display_level=2, description='example result float2', 
                   units='', valpaccpcrit=[1., 5., 10.])

    
    _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)
    
# Start building
#
#
def writeFBIRNQCConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/fBIRN_QC for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}
    
    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=7)# every 7 days

    if m_id == 'philips':
        comments['description'] += 'Philips'
        comments['version'] = '20161026'
        _addaction(config, 'header_series', params=hdr_params, info=hdr_info)
        _addaction(config, 'qc_series', params=qc_params, info=qc_info)

    _writeConfig(filename, config, meta)
    
def writeFBIRNB0Config(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/fBIRN_B0map for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=7)# every 7 days

    if m_id == 'philips':
        comments['description'] += 'Philips'
        comments['version'] = '20161026'
        _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
        _addparaminfo(qc_params, qc_info, 'circleradiusfactor', 0.85, 'factor to adjust circle radius to stay within phantom')
        _addaction(config, 'B0_series', params=qc_params, filters={}, info=qc_info)

    _writeConfig(filename, config, meta)
    
def writeFBIRNB1Config(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/fBIRN_B1map for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=7)# every 7 days

    if m_id == 'philips':
        comments['description'] += 'Philips'
        comments['version'] = '20161118'
        _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)

        _addparaminfo(qc_params, qc_info, 'circleradiusfactor', 0.75, 'factor to adjust circle radius to stay within phantom')
        _addaction(config, 'B1_series', params=qc_params, filters={}, info=qc_info)


    _writeConfig(filename, config, meta)

def writeFBIRNSNRConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/fBIRN_PureSNR for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=7)# every 7 days

    if m_id == 'philips':
        comments['description'] += 'Philips'
        comments['version'] = '20161026'
        _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)

        _addparaminfo(qc_params, qc_info, 'circleradiusfactor', 0.75, 'factor to adjust circle radius to stay within phantom')
        _addaction(config, 'snr_series', params=qc_params, filters={}, info=qc_info)


    _writeConfig(filename, config, meta)

def writeFBIRNReceiveConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/fBIRN_ReceiveField for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=7)# every 7 days

    if m_id == 'philips':
        comments['description'] += 'Philips (module not yet implemented)'
        comments['version'] = '20170320'

    _writeConfig(filename, config, meta)

def writeSRConfig(m_id, filename=None):
    """
    config for SR module for DEXA
    """
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "SR/DEXA for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}

    qc_params = {}
    qc_info = {}

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    # 
    for loc in ['L1', 'L1-L4', 'L2', 'L2-L4', 'L3', 'L4']:
        _addresult(meta, '{}_Area'.format(loc),
                   display_name='Area_{}'.format(loc), display_level=1, description='ROI area in {}'.format(loc), units='cm2')
        _addresult(meta, '{}_BMC'.format(loc),
                   display_name='BMC_{}'.format(loc), display_level=1, description='Bone Mineral Content in {}'.format(loc), units='g')
        _addresult(meta, '{}_BMD'.format(loc),
                   display_name='BMD_{}'.format(loc), display_level=1, description='Bone Mineral Density in {}'.format(loc), units='g/cm2')

    if m_id.startswith('Horizon_'):
        comments['description'] += 'Hologic Horizon'
        comments['version'] = '20211117'

        if m_id.endswith('_Summary'):
            _addparaminfo(qc_params, qc_info, 'section', 'Summary', 'extract only this section (Summary, History) [Summary]')
        elif m_id.endswith('_History'):
            _addparaminfo(qc_params, qc_info, 'section', 'History', 'extract only this section (Summary, History) [Summary]')

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})
    _addaction(config,'qc_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)

def writeOCRConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "OCR/Generic for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}
    qc_params['ocr_regions'] = {}
    qc_info['ocr_regions'] = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == 'epiq':
        comments['description'] += 'Philips Epiq'
        comments['version'] = '20230907'

        ocrname = 'TissueIndex'
        qc_params['ocr_regions'][ocrname] = {}
        qc_info['ocr_regions'][ocrname] = {}
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      'xywh', '822;24;90;23', 'x0;y0;width;height of bounding box')
        _addparaminfo(qc_params['ocr_regions']['TissueIndex'], qc_info['ocr_regions']['TissueIndex'], 
                      'prefix', 'TIS', 'text in bounding box starts with this')
        _addparaminfo(qc_params['ocr_regions']['TissueIndex'], qc_info['ocr_regions']['TissueIndex'], 
                      'type', 'float', 'contents (minus prefix) of bounding box should be of this type')

        ocrname = 'MechIndex'
        qc_params['ocr_regions'][ocrname] = {}
        qc_info['ocr_regions'][ocrname] = {}
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      'xywh', '913;24;80;23', 'x0;y0;width;height of bounding box')
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      'prefix', 'MI', 'text in bounding box starts with this')
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents (minus prefix) of bounding box should be of this type')
        _addaction(config,'qc_series', params=qc_params, filters={}, info=qc_info)


    if m_id == "Intevo_LEHR": # make a config with all params
        comments['description'] += 'Intevo_LEHR'
        comments['version'] = '20180209'

        # for unif only
        _addparaminfo(qc_params, qc_info, "channel", "avg", "which channel to take for multi-channel data; either an integer or 'avg', or 'rgb' (avg)") 
        _addparaminfo(qc_params, qc_info, "slicenr", -1, "which slice to analyse if enhanced dicom is used (-1)")

        ocrname = 'Detector'
        qc_params['ocr_regions'][ocrname] = {}
        qc_info['ocr_regions'][ocrname] = {}
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      "type", "float", "result should be interpreted as variable of this type")
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      "xywh", "25;120;100;25", "x0;y0;width;height of bounding box")
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      "prefix", "Detector", "text in box starts with this and this should be stripped")

        ocrname = 'Integral'
        qc_params['ocr_regions'][ocrname] = {}
        qc_info['ocr_regions'][ocrname] = {}
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      "type", "float", "result should be interpreted as variable of this type")
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      "xywh", "882;294;100;25", "x0;y0;width;height of bounding box")
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      "suffix", "%", "text in box ends with this and this should be stripped")

        ocrname = 'Label'
        qc_params['ocr_regions'][ocrname] = {}
        qc_info['ocr_regions'][ocrname] = {}
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      "type", "string", "result should be interpreted as variable of this type")
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      "xywh", "64;416;200;25", "x0;y0;width;height of bounding box")

        _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)

    if m_id == "ALOKAG8":
        comments['description'] += 'ALOKA G8'
        comments['version'] = '20180328'

        _addparaminfo(qc_params, qc_info, 'ocr_threshold', 80, 'threshold on pixel values to remove background gradient (0)')
        _addparaminfo(qc_params, qc_info, 'ocr_zoom', 2, 'zoom factor before ocr analysis (10)')
        _addparaminfo(qc_params, qc_info, 'channel', 2, 'convert to RGB by using channel number 0,1,2 or "avg", or "rgb" (2)')

        ocrname = 'probeID'
        qc_params['ocr_regions'][ocrname] = {}
        qc_info['ocr_regions'][ocrname] = {}
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      'xywh', '676;167;34;15', 'bounding box of probeID if specified, this will be used as identifier for values, else from dicom headers')
        _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                      'type', 'string', 'contents of bounding box of OCR_probeID should be of this type')
        _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)

    if m_id == "Horizon":
        comments['description'] += 'Hologic Horizon'
        comments['version'] = '20210531'

        _addparaminfo(qc_params, qc_info, 'ocr_zoom', 3, 'zoom factor before ocr analysis (10)')
        _addparaminfo(qc_params, qc_info, 'channel', 0, 'convert to RGB by using channel number 0,1,2 or "avg", or "rgb" (2)')

        labval = [
            ("L1",    "485;710;67;22", "566;710;67;22", "664;710;67;22"),
            ("L2",    "485;743;67;22", "566;743;67;22", "664;743;67;22"),
            ("L3",    "485;776;67;22", "566;776;67;22", "664;776;67;22"),
            ("L4",    "485;809;67;22", "566;809;67;22", "664;809;67;22"),
            ("Total", "485;842;67;22", "566;842;67;22", "664;842;67;22"),
            
        ]
        for suf,area,bmc,bmd in labval:
            for lab,xywh in [('Area', area), ('BMC', bmc), ('BMD', bmd)]:
                ocrname = "{}_{}".format(lab, suf)
                qc_params['ocr_regions'][ocrname] = {}
                qc_info['ocr_regions'][ocrname] = {}
                _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                              'xywh', xywh, 'bounding box of {}'.format(ocrname))
                _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                              'type', 'float', 'contents of bounding box of {} should be of this type'.format(ocrname))

        _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)

    if m_id == "HorizonHistory":
        comments['description'] += 'Hologic Horizon (with history)'
        comments['version'] = '20211019'

        _addparaminfo(qc_params, qc_info, 'ocr_zoom', 3, 'zoom factor before ocr analysis (10)')
        _addparaminfo(qc_params, qc_info, 'channel', 0, 'convert to RGB by using channel number 0,1,2 or "avg", or "rgb" (2)')

        labval = [
            ("L1",    "566;710;67;22", "664;710;67;22", "743;710;67;22"),
            ("L2",    "566;743;67;22", "664;743;67;22", "743;743;67;22"),
            ("L3",    "566;776;67;22", "664;776;67;22", "743;776;67;22"),
            ("L4",    "566;809;67;22", "664;809;67;22", "743;809;67;22"),
            ("Total", "566;842;67;22", "664;842;67;22", "743;842;67;22"),
            
        ]
        for suf,area,bmc,bmd in labval:
            for lab,xywh in [('Area', area), ('BMC', bmc), ('BMD', bmd)]:
                ocrname = "{}_{}".format(lab, suf)
                qc_params['ocr_regions'][ocrname] = {}
                qc_info['ocr_regions'][ocrname] = {}
                _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                              'xywh', xywh, 'bounding box of {}'.format(ocrname))
                _addparaminfo(qc_params['ocr_regions'][ocrname], qc_info['ocr_regions'][ocrname],
                              'type', 'float', 'contents of bounding box of {} should be of this type'.format(ocrname))

        _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)

    if not 'usage' in comments:
        comments['usage'] = []
    comments['usage'].extend([
        '  "OCR_xxx:prefix": "mm" # text in bounding box of OCR_xxx starts with this and this should be stripped',
        '  "OCR_xxx:suffix": "mm" # text in bounding box of OCR_xxx ends with this and this should be stripped',
        '\n'
    ])

    _writeConfig(filename, config, meta)

    
def writeUSConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "US/AirReverberations for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}
    ocr_params = {}
    ocr_info = {}
    ocr_params['ocr_regions'] = {}
    ocr_info['ocr_regions'] = {}
    
    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    # Shared Philips
    _addparaminfo(qc_params, qc_info, 'uni_filter', 5,   'running average of this width before peak detection')
    _addparaminfo(qc_params, qc_info, 'uni_delta',  0.05, 'a dip in normalized reverb pattern must be at least <delta>')
    _addparaminfo(qc_params, qc_info, 'uni_depth',  5, 'depth in mm of size of ROI for reverb pattern analysis from top of profile. (5)')
    _addparaminfo(qc_params, qc_info, 'uni_start',  2, 'offset in mm to start of ROI for reverb pattern analysis from top of profile. (2)') # 3?
    _addparaminfo(qc_params, qc_info, 'uni_range_model',  'skip10pct', 'method to determine y-range for uniformity profile. absolute/skip10pct/skip20pct/maxsenslimit (absolute)') # 3?
    _addparaminfo(qc_params, qc_info, 'sen_filter', 5,   'running average of this width for sensitivity data')
    _addparaminfo(qc_params, qc_info, 'sen_delta',  0.1, 'a peak in sensitivity profile must be at least <fdelta>*(max-noise)')
    _addparaminfo(qc_params, qc_info, 'ver_offset', 2,   'default lines to exclude from top and bottom when making profiles (10)') # 2 at least for iu22 (white line)
    _addparaminfo(qc_params, qc_info, 'hor_offset', 3,   'default lines to exclude from left and right when making profiles (10)') # with 2 kurtosis might explode
    _addparaminfo(qc_params, qc_info, 'fitcircle_frac', 1/3., 'use only this central fraction for circle fitting')
    _addparaminfo(qc_params, qc_info, 'cluster_fminsize', 300., 'ignore clusters smaller than imwidth*imheight/fminsize')
    _addparaminfo(qc_params, qc_info, 'signal_thresh', 0, 'threshold on pixelvalues for reverberation patterns; use>0 for noisy data (0)')
    _addparaminfo(qc_params, qc_info, 'cluster_mode', 'all_middle', 'mode of connected components analysis; either all_middle (def) or largest_only')
    _addparaminfo(qc_params, qc_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
    _addparaminfo(ocr_params, ocr_info, 'ocr_threshold', 0, 'threshold on pixel values to remove background gradient (0)')
    _addparaminfo(ocr_params, ocr_info, 'ocr_zoom', 10, 'zoom factor before ocr analysis (10)')
    _addparaminfo(ocr_params, ocr_info, 'ocr_border', 0, 'add extra border around text in ROI before ocr analysis (10)')
    _addparaminfo(ocr_params, ocr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
    _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')

    if m_id.startswith('epiq'):
        comments['description'] += 'Philips Epiq'
        comments['version'] = '20210331'
        ocr_params['ocr_zoom'] = 3
        ocr_params['ocr_border'] = 10
        if 'L12_3' in m_id:
            _addparaminfo(qc_params, qc_info, 'rev_bbox', '159;865;68;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L12_5' in m_id:
            _addparaminfo(qc_params, qc_info, 'rev_bbox', '143;879;68;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L15_7io' in m_id:
            _addparaminfo(qc_params, qc_info, 'rev_bbox', '225;818;129;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L17_5' in m_id:
            _addparaminfo(qc_params, qc_info, 'rev_bbox', '152;870;68;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'eL18_4' in m_id:
            _addparaminfo(qc_params, qc_info, 'rev_bbox', '159;865;68;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
            
        ocrname = 'OCR_TissueIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '822;23;90;24', 'x0;y0;width;height of bounding box of OCR_TissueIndex')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'TIS', 'text in bounding box of OCR_TissueIndex starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of OCR_TissueIndex should be of this type')

        ocrname = 'OCR_MechIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '913;23;80;24', 'x0;y0;width;height of bounding box of OCR_MechIndex')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'MI', 'text in bounding box of OCR_MechIndex starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of OCR_MechIndex should be of this type')

        ocrname = 'OCR_Gain'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '21;159;27;18', 'x0;y0;width;height of bounding box of OCR_Gain')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of OCR_Gain should be of this type')

        ocrname = 'OCR_DynRange'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '50;179;60;18', 'x0;y0;width;height of bounding box of OCR_DynRange')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'R', 'text in bounding box of OCR_DynRange starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of OCR_DynRange should be of this type')

        ocrname = 'OCR_FrameRate'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '11;76;79;20', 'x0;y0;width;height of bounding box of OCR_FrameRate')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of OCR_FrameRate should be of this type')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'suffix', 'Hz', 'text in bounding box of OCR_FrameRate starts with this')

        ocrname = 'OCR_ResSpd'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '11;97;59;20', 'x0;y0;width;height of bounding box of OCR_ResSpd')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'string', 'contents minus prefix of bounding box of OCR_ResSpd should be of this type')

        ocrname = 'OCR_Persistence'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '14;200;82;20', 'x0;y0;width;height of bounding box of OCR_Persistence')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'string', 'contents minus prefix of bounding box of OCR_Persistence should be of this type')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'P', 'text in bounding box of OCR_Persistence starts with this')

        ocrname = 'OCR_PenResGen'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '14;218;82;20', 'x0;y0;width;height of bounding box of OCR_PenResGen')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'string', 'contents minus prefix of bounding box of OCR_PenResGen should be of this type')

        ocrname = 'OCR_LUT'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '959;78;36;17', 'x0;y0;width;height of bounding box of OCR_LUT')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'string', 'contents minus prefix of bounding box of OCR_LUT should be of this type')


    elif m_id.startswith('iU22'):
        comments['description'] += 'Philips iU22'
        comments['version'] = '20170907'

        if 'L9_3' in m_id:
            _addparaminfo(qc_params, qc_info, 'rev_bbox', '217;876;125;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L12_5' in m_id:
            _addparaminfo(qc_params, qc_info, 'rev_bbox', '202;891;125;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L15_7io' in m_id:
            _addparaminfo(qc_params, qc_info, 'rev_bbox', '198;893;125;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L17_5' in m_id:
            _addparaminfo(qc_params, qc_info, 'rev_bbox', '212;881;125;767', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')

        ocrname = 'OCR_TissueIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '824;6;86;26', 'x0;y0;width;height of bounding box of OCR_TissueIndex')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'prefix', 'TIS', 'text in bounding box of OCR_TissueIndex starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_TissueIndex should be of this type')

        ocrname = 'OCR_MechIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '910;6;90;26', 'x0;y0;width;height of bounding box of OCR_MechIndex')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'prefix', 'MI', 'text in bounding box of OCR_MechIndex starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_MechIndex should be of this type')

        ocrname = 'OCR_Gain'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '14;159;28;19', 'x0;y0;width;height of bounding box of OCR_Gain')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_Gain should be of this type')

        ocrname = 'OCR_DynRange'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '28;179;35;19', 'x0;y0;width;height of bounding box of OCR_DynRange')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_DynRange should be of this type')

        ocrname = 'OCR_FrameRate'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '46;77;78;20', 'x0;y0;width;height of bounding box of OCR_FrameRate')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_FrameRate should be of this type')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'suffix', 'Hz', 'text in bounding box of OCR_FrameRate starts with this')

        ocrname = 'OCR_ResSpd'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '14;97;59;20', 'x0;y0;width;height of bounding box of OCR_ResSpd')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'string', 'contents minus prefix of bounding box of OCR_ResSpd should be of this type')

        ocrname = 'OCR_Persistence'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '14;200;82;20', 'x0;y0;width;height of bounding box of OCR_Persistence')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'string', 'contents minus prefix of bounding box of OCR_Persistence should be of this type')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'prefix', 'P', 'text in bounding box of OCR_Persistence starts with this')

        ocrname = 'OCR_PenResGen'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '14;218;82;20', 'x0;y0;width;height of bounding box of OCR_PenResGen')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'string', 'contents minus prefix of bounding box of OCR_PenResGen should be of this type')

        ocrname = 'OCR_LUT'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '959;78;36;17', 'x0;y0;width;height of bounding box of OCR_LUT')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'string', 'contents minus prefix of bounding box of OCR_LUT should be of this type')

    elif m_id == 'CX50':
        comments['description'] += 'Philips CX50'
        comments['version'] = '20170503'

        ocrname = 'OCR_TissueIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '565;30;94;26', 'x0;y0;width;height of bounding box of OCR_TissueIndex')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'prefix', 'TIS', 'text in bounding box of OCR_TissueIndex starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_TissueIndex should be of this type')

        ocrname = 'OCR_MechIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '565;4;94;26', 'x0;y0;width;height of bounding box of OCR_MechIndex')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'prefix', 'MI', 'text in bounding box of OCR_MechIndex starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_MechIndex should be of this type')

        ocrname = 'OCR_Gain'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '4;200;75;19', 'x0;y0;width;height of bounding box of OCR_Gain')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'prefix', 'Gn', 'text in bounding box of OCR_Gain starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_Gain should be of this type')

        ocrname = 'OCR_DynRange'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '4;219;75;19', 'x0;y0;width;height of bounding box of OCR_DynRange')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'prefix', 'C', 'text in bounding box of OCR_DynRange starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_DynRange should be of this type')

    elif m_id == 'HD11':
        comments['description'] += 'Philips HD11'
        comments['version'] = '20170901'

        ocrname = 'OCR_TissueIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '565;30;94;26', 'x0;y0;width;height of bounding box of OCR_TissueIndex')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'prefix', 'TIS', 'text in bounding box of OCR_TissueIndex starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_TissueIndex should be of this type')

        ocrname = 'OCR_MechIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '565;4;94;26', 'x0;y0;width;height of bounding box of OCR_MechIndex')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'prefix', 'MI', 'text in bounding box of OCR_MechIndex starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_MechIndex should be of this type')

        ocrname = 'OCR_Gain'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '4;200;75;19', 'x0;y0;width;height of bounding box of OCR_Gain')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'prefix', 'Gn', 'text in bounding box of OCR_Gain starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'float', 'contents minus prefix of bounding box of OCR_Gain should be of this type')

    elif m_id == 'VolusonE8':
        comments['description'] += 'GE Voluson E8'
        comments['author'] = 'Pepijn van Horssen, VUmc'
        comments['version'] = '20180328'
        qc_params = {}
        hdr_params = {}
        ocr_params = {}
        qc_info = {}
        hdr_info = {}
        ocr_info = {}
        ocr_params['ocr_regions'] = {}
        ocr_info['ocr_regions'] = {}
        
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')

        _addparaminfo(qc_params, qc_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(qc_params, qc_info, 'uni_filter', 5,   'running average of this width before peak detection')
        _addparaminfo(qc_params, qc_info, 'uni_delta',  0.3, 'a dip in normalized reverb pattern must be at least <delta>')
        _addparaminfo(qc_params, qc_info, 'uni_depth',  5, 'depth in mm of size of ROI for reverb pattern analysis from top of profile. (5)')
        _addparaminfo(qc_params, qc_info, 'uni_start',  2, 'offset in mm to start of ROI for reverb pattern analysis from top of profile. (2)')
        _addparaminfo(qc_params, qc_info, 'sen_filter', 5,   'running average of this width for sensitivity data')
        _addparaminfo(qc_params, qc_info, 'sen_delta',  0.2, 'a peak in sensitivity profile must be at least <fdelta>*(max-noise)')
        _addparaminfo(qc_params, qc_info, 'ver_offset', 1,   'default lines to exclude from top and bottom when making profiles (10)')
        _addparaminfo(qc_params, qc_info, 'hor_offset', 5,   'default lines to exclude from left and right when making profiles (10)')
        _addparaminfo(qc_params, qc_info, 'fitcircle_frac', 1, 'use only this central fraction for circle fitting')
        _addparaminfo(qc_params, qc_info, 'cluster_fminsize', 5, 'ignore clusters smaller than imwidth*imheight/fminsize')
        _addparaminfo(qc_params, qc_info, 'signal_thresh', 25, 'threshold on pixelvalues for reverberation patterns; use>0 for noisy data (0)')
        _addparaminfo(qc_params, qc_info, 'cluster_mode', 'all_middle', 'mode of connected components analysis; either all_middle (def) or largest_only')
        _addparaminfo(qc_params, qc_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')

        _addparaminfo(ocr_params, ocr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(ocr_params, ocr_info, 'ocr_threshold', 80, 'threshold on pixel values to remove background gradient (0)')
        _addparaminfo(ocr_params, ocr_info, 'ocr_zoom', 2, 'zoom factor before ocr analysis (10)')

        ocrname = 'OCR_probeID'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '334;2;139;20', 'bounding box of probeID. If OCR_probeID is specified, this will be used as identifier for values, else from dicom headers')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'string', 'contents of bounding box of OCR_probeID should be of this type')
        _addparaminfo(ocr_params, ocr_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')

    elif m_id == 'ALOKAG8':
        comments['description'] += 'ALOKA G8 C15'
        comments['author'] = 'Pepijn van Horssen, VUmc'
        comments['version'] = '20180328'
        qc_params = {}
        hdr_params = {}
        ocr_params = {}
        qc_info = {}
        hdr_info = {}
        ocr_info = {}
        ocr_params['ocr_regions'] = {}
        ocr_info['ocr_regions'] = {}

        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
    
        _addparaminfo(qc_params, qc_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(qc_params, qc_info, 'uni_filter', 5,   'running average of this width before peak detection')
        _addparaminfo(qc_params, qc_info, 'uni_delta',  0.3, 'a dip in normalized reverb pattern must be at least <delta>')
        _addparaminfo(qc_params, qc_info, 'uni_depth',  5, 'depth in mm of size of ROI for reverb pattern analysis from top of profile. (5)')
        _addparaminfo(qc_params, qc_info, 'uni_start',  2, 'offset in mm to start of ROI for reverb pattern analysis from top of profile. (2)')
        _addparaminfo(qc_params, qc_info, 'sen_filter', 5,   'running average of this width for sensitivity data')
        _addparaminfo(qc_params, qc_info, 'sen_delta',  0.2, 'a peak in sensitivity profile must be at least <fdelta>*(max-noise)')
        _addparaminfo(qc_params, qc_info, 'ver_offset', 1,   'default lines to exclude from top and bottom when making profiles (10)')
        _addparaminfo(qc_params, qc_info, 'hor_offset', 5,   'default lines to exclude from left and right when making profiles (10)')
        _addparaminfo(qc_params, qc_info, 'fitcircle_frac', 1, 'use only this central fraction for circle fitting')
        _addparaminfo(qc_params, qc_info, 'cluster_fminsize', 5, 'ignore clusters smaller than imwidth*imheight/fminsize')
        _addparaminfo(qc_params, qc_info, 'signal_thresh', 25, 'threshold on pixelvalues for reverberation patterns; use>0 for noisy data (0)')
        _addparaminfo(qc_params, qc_info, 'cluster_mode', 'all_middle', 'mode of connected components analysis; either all_middle (def) or largest_only')
        _addparaminfo(qc_params, qc_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addparaminfo(qc_params, qc_info, 'rev_bbox', '223;547;139;524', 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')

        _addparaminfo(ocr_params, ocr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(ocr_params, ocr_info, 'ocr_threshold', 80, 'threshold on pixel values to remove background gradient (0)')
        _addparaminfo(ocr_params, ocr_info, 'ocr_zoom', 2, 'zoom factor before ocr analysis (10)')

        ocrname = 'OCR_probeID'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '676;167;34;15', 'bounding box of probeID. If OCR_probeID is specified, this will be used as identifier for values, else from dicom headers')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'type', 'string', 'contents of bounding box of OCR_probeID should be of this type')
        _addparaminfo(ocr_params, ocr_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')

    _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info) 
    _addaction(config, 'ocr_series', params=ocr_params, filters={}, info=ocr_info)

    if not 'usage' in comments:
        comments['usage'] = []
    comments['usage'].extend([
        '  "OCR_xxx": "xywh": "10;30;50;25" # x;y;width;height of bounding box of OCR_xxx',
        '  "OCR_xxx": "prefix": "TI" # text in bounding box of OCR_xxx starts with this and this should be stripped',
        '  "OCR_xxx": "suffix": "mm" # text in bounding box of OCR_xxx ends with this and this should be stripped',
        '  "OCR_xxx": "type": "float" # after stripping prefix and suffix, text should be converted to this data type',
        '\n',
        '  If "OCR_probeID" is specified, OCR will be used to find an identifier for values, info from the dicom headers is used',
        '\n'
    ])

    _writeConfig(filename, config, meta)

def writeUS2Config(m_id, filename=None):
    """
    Configs for US/AirReverberations2
    """
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "US/AirReverberations2 for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}
    ocr_params = {}
    ocr_info = {}
    ocr_params['ocr_regions'] = {}
    ocr_info['ocr_regions'] = {}
    
    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    """
    wrapper_params = [
        X 'rgbchannel', 
        X 'auto_suffix',
    ] # parameters for wrapper only 

        self.params = {
            # bounding box of pattern in px
            'init_pt_x0y0x1y1': None, #[158, 68, 865, 143], # in px to be used to restrict initial search of reverb pattern; three full rings (peak-to-peak); approx 3*25
            'pt_x0y0x1y1': None, #[158, 68, 865, 143], # in px to be used to restrict reverb pattern; three full rings (peak-to-peak); approx 3*25
            X'f_weak': .5, #below this fraction of avg signal, the element is weak
            X'f_dead': .3, #below this fraction of avg signal, the element is dead
            X'signal_thresh': 0, # only pixelvalues above this number can be part of reverberations (set >0 if very noisy)
            X'cluster_mode': 'all_middle', # 'largest_only' default mode of dataroi selection
            X'cluster_fminsize': 10*10*3, # ignore clusters of size smaller than imwidth*imheigth/minsizefactor (wid/10*hei/10)/3
            X'circle_fitfrac': 1./3, # by default use only central 1/3 of circle for fitting, as deformations towards edge can occur. 
                                      # use >1 for full fit. 1 is best for GE, 1/3 is best for Philips
            'pt_curve_radii_px': None,  #[Rc,maxrad ]
            'pt_curve_origin_px': None, #[xc,yc,Rc]
            'pt_curve_angles_deg': None, # [ang0, ang1]
            X'hcor_px':0, # skip pix left and right in auto mode
            X'vcor_px':0, # skip pix above and below in auto mode
        }
    
    """

    # declare all results here in order of appearance. Details can be added/altered later on
    
    #_addresult(meta, 'unif_max_pixval', display_name='Max PixelValue', display_level=2, 
    #           description='maximum pixel value in reverberation pattern', units='', 
    #           minlowhighmax=[None, None, 253, None]) # 8 bit clipping

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'unif_dead_*00_10', display_name='DeadElements outer 20pct', display_level=2, 
               description='Number of dead elements in outer 20pct', units='', 
               minlowhighmax=[None, None, None, 4]) 
    _addresult(meta, 'unif_dead_10_90', display_name='DeadElements inner 80pct', display_level=2, 
               description='Number of dead elements in inner 80pct', units='', 
               minlowhighmax=[None, None, None, 0]) 
    _addresult(meta, 'unif_weaknbs_*00_10', display_name='WeakNeighbors outer 20pct', display_level=2, 
               description='Number of weak adjacent elements in outer 20pct', units='', 
               minlowhighmax=[None, None, None, 2]) 
    _addresult(meta, 'unif_weaknbs_10_90', display_name='WeakNeighbors inner 80pct', display_level=2, 
               description='Number of weak adjacent elements in inner 80pct', units='', 
               minlowhighmax=[None, None, None, 1])

    # tracking weakest element
    _addresult(meta, 'unif_relmin_10_90', display_name='Weakest element inner 80pct', display_level=2, 
               description='Relative strength of weakest elements in inner 80pct', units='', 
               minlowhighmax=[.3, .9, None, None])

    # tracking signal at edges
    _addresult(meta, 'unif_relmean_*00_10', display_name='SignalStrength outer 20pct', display_level=2, 
               description='Relative signal strength of outer 20pct', units='', 
               minlowhighmax=[.75, .85, 1.15, 1.25])
    # tracking signal at edges
    _addresult(meta, 'unif_relmean_*10_30', display_name='SignalStrength outer 20-40pct', display_level=2, 
               description='Relative signal strength of outer 20-40pct', units='', 
               minlowhighmax=[.9, .95, 1.05, 1.1])

    if m_id.startswith('epiq'):
        comments['description'] += 'Philips Epiq'
        comments['version'] = '20200722'

        # Shared Philips
        _addparaminfo(qc_params, qc_info, 'f_weak', .5,   'below this fraction of avg signal, the element is considered weak (.5)') # .5 sensible for true crystal performance
        _addparaminfo(qc_params, qc_info, 'f_dead', .3,   'below this fraction of avg signal, the element is considered dead (.5)') # .5 sensible for true crystal performance
        _addparaminfo(qc_params, qc_info, 'vcor_px', 2,   'default lines to exclude from top and bottom when making profiles (10)') # 2 at least for iu22 (white line)
        _addparaminfo(qc_params, qc_info, 'hcor_px', 3,   'default lines to exclude from left and right when making profiles (10)') # with 2 kurtosis might explode
        _addparaminfo(qc_params, qc_info, 'circle_fitfrac', 1/3., 'use only this central fraction for circle fitting')
        _addparaminfo(qc_params, qc_info, 'cluster_fminsize', 300., 'ignore clusters smaller than imwidth*imheight/fminsize')
        _addparaminfo(qc_params, qc_info, 'cluster_mode', 'all_middle', 'mode of connected components analysis; either all_middle (def) or largest_only')
        _addparaminfo(qc_params, qc_info, 'signal_thresh', 0, 'threshold on pixelvalues for reverberation patterns; use>0 for noisy data (0)')
        _addparaminfo(qc_params, qc_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(qc_params, qc_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addparaminfo(ocr_params, ocr_info, 'ocr_threshold', 0, 'threshold on pixel values to remove background gradient (0)')
        _addparaminfo(ocr_params, ocr_info, 'ocr_zoom', 3, 'zoom factor before ocr analysis (10)')
        _addparaminfo(ocr_params, ocr_info, 'ocr_border', 10, 'add extra border around text in ROI before ocr analysis (10)')
        _addparaminfo(ocr_params, ocr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(ocr_params, ocr_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(hdr_params, hdr_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')

        # OCR found settings
        if "umcu" in m_id:
            _addresult(meta, "OCR_DynRange", display_name=None, display_level=2, 
                       description="Dynamic Range setting found by OCR", units='', 
                       minlowhighmax=[54.9, None, None, 55.1])
            _addresult(meta, "OCR_LUT", display_name=None, display_level=2, 
                       description="look-up-table used found by OCR", units='', 
                       equals="M3")
            _addresult(meta, "OCR_Persistence", display_name=None, display_level=2, 
                       description="number of time frames used found by OCR", units='', 
                       equals="Off")
            _addresult(meta, "OCR_PenResGen", display_name=None, display_level=2, 
                       description="image optimized for resolution, or penetration, or generic used found by OCR", units='', 
                       equals="Res")
            _addresult(meta, "OCR_ResSpd", display_name=None, display_level=2, 
                       description="image resolution vs speed used found by OCR", units='', 
                       equals="R1")

            if 'C5_1_' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.89, None, None, .91])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.39, None, None, .41])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[53.9, None, None, 54.1])
            elif 'mC7_2_' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.69, None, None, .71])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.49, None, None, .51])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[40.9, None, None, 41.1])
            elif 'C7_2_' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.79, None, None, .81])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.49, None, None, .51])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[44.9, None, None, 45.1])
            elif 'C8_5_' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.79, None, None, .81])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.39, None, None, .41])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[70.9, None, None, 71.1])
            elif 'L12_3_' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.79, None, None, .81])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.29, None, None, .31])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[50.9, None, None, 51.1])
            elif 'L12~3_' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[1.09, None, None, 1.11])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.29, None, None, .31])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[63.9, None, None, 64.1])
            elif 'L12_5_' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.99, None, None, 1.01])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.09, None, None, .11])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[22.9, None, None, 23.1])
            elif 'L15_7io_' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.89, None, None, .91])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.09, None, None, .11])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[120.9, None, None, 121.1])
            elif 'L17_5_' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.89, None, None, .91])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.09, None, None, .11])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[39.9, None, None, 40.1])
            elif 'eL18_4' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.79, None, None, .81])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.08, None, None, .12])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[32.9, None, None, 33.1])
            elif 'XL14_3' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.79, None, None, .81])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.0, None, None, .01])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[8.9, None, None, 9.1])
            elif 'X6_1' in m_id:
                # OCR results
                _addresult(meta, 'OCR_MechIndex', display_name=None, display_level=2, 
                           description='Mechanical Index setting found by OCR', units='', 
                           minlowhighmax=[.49, None, None, .51])
                _addresult(meta, 'OCR_TissueIndex', display_name=None, display_level=2, 
                           description='Tissue Index setting found by OCR', units='', 
                           minlowhighmax=[.09, None, None, .11])
                _addresult(meta, 'OCR_FrameRate', display_name=None, display_level=2, 
                           description='Frame generation found by OCR', units='', 
                           minlowhighmax=[10.9, None, None, 11.1])


        if 'L12_3' in m_id:
            _addparaminfo(qc_params, qc_info, 'init_pt_x0y0x1y1', [159,68, 865,767], 'restrict initial search of reverb pattern in px')
        elif 'L12~3' in m_id:
            _addparaminfo(qc_params, qc_info, 'init_pt_x0y0x1y1', [159,68, 865,767], 'restrict initial search of reverb pattern in px')
        elif 'L12_5' in m_id:
            _addparaminfo(qc_params, qc_info, 'init_pt_x0y0x1y1', [143,68, 879,767], 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L15_7io' in m_id:
            _addparaminfo(qc_params, qc_info, 'init_pt_x0y0x1y1', [184,111, 837,240], 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'L17_5' in m_id:
            _addparaminfo(qc_params, qc_info, 'init_pt_x0y0x1y1', [153,68, 870,147], 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'eL18_4' in m_id:
            _addparaminfo(qc_params, qc_info, 'init_pt_x0y0x1y1', [176,93, 848,143], 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
        elif 'XL14_3' in m_id:
            _addparaminfo(qc_params, qc_info, 'init_pt_x0y0x1y1', [166,93, 857,166], 'restrict reverberation pattern to bounding box xmin;xmax;ymin;ymax in px')
            
        ocrname = 'OCR_TissueIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      'xywh', '822;23;90;24', 'x0;y0;width;height of bounding box of OCR_TissueIndex')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'TIS', 'text in bounding box of OCR_TissueIndex starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of OCR_TissueIndex should be of this type')

        ocrname = 'OCR_MechIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '913;23;80;24', 'x0;y0;width;height of bounding box of OCR_MechIndex')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'MI', 'text in bounding box of OCR_MechIndex starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of OCR_MechIndex should be of this type')

        ocrname = 'OCR_Gain'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '21;159;27;18', 'x0;y0;width;height of bounding box of OCR_Gain')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of OCR_Gain should be of this type')

        ocrname = 'OCR_DynRange'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '50;179;60;18', 'x0;y0;width;height of bounding box of OCR_DynRange')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'R', 'text in bounding box of OCR_DynRange starts with this')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of OCR_DynRange should be of this type')

        ocrname = 'OCR_FrameRate'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '11;76;79;20', 'x0;y0;width;height of bounding box of OCR_FrameRate')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of OCR_FrameRate should be of this type')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'suffix', 'Hz', 'text in bounding box of OCR_FrameRate starts with this')

        ocrname = 'OCR_ResSpd'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '11;97;59;20', 'x0;y0;width;height of bounding box of OCR_ResSpd')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'string', 'contents minus prefix of bounding box of OCR_ResSpd should be of this type')

        ocrname = 'OCR_Persistence'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '14;200;82;20', 'x0;y0;width;height of bounding box of OCR_Persistence')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'string', 'contents minus prefix of bounding box of OCR_Persistence should be of this type')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'P', 'text in bounding box of OCR_Persistence starts with this')

        ocrname = 'OCR_PenResGen'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '14;218;82;20', 'x0;y0;width;height of bounding box of OCR_PenResGen')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'string', 'contents minus prefix of bounding box of OCR_PenResGen should be of this type')

        ocrname = 'OCR_LUT'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'xywh', '959;78;36;17', 'x0;y0;width;height of bounding box of OCR_LUT')
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'string', 'contents minus prefix of bounding box of OCR_LUT should be of this type')


    if m_id.startswith('sequoia'):
        comments['description'] += 'Siemens Sequoia'
        comments['version'] = '20200803'
        
        # Shared Siemens
        _addparaminfo(qc_params, qc_info, 'f_weak', .5,   'below this fraction of avg signal, the element is considered weak (.5)') # .5 sensible for true crystal performance
        _addparaminfo(qc_params, qc_info, 'f_dead', .3,   'below this fraction of avg signal, the element is considered dead (.5)') # .5 sensible for true crystal performance
        _addparaminfo(qc_params, qc_info, 'vcor_px', 0,   'default lines to exclude from top and bottom when making profiles (10)') # 2 at least for iu22 (white line)
        _addparaminfo(qc_params, qc_info, 'hcor_px', 3,   'default lines to exclude from left and right when making profiles (10)') # with 2 kurtosis might explode
        _addparaminfo(qc_params, qc_info, 'circle_fitfrac', 1/3., 'use only this central fraction for circle fitting')
        _addparaminfo(qc_params, qc_info, 'cluster_fminsize', 300., 'ignore clusters smaller than imwidth*imheight/fminsize')
        _addparaminfo(qc_params, qc_info, 'cluster_mode', 'all_middle', 'mode of connected components analysis; either all_middle (def) or largest_only')
        _addparaminfo(qc_params, qc_info, 'signal_thresh', 0, 'threshold on pixelvalues for reverberation patterns; use>0 for noisy data (0)')
        _addparaminfo(qc_params, qc_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(qc_params, qc_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addparaminfo(ocr_params, ocr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(ocr_params, ocr_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(hdr_params, hdr_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        
        if "umcu" in m_id:
            _addresult(meta, "DynamicRange", display_name=None, display_level=2, 
                       description="Dynamic Range setting", units='', 
                       minlowhighmax=[64.9, None, None, 65.1])
        if '18L6' in m_id:
            _addparaminfo(qc_params, qc_info, 'init_pt_x0y0x1y1', [130,36, 893,767], 'restrict initial search of reverb pattern in px')

    if m_id.startswith('te7'):
        comments['description'] += 'MindRay TE7'
        comments['version'] = '20230222'

        # Shared Mindray
        _addparaminfo(qc_params, qc_info, 'f_weak', .5,   'below this fraction of avg signal, the element is considered weak (.5)') # .5 sensible for true crystal performance
        _addparaminfo(qc_params, qc_info, 'f_dead', .3,   'below this fraction of avg signal, the element is considered dead (.5)') # .5 sensible for true crystal performance
        _addparaminfo(qc_params, qc_info, 'vcor_px', 2,   'default lines to exclude from top and bottom when making profiles (10)') # 2 at least for iu22 (white line)
        _addparaminfo(qc_params, qc_info, 'hcor_px', 3,   'default lines to exclude from left and right when making profiles (10)') # with 2 kurtosis might explode
        _addparaminfo(qc_params, qc_info, 'circle_fitfrac', 1/3., 'use only this central fraction for circle fitting')
        _addparaminfo(qc_params, qc_info, 'cluster_fminsize', 300., 'ignore clusters smaller than imwidth*imheight/fminsize')
        _addparaminfo(qc_params, qc_info, 'cluster_mode', 'all_middle', 'mode of connected components analysis; either all_middle (def) or largest_only')
        _addparaminfo(qc_params, qc_info, 'signal_thresh', 5, 'threshold on pixelvalues for reverberation patterns; use>0 for noisy data (0)')
        _addparaminfo(qc_params, qc_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(qc_params, qc_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addparaminfo(ocr_params, ocr_info, 'ocr_threshold', 1, 'threshold on pixel values to remove background gradient (0)')
        _addparaminfo(ocr_params, ocr_info, 'ocr_zoom', 3, 'zoom factor before ocr analysis (10)')
        _addparaminfo(ocr_params, ocr_info, 'ocr_border', 0, 'add extra border around text in ROI before ocr analysis (10)')
        _addparaminfo(ocr_params, ocr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(ocr_params, ocr_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add a probename as suffix to all results (True)')
        _addparaminfo(hdr_params, hdr_info, 'rgbchannel', 'B', 'use this R-G-B channel (B)')

        ocrname = 'OCR_DynRange'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      #'xywh', '1;126;58;14', 'x0;y0;width;height of bounding box of {}'.format(ocrname))
                      'xywh', '1;89;58;14', 'x0;y0;width;height of bounding box of {}'.format(ocrname))
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'DR', 'text in bounding box of {} starts with this'.format(ocrname))
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of {} should be of this type'.format(ocrname))

        ocrname = 'OCR_FrameRate'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      #'xywh', '1;139;58;14', 'x0;y0;width;height of bounding box of {}'.format(ocrname))
                      'xywh', '1;104;58;14', 'x0;y0;width;height of bounding box of {}'.format(ocrname))
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of {} should be of this type'.format(ocrname))
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'FR', 'text in bounding box of {} starts with this'.format(ocrname))

        ocrname = 'OCR_Gain'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      #'xywh', '1;165;58;14', 'x0;y0;width;height of bounding box of {}'.format(ocrname))
                      'xywh', '1;132;58;14', 'x0;y0;width;height of bounding box of {}'.format(ocrname))
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of {} should be of this type'.format(ocrname))
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'G', 'text in bounding box of {} starts with this'.format(ocrname))

        ocrname = 'OCR_MechIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      #'xywh', '1;50;58;14', 'x0;y0;width;height of bounding box of {}'.format(ocrname))
                      'xywh', '150;41;53;13', 'x0;y0;width;height of bounding box of {}'.format(ocrname))
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'MI', 'text in bounding box of {} starts with this'.format(ocrname))
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of {} should be of this type'.format(ocrname))

        ocrname = 'OCR_TissueIndex'
        ocr_params['ocr_regions'][ocrname] = {}
        ocr_info['ocr_regions'][ocrname] = {}
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname],
                      #'xywh', '1;63;58;14', 'x0;y0;width;height of bounding box of {}'.format(ocrname))
                      'xywh', '204;41;50;13', 'x0;y0;width;height of bounding box of {}'.format(ocrname))
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'prefix', 'TIS', 'text in bounding box of {} starts with this'.format(ocrname))
        _addparaminfo(ocr_params['ocr_regions'][ocrname], ocr_info['ocr_regions'][ocrname], 
                      'type', 'float', 'contents minus prefix of bounding box of {} should be of this type'.format(ocrname))

        if 'L20_5s' in m_id:
            _addparaminfo(qc_params, qc_info, 'init_pt_x0y0x1y1', [170,52, 627,205], 'restrict initial search of reverb pattern in px')

    _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info) 
    _addaction(config, 'ocr_series', params=ocr_params, filters={}, info=ocr_info)

    if not 'usage' in comments:
        comments['usage'] = []
    comments['usage'].extend([
        '  "OCR_xxx": "xywh": "10;30;50;25" # x;y;width;height of bounding box of OCR_xxx',
        '  "OCR_xxx": "prefix": "TI" # text in bounding box of OCR_xxx starts with this and this should be stripped',
        '  "OCR_xxx": "suffix": "mm" # text in bounding box of OCR_xxx ends with this and this should be stripped',
        '  "OCR_xxx": "type": "float" # after stripping prefix and suffix, text should be converted to this data type',
        '\n',
        '  If "OCR_probeID" is specified, OCR will be used to find an identifier for values, info from the dicom headers is used',
        '\n'
    ])

    _writeConfig(filename, config, meta)

def writeRFConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "RF/Pehamed for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    _addresult(meta, 'PhantomOrientation',
               display_name='Phantom Orientation', display_level=1, description='check if orientation of phantom is correct', units='')
    _addresult(meta, 'AlignConfidence',
               display_name='Align Confidence', display_level=1, description='check if outline of phantom correctly located', units='%')
    _addresult(meta, 'CuConfidence',
               display_name='Cu Confidence', display_level=1, description='check if copper wedge of phantom correctly located', units='%')

    if m_id == 'DDL':
        comments['description'] += 'Philips Omni Diagnost Eleva'
        comments['version'] = '20170828'
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'attempt to add suffix to results based on protocol name') 
        qc_params = hdr_params.copy()
        qc_info = hdr_info.copy()

        _addresult(meta, 'PhantomOrientation', minlowhighmax=[-1, -1, 1, 1])
        _addresult(meta, 'AlignConfidence', display_level=1, description='check if orientation of phantom is correct', 
                   units='', minlowhighmax=[80, 90, 100, 100])
        _addresult(meta, 'CuConfidence', display_level=1, description='check if copper wedge of phantom is located correctly',
                   units='', minlowhighmax=[80, 90, 100, 100])
        _addresult(meta, 'PhantomAngle', display_level=2, description='rotation angle of phantom',
                   units='deg', minlowhighmax=[-2.5, -2.5, 2.5, 2.5])
        _addresult(meta, 'PhantomOrientation', display_level=1, description='check if orientation of phantom is correct',
                   units='deg', minlowhighmax=[-1, -1, 1, 1])
        _addresult(meta, 'CuDR0.6_0.0', display_name='DynamicRange', display_level=2, 
                   description='dynamic range over Cu wedge', units='', minlowhighmax=[1.5, 1.5, 15., 15.])
        _addresult(meta, 'CuSNR_0.6_0.0', display_name='SNR', display_level=2,
                   description='signal to noise ratio in Cu wedge thickness 0.0mm', units='', minlowhighmax=[40, 40, 100, 100])
        _addresult(meta, 'ROIUniformity', display_name='Uniformity', display_level=2,
                   description='compare uniformity between ROIs left and right with cente', units='', minlowhighmax=[0, 0, 15, 20])

        # results to show, without action limits (yet)
        _addresult(meta, "AnnotatedImage",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    _addaction(config,'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config,'qc_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)


def writeCTConfig(m_id, anatomy, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "CT/PerformanceQuickIQ for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    meta_Head = { "metaformat": METAFORMAT,}
    meta_Body = { "metaformat": METAFORMAT,} #will be added at the end if there is a body
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=7)# every 7 days


    # declare all results here in order of appearance. Details can be added/altered later on
    ## Head phantom
    _addresult(meta_Head, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    # results that are the same between scanners
    _addresult(meta_Head, 'Patient Position', display_level=1, equals='HFS')
    _addresult(meta_Head, 'MeanCenter',
               display_name='HU Water HEAD', display_level=2, description='mean HU value in ROI in center of phantom', 
               units='HU', minlowhighmax=[-4, -3, 3, 4])
    _addresult(meta_Head, 'MeanAir',
               display_name='HU Air HEAD', display_level=2, description='mean HU value in ROI outside phantom', 
               units='HU', minlowhighmax=[-1004, -1003, -997, -996])
    _addresult(meta_Head, 'maxdev',
               display_name='HU Max Deviation HEAD', display_level=2, description='max deviation from reference HU values for objects in phantom', 
               units='HU', minlowhighmax=[-8, -7, 7, 8])
    _addresult(meta_Head, 'linearity',
               display_name='HU linearity fit HEAD', display_level=1, description='r2 coefficient of linear fit between measured and reference HU values', 
               units='', valpaccpcrit=[1., 7.5, 10.])

    # results that are different between scanners
    _addresult(meta_Head, 'Protocol Name', display_level=1, display_name='Protocol HEAD')
    _addresult(meta_Head, 'roisd',
               display_name='Noise HEAD', display_level=2, description='noise in ROI in center of phantom', units='HU')
    _addresult(meta_Head, 'snr_hol',
               display_name='SNR HEAD', display_level=1, description='(mean+1000) to noise ratio in ROI in center of phantom', units='')
    _addresult(meta_Head, 'unif',
               display_name='Non-uniformity HEAD', display_level=2, description='Non-uniformity of phantom', units='HU')
    _addresult(meta_Head, 'MeanHigh',
               display_name='HU High HEAD', display_level=1, description='mean value for high HU object', 
               units='HU')

    ## Body phantom
    _addresult(meta_Body, 'AcquisitionDateTime',
               display_name='DateTime', display_level=2, description='date and time of acquisition', units='', period=14)# every 14 days

    # results that are the same between scanners
    _addresult(meta_Body, 'Patient Position', display_level=1, equals='HFS')
    _addresult(meta_Body, 'MeanCenter',
               display_name='HU Center BODY', display_level=2, description='mean HU value in ROI in center of phantom', 
               units='HU', minlowhighmax=[-4, -3, 3, 4])
    _addresult(meta_Body, 'MeanAir',
               display_name='HU Air BODY', display_level=2, description='mean HU value in ROI outside phantom', 
               units='HU', minlowhighmax=[-1004, -1003, -997, -996])
    _addresult(meta_Body, 'maxdev',
               display_name='HU Max Deviation BODY', display_level=2, description='max deviation from reference HU values for objects in phantom', 
               units='HU', minlowhighmax=[-8, -4, 4, 8])
    _addresult(meta_Body, 'linearity',
               display_name='HU linearity fit BODY', display_level=1, description='r2 coefficient of linear fit between measured and reference HU values', 
               units='', valpaccpcrit=[1., 7.5, 10.])
    _addresult(meta_Body, 'MeanHigh',
               display_name='HU High BODY', display_level=1, description='mean value for high HU object', 
               units='HU')

    # results that are different between scanners
    _addresult(meta_Body, 'Protocol Name', display_level=1, display_name='Protocol BODY')
    _addresult(meta_Body, 'roisd',
               display_name='Noise BODY', display_level=2, description='noise in ROI in center of phantom', units='HU')
    _addresult(meta_Body, 'snr_hol',
               display_name='SNR BODY', display_level=1, description='(mean+1000) to noise ratio in ROI in center of phantom', units='')
    _addresult(meta_Body, 'unif',
               display_name='Non-uniformity BODY', display_level=2, description='Non-uniformity of phantom', units='HU')

    if m_id == 'CT1':
        comments['description'] += 'Philips Brilliance 64'
        #comments['version'] = '20170622'
        comments['version'] = '20181017'

        _addparaminfo(qc_params, qc_info, 'scannername', 'Brilliance 64', 'your identifier for this scanner')
        _addparaminfo(qc_params, qc_info, 'headHU_air',   -1000., 'HU value of air for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_water',     1.2, 'HU value of water for Head phantom')
        #_addparaminfo(qc_params, qc_info, 'headHU_pvc',    1185., 'HU value of PVC shell for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_pvc',    885., 'HU value of PVC shell for Head phantom') # nw phantom 201810
        _addparaminfo(qc_params, qc_info, 'headdiammm_in',  188., 'inner diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'headdiammm_out', 192., 'outer diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'bodyHU_aculon',   98., 'HU value of aculon for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_teflon',  878., 'HU value of teflon for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_water',     1.2, 'HU value of water for Body phantom')
        _addparaminfo(qc_params, qc_info, 'use_headairdistmm', 93., 'distance in mm to ROI for Air for Head (def. 79.75)')

        _addresult(meta_Head, 'Protocol Name', equals='IQ Check/Head')
        _addresult(meta_Head, 'roisd', valpaccpcrit=[2.75, 7.5, 10.])
        _addresult(meta_Head, 'snr_hol', valpaccpcrit=[360., 25., 30.])
        _addresult(meta_Head, 'unif', minlowhighmax=[-3.7, -3.3, 0.3, 0.7])

    if m_id == 'CT1_iPatient':
        comments['description'] += 'Philips Brilliance 64'
        comments['version'] = '20190611'

        _addparaminfo(qc_params, qc_info, 'scannername', 'Brilliance 64', 'your identifier for this scanner')
        _addparaminfo(qc_params, qc_info, 'headHU_air',   -1000., 'HU value of air for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_water',     1.2, 'HU value of water for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_pvc',    897., 'HU value of PVC shell for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headdiammm_in',  188., 'inner diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'headdiammm_out', 192., 'outer diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'bodyHU_aculon',  112., 'HU value of aculon for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_teflon',  941., 'HU value of teflon for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_water',     1.2, 'HU value of water for Body phantom')
        _addparaminfo(qc_params, qc_info, 'use_headairdistmm', 93., 'distance in mm to ROI for Air for Head (def. 79.75)')

        _addresult(meta_Head, 'roisd', valpaccpcrit=[2.75, 7.5, 10.])
        _addresult(meta_Head, 'snr_hol', valpaccpcrit=[360., 25., 30.])
        _addresult(meta_Head, 'unif', minlowhighmax=[-3.7, -3.3, 0.3, 0.7])

        _addresult(meta_Body, 'MeanCenter', minlowhighmax=[108, 108, 116, 116])
        _addresult(meta_Body, 'roisd',  valpaccpcrit=[9.3, 10., 15.])
        _addresult(meta_Body, 'snr_hol', valpaccpcrit=[120., 15., 20.])
        _addresult(meta_Body, 'unif', minlowhighmax=[-8, -8, 8, 8])

    if m_id == 'CT2leen':
        comments['description'] += 'Philips Brilliance 64 leen'
        comments['version'] = '20160413'
        
        _addparaminfo(qc_params, qc_info, 'scannername', 'Brilliance 64', 'your identifier for this scanner')
        _addparaminfo(qc_params, qc_info, 'headHU_air',   -1000., 'HU value of air for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_water',     1.2, 'HU value of water for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_pvc',    1212., 'HU value of PVC shell for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headdiammm_in',  188., 'inner diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'headdiammm_out', 192., 'outer diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'bodyHU_aculon',   98., 'HU value of aculon for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_teflon',  878., 'HU value of teflon for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_water',     1.2, 'HU value of water for Body phantom')

        _addresult(meta_Head, 'Protocol Name', equals='IQ Check/Head')
        _addresult(meta_Head, 'roisd',  valpaccpcrit=[2.75, 7.5, 10.])
        _addresult(meta_Head, 'snr_hol', valpaccpcrit=[360., 25., 30.])
        _addresult(meta_Head, 'unif', minlowhighmax=[-3.7, -3.2, 0.3, 0.7])

    if m_id == 'CT2':
        comments['description'] += 'Philips Mx8000IDT'
        comments['version'] = '20160413'
        
        _addparaminfo(qc_params, qc_info, 'scannername', 'Mx8000IDT', 'your identifier for this scanner')
        _addparaminfo(qc_params, qc_info, 'headHU_air',   -1000., 'HU value of air for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_water',     0., 'HU value of water for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_pvc',    1205., 'HU value of PVC shell for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headdiammm_in',  188., 'inner diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'headdiammm_out', 192., 'outer diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'bodyHU_aculon',  100., 'HU value of aculon for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_teflon',  915., 'HU value of teflon for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_water',     0., 'HU value of water for Body phantom')

        _addresult(meta_Head, 'Protocol Name', equals='Head QA/Head')
        _addresult(meta_Head, 'roisd',  minlowhighmax=[4., 4.25, 4.75, 5.])
        _addresult(meta_Head, 'snr_hol', valpaccpcrit=[222., 15., 30.])
        _addresult(meta_Head, 'unif', minlowhighmax=[-2.5, -1.5, 0.5, 1.5])

        _addresult(meta_Body, 'Protocol Name', equals='QA body/Abdomen/Ax')
        _addresult(meta_Body, 'MeanCenter', minlowhighmax=[95, 97, 101, 102])
        _addresult(meta_Body, 'roisd',  valpaccpcrit=[13.5, 5., 10.])
        _addresult(meta_Body, 'snr_hol', valpaccpcrit=[80., 43.75, 87.5])
        _addresult(meta_Body, 'unif', minlowhighmax=[-2.5, -1.5, 0.5, 1.5])

    if m_id == 'CT3':
        comments['description'] += 'Philips iCT 256'
        comments['version'] = '20160622'
        
        _addparaminfo(qc_params, qc_info, 'use_headairdistmm', 93., 'distance in mm to ROI for Air for Head (def. 79.75)')
        _addparaminfo(qc_params, qc_info, 'use_bodyairdistmm', 130., 'distance in mm to ROI for Air for Body (def. 115.75)')

        _addparaminfo(qc_params, qc_info, 'scannername', 'iCT 256', 'your identifier for this scanner')
        _addparaminfo(qc_params, qc_info, 'headHU_air',   -1000., 'HU value of air for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_water',     1.2, 'HU value of water for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_pvc',    1128., 'HU value of PVC shell for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headdiammm_in',  190., 'inner diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'headdiammm_out', 194., 'outer diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'bodyHU_aculon',    0., 'HU value of aculon for Body phantom') # water!
        _addparaminfo(qc_params, qc_info, 'bodyHU_teflon',  920., 'HU value of teflon for Body phantom') # 936 old phantom
        #_addparaminfo(qc_params, qc_info, 'bodyHU_water',     0., 'HU value of water for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_air',   -1000., 'HU value of air for Body phantom')

        # _addresult(meta_Head, 'Protocol Name', equals='Head QA/Head') # Unknown
        _addresult(meta_Head, 'roisd',  valpaccpcrit=[2.6, 7.5, 10.])
        _addresult(meta_Head, 'snr_hol', valpaccpcrit=[385., 15., 20.])
        _addresult(meta_Head, 'unif', minlowhighmax=[-3.5, -3.0, -0.5, 0.5])

        #_addresult(meta_Body, 'Protocol Name', equals='QA body/Abdomen/Ax') # Unknown
        _addresult(meta_Body, 'MeanCenter', minlowhighmax=[-4, -3, 3, 4])
        _addresult(meta_Body, 'roisd',  valpaccpcrit=[7, 10., 15.])
        _addresult(meta_Body, 'snr_hol', valpaccpcrit=[140., 15., 20.])
        _addresult(meta_Body, 'unif', minlowhighmax=[-8, -8, 8, 8])

    if m_id.startswith('CT2_iqon'):
        comments['description'] += 'Philips IQON 64'
        comments['version'] = '20160622'
        
        _addparaminfo(qc_params, qc_info, 'use_headairdistmm', 93., 'distance in mm to ROI for Air for Head (def. 79.75)')
        _addparaminfo(qc_params, qc_info, 'use_bodyairdistmm', 130., 'distance in mm to ROI for Air for Body (def. 115.75)')
        _addparaminfo(qc_params, qc_info, 'scannername', 'IQon 64', 'your identifier for this scanner')
        _addparaminfo(qc_params, qc_info, 'headHU_air',   -1000., 'HU value of air for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_water',    -0.5, 'HU value of water for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headdiammm_in',  190., 'inner diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'headdiammm_out', 194., 'outer diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'bodyHU_aculon',    0., 'HU value of aculon for Body phantom')  # water!
        _addparaminfo(qc_params, qc_info, 'bodyHU_air',   -1000., 'HU value of air for Body phantom')
        #_addparaminfo(qc_params, qc_info, 'bodyHU_water',     0, 'HU value of water for Body phantom')

        # _addresult(meta_Head, 'Protocol Name', equals='Head QA/Head') # Unknown
        _addresult(meta_Head, 'snr_hol', valpaccpcrit=[385., 15., 20.])
        _addresult(meta_Head, 'unif', minlowhighmax=[-4, -4, 4, 4])

        #_addresult(meta_Body, 'Protocol Name', equals='QA body/Abdomen/Ax') # Unknown
        _addresult(meta_Body, 'MeanCenter', minlowhighmax=[-4, -3, 3, 4])
        _addresult(meta_Body, 'snr_hol', valpaccpcrit=[125., 15., 20.])
        _addresult(meta_Body, 'unif', minlowhighmax=[-8, -8, 8, 8])
        
        if m_id.endswith('MonoE'):
            _addparaminfo(qc_params, qc_info, 'headHU_pvc',     161., 'HU value of PVC shell for Head phantom')
            _addparaminfo(qc_params, qc_info, 'bodyHU_teflon',  866., 'HU value of teflon for Body phantom')
            _addresult(meta_Head, 'roisd',  valpaccpcrit=[3.3, 10., 15.]) # 2.5 for old iqon
            _addresult(meta_Body, 'roisd',  valpaccpcrit=[9.5, 10., 15.]) 
        else:
            _addparaminfo(qc_params, qc_info, 'headHU_pvc',     122., 'HU value of PVC shell for Head phantom')
            _addparaminfo(qc_params, qc_info, 'bodyHU_teflon',  923., 'HU value of teflon for Body phantom')
            _addresult(meta_Head, 'roisd',  valpaccpcrit=[3.5, 10., 15.]) # 2.5 for old iqon
            _addresult(meta_Body, 'roisd',  valpaccpcrit=[11.5, 10., 15.]) # 8 for old iqon
            
    if m_id == 'CT4':
        comments['description'] += 'Philips Brilliance 16P'
        comments['version'] = '20160413'
        
        _addparaminfo(qc_params, qc_info, 'scannername', 'Brilliance 16P', 'your identifier for this scanner')
        _addparaminfo(qc_params, qc_info, 'headHU_air',   -1000., 'HU value of air for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_water',     0., 'HU value of water for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_pvc',    1206., 'HU value of PVC shell for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headdiammm_in',  188., 'inner diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'headdiammm_out', 192., 'outer diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'bodyHU_aculon',  105., 'HU value of aculon for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_teflon',  908., 'HU value of teflon for Body phantom')
        _addparaminfo(qc_params, qc_info, 'bodyHU_water',     0, 'HU value of water for Body phantom')

        _addresult(meta_Head, 'Protocol Name', equals='quick_proc/Head')
        _addresult(meta_Head, 'roisd',  valpaccpcrit=[3.9, 5., 10.])
        _addresult(meta_Head, 'snr_hol', valpaccpcrit=[260., 12.5, 25.])
        _addresult(meta_Head, 'unif', minlowhighmax=[-2.5, -1.5, 0.5, 1.5])

    if m_id.startswith('CT4F'):
        comments['description'] += 'Siemens SOMATOM Force'
        comments['version'] = '20170621'
        
        _addparaminfo(qc_params, qc_info, 'scannername', 'SOMATOM Force', 'your identifier for this scanner') # do not try to determine anatomy, use this one
        _addparaminfo(qc_params, qc_info, 'use_anatomy', 'head', 'must use this anatomy (head or body)')
        _addparaminfo(qc_params, qc_info, 'headHU_air',   -1000., 'HU value of air for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headHU_water',     0., 'HU value of water for Head phantom')
        _addparaminfo(qc_params, qc_info, 'headdiammm_in',  193., 'inner diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'headdiammm_out', 196., 'outer diameter of PVC shell of head phantom in mm')
        _addparaminfo(qc_params, qc_info, 'bodyHU_aculon',  105., 'dummy') # dummy, only head phantom used
        _addparaminfo(qc_params, qc_info, 'bodyHU_teflon',  908., 'dummy') # dummy, only head phantom used
        _addparaminfo(qc_params, qc_info, 'bodyHU_water',     0, 'dummy')  # dummy, only head phantom used
        _addparaminfo(qc_params, qc_info, 'use_headairdistmm', 93., 'distance in mm to ROI for Air for Head (def. 79.75)')
        _addparaminfo(qc_params, qc_info, 'use_bodyairdistmm', 93., 'distance in mm to ROI for Air for Body (def. 115.75)')
        
        _addresult(meta_Head, 'Protocol Name', equals='quick_proc/Head')
        _addresult(meta_Head, 'snr_hol', valpaccpcrit=[260., 12.5, 25.])
        _addresult(meta_Head, 'unif', minlowhighmax=[-2.5, -1.5, 0.5, 1.5])
        
        if m_id.endswith('_BodyAa'): # 6.5
            _addparaminfo(qc_params, qc_info, 'headHU_shell',   108., 'HU value of shell for Head phantom') # not pvc, but some other material for shell
            _addresult(meta_Head, 'roisd',  valpaccpcrit=[6.5, 5., 10.]) 
        elif m_id.endswith('_BodyDual'):
            _addparaminfo(qc_params, qc_info, 'headHU_shell',   109., 'HU value of shell for Head phantom') # not pvc, but some other material for shell
            _addresult(meta_Head, 'roisd',  valpaccpcrit=[5.25, 5., 10.]) 
        elif m_id.endswith('_BodyPedAa'): # 11.
            _addparaminfo(qc_params, qc_info, 'headHU_shell',   100., 'HU value of shell for Head phantom') # not pvc, but some other material for shell
            _addresult(meta_Head, 'roisd',  valpaccpcrit=[11., 5., 10.]) 
        elif m_id.endswith('_HeadAa'):
            _addparaminfo(qc_params, qc_info, 'headHU_shell',   120., 'HU value of shell for Head phantom') # not pvc, but some other material for shell
            _addresult(meta_Head, 'roisd',  valpaccpcrit=[10.5, 5., 10.]) 

    _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)
    
    _writeConfig(filename, config, meta_Head if anatomy == 'head' else meta_Body)

def writeCT2Config(m_id, filename=None):
    """
    for new wrapper. uses dictionary params.
    Air is expected to be lin_roi[1]
    """
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "CT/PerformanceQuickIQ2 for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_pars = {}
    hdr_info = {}
    qc_pars = {}
    qc_info = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # description for roi parameters
    description = {
        'radmm': 'radial position of roi center from phantom center (mm)',
        'angdeg': 'angular position of roi center wrt phantom center (degrees)',
        'diammm': 'diameter of roi (mm)',
        'HU': 'groundtruth value (HU)',
        'thresh_lo': "minimum value when looking for insert (HU)",
        'thresh_hi': "maximum value when looking for insert (HU)",
        'sigmapx': "preprocessing Gaussian sigma when looking for insert (px)",
        'cutdiammm': 'diameter of roi around insert position to look for shifted object center (mm)',
        'diam_minmm': 'minimum diameter of ring shaped roi (mm)',
        'diam_maxmm': 'maximum diameter of ring shaped roi (mm)'
    }

    # common limits
    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 7 days
    
    ## Head phantom
    _addresult(meta, 'Patient Position', display_level=1, equals='HFS')
    _addresult(meta, 'MeanCenter',
               display_name='HU Water', display_level=2, description='mean HU value in ROI in center of phantom', 
               units='HU', minlowhighmax=[-4, -3, 3, 4])
    _addresult(meta, 'MeanAir',
               display_name='HU Air', display_level=2, description='mean HU value in ROI outside phantom', 
               units='HU', minlowhighmax=[-1004, -1003, -997, -996])
    _addresult(meta, 'maxdev',
               display_name='HU Max Deviation', display_level=2, 
               description='Max deviation from reference HU values for objects in phantom. The reference values are defined in the config.', 
               units='HU', minlowhighmax=[-8, -7, 7, 8])
    _addresult(meta, 'linearity',
               display_name='HU linearity fit', display_level=1, 
               description='r2 coefficient of linear fit between measured and reference HU values. The reference values are defined in the config.', 
               units='', valpaccpcrit=[1., 7.5, 10.])

    # results that are different between scanners
    _addresult(meta, 'Protocol Name', display_level=1, display_name='Protocol')
    _addresult(meta, 'roisd',
               display_name='Noise', display_level=2, description='noise in ROI in center of phantom', units='HU')
    _addresult(meta, 'snr_hol',
               display_name='SNR', display_level=1, description='(mean+1000) to noise ratio in ROI in center of phantom', units='')
    _addresult(meta, 'unif',
               display_name='Non-uniformity', display_level=2, description='Non-uniformity of phantom', units='HU')
    
    
    # parameters to help finding the shifted phantom; seems the same for all systems and phantom sizes
    phantom_pars = {}
    phantom_info = {}
    _setparaminfo(phantom_pars, phantom_info, 
                  'thresh_lo', -250, "minimal value when looking for phantom (HU)")
    #_setparaminfo(phantom_pars, phantom_info, 
    #              'sigmapx', 7.0, "preprocessing Gaussian sigma when looking for phantom (px)")
    qc_pars["phantom"] = dict(phantom_pars)
    qc_info["phantom"] = dict(phantom_info)

    if m_id.startswith("Philips7500_Body"):
        comments['description'] += 'Philips 7500'
        comments['version'] = '20210420'
        
        ## parameters defining the uniformity rois in an UNshifted phantom
        unifrois = [
            #roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            {'radmm':   0., 'angdeg':   0, 'diammm': 30},
            {'radmm': 120., 'angdeg': -90, 'diammm': 30},
            {'radmm': 120., 'angdeg':   0, 'diammm': 30},
        ]

        ## parameters defining the noise rois in an UNshifted phantom
        noiserois = [
            #body: roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            #body: sddimIEC    = int(3.25*roidimIEC)#IEC say at least 40% of diam (178mm) and may not overlap with other structures
            {'radmm':   0., 'angdeg':   0, 'diammm': 30*3.25},
        ]

        ## parameters defining the linearity rois in an UNshifted phantom
        linrois = [
                {'radmm':    0., 'angdeg':   0, 'diammm': 20., 'HU': 0}, # water
                {'radmm':  185., 'angdeg': -45, 'diammm': 20., 'HU': -1000}, # air always second position
                {'radmm':    0., 'angdeg':   0, 'diam_minmm': 294., 'diam_maxmm': 296., 'HU': 105}, # between rad_in and rad_out # pvc
        ]
        if m_id.endswith("_MonoE"):
            linrois[2]['HU'] = 150.
            _setparaminfo(phantom_pars, phantom_info, 
                          'thresh_hi', 90, "maximal value when looking for phantom (HU)")

        ## add limits
        unif_lims = [-8, None, None, 8] # Body
        _addresult(meta, 'unif', minlowhighmax=unif_lims)
        if m_id.endswith("_MonoE"):
            # specfic mono limits
            noise_val = 9.
        else:
            # conventional limits
            noise_val = 10.5
        _addresult(meta, 'roisd',  valpaccpcrit=[noise_val, 10., 15.]) # 8 for old iqon
        _addresult(meta, 'snr_hol', valpaccpcrit=[(linrois[0]['HU']+1000)/noise_val, 15., 20.])

    if m_id.startswith("Philips7500_Head"):
        comments['description'] += 'Philips 7500'
        comments['version'] = '20210420'
        
        ## parameters defining the uniformity rois in an UNshifted phantom
        unifrois = [
            #roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            {'radmm':  0., 'angdeg':   0, 'diammm': 18},
            {'radmm': 65., 'angdeg': -90, 'diammm': 18},
            {'radmm': 65., 'angdeg':   0, 'diammm': 18},
        ]
        
        ## parameters defining the noise rois in an UNshifted phantom
        noiserois = [
            #body: roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            #body: sddimIEC    = int(3.25*roidimIEC)#IEC say at least 40% of diam (178mm) and may not overlap with other structures
            {'radmm':   0., 'angdeg':   0, 'diammm': 18*4},
        ]

        ## parameters defining the linearity rois in an UNshifted phantom
        linrois = [
            {'radmm':    0., 'angdeg':   0, 'diammm': 18., 'HU': 0}, # water
            {'radmm':  130., 'angdeg': -45, 'diammm': 18., 'HU': -1000}, # air
            {'radmm':   65., 'angdeg':  45, 'diammm': 18., 'HU':  -65, 'thresh_hi': -45, 'cutdiammm': 35}, # aculon
            {'radmm':   65., 'angdeg': 225, 'diammm': 18., 'HU':  125, 'thresh_lo': 100, 'cutdiammm': 35}, # teflon
            {'radmm':    0., 'angdeg':   0, 'diam_minmm': 191., 'diam_maxmm': 196., 'HU': 125}, # between rad_in and rad_out # pvc
        ]
        if m_id.endswith("_MonoE"):
            linrois[2] = {'radmm':   65., 'angdeg':  45, 'diammm': 18., 'HU':  -20, 'thresh_hi': -10, 'cutdiammm': 35} # aculon
            linrois[3]['HU'] = 160.
            linrois[4]['HU'] = 160.

        ## add limits
        unif_lims = [-4, None, None, 4] # Head
        _addresult(meta, 'unif', minlowhighmax=unif_lims)
        if m_id.endswith("_MonoE"):
            # specfic mono limits
            noise_val = 3.3
        else:
            # conventional limits
            noise_val = 3.5
        _addresult(meta, 'roisd',  valpaccpcrit=[noise_val, 10., 15.]) # 8 for old iqon
        _addresult(meta, 'snr_hol', valpaccpcrit=[(linrois[0]['HU']+1000)/noise_val, 15., 20.])

    if m_id.startswith("PhilipsIQon_Body"):
        comments['description'] += 'Philips IQon64'
        comments['version'] = '20210420'
        
        ## parameters defining the uniformity rois in an UNshifted phantom
        unifrois = [
            #roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            {'radmm':   0., 'angdeg':   0, 'diammm': 30},
            {'radmm': 120., 'angdeg': -90, 'diammm': 30},
            {'radmm': 120., 'angdeg': -45, 'diammm': 30},
        ]

        ## parameters defining the noise rois in an UNshifted phantom
        noiserois = [
            #body: roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            #body: sddimIEC    = int(3.25*roidimIEC)#IEC say at least 40% of diam (178mm) and may not overlap with other structures
            {'radmm':   0., 'angdeg':   0, 'diammm': 30*3.25},
        ]

        ## parameters defining the linearity rois in an UNshifted phantom
        linrois = [
                {'radmm':    0., 'angdeg':   0, 'diammm': 20., 'HU': 0}, # water
                {'radmm':  185., 'angdeg': -45, 'diammm': 20., 'HU': -1000}, # air
                {'radmm':   75., 'angdeg':   0, 'diammm': 20., 'thresh_lo':250, 'cutdiammm': 55, 'HU': 927}, # teflon
                {'radmm':    0., 'angdeg':   0, 'diam_minmm': 296., 'diam_maxmm': 299., 'HU': 130}, # between rad_in and rad_out # pvc
        ]
        if m_id.endswith("_MonoE"):
            linrois[2]['HU'] = 866.
            linrois[3]['HU'] = 160.

        ## add limits
        unif_lims = [-8, None, None, 8] # Body
        _addresult(meta, 'unif', minlowhighmax=unif_lims)
        if m_id.endswith("_MonoE"):
            # specfic mono limits
            noise_val = 9.5
        else:
            # conventional limits
            noise_val = 11.5
        _addresult(meta, 'roisd',  valpaccpcrit=[noise_val, 10., 15.]) # 8 for old iqon
        _addresult(meta, 'snr_hol', valpaccpcrit=[(linrois[0]['HU']+1000)/noise_val, 15., 20.])

    if m_id.startswith("PhilipsIQon_Head"):
        comments['description'] += 'Philips IQon64'
        comments['version'] = '20210420'
        
        ## parameters defining the uniformity rois in an UNshifted phantom
        unifrois = [
            #roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            {'radmm':  0., 'angdeg':   0, 'diammm': 18},
            {'radmm': 65., 'angdeg': -90, 'diammm': 18},
            {'radmm': 65., 'angdeg':   0, 'diammm': 18},
        ]
        
        ## parameters defining the noise rois in an UNshifted phantom
        noiserois = [
            #body: roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            #body: sddimIEC    = int(3.25*roidimIEC)#IEC say at least 40% of diam (178mm) and may not overlap with other structures
            {'radmm':   0., 'angdeg':   0, 'diammm': 18*4},
        ]

        ## parameters defining the linearity rois in an UNshifted phantom
        linrois = [
            {'radmm':    0., 'angdeg':   0, 'diammm': 18., 'HU': 0}, # water
            {'radmm':  130., 'angdeg': -45, 'diammm': 18., 'HU': -1000}, # air
            {'radmm':    0., 'angdeg':   0, 'diam_minmm': 191., 'diam_maxmm': 196., 'HU': 122}, # between rad_in and rad_out # pvc
        ]
        if m_id.endswith("_MonoE"):
            linrois[2]['HU'] = 161.

        ## add limits
        unif_lims = [-4, None, None, 4] # Head
        _addresult(meta, 'unif', minlowhighmax=unif_lims)
        if m_id.endswith("_MonoE"):
            # specfic mono limits
            noise_val = 3.2
        else:
            # conventional limits
            noise_val = 3.5
        _addresult(meta, 'roisd',  valpaccpcrit=[noise_val, 10., 15.]) # 8 for old iqon
        _addresult(meta, 'snr_hol', valpaccpcrit=[(linrois[0]['HU']+1000)/noise_val, 15., 20.])

    if m_id == "PhilipsiCT_Body":
        comments['description'] += 'Philips iCT256'
        comments['version'] = '20210420'

        ## parameters defining the uniformity rois in an UNshifted phantom
        unifrois = [
            #roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            {'radmm':   0., 'angdeg':   0, 'diammm': 30},
            {'radmm': 120., 'angdeg': -90, 'diammm': 30},
            {'radmm': 120., 'angdeg': -45, 'diammm': 30},
        ]

        ## parameters defining the noise rois in an UNshifted phantom
        noiserois = [
            #body: roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            #body: sddimIEC    = int(3.25*roidimIEC)#IEC say at least 40% of diam (178mm) and may not overlap with other structures
            {'radmm':   0., 'angdeg':   0, 'diammm': 30*3.25},
        ]

        ## parameters defining the linearity rois in an UNshifted phantom
        linrois = [
                {'radmm':    0., 'angdeg':   0, 'diammm': 20., 'HU': 0}, # water
                {'radmm':  185., 'angdeg': -45, 'diammm': 20., 'HU': -1000}, # air
                {'radmm':   75., 'angdeg':   0, 'diammm': 20., 'thresh_lo':250, 'cutdiammm': 55, 'HU': 920}, # teflon
                {'radmm':    0., 'angdeg':   0, 'diam_minmm': 295., 'diam_maxmm': 299., 'HU': 127}, # between rad_in and rad_out # pvc
        ]

        ## add limits
        unif_lims = [-8, None, None, 8] # unif_lims
        _addresult(meta, 'unif', minlowhighmax=unif_lims)
        noise_val = 7.
        _addresult(meta, 'roisd',  valpaccpcrit=[noise_val, 10., 15.]) 
        _addresult(meta, 'snr_hol', valpaccpcrit=[(linrois[0]['HU']+1000)/noise_val, 15., 20.])


    if m_id == "PhilipsiCT_Head":
        comments['description'] += 'Philips iCT256'
        comments['version'] = '20210420'
        
        ## parameters defining the uniformity rois in an UNshifted phantom
        unifrois = [
            #roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            {'radmm':  0., 'angdeg':   0, 'diammm': 18},
            {'radmm': 65., 'angdeg': -90, 'diammm': 18},
            {'radmm': 65., 'angdeg':   0, 'diammm': 18},
        ]
        
        ## parameters defining the noise rois in an UNshifted phantom
        noiserois = [
            #body: roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            #body: sddimIEC    = int(3.25*roidimIEC)#IEC say at least 40% of diam (178mm) and may not overlap with other structures
            {'radmm':   0., 'angdeg':   0, 'diammm': 18*4},
        ]

        ## parameters defining the linearity rois in an UNshifted phantom
        linrois = [
            {'radmm':    0., 'angdeg':   0, 'diammm': 18., 'HU': 1.2}, # water
            {'radmm':  130., 'angdeg': -45, 'diammm': 18., 'HU': -1000}, # air
            {'radmm':    0., 'angdeg':   0, 'diam_minmm': 190., 'diam_maxmm': 194., 'HU': 1128}, # between rad_in and rad_out # pvc
        ]
        
        ## add limits
        # _addresult(meta_Head, 'Protocol Name', equals='Head QA/Head') # Unknown
        unif_lims = [-4, None, None, 4] # head
        _addresult(meta, 'unif', minlowhighmax=unif_lims)
        noise_val = 2.6
        _addresult(meta, 'roisd',  valpaccpcrit=[noise_val, 10., 15.]) 
        _addresult(meta, 'snr_hol', valpaccpcrit=[(linrois[0]['HU']+1000)/noise_val, 15., 20.])

    if m_id == "PhilipsB64IP_Body":
        comments['description'] += 'Philips Brilliance64 with IPatient'
        comments['version'] = '20210420'
        
        ## parameters defining the uniformity rois in an UNshifted phantom
        unifrois = [
            #roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            {'radmm':   0., 'angdeg':   0, 'diammm': 30},
            {'radmm': 120., 'angdeg': -90, 'diammm': 30},
            {'radmm': 120., 'angdeg': -45, 'diammm': 30},
        ]

        ## parameters defining the noise rois in an UNshifted phantom
        noiserois = [
            #body: roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            #body: sddimIEC    = int(3.25*roidimIEC)#IEC say at least 40% of diam (178mm) and may not overlap with other structures
            {'radmm':   0., 'angdeg':   0, 'diammm': 30*3.25},
        ]

        ## parameters defining the linearity rois in an UNshifted phantom
        linrois = [
                {'radmm':    0., 'angdeg':   0, 'diammm': 20., 'HU': 112}, # aculon
                {'radmm':  185., 'angdeg': -45, 'diammm': 20., 'HU': -1000}, # air
                {'radmm':   75., 'angdeg': 180, 'diammm': 20., 'thresh_hi': 50, 'cutdiammm': 55, 'HU': 1.2}, # water
                {'radmm':   75., 'angdeg':   0, 'diammm': 20., 'thresh_lo':250, 'cutdiammm': 55, 'HU': 941}, # teflon
        ]

        ## add limits
        unif_lims = [-8, None, None, 8] # Body
        gt0 = linrois[0]['HU']
        _addresult(meta, 'MeanCenter', minlowhighmax=[gt0-4, None, None, gt0+4])
        _addresult(meta, 'unif', minlowhighmax=unif_lims)
        noise_val = 9.3
        _addresult(meta, 'roisd',  valpaccpcrit=[noise_val, 10., 15.]) 
        _addresult(meta, 'snr_hol', valpaccpcrit=[(linrois[0]['HU']+1000)/noise_val, 15., 20.])

    if m_id == "PhilipsB64IP_Head":
        comments['description'] += 'Philips Brilliance64 with IPatient'
        comments['version'] = '20210420'
        
        ## parameters defining the uniformity rois in an UNshifted phantom
        unifrois = [
            #roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            {'radmm':  0., 'angdeg':   0, 'diammm': 18},
            {'radmm': 65., 'angdeg': -90, 'diammm': 18},
            {'radmm': 65., 'angdeg':   0, 'diammm': 18},
        ]
        
        ## parameters defining the noise rois in an UNshifted phantom
        noiserois = [
            #body: roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            #body: sddimIEC    = int(3.25*roidimIEC)#IEC say at least 40% of diam (178mm) and may not overlap with other structures
            {'radmm':   0., 'angdeg':   0, 'diammm': 18*4},
        ]

        ## parameters defining the linearity rois in an UNshifted phantom
        linrois = [
            {'radmm':    0., 'angdeg':   0, 'diammm': 18., 'HU': 1.2}, # water
            {'radmm':  130., 'angdeg': -45, 'diammm': 18., 'HU': -1000}, # air
            {'radmm':    0., 'angdeg':   0, 'diam_minmm': 188., 'diam_maxmm': 192., 'HU': 897}, # between rad_in and rad_out # pvc
        ]
        
        ## add limits
        # _addresult(meta_Head, 'Protocol Name', equals='Head QA/Head') # Unknown
        unif_lims = [-4, None, None, 4] # Head
        _addresult(meta, 'unif', minlowhighmax=unif_lims)
        noise_val = 2.7
        _addresult(meta, 'roisd',  valpaccpcrit=[noise_val, 10., 15.]) 
        _addresult(meta, 'snr_hol', valpaccpcrit=[(linrois[0]['HU']+1000)/noise_val, 15., 20.])

    if m_id.startswith("Force_"):
        # uses the Head phantom for all
        comments['description'] += 'Siemens SOMATOM Force'
        comments['version'] = '20210420'
        
        ## parameters defining the uniformity rois in an UNshifted phantom
        unifrois = [
            #roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            {'radmm':  0., 'angdeg':   0, 'diammm': 18},
            {'radmm': 65., 'angdeg': -90, 'diammm': 18},
            {'radmm': 65., 'angdeg':   0, 'diammm': 18},
        ]
        
        ## parameters defining the noise rois in an UNshifted phantom
        noiserois = [
            #body: roidimIEC   = int(self.phantommm2pix(cs,30.*datadiam/350)+.5) # 25 for body // IEC say at least 10% of diam (178mm)
            #body: sddimIEC    = int(3.25*roidimIEC)#IEC say at least 40% of diam (178mm) and may not overlap with other structures
            {'radmm':   0., 'angdeg':   0, 'diammm': 18*4},
        ]

        ## parameters defining the linearity rois in an UNshifted phantom
        linrois = [
            {'radmm':    0., 'angdeg':   0, 'diammm': 18., 'HU': 0}, # water
            {'radmm':  130., 'angdeg': -45, 'diammm': 18., 'HU': -1000}, # air
            {'radmm':    0., 'angdeg':   0, 'diam_minmm': 193., 'diam_maxmm': 196., 'HU': 120}, # between rad_in and rad_out # pvc
        ]
        
        ## add limits
        unif_lims = [-4, None, None, 4] # Head
        _addresult(meta, 'Protocol Name', equals='quick_proc/Head')
        _addresult(meta, 'unif', minlowhighmax=unif_lims)
        if m_id.endswith("_Body_Aa"):
            noise_val = 6.5
            linrois[-1]['HU'] = 108. # update value
        elif m_id.endswith('_Body_Dual'):
            noise_val = 5.25
            linrois[-1]['HU'] = 109. # update value
        elif m_id.endswith('_Pediatric_Aa'): # 11.
            noise_val = 11
            linrois[-1]['HU'] = 100. # update value
        elif m_id.endswith('_Head_Aa'):
            noise_val = 10.5
            linrois[-1]['HU'] = 120. # update value
        _addresult(meta, 'roisd',  valpaccpcrit=[noise_val, 10., 15.]) 
        _addresult(meta, 'snr_hol', valpaccpcrit=[(linrois[0]['HU']+1000)/noise_val, 15., 20.])

    # transform rois into config structures
    for test, lab, rois in [("unifrois", "uniformity", unifrois), 
                     ("noiserois", "noise", noiserois),
                     ("linrois", "linearity", linrois)]:
        rois_pars = []
        rois_info = []
        for i,r in enumerate(rois):
            _info = {}
            _pars = {}
            for key,val in r.items():
                _setparaminfo(_pars, _info, key,
                              r[key], "{} roi_{}: {}".format(lab, i, description[key]))
            rois_pars.append(_pars)
            rois_info.append(_info)

        qc_pars[test] = list(rois_pars)
        qc_info[test] = list(rois_info)

    # add groundtruth value as constraint
    for i,roi in enumerate(linrois):
        gt = roi['HU']
        _addresult(meta, "lin_avg_{}".format(i), display_level=2,
                   description='mean HU value in linearity ROI {}'.format(i), units="HU",
                   minlowhighmax=[gt-8, None, None, gt+8])

    for i,roi in enumerate(linrois):
        # all values should be close to center value
        gt = linrois[0]['HU']
        _addresult(meta, "unif_avg_{}".format(i), display_level=2,
                   description='mean HU value in uniformity ROI {}'.format(i), units="HU",
                   minlowhighmax=[gt+unif_lims[0], None, None, gt+unif_lims[3]])
        
    _addaction(config, 'header_series', params=hdr_pars, filters={}, info=hdr_info)
    _addaction(config, 'qc_series', params=qc_pars, filters={}, info=qc_info)
    
    _writeConfig(filename, config, meta)

def writeCRConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "CR_DR/Pehamed_Wellhofer for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    _addresult(meta, 'PhantomOrientation_Table',
               display_name='Phantom Orientation Table', display_level=1, description='check if orientation of phantom is correct', 
               units='degrees', minlowhighmax=[-1, -1, 1, 1])
    _addresult(meta, 'AlignConfidence_Table',
               display_name='Align Confidence Table', display_level=1, description='check if outline of phantom correctly located', 
               units='%', minlowhighmax=[70, 80, 100, 100])
    _addresult(meta, 'xray[N]cm_Table',
               display_name='xray[N] Table', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[11.3, 11.9, 13.1, 13.7])
    _addresult(meta, 'xray[E]cm_Table',
               display_name='xray[E] Table', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[11.3, 11.9, 13.1, 13.7])
    _addresult(meta, 'xray[S]cm_Table',
               display_name='xray[S] Table', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[11.3, 11.9, 13.1, 13.7])
    _addresult(meta, 'xray[W]cm_Table',
               display_name='xray[W] Table', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[11.3, 11.9, 13.1, 13.7])
    _addresult(meta, 'xrayDev_Table',
               display_name='xrayDev Table', display_level=2, description='max deviation of x-ray field', 
               units='%', minlowhighmax=[-1.1, -.5, .5, 1.1])
    _addresult(meta, 'ROIUniformity_Table',
               display_name='ROIUniformity Table', display_level=2, description='Uniformity difference accross horizontal ROI', 
               units='%', minlowhighmax=[-10, -5, 5, 10])
    _addresult(meta, 'MTFPosConfidence_Table',
               display_name='MTFPosition Confidence Table', display_level=1, description='check if line pairs in phantom correctly located', 
               units='%', minlowhighmax=[70, 80, 100, 100])
    _addresult(meta, 'MTFFreqConfidence_Table',
               display_name='MTFFequencies Confidence Table', display_level=1, description='check if line pairs in phantom correctly translated to frequencies', 
               units='%', minlowhighmax=[70, 80, 100, 100])

    _addresult(meta, 'PhantomOrientation_Wall',
               display_name='Phantom Orientation Wall', display_level=1, description='check if orientation of phantom is correct', 
               units='degrees', minlowhighmax=[-1, -1, 1, 1])
    _addresult(meta, 'AlignConfidence_Wall',
               display_name='Align Confidence Wall', display_level=1, description='check if outline of phantom correctly located', 
               units='%', minlowhighmax=[70, 80, 100, 100])
    _addresult(meta, 'xray[N]cm_Wall',
               display_name='xray[N] Wall', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[10.5, 11.5, 13.5, 14.5])
    _addresult(meta, 'xray[E]cm_Wall',
               display_name='xray[E] Wall', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[10.5, 11.5, 13.5, 14.5])
    _addresult(meta, 'xray[S]cm_Wall',
               display_name='xray[S] Wall', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[10.5, 11.5, 13.5, 14.5])
    _addresult(meta, 'xray[W]cm_Wall',
               display_name='xray[W] Wall', display_level=1, description='edge of x-ray field', 
               units='cm', minlowhighmax=[10.5, 11.5, 13.5, 14.5])
    _addresult(meta, 'xrayDev_Wall',
               display_name='xrayDev Wall', display_level=2, description='max deviation of x-ray field', 
               units='%', minlowhighmax=[-1.1, -.5, .5, 1.1])
    _addresult(meta, 'LRUniformity_Wall',
               display_name='ROIUniformity Wall', display_level=2, description='Uniformity difference between left and right', 
               units='%', minlowhighmax=[-10, -5, 5, 10])
    _addresult(meta, 'MTFPosConfidence_Wall',
               display_name='MTFPosition Confidence Wall', display_level=1, description='check if line pairs in phantom correctly located', 
               units='%', minlowhighmax=[70, 80, 100, 100])
    _addresult(meta, 'MTFFreqConfidence_Wall',
               display_name='MTFFequencies Confidence Wall', display_level=1, description='check if line pairs in phantom correctly translated to frequencies', 
               units='%', minlowhighmax=[70, 80, 100, 100])

    # changing limits
    _addresult(meta, 'CuSNR_0.0_Table',
               display_name='CuSNR_0.0 Table', display_level=1, description='SNR in copper of thickness 0.0', units='')
    _addresult(meta, 'CuCNRmin_Table',
               display_name='CuCNRmin Table', display_level=1, description='minimal CNR across copper wedge', units='')
    _addresult(meta, 'CuDR0.6_0.0_Table',
               display_name='CuDR Table', display_level=1, description='dynamic range across copper wedge', units='')
    _addresult(meta, 'lowCNR_0_Table',
               display_name='lowCNR Table', display_level=1, description='CNR of low contrast object', units='')
    _addresult(meta, 'AreaContrast5_Table',
               display_name='AreaC5 Table', display_level=1, description='area under contrast curve', units='')
    _addresult(meta, 'AreaMTF5_Table',
               display_name='AreaMTF5 Table', display_level=1, description='area under MTF curve', units='')
    _addresult(meta, 'MTF10_Table',
               display_name='MTF10 Table', display_level=1, description='MTF 10 pct cut off', units='')

    _addresult(meta, 'CuSNR_0.0_Wall',
               display_name='CuSNR_0.0 Wall', display_level=1, description='SNR in copper of thickness 0.0', units='')
    _addresult(meta, 'CuCNRmin_Wall',
               display_name='CuCNRmin Wall', display_level=1, description='minimal CNR across copper wedge', units='')
    _addresult(meta, 'CuDR0.6_0.0_Wall',
               display_name='CuDR Wall', display_level=1, description='dynamic range across copper wedge', units='')
    _addresult(meta, 'lowCNR_0_Wall',
               display_name='lowCNR Wall', display_level=1, description='CNR of low contrast object', units='')
    _addresult(meta, 'AreaContrast5_Wall',
               display_name='AreaC5 Wall', display_level=1, description='area under contrast curve', units='')
    _addresult(meta, 'AreaMTF5_Wall',
               display_name='AreaMTF5 Wall', display_level=1, description='area under MTF curve', units='')
    _addresult(meta, 'MTF10_Wall',
               display_name='MTF10 Wall', display_level=1, description='MTF 10 pct cut off', units='')

    if m_id == 'AZUDIDI':
        comments['description'] += 'Philips Digital Diagnost with Pehamed'
        comments['version'] = '20160413'
        _addparam(hdr_params, 'roomname', 'DiDi')
        _addparam(hdr_params, 'tablepidmm', 85)
        _addparam(hdr_params, 'wallpidmm', 65)
        _addparam(hdr_params, 'phantom', 'pehamed') # pehamed nr 100876
        _addparam(hdr_params, 'linepair_type', 'typ38') # pehamed nr 100876
        _addparam(hdr_params, 'xymm1.8', '53.1;27.1') # linepair_typ38
        _addparam(hdr_params, 'xymm0.6', '80.1;-04.5') # linepair_typ38
        _addparam(hdr_params, 'xymm1.4', '59.4;-24.7') # linepair_typ38
        _addparam(hdr_params, 'xymm4.6', '28.0;02.0') # linepair_typ38

        qc_params = hdr_params.copy()
        qc_info = hdr_info.copy()
        
        _addresult(meta, 'Grid_Wall',
                   display_name='Grid Wall', display_level=1, description='', units='', equals='IN')
        _addresult(meta, 'Postprocessing_Wall',
                   display_name='Postprocessing Wall', display_level=1, description='', units='', equals='constancy')
        _addresult(meta, 'FocalSpot(s)_Wall',
                   display_name='FocalSpot(s) Wall', display_level=1, description='', units='', equals='2.000')
        _addresult(meta, 'FilterType_Wall',
                   display_name='FilterType Wall', display_level=1, description='tube filtration', units='', equals='0.2Cu,1Al')
        _addresult(meta, 'Grid_Table',
                   display_name='Grid Table', display_level=1, description='', units='', equals='IN')
        _addresult(meta, 'Postprocessing_Table',
                   display_name='Postprocessing Table', display_level=1, description='', units='', equals='constancy')
        _addresult(meta, 'FocalSpot(s)_Table',
                   display_name='FocalSpot(s) Table', display_level=1, description='', units='', equals='0.000')
        _addresult(meta, 'FilterType_Table',
                   display_name='FilterType Table', display_level=1, description='tube filtration', units='', equals='02mmAl')

        _addresult(meta, 'kVp_Wall',
                   display_name='kVp Wall', display_level=2, description='tube voltage', 
                   units='kV', minlowhighmax=[124.5, 124.99, 125.01,125.5])
        _addresult(meta, 'kVp_Table',
                   display_name='kVp Table', display_level=2, description='tube voltage', 
                   units='kV', minlowhighmax=[72.5, 72.99, 73.01, 73.5])
        _addresult(meta, 'ImageAreaDoseProduct_Wall',
                   display_name='DAP Wall', display_level=2, description='dose area product', 
                   units='dGy mm2', minlowhighmax=[.21, .25, .31, .35])
        _addresult(meta, 'ImageAreaDoseProduct_Table',
                   display_name='DAP Table', display_level=2, description='dose area product', 
                   units='dGy mm2', minlowhighmax=[1.7, 1.8, 2.2, 2.3])
        _addresult(meta, 'DistanceSourceToDetector (mm)_Wall',
                   display_name='distance Wall', display_level=2, description='distance between source and detector', 
                   units='mm', minlowhighmax=[1990., 1995., 2005., 2010.])
        _addresult(meta, 'DistanceSourceToDetector (mm)_Table',
                   display_name='distance Table', display_level=2, description='distance between source and detector', 
                   units='mm', minlowhighmax=[1190., 1195., 1205., 1210.])
        _addresult(meta, 'Exposure (mAs)_Wall',
                   display_name='mAs Wall', display_level=2, description='tube load', 
                   units='mAs', minlowhighmax=[2., 2., 3., 3.])
        _addresult(meta, 'Exposure (mAs)_Table',
                   display_name='mAs Table', display_level=2, description='tube load', 
                   units='mAs', minlowhighmax=[11., 11., 14., 14.])
        _addresult(meta, 'ExposureTime (ms)_Wall',
                   display_name='ms Wall', display_level=2, description='pulse length', 
                   units='ms', minlowhighmax=[4., 4., 7., 7.])
        _addresult(meta, 'ExposureTime (ms)_Table',
                   display_name='ms Table', display_level=2, description='pulse length', 
                   units='ms', minlowhighmax=[27., 27., 43., 43.])

        _addresult(meta, 'CuSNR_0.0_Table', minlowhighmax=[30., 50., 70., 90.])
        _addresult(meta, 'CuCNRmin_Table', minlowhighmax=[3., 3.5, 5., 6.])
        _addresult(meta, 'CuDR0.6_0.0_Table', minlowhighmax=[1.5, 3.25, 4.5, 6.])
        _addresult(meta, 'lowCNR_0_Table', minlowhighmax=[2.0, 2.5, 4.5, 4.5])
        _addresult(meta, 'AreaContrast5_Table', minlowhighmax=[0.05, 0.0625, .1, .1])
        _addresult(meta, 'AreaMTF5_Table', minlowhighmax=[.45, .48, 1., 1.])
        _addresult(meta, 'MTF10_Table', minlowhighmax=[3.1, 3.4, 5., 5.])

        _addresult(meta, 'CuSNR_0.0_Wall', minlowhighmax=[10., 40., 70., 80.])
        _addresult(meta, 'CuCNRmin_Wall', minlowhighmax=[1.1, 1.2, 2.5, 3.0])
        _addresult(meta, 'CuDR0.6_0.0_Wall', minlowhighmax=[1.3, 1.6, 1.9, 2.0])
        _addresult(meta, 'lowCNR_0_Wall', minlowhighmax=[1.25, 2.0, 4.0, 4.0])
        _addresult(meta, 'AreaContrast5_Wall', minlowhighmax=[0.025, 0.0525, .1, .1])
        _addresult(meta, 'AreaMTF5_Wall', minlowhighmax=[.40, .45, 1., 1.])
        _addresult(meta, 'MTF10_Wall', minlowhighmax=[3.0, 3.325, 5., 5])
    
    elif 'WKZFCR' in m_id:
        comments['description'] += 'Siemens FCR with Wellhofer'
        comments['version'] = '20160413'
        
        _addparam(hdr_params, 'tablesidmm', 1150)
        _addparam(hdr_params, 'tablepidmm',   65)
        _addparam(hdr_params, 'wallsidmm',  2000)
        _addparam(hdr_params, 'wallpidmm',    50)
        _addparam(hdr_params, 'outvalue',   1023)
        _addparam(hdr_params, 'phantom', 'wellhofer') # 
        _addparam(hdr_params, 'linepair_type', 'typ38') # pehamed nr 100876
        _addparam(hdr_params, 'xymm1.8', '53.7;27.1') # linepair_typ38
        _addparam(hdr_params, 'xymm0.6', '80.6;-04.7') # linepair_typ38
        _addparam(hdr_params, 'xymm1.4', '59.9;-24.9') # linepair_typ38
        _addparam(hdr_params, 'xymm4.6', '28.4;01.8') # linepair_typ38
        if m_id == 'WKZFCR1':
            _addparam(hdr_params, 'roomname', 'WKZ1')
            datevalue =   '20110000;500'
            datevalue += '|20121130;450'
            datevalue += '|20130611;350'
            datevalue += '|40000000;403'
            #<sdthreshold>40</sdthreshold>
        elif m_id == 'WKZFCR2':
            _addparam(hdr_params, 'roomname', 'WKZ2')
            datevalue =   '20110500;700'
            datevalue += '|20110516;350'
            datevalue += '|20110625;600'
            datevalue += '|20111115;350'
            datevalue += '|20121130;400'
            datevalue += '|20110516;600'
            
        _addparam(hdr_params, 'sensitivitydatavalue', datevalue)

        qc_params = hdr_params.copy()
        qc_info = hdr_info.copy()
        
        _addresult(meta, 'SoftwareVersions_Wall',
                   display_name='SoftwareVersions Wall', display_level=1, description='', units='', equals='A19')
        _addresult(meta, 'Acquisition Device Processing Code_Wall',
                   display_name='Processing Wall', display_level=1, description='', units='', equals='0904')
        _addresult(meta, 'SoftwareVersions_Table',
                   display_name='SoftwareVersions Table', display_level=1, description='', units='', equals='A19')
        _addresult(meta, 'Acquisition Device Processing Code_Table',
                   display_name='Processing Table', display_level=1, description='', units='', equals='0904')

        _addresult(meta, 'CuSNR_0.0_Table', minlowhighmax=[40.5, 42.75, 49.5, 49.5])
        _addresult(meta, 'CuCNRmin_Table', minlowhighmax=[2.97, 3.135, 3.63, 3.63])
        _addresult(meta, 'CuDR0.6_0.0_Table', minlowhighmax=[1.89, 1.995, 2.31, 2.31])
        _addresult(meta, 'lowCNR_0_Table', minlowhighmax=[1.8, 1.9, 2.2, 2.2])
        _addresult(meta, 'AreaContrast5_Table', minlowhighmax=[0.04, 0.0425, .05, .05])
        _addresult(meta, 'AreaMTF5_Table', minlowhighmax=[.3, .325, .5, .5])
        _addresult(meta, 'MTF10_Table', minlowhighmax=[3., 3.5, 5., 5.])

        _addresult(meta, 'CuSNR_0.0_Wall', minlowhighmax=[31.5, 33.25, 40., 40.])
        _addresult(meta, 'CuCNRmin_Wall', minlowhighmax=[1.17, 1.235, 2., 2.])
        _addresult(meta, 'CuDR0.6_0.0_Wall', minlowhighmax=[1.35, 1.425, 2., 2.])
        _addresult(meta, 'lowCNR_0_Wall', minlowhighmax=[1., 1.05, 2., 2.])
        _addresult(meta, 'AreaContrast5_Wall', minlowhighmax=[0.03, 0.0325, .04, .04])
        _addresult(meta, 'AreaMTF5_Wall', minlowhighmax=[.3, .325, .5, .5])
        _addresult(meta, 'MTF10_Wall', minlowhighmax=[3., 3.25, 5., 5.])
    
        comments['usage'] = [
            'params that must be set for both actions below:',
            '  "roomname": "Bucky1" # any identifier',
            '  "tablepidmm": 85 # distance [mm] between phantom on table and image detector',
            '  "wallpidmm": 85 # distance [mm] between phantom on wall and image detector',
            '  "phantom": "pehamed" # type of phantom used, either pehamed or wellhofer',
            '\n' + \
            'params that must be set too for phosphor readers for both actions below:',
            '  "tablesidmm": 1150 # distance [mm] between source and table image detector',
            '  "wallsidmm": 2000 # distance [mm] between source and wall image detector',
            '  "outvalue": 1023 # pixelvalue of outside phantom bounds',
            '  "sdthreshold": 40 # threshold on standard deviation in uniform area to determine table or wall',
            '    or define a list of (date;threshold) on sensitivity:',
            '  "sensitivitydatavalue": "20110000;500|20130611;350|40000000:403',
            '     # threshold is 500 upto date 20110000',
            '     # threshold is 350 between 20110001 and 20130611',
            '     # threshold is 403 between 20130611 and 40000000',
            'params that can be set if the line pair phantom is not found correctly:',
            '  "linepair_type": "typ38" # linepair_typ38\n'+\
            '    "xymm1.8": "53.7;27.1" # the location of the dot in 1.8 on the line pair pattern',
            '    "xymm0.6": "80.6;-04.7" # the location of the dot in 0.6 on the line pair pattern',
            '    "xymm1.4": "59.9;-24.9" # the location of the dot in 1.4 on the line pair pattern',
            '    "xymm4.6": "28.4;01.8" # the location of the dot in 4.6 on the line pair pattern',
        ]
    elif 'YSIO' in m_id:
        comments['description'] += 'Siemens Ysio with Wellhofer'
        comments['version'] = '20161216'
        
        _addparam(hdr_params, 'roomname', 'YSIO')
        _addparam(hdr_params, 'use_mustbeinverted', 'False'), # override
        _addparam(hdr_params, 'tablepidmm',   58)
        _addparam(hdr_params, 'wallpidmm',    65)
        _addparam(hdr_params, 'outvalue',   20)
        _addparam(hdr_params, 'phantom', 'wellhofer') # 
        _addparam(hdr_params, 'linepair_type', 'typ38') # pehamed nr 100876
        _addparam(hdr_params, 'xymm1.8', '53.7;27.1') # linepair_typ38
        _addparam(hdr_params, 'xymm0.6', '80.6;-04.7') # linepair_typ38
        _addparam(hdr_params, 'xymm1.4', '59.9;-24.9') # linepair_typ38
        _addparam(hdr_params, 'xymm4.6', '28.4;01.8') # linepair_typ38

        qc_params = hdr_params.copy()
        qc_info = hdr_info.copy()
        
    _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)

    
def writeOTConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "OT/Generic for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    qc_params = {}
    qc_info = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == 'input2dicom':
        comments['description'] += 'Input2DICOM from web2py'
        comments['version'] = '20160413'

        _addparam(qc_params, 'private_group', '0x0071')
        _addparam(qc_params, 'private_element', '0x9999')

        _addresult(meta, 'example string',
                   display_name='example string', display_level=2, description='example result string', 
                   units='', equals='WITHIN SPECS')
        _addresult(meta, 'example float',
                   display_name='example float', display_level=2, description='example result float', 
                   units='', minlowhighmax=[40., 50., 70., 80.])

    _addaction(config,'qc_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)


def writeNMConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "NM/Generic for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    qc_params = {}
    qc_info = {}
    qc_filters = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == 'mCT':
        comments['description'] = 'PET/SiemensPETQCparser for Siemens Biograph mCT'
        comments['version'] = '20160413'
        comments['TODO'] = {
            'function':'PET.SiemensPETQCparser.parser.parseqcreport',
            'configtagvalues': 'params'
        }

        _addparam(qc_params, 'name', 'PET Siemens DailyQC')
        _addparam(qc_params, 'description', 'PET Siemens cfg')
        _addparam(qc_params, 'version', '20150131')
        _addparam(qc_params, 'use_private_tag', '0x0071,0x9999')

        _addresult(meta, 'Isotope',
                   display_name='isotope', display_level=2, description='isotope used', units='', equals='Ge-68')
        _addresult(meta, 'System Quality Results',
                   display_name='quality', display_level=2, description='system quality', units='', equals='Passed')
        _addresult(meta, 'QC Phantom Activity',
                   display_name='activity', display_level=2, description='activity in phantom', units='', equals='Passed')
        _addresult(meta, 'Block Noise',
                   display_name='noise', display_level=2, description='block noise', units='', minlowhighmax=[0, 0, 3, 3])
        _addresult(meta, 'Block Efficiency', # always zero, but passed!
                   display_name='block efficiency', display_level=2, description='block efficiency', units='', minlowhighmax=[0, 0, 120, 120])
        _addresult(meta, 'Measured Randoms',
                   display_name='randoms', display_level=2, description='measured randoms', units='', minlowhighmax=[85, 85, 115, 115])
        _addresult(meta, 'Scanner Efficiency',
                   display_name='scanner efficiency', display_level=2, description='scanner efficiency', units='', minlowhighmax=[25.48, 25.48, 47.32, 47.32])
        _addresult(meta, 'Scatter Ratio',
                   display_name='scatter', display_level=2, description='scatter ratio', units='', minlowhighmax=[-28.8, -28.8, 35.2, 35.2])
        _addresult(meta, 'ECF',
                   display_name='ECF', display_level=2, description='ECF', units='', minlowhighmax=[20000000, 20000000, 40000000, 40000000])
        _addresult(meta, 'Plane efficiency',
                   display_name='plane efficiency', display_level=2, description='plane efficiency', units='', minlowhighmax=[-5, -5, 5, 5])
        _addresult(meta, 'Time alignment residual',
                   display_name='residual', display_level=2, description='time alignment resdual', units='', minlowhighmax=[0, 0, 2, 2])

    elif m_id == 'spect2':
        comments['description'] = 'SPECT/NEMA_Uniformity for Siemens Symbia T2'
        comments['version'] = '20160413'
        comments['TODO'] = {
            'function':'NM.NM_NEMA_Uniformity.QCUnif_wadwrapper.nm_uniformity',
            'configtagvalues': 'params'
        }
        _addparam(qc_params, 'name', 'SPECT Siemens SymbiaT2')
        _addparam(qc_params, 'description', 'SPECT Siemens cfg')
        _addparam(qc_params, 'version', '20150429')
        _addparam(qc_params, 'function', 'unif1')
        _addparam(qc_params, 'modality', 'NM')
        _addparam(qc_params, 'perform_dome_correction', '0')
        _addparam(qc_params, 'instancetag', '0008,0060')
        _addfilter(qc_filters,{'seriesdescription':'Extrinsic Sweep Verification Flood [Extrinsic Sweep]'})
        #params_qc.append( ('unif1','NM',[('seriesdescription','Extrinsic Sweep Verification Flood [Extrinsic Sweep]'),('instancetag','0008,0060')]) )

        _addresult(meta, 'det1_DU_x (UFOV)',
                   display_name='det1 DU x', display_level=2, description='det1_DU_x (UFOV)', units='', minlowhighmax=[0, 0, 5, 8])
        _addresult(meta, 'det1_DU_y (UFOV)',
                   display_name='det1 DU y', display_level=2, description='det1_DU_y (UFOV)', units='', minlowhighmax=[0, 0, 5, 8])
        _addresult(meta, 'det2_DU_x (UFOV)',
                   display_name='det2 DU x', display_level=2, description='det2_DU_x (UFOV)', units='', minlowhighmax=[0, 0, 5, 8])
        _addresult(meta, 'det2_DU_y (UFOV)',
                   display_name='det2 DU y', display_level=2, description='det2_DU_y (UFOV)', units='', minlowhighmax=[0, 0, 5, 8])

    _addaction(config, 'qc_series', params=qc_params, filters=qc_filters, info=qc_info)
    _writeConfig(filename, config, meta)

def writeMGCDConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MG/CDMAM for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == 'CDMam32':
        comments['description'] += 'Philips MicrodoseL50'
        comments['version'] = '20170802'

        _addparaminfo(qc_params, qc_info, 'modeCDCOM', 'False', 'use precompiled cdcom.exe for detection or use python implementation') 
        _addparaminfo(qc_params, qc_info, 'parsCDCOM', '', 'use extra flags for cdcom.exe, e.g use "c;high" to pass "c" and "high"') 
        _addparaminfo(qc_params, qc_info, 'phantomversion', '3.2', 'either 3.2 or 3.4') 

        _addresult(meta, 'EnergyComponent_ALsuproc',
                   display_name='energy component', display_level=2, description='displayed data', units='', equals='SUM FOR PROCESSING')
        _addresult(meta, 'iqf_ALsuproc',
                   display_name='iqf', display_level=2, description='image quality factor', units='', minlowhighmax=[1., 1., 1.25, 1.5])

    _addaction(config,'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config,'qc_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)

def writeDXHoughConfig(m_id, filename=None):
    """
    Normi13 analysis with Hough transform
    """
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "DX/Normi13Hough for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}
    unif_params = {}
    unif_info = {}
    
    # default hough parameters
    hough_params = {}
    hough_info = {}
    _setparaminfo(hough_params, hough_info, 
                  'pre_blur', 0.66, "before normalization reduce noise by gaussian blurring with this sigma (px)")
    _setparaminfo(hough_params, hough_info, 
                  'hough_mode', "meijering", "filter to use enhance line structures")
    _setparaminfo(hough_params, hough_info, 
                  'frac_hi', 0.0, "ignore values of the normalized image above dip+frac_hi*(avg-dip)")
    _setparaminfo(hough_params, hough_info, 
                  'edge_mfrac', 0.5, "binarization threshold of line enhanced image")
    _setparaminfo(hough_params, hough_info, 
                  'min_len_mm', 90., "a detected line should be at least this length in the line enhanced image")

    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == "FULL": # make a config with all params
        comments['description'] += 'Generic'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'Generic', 'your identifier of this room') 

        # distances for auto suffix or if not provided properly
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results')
        _addparaminfo(hdr_params, hdr_info, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
        _addparaminfo(hdr_params, hdr_info, 'wallpidmm',     84, 'distance between phantom on wall stand and image detector')
        _addparaminfo(hdr_params, hdr_info, 'pidmm',         70, 'distance between phantom and image detector (both table and wall)')
        _addparaminfo(hdr_params, hdr_info, 'sidmm',       1000, 'distance between source and image detector (both table and wall)')
        _addparaminfo(hdr_params, hdr_info, 'detector_names', 'SN152495;Tafel|SN152508;Wand', 'translation of serial numbers into your identifiers') 

        # for unif only
        unif_params = dict(hdr_params) # copy
        unif_info = dict(hdr_info)
        _addparaminfo(unif_params, unif_info, 'artefactborderpx', '35;35;35;35', 'number of pixels to skip on each side (xmin, xmax, ymin, ymax)') 
        _addparaminfo(unif_params, unif_info, 'skip_cropping',  True, 'do not attempt to crop image.')
        _addparaminfo(unif_params, unif_info, 'artefactborder_is_circle', True, 'interpret artefactborderpx as circle.') 
        # overrides
        _addparaminfo(unif_params, unif_info, 'use_pixmm',            0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(unif_params, unif_info, 'use_mustbeinverted',   False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(unif_params, unif_info, 'use_mustbeprecropped', '100;1139;0;959', 'use cropping to xmin_px;xmax_px;ymin_px;ymax_px before processing')
        _addparaminfo(unif_params, unif_info, 'use_mustbemirrored',   False, 'force mirroring of image before analysis.')
        
        # below is for QC only
        qc_params = dict(hdr_params) # copy
        qc_info = dict(hdr_info)

        # overrides
        _addparaminfo(qc_params, qc_info, 'use_pixmm',            0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',   False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbeprecropped', '100;1139;0;959', 'use cropping to xmin_px;xmax_px;ymin_px;ymax_px before processing')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',   False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'skip_cropping',  True, 'do not attempt to crop image.')
        _addparaminfo(qc_params, qc_info, 'use_phantomrotation',  0, 'skip autodetect phantom orientation and use this angle (0, 90, 180, 270)')

        # xray edges
        _addparaminfo(qc_params, qc_info, 'outvalue',   1023, 'value of pixels outside xray field')

        # line pair insert
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6 (in RXT02 or typ38)') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0 (in RXT02)') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4 (in typ38)') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8 (in typ38)') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6 (in typ38)') 

        _addaction(config, 'uniformity_series', params=unif_params, filters={}, info=unif_info)
    elif 'AZUDIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R3'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'AZU', 'your identifier of this room') 

        if m_id == 'AZUDIDI':
            comments['version'] = '20210127'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results')
            _addparaminfo(hdr_params, hdr_info, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, hdr_info, 'wallpidmm',     84, 'distance between phantom on wall stand and image detector')
        elif m_id == 'AZUDIDITABLE':
            comments['version'] = '20210127'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[13., 13., 20., 20.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.2, 1.2, 2.0, 2.0])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.5, 3.5, 4.5, 4.5])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])

            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1180, 1190, 1210, 1220])
            _addresult(meta, 'FilterType',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="2mmAl")
            _addresult(meta, 'FocalSpot(s)',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.000")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])

            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (mAs)',
                       display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(meta, 'ExposureTime (ms)',
                       display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
        
        elif m_id == 'AZUDIDIWALL':
            comments['version'] = '20210127'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       84, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                               display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[2.5, 2.5, 3.5, 3.5])
            _addresult(meta, 'lowCNR_0',
                               display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 1.5, 1.5])
            _addresult(meta, 'MTF10',
                               display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.25, 3.25, 4.25, 4.25])
            _addresult(meta, 'PhantomOrientation',
                               display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                               display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[N]cm',
                               display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[S]cm',
                               display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[W]cm',
                               display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                               display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(meta, 'FilterType',
                               display_name='Filtration', display_level=2, description='tube filtration', units='', equals="0.2Cu,1Al")
            _addresult(meta, 'FocalSpot(s)',
                               display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="2.000")
            _addresult(meta, 'Grid',
                               display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                               display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                               display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                               display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (mAs)',
                               display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(meta, 'ExposureTime (ms)',
                               display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(meta, 'ImageAreaDoseProduct',
                               display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                               display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                               display_name='overview', display_level=2, description='overview of analysis element', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        if qc_info == {}: qc_info = dict(hdr_info)
            
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    elif 'AZUR4DIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R4'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'AZU', 'your identifier of this room') 

        if m_id == 'AZUR4DIDITABLE':
            comments['version'] = '20210127'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       70, 'distance between phantom and image detector')
    
            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.5, 1.5, 3.0, 3.0])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.25, 3.25, 4.24, 4.24])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1090, 1090, 1110, 1110])
            _addresult(meta, 'FilterMaterial',
                       display_name='FilterMaterial', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
    
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (mAs)',
                       display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(meta, 'ExposureTime (ms)',
                       display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
        
        elif m_id == 'AZUR4DIDIWALL':
            comments['version'] = '20210127'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       84, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                               display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[2.3, 2.3, 3.3, 3.3])
            _addresult(meta, 'lowCNR_0',
                               display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 1.5, 1.5])
            _addresult(meta, 'MTF10',
                               display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.25, 3.25, 4.25, 4.25])
            _addresult(meta, 'PhantomOrientation',
                               display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                               display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[N]cm',
                               display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[S]cm',
                               display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[W]cm',
                               display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                               display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(meta, 'FilterMaterial',
                       display_name='FilterMaterial', display_level=2, description='tube filtration', units='', equals="['COPPER', 'ALUMINIUM']")
            _addresult(meta, 'Grid',
                               display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                               display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                               display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                               display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (mAs)',
                               display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(meta, 'ExposureTime (ms)',
                               display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(meta, 'ImageAreaDoseProduct',
                               display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                               display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                               display_name='overview', display_level=2, description='overview of analysis element', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        if qc_info == {}: qc_info = dict(hdr_info)

        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 
    
    elif 'WKZDIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R4'

        if m_id == 'WKZDIDI1':
            comments['version'] = '20210127'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, hdr_info, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, hdr_info, 'wallpidmm',     50, 'distance between phantom on wall stand and image detector')
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZ1', 'your identifier of this room')
            _addparaminfo(hdr_params, hdr_info, 'detector_names', 'SN152495;Tafel|SN152508;Wand', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDI2':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, hdr_info, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, hdr_info, 'wallpidmm',     50, 'distance between phantom on wall stand and image detector')
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZ2', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'detector_names', 'SN1525UU;Tafel|SN152590;Wand', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDILOS1': # Large!
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZLOS1', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 2., 2.])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3., 3., 4., 4.])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        elif m_id == 'WKZDIDILOS2': #Small!
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZLOS2', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 2., 2.])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3., 3., 4., 4.])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[10.8, 11., 13., 13.2])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[7.8, 8., 10., 10.2])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[7.8, 8., 10., 10.2])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[10.8, 11., 13., 13.2])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        elif m_id == 'WKZDIDILOS':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZLOS', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, hdr_info, 'pidmm',    0, 'distance between phantom and image detector')
            _addparaminfo(hdr_params, hdr_info, 'detector_names', 'SN1537DT;Klein2|SN143070;Klein2|SN1522YG;Klein2|SN151685;Groot1', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDITABLE':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname',    'WKZ', 'your identifier of this room')
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.5, 1.5, 3.0, 3.0])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.4, 3.4, 4.4, 4.4])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        elif m_id == 'WKZDIDIWALL':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname',    'WKZ', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       50, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[2., 2., 4., 4.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.1, 1.1, 2.1, 2.1])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.4, 3.4, 4.4, 4.4])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="['COPPER', 'ALUMINIUM']")
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="LARGE")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        if qc_info == {}: qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

    elif m_id == 'DRXLarge':
        comments['description'] += 'Carestream DRX-Revolution'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'DRXLARGE', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
    
        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)

        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1250, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[1.75, 1.75, 4.5, 4.5])
        _addresult(meta, 'lowCNR_0',
                   display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.3, 1.3, 3.0, 3.0])
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
    
        _addresult(meta, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[105., 105., 105., 105.])
        _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs', minlowhighmax=[1599, 1599, 1601, 1601])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(meta, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(meta, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(meta, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(meta, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id == 'DRXSmall':
        comments['description'] += 'Carestream DRX-Revolution'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'DRXSMALL', 'your identifier of this room') 
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)

        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1250, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 
    
        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[1.75, 1.75, 4.5, 4.5])
        _addresult(meta, 'lowCNR_0',
                   display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.15, 1.15, 3.0, 3.0])
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
    
        _addresult(meta, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[105., 105., 105., 105.])
        _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs', minlowhighmax=[1599, 1599, 1601, 1601])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(meta, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(meta, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(meta, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(meta, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id == 'DRXLargeRXT02':
        comments['description'] += 'Carestream DRX-Revolution'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'DRXLARGE', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
    
        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)

        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1250, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 
        _addparaminfo(qc_params, qc_info, 'use_phantomrotation',  0, 'skip autodetect phantom orientation and use this angle (0, 90, 180, 270)')

        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[1.75, 1.75, 4.5, 4.5])
        _addresult(meta, 'lowCNR_0',
                   display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.3, 1.3, 3.0, 3.0])
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
    
        _addresult(meta, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[105., 105., 105., 105.])
        _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs', minlowhighmax=[1599, 1599, 1601, 1601])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(meta, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(meta, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(meta, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(meta, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id == 'ElevaAcq':
        comments['description'] += 'Philips MultiDiagnost Eleva'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZEleva', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)

        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(meta, 'PhantomOrientation',
                   display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="270.0")
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
        _addresult(meta, 'DistanceSourceToDetector (mm)',
                   display_name='SID', display_level=2, description='distance between source and detector', units='cm', minlowhighmax=[118, 119, 121, 122])
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2', minlowhighmax=[2.52, 2.6, 3., 3.08])
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[80., 80., 80., 80.])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='')
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id == 'ElevaCine':
        comments['description'] += 'Philips MultiDiagnost Eleva'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZEleva', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)

        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(meta, 'PhantomOrientation',
                   display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="270.0")
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
        _addresult(meta, 'DistanceSourceToDetector (mm)',
                   display_name='SID', display_level=2, description='distance between source and detector', units='cm', minlowhighmax=[118, 119, 121, 122])
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2', minlowhighmax=[2.52, 2.6, 3., 3.08])
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[80., 81., 85., 86.])
        _addresult(meta, 'num_slices',
                   display_name='frames', display_level=2, description='number of frames', units='kV', minlowhighmax=[9., 9.1, 12.9, 13.])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='')
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id == 'FD20':
        comments['description'] += 'Philips AlluraXper FD20C'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'ANG1', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)

        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.302, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1000, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    elif m_id == 'FD2020_F':
        comments['description'] += 'Philips AlluraXper FD2020 Frontal'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'ANG2', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)

        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.294, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1000, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    elif m_id == 'FD2020_L':
        comments['description'] += 'Philips AlluraXper FD2020 Lateral'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'ANG2', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)

        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.249, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1300, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  True, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    elif m_id in ['F7', "F7invert"]:
        comments['description'] += 'Philips Omni Diagnost Eleva'
        comments['version'] = '20210127'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'F7', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)

        _setparaminfo(hough_params, hough_info, 
                      'pre_blur', 1.0, "before normalization reduce noise by gaussian blurring with this sigma (px)")

        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.315, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  True if m_id=="F7invert" else False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'skip_cropping',  True, 'do not attempt to crop image.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    elif m_id in ["CiosAlphaAcq", "CiosAlphaCine"]:
        comments['description'] += 'Siemens Cios Alpha'
        comments['version'] = '20210202'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'AZUCiosAlpha', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)

        _setparaminfo(hough_params, hough_info, 
                      'pre_blur', 1.5 if m_id=="CiosAlphaAcq" else 1.0, "before normalization reduce noise by gaussian blurring with this sigma (px)")

        _addparaminfo(qc_params, qc_info, 'pidmm',       36, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  True, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    comments['usage'] = [
        'params that must be set for both actions below:',
        '  "roomname": "Bucky1" # any identifier',
        'and either (to set autodetection of table/wall stand):',
        '  "tablepidmm": 85 # distance [mm] between phantom on table and image detector',
        '  "wallpidmm": 85 # distance [mm] between phantom on wall and image detector',
        'or (to set forced usage of one distance e.g. for portable detectors):',
        '  "pidmm": 0 # distance [mm] between phantom and image detector',
        'params that can be set if the line pair phantom is not found correctly:',
        '  "linepair_typ": "RXT02"',
        '    "xymm0.6": "-83.0;-25.0" # the location of the dot in 0.6 on the line pair pattern',
        '    "xymm1.0": "-99.0;-8.0"  # the location of the dot in 1.0 on the line pair pattern',
        '  "linepair_typ": "typ38"',
        '    "xymm0.6": "-108.5;3.8"  # the location of the dot in 0.6 on the line pair pattern', 
        '    "xymm1.4": "-87.9;24.2"  # the location of the dot in 1.4 on the line pair pattern', 
        '    "xymm1.8": "-81.3;-27.3"  # the location of the dot in 1.8 on the line pair pattern', 
        '    "xymm4.6": "-56.2;-2.2"  # the location of the dot in 4.6 on the line pair pattern', 
        'params that can be set if the phantom orientation is not found correctly:',
        '  "use_mustbeprecropped": "100;1139;0;959" # crop to [xmin_px, xmax_px, ymin_px, ymax_px] before processing',
        '  "use_mustbemirrored": True # force mirroring of image before analysis',
        'optionally stands can be identified by their detectorId:',
        '  "detector_names": "SN1525UU;Tafel|SN152590;Wand"',
        'optionally the source to detector distance can be set manually (with autodetection of table/wall stand)',
        '  "tablesidmm": 1150 # distance [mm] between source and image detector in table',
        '  "wallsidmm": 2000 # distance [mm] between source and image detector in wall',
        'or (to set forced usage of one distance e.g. for portable detectors):',
        '  "sidmm": 1150 # distance [mm] between source and image detector',
        'the following params can be supplied to override automatic determination:',
        '  "use_pixmm": 0.262 # the size of one pixel in mm (on the phantom)',
        '  "use_mustbeinverted": True # higher pixelvalues should mean less attenuation',
        'the following params can be supplied to override auto cropping of image before analysis:',
        '  "skip_cropping": True # do not attempt to crop',
        '  "artefactborder_is_circle": True # the artefactborder is a circle',
        'optionally set the following parameters:',
        '  "auto_suffix": true or false [false]  # to set or suppress auto_suffix generation',
    ]

    # set for QC
    if qc_params.get('hough_options', {}) == {}:
        qc_params["hough_options"] = dict(hough_params)
        qc_info["hough_options"] = dict(hough_info)

    _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)

def writeDXConfig(m_id, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "DX/Normi13 for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}
    unif_params = {}
    unif_info = {}
    
    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    if m_id == "FULL": # make a config with all params
        comments['description'] += 'Generic'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'Generic', 'your identifier of this room') 

        # distances for auto suffix or if not provided properly
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results')
        _addparaminfo(hdr_params, hdr_info, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
        _addparaminfo(hdr_params, hdr_info, 'wallpidmm',     84, 'distance between phantom on wall stand and image detector')
        _addparaminfo(hdr_params, hdr_info, 'pidmm',         70, 'distance between phantom and image detector (both table and wall)')
        _addparaminfo(hdr_params, hdr_info, 'sidmm',       1000, 'distance between source and image detector (both table and wall)')
        _addparaminfo(hdr_params, hdr_info, 'detector_names', 'SN152495;Tafel|SN152508;Wand', 'translation of serial numbers into your identifiers') 

        # for unif only
        unif_params = dict(hdr_params) # copy
        unif_info = dict(hdr_info)
        _addparaminfo(unif_params, unif_info, 'artefactborderpx', '35;35;35;35', 'number of pixels to skip on each side (xmin, xmax, ymin, ymax)') 
        _addparaminfo(unif_params, unif_info, 'skip_cropping',  True, 'do not attempt to crop image.')
        _addparaminfo(unif_params, unif_info, 'artefactborder_is_circle', True, 'interpret artefactborderpx as circle.') 
        # overrides
        _addparaminfo(unif_params, unif_info, 'use_pixmm',            0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(unif_params, unif_info, 'use_mustbeinverted',   False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(unif_params, unif_info, 'use_mustbeprecropped', '100;1139;0;959', 'use cropping to xmin_px;xmax_px;ymin_px;ymax_px before processing')
        _addparaminfo(unif_params, unif_info, 'use_mustbemirrored',   False, 'force mirroring of image before analysis.')
        
        # below is for QC only
        qc_params = dict(hdr_params) # copy
        qc_info = dict(hdr_info)
        # overrides
        _addparaminfo(qc_params, qc_info, 'use_pixmm',            0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',   False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbeprecropped', '100;1139;0;959', 'use cropping to xmin_px;xmax_px;ymin_px;ymax_px before processing')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',   False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'skip_cropping',  True, 'do not attempt to crop image.')
        _addparaminfo(qc_params, qc_info, 'use_phantomrotation',  0, 'skip autodetect phantom orientation and use this angle (0, 90, 180, 270)')

        # xray edges
        _addparaminfo(qc_params, qc_info, 'outvalue',   1023, 'value of pixels outside xray field')

        # line pair insert
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6 (in RXT02 or typ38)') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0 (in RXT02)') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4 (in typ38)') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8 (in typ38)') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6 (in typ38)') 

        _addaction(config, 'uniformity_series', params=unif_params, filters={}, info=unif_info)
    elif 'AZUDIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R3'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'AZU', 'your identifier of this room') 

        if m_id == 'AZUDIDI':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results')
            _addparaminfo(hdr_params, hdr_info, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, hdr_info, 'wallpidmm',     84, 'distance between phantom on wall stand and image detector')
        elif m_id == 'AZUDIDITABLE':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[13., 13., 20., 20.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.2, 1.2, 2.0, 2.0])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.5, 3.5, 4.5, 4.5])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])

            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1180, 1190, 1210, 1220])
            _addresult(meta, 'FilterType',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="2mmAl")
            _addresult(meta, 'FocalSpot(s)',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.000")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])

            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (mAs)',
                       display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(meta, 'ExposureTime (ms)',
                       display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
        
        elif m_id == 'AZUDIDIWALL':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       84, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                               display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[2.5, 2.5, 3.5, 3.5])
            _addresult(meta, 'lowCNR_0',
                               display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 1.5, 1.5])
            _addresult(meta, 'MTF10',
                               display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.25, 3.25, 4.25, 4.25])
            _addresult(meta, 'PhantomOrientation',
                               display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                               display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[N]cm',
                               display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[S]cm',
                               display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[W]cm',
                               display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                               display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(meta, 'FilterType',
                               display_name='Filtration', display_level=2, description='tube filtration', units='', equals="0.2Cu,1Al")
            _addresult(meta, 'FocalSpot(s)',
                               display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="2.000")
            _addresult(meta, 'Grid',
                               display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                               display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                               display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                               display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (mAs)',
                               display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(meta, 'ExposureTime (ms)',
                               display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(meta, 'ImageAreaDoseProduct',
                               display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                               display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                               display_name='overview', display_level=2, description='overview of analysis element', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        if qc_info == {}: qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    elif 'AZUR4DIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R4'
        comments['version'] = '20180417'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'AZU', 'your identifier of this room') 

        if m_id == 'AZUR4DIDITABLE':
            comments['version'] = '20180417'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       70, 'distance between phantom and image detector')
    
            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.5, 1.5, 3.0, 3.0])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.25, 3.25, 4.24, 4.24])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1090, 1090, 1110, 1110])
            _addresult(meta, 'FilterMaterial',
                       display_name='FilterMaterial', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
    
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (mAs)',
                       display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(meta, 'ExposureTime (ms)',
                       display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
        
        elif m_id == 'AZUR4DIDIWALL':
            comments['version'] = '20180417'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       84, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                               display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[2.3, 2.3, 3.3, 3.3])
            _addresult(meta, 'lowCNR_0',
                               display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 1.5, 1.5])
            _addresult(meta, 'MTF10',
                               display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.25, 3.25, 4.25, 4.25])
            _addresult(meta, 'PhantomOrientation',
                               display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                               display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[N]cm',
                               display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[S]cm',
                               display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
            _addresult(meta, 'xray[W]cm',
                               display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.0, 11.5, 14.5, 15.0])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                               display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(meta, 'FilterMaterial',
                       display_name='FilterMaterial', display_level=2, description='tube filtration', units='', equals="['COPPER', 'ALUMINIUM']")
            _addresult(meta, 'Grid',
                               display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                               display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                               display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                               display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (mAs)',
                               display_name='mAs', display_level=2, description='tube load', units='mAs')
            _addresult(meta, 'ExposureTime (ms)',
                               display_name='ms', display_level=2, description='duration of exposure', units='ms')
            _addresult(meta, 'ImageAreaDoseProduct',
                               display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                               display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                               display_name='overview', display_level=2, description='overview of analysis element', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        if qc_info == {}: qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 
    
    elif 'WKZDIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R4'

        if m_id == 'WKZDIDI1':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, hdr_info, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, hdr_info, 'wallpidmm',     50, 'distance between phantom on wall stand and image detector')
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZ1', 'your identifier of this room')
            _addparaminfo(hdr_params, hdr_info, 'detector_names', 'SN152495;Tafel|SN152508;Wand', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDI2':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, hdr_info, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, hdr_info, 'wallpidmm',     50, 'distance between phantom on wall stand and image detector')
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZ2', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'detector_names', 'SN1525UU;Tafel|SN152590;Wand', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDILOS1': # Large!
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZLOS1', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 2., 2.])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3., 3., 4., 4.])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        elif m_id == 'WKZDIDILOS2': #Small!
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZLOS2', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[0.75, 0.75, 2., 2.])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3., 3., 4., 4.])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[10.8, 11., 13., 13.2])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[7.8, 8., 10., 10.2])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[7.8, 8., 10., 10.2])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[10.8, 11., 13., 13.2])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        elif m_id == 'WKZDIDILOS':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZLOS', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, hdr_info, 'pidmm',    0, 'distance between phantom and image detector')
            _addparaminfo(hdr_params, hdr_info, 'detector_names', 'SN1537DT;Klein2|SN143070;Klein2|SN1522YG;Klein2|SN151685;Groot1', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDITABLE':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname',    'WKZ', 'your identifier of this room')
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[5., 5., 10., 10.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.5, 1.5, 3.0, 3.0])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.4, 3.4, 4.4, 4.4])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="ALUMINIUM")
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        elif m_id == 'WKZDIDIWALL':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname',    'WKZ', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       50, 'distance between phantom and image detector')

            _addresult(meta, 'CuDR2.3_0.0',
                       display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[2., 2., 4., 4.])
            _addresult(meta, 'lowCNR_0',
                       display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.1, 1.1, 2.1, 2.1])
            _addresult(meta, 'MTF10',
                       display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm', minlowhighmax=[3.4, 3.4, 4.4, 4.4])
            _addresult(meta, 'PhantomOrientation',
                       display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="0.0")
            _addresult(meta, 'xray[E]cm',
                       display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
            _addresult(meta, 'xray[N]cm',
                       display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
            _addresult(meta, 'xray[S]cm',
                       display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
            _addresult(meta, 'xray[W]cm',
                       display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11., 11.5, 14.5, 15.])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="['COPPER', 'ALUMINIUM']")
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="LARGE")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="IN")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'CuSNR_0.0',
                       display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "normi13",
                       display_name='overview', display_level=2, description='overview of analysis element', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        if qc_info == {}: qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

    elif m_id == 'DRXLarge':
        comments['description'] += 'Carestream DRX-Revolution'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'DRXLARGE', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
    
        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1250, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[1.75, 1.75, 4.5, 4.5])
        _addresult(meta, 'lowCNR_0',
                   display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.3, 1.3, 3.0, 3.0])
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
    
        _addresult(meta, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[105., 105., 105., 105.])
        _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs', minlowhighmax=[1599, 1599, 1601, 1601])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(meta, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(meta, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(meta, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(meta, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id == 'DRXSmall':
        comments['description'] += 'Carestream DRX-Revolution'
        comments['version'] = '20180418'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'DRXSMALL', 'your identifier of this room') 
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1250, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 
    
        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[1.75, 1.75, 4.5, 4.5])
        _addresult(meta, 'lowCNR_0',
                   display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.15, 1.15, 3.0, 3.0])
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[10.75, 11., 13., 13.25])
    
        _addresult(meta, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[105., 105., 105., 105.])
        _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs', minlowhighmax=[1599, 1599, 1601, 1601])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(meta, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(meta, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(meta, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(meta, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id == 'DRXLargeRXT02':
        comments['description'] += 'Carestream DRX-Revolution'
        comments['version'] = '20190611'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'DRXLARGE', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
    
        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1250, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 
        _addparaminfo(qc_params, qc_info, 'use_phantomrotation',  0, 'skip autodetect phantom orientation and use this angle (0, 90, 180, 270)')

        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='', minlowhighmax=[1.75, 1.75, 4.5, 4.5])
        _addresult(meta, 'lowCNR_0',
                   display_name='lowCNR', display_level=2, description='low contrast to noise', units='', minlowhighmax=[1.3, 1.3, 3.0, 3.0])
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.75, 12., 14., 14.25])
    
        _addresult(meta, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[105., 105., 105., 105.])
        _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs', minlowhighmax=[1599, 1599, 1601, 1601])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(meta, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(meta, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(meta, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(meta, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id == 'ElevaAcq':
        comments['description'] += 'Philips MultiDiagnost Eleva'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZEleva', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(meta, 'PhantomOrientation',
                   display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="270.0")
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
        _addresult(meta, 'DistanceSourceToDetector (mm)',
                   display_name='SID', display_level=2, description='distance between source and detector', units='cm', minlowhighmax=[118, 119, 121, 122])
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2', minlowhighmax=[2.52, 2.6, 3., 3.08])
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[80., 80., 80., 80.])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='')
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id == 'ElevaCine':
        comments['description'] += 'Philips MultiDiagnost Eleva'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZEleva', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(meta, 'PhantomOrientation',
                   display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="270.0")
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
        _addresult(meta, 'DistanceSourceToDetector (mm)',
                   display_name='SID', display_level=2, description='distance between source and detector', units='cm', minlowhighmax=[118, 119, 121, 122])
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2', minlowhighmax=[2.52, 2.6, 3., 3.08])
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[80., 81., 85., 86.])
        _addresult(meta, 'num_slices',
                   display_name='frames', display_level=2, description='number of frames', units='kV', minlowhighmax=[9., 9.1, 12.9, 13.])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='')
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id == 'FD20':
        comments['description'] += 'Philips AlluraXper FD20C'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'ANG1', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.302, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1000, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    elif m_id == 'FD2020_F':
        comments['description'] += 'Philips AlluraXper FD2020 Frontal'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'ANG2', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.294, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1000, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    elif m_id == 'FD2020_L':
        comments['description'] += 'Philips AlluraXper FD2020 Lateral'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'ANG2', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.249, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1300, 'distance between source and image detector')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  True, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    elif m_id == 'HCK_WKZ_A':
        comments['description'] += 'Siemens AXIOM Artis Tube A'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'HCK_WKZ', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.279, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbeprecropped', '100;1139;0;959', 'use cropping to [xmin_px, xmax_px, ymin_px, ymax_px] before processing')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'typ38', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-108.5;3.8', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.8', '-81.3;-27.3', 'position of the dot in 1.8') 
        _addparaminfo(qc_params, qc_info, 'xymm1.4', '-87.9;24.2', 'position of the dot in 1.4') 
        _addparaminfo(qc_params, qc_info, 'xymm4.6', '-56.2;-2.2', 'position of the dot in 4.6') 

        _addresult(meta, 'PhantomOrientation',
                   display_name='Orientation', display_level=2, description='Orientation of phantom', units='deg', equals="270.0")
        _addresult(meta, 'xray[E]cm',
                   display_name='xray[E]', display_level=2, description='x-ray boundary east side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
        _addresult(meta, 'xray[N]cm',
                   display_name='xray[N]', display_level=2, description='x-ray boundary north side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(meta, 'xray[S]cm',
                   display_name='xray[S]', display_level=2, description='x-ray boundary south side', units='cm', minlowhighmax=[9.8, 10., 12., 12.2])
        _addresult(meta, 'xray[W]cm',
                   display_name='xray[W]', display_level=2, description='x-ray boundary west side', units='cm', minlowhighmax=[11.8, 12., 14., 14.2])
    
        _addresult(meta, 'DistanceSourceToDetector (mm)',
                   display_name='SID', display_level=2, description='distance between source and detector', units='cm', minlowhighmax=[118, 119, 121, 122])
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2', minlowhighmax=[2.52, 2.6, 3., 3.08])
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[80., 80., 80., 80.])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'CuDR2.3_0.0',
                   display_name='DynamicRange', display_level=2, description='dynamic range over Cu wedge', units='')
        _addresult(meta, 'CuSNR_0.0',
                   display_name='SNR', display_level=2, description='signal to noise ratio in Cu wedge thickness 0.0mm', units='')
        _addresult(meta, 'MTF10',
                   display_name='Resolution', display_level=2, description='10% MTF cut-off', units='lp/mm')
        _addresult(meta, "normi13",
                   display_name='overview', display_level=2, description='overview of analysis element', units='')

    elif m_id in ['F7', 'F7invert']:
        comments['description'] += 'Philips Omni Diagnost Eleva'
        comments['version'] = '20180903'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'F7', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.315, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  True if m_id=="F7invert" else False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'skip_cropping',  True, 'do not attempt to crop image.')
        _addparaminfo(qc_params, qc_info, 'linepair_type', 'RXT02', 'must be RXT02 or typ38')
        _addparaminfo(qc_params, qc_info, 'xymm0.6', '-83.0;-25.0', 'position of the dot in 0.6') 
        _addparaminfo(qc_params, qc_info, 'xymm1.0', '-99.0;-8.0', 'position of the dot in 1.0') 

    comments['usage'] = [
        'params that must be set for both actions below:',
        '  "roomname": "Bucky1" # any identifier',
        'and either (to set autodetection of table/wall stand):',
        '  "tablepidmm": 85 # distance [mm] between phantom on table and image detector',
        '  "wallpidmm": 85 # distance [mm] between phantom on wall and image detector',
        'or (to set forced usage of one distance e.g. for portable detectors):',
        '  "pidmm": 0 # distance [mm] between phantom and image detector',
        'params that can be set if the line pair phantom is not found correctly:',
        '  "linepair_typ": "RXT02"',
        '    "xymm0.6": "-83.0;-25.0" # the location of the dot in 0.6 on the line pair pattern',
        '    "xymm1.0": "-99.0;-8.0"  # the location of the dot in 1.0 on the line pair pattern',
        '  "linepair_typ": "typ38"',
        '    "xymm0.6": "-108.5;3.8"  # the location of the dot in 0.6 on the line pair pattern', 
        '    "xymm1.4": "-87.9;24.2"  # the location of the dot in 1.4 on the line pair pattern', 
        '    "xymm1.8": "-81.3;-27.3"  # the location of the dot in 1.8 on the line pair pattern', 
        '    "xymm4.6": "-56.2;-2.2"  # the location of the dot in 4.6 on the line pair pattern', 
        'params that can be set if the phantom orientation is not found correctly:',
        '  "use_mustbeprecropped": "100;1139;0;959" # crop to [xmin_px, xmax_px, ymin_px, ymax_px] before processing',
        '  "use_mustbemirrored": True # force mirroring of image before analysis',
        'optionally stands can be identified by their detectorId:',
        '  "detector_names": "SN1525UU;Tafel|SN152590;Wand"',
        'optionally the source to detector distance can be set manually (with autodetection of table/wall stand)',
        '  "tablesidmm": 1150 # distance [mm] between source and image detector in table',
        '  "wallsidmm": 2000 # distance [mm] between source and image detector in wall',
        'or (to set forced usage of one distance e.g. for portable detectors):',
        '  "sidmm": 1150 # distance [mm] between source and image detector',
        'the following params can be supplied to override automatic determination:',
        '  "use_pixmm": 0.262 # the size of one pixel in mm (on the phantom)',
        '  "use_mustbeinverted": True # higher pixelvalues should mean less attenuation',
        'the following params can be supplied to override auto cropping of image before analysis:',
        '  "skip_cropping": True # do not attempt to crop',
        '  "artefactborder_is_circle": True # the artefactborder is a circle',
        'optionally set the following parameters:',
        '  "auto_suffix": true or false [false]  # to set or suppress auto_suffix generation',
    ]

    _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)

def writeDXUConfig(m_id,filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "DX/Uniformity for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}
    
    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    if 'AZUDIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R3'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'AZU', 'your identifier of this room') 

        if m_id == 'AZUDIDI':
            comments['version'] = '20160825'
            _addparaminfo(hdr_params, hdr_info, 'tablepidmm',    70, 'distance between phantom on table top and image detector')
            _addparaminfo(hdr_params, hdr_info, 'wallpidmm',     84, 'distance between phantom on wall stand and image detector')
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
        elif m_id == 'AZUDIDITABLE':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(meta, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(meta, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1180, 1190, 1210, 1220])
            _addresult(meta, 'FilterType',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="0mmAl")
            _addresult(meta, 'FocalSpot(s)',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.000")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        elif m_id == 'AZUDIDIWALL':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       84, 'distance between phantom and image detector')

            _addresult(meta, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(meta, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(meta, 'FilterType',
                       display_name='Filtration', display_level=2, description='tube filtration', units='', equals="0mmAl")
            _addresult(meta, 'FocalSpot(s)',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.000")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        if qc_info == {}: qc_info = dict(hdr_info)
        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') # 35 px = 5mm at 0.143 mm resolution
        
    elif 'AZUR4DIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R4'
        comments['version'] = '20180417'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'AZU', 'your identifier of this room') 

        if m_id == 'AZUR4DIDITABLE':
            comments['version'] = '20180417'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(meta, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(meta, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1090, 1090, 1110, 1110])
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        elif m_id == 'AZUR4DIDIWALL':
            comments['version'] = '20180417'
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       84, 'distance between phantom and image detector')

            _addresult(meta, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(meta, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') # 35 px = 5mm at 0.143 mm resolution
        
    elif 'WKZDIDI' in m_id:
        comments['description'] += 'Philips Digital Diagnost R4'

        if m_id == 'WKZDIDI1':
            comments['version'] = '20180206'
            _addparam(hdr_params, 'roomname', 'WKZ1') 
            _addparam(hdr_params, 'auto_suffix', True) 
            _addparam(hdr_params, 'tablepidmm',    70)
            _addparam(hdr_params, 'wallpidmm',     50)
            _addparam(hdr_params, 'detector_names', 'SN152495;Tafel|SN152508;Wand') 
        elif m_id == 'WKZDIDI2':
            comments['version'] = '20180206'
            _addparam(hdr_params, 'roomname', 'WKZ2') 
            _addparam(hdr_params, 'auto_suffix', True) 
            _addparam(hdr_params, 'tablepidmm',    70)
            _addparam(hdr_params, 'wallpidmm',     50)
            _addparam(hdr_params, 'detector_names', 'SN1525UU;Tafel|SN152590;Wand') 
        elif m_id == 'WKZDIDILOS':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZLOS', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', True, 'add suffix based on detector name or table/wall distinction to results') 
            _addparaminfo(hdr_params, hdr_info, 'pidmm',    0, 'distance between phantom and image detector')
            _addparaminfo(hdr_params, hdr_info, 'detector_names', 'SN1537DT;Klein2|SN143070;Klein2|SN1522YG;Klein2|SN151685;Groot1', 'translation of serial numbers into your identifiers') 
        elif m_id == 'WKZDIDILOS1':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZLOS1', 'your identifier of this room')
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 
            
            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')

            _addresult(meta, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(meta, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='')
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
            _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        elif m_id == 'WKZDIDILOS2':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZLOS2', 'your identifier of this room')
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')

            _addresult(meta, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(meta, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='')
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
            _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        elif m_id == 'WKZDIDITABLE':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname',    'WKZ', 'your identifier of this room') 
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       70, 'distance between phantom and image detector')

            _addresult(meta, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(meta, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1130, 1140, 1160, 1170])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='')
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[73, 73, 73, 73])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
            _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        elif m_id == 'WKZDIDIWALL':
            comments['version'] = '20180206'
            _addparaminfo(hdr_params, hdr_info, 'roomname',    'WKZ', 'your identifier of this room')
            _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

            qc_params = dict(hdr_params)
            qc_info = dict(hdr_info)
            _addparaminfo(qc_params, qc_info, 'pidmm',       50, 'distance between phantom and image detector')

            _addresult(meta, 'Artefacts',
                       display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
            _addresult(meta, 'Uniformity_(%)',
                       display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
        
            _addresult(meta, 'DistanceSourceToDetector (mm)',
                       display_name='SID', display_level=2, description='distance between source and detector', units='mm', minlowhighmax=[1980, 1990, 2010, 2020])
            _addresult(meta, 'FilterMaterial',
                       display_name='Filtration', display_level=2, description='tube filtration', units='')
            _addresult(meta, 'FocalSpot',
                       display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="SMALL")
            _addresult(meta, 'Grid',
                       display_name='Grid', display_level=2, description='scatter grid position', units='', equals="NONE")
            _addresult(meta, 'kVp',
                       display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[125, 125, 125, 125])
        
            # results to show, without action limits (yet)
            _addresult(meta, 'Date of Last Calibration',
                       display_name='CalibrationDate', display_level=2, description='date of last calibration', units='')
            _addresult(meta, 'Exposure (uAs)',
                       display_name='uAs', display_level=2, description='tube load', units='uAs')
            _addresult(meta, 'ExposureTime (us)',
                       display_name='us', display_level=2, description='duration of exposure', units='us')
            _addresult(meta, 'ImageAreaDoseProduct',
                       display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
            _addresult(meta, "Operator's Name",
                       display_name='Operator', display_level=2, description='name of operator', units='')
            _addresult(meta, "CollimatorDown",
                       display_name='CollimatorDown', display_level=1, description='position of down collimator', units='')
            _addresult(meta, "CollimatorLeft",
                       display_name='CollimatorLeft', display_level=1, description='position of left collimator', units='')
            _addresult(meta, "CollimatorRight",
                       display_name='CollimatorRight', display_level=1, description='position of right collimator', units='')
            _addresult(meta, "CollimatorUp",
                       display_name='CollimatorUp', display_level=1, description='position of up collimator', units='')
            _addresult(meta, "EntranceDose_mGy",
                       display_name='EntranceDose', display_level=2, description='entrance dose', units='mGy')
            _addresult(meta, "RelativeXRayExposure",
                       display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
            _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
            _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        if qc_params == {}: qc_params = dict(hdr_params)
        if qc_info == {}: qc_info = dict(hdr_info)
        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

    elif m_id == 'DRXLarge':
        comments['description'] += 'Carestream DRX-Revolution with large detector'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'DRXLARGE', 'your identifier of this room') 
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1250, 'distance between source and image detector')

        _addresult(meta, 'Artefacts',
                   display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 5])
        _addresult(meta, 'Uniformity_(%)',
                   display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 25, 25])
    
        _addresult(meta, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[90., 90., 90., 90.])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'Exposure (uAs)',
                   display_name='uAs', display_level=2, description='tube load', units='uAs')
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(meta, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(meta, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(meta, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(meta, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(meta, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(meta, "artefacts",
                   display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(meta, "uniformity",
                   display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

    elif m_id == 'DRXSmall':
        comments['description'] += 'Carestream DRX-Revolution with small detector'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'DRXSMALL', 'your identifier of this room') 
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'pidmm',    0, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'sidmm', 1250, 'distance between source and image detector')

        _addresult(meta, 'Artefacts',
                   display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 5])
        _addresult(meta, 'Uniformity_(%)',
                   display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 12.5, 12.5])
    
        _addresult(meta, 'Focal Spot(s)',
                   display_name='FocalSpot', display_level=2, description='size of focal spot', units='', equals="0.6")
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[90., 90., 90., 90.])
    
        # results to show, without action limits (yet)
        _addresult(meta, 'Exposure (uAs)',
                   display_name='uAs', display_level=2, description='tube load', units='uAs')
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2')
        _addresult(meta, "Operator's Name",
                   display_name='Operator', display_level=2, description='name of operator', units='')
        _addresult(meta, "DeviceSerialNumber",
                   display_name='DeviceSerialNumber', display_level=1, description='serial number of device', units='')
        _addresult(meta, "SoftwareVersions",
                   display_name='SoftwareVersions', display_level=1, description='software version of device', units='') 
        _addresult(meta, "Sensitivity",
                   display_name='Sensitivity', display_level=2, description='sensitivity', units='')
        _addresult(meta, "RelativeXRayExposure",
                   display_name='RelativeXRayExposure', display_level=2, description='x-ray exposure relative to target exposure', units='')
        _addresult(meta, "artefacts",
                   display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(meta, "uniformity",
                   display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

    elif m_id == 'ElevaAcq':
        comments['description'] += 'Philips MultiDiagnost Eleva'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'WKZEleva', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.262, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')

        _addresult(meta, 'Artefacts',
                   display_name='Artefacts', display_level=2, description='number of isolated artefacts', units='', minlowhighmax=[0, 0, 5, 10])
        _addresult(meta, 'Uniformity_(%)',
                   display_name='Uniformity', display_level=2, description='measure of non-uniformity', units='%', minlowhighmax=[0, 0, 9, 10])

        _addresult(meta, 'DistanceSourceToDetector (mm)',
                   display_name='SID', display_level=2, description='distance between source and detector', units='cm', minlowhighmax=[118, 119, 121, 122])
        _addresult(meta, 'ImageAreaDoseProduct',
                   display_name='DAP', display_level=2, description='dose area product', units='dGycm2', minlowhighmax=[0.79, 0.8, 0.96, 0.97])
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='kV', minlowhighmax=[80., 80., 80., 80.])
    
        # results to show, without action limits (yet)
        _addresult(meta, "artefacts",
                   display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(meta, "uniformity",
                   display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

    elif m_id == 'FD20':
        comments['description'] += 'Philips AlluraXper FD20C'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'ANG1', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.302, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')

        # results to show, without action limits (yet)
        _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

    elif m_id == 'FD2020_F':
        comments['description'] += 'Philips AlluraXper FD2020 Frontal'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'ANG2', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.294, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')

        # results to show, without action limits (yet)
        _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 
        
    elif m_id == 'FD2020_L':
        comments['description'] += 'Philips AlluraXper FD2020 Lateral'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'ANG2', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.249, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  True, 'force mirroring of image before analysis.')

        # results to show, without action limits (yet)
        _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;95;135') 

    elif m_id == 'HCK_WKZ_A':
        comments['description'] += 'Siemens AXIOM Artis Tube A'
        comments['version'] = '20180206'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'HCK_WKZ', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.279, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbeprecropped', '100;1139;0;959', 'use cropping to [xmin_px, xmax_px, ymin_px, ymax_px] before processing')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

        # results to show, without action limits (yet)
        _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

    elif m_id == 'HCK_WKZ_B':
        comments['description'] += 'Siemens AXIOM Artis Tube B'
        comments['version'] = '20180312'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'HCK_WKZ', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'pidmm',    0., 'distance [mm] between phantom and image detector')
        #_addparaminfo(qc_params, qc_info, 'use_pixmm',    0.279, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')

        _addparam(qc_params, 'linepair_type', 'None')
        _addparam(qc_params, 'artefactborderpx', '35;35;35;35') 

        # results to show, without action limits (yet)
        _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

    elif m_id == 'F7':
        comments['description'] += 'Philips Omni Diagnost Eleva'
        comments['version'] = '20180903'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'F7', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'use_pixmm',    0.315, 'override determination of pixel size in mm (on phantom)')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  False, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'skip_cropping',  True, 'do not attempt to crop image.')
        _addparaminfo(qc_params, qc_info, 'artefactborder_is_circle', True, 'interpret artefactborderpx as circle.')
        _addparaminfo(qc_params, qc_info, 'artefactborderpx', '45;45;35;35', 'number of pixels to skip on each side (xmin, xmax, ymin, ymax)') 

        # results to show, without action limits (yet)
        _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(meta, 'linepair_type', 'None')

    elif m_id == 'CiosAlpha':
        comments['description'] += 'Siemens Cios Alpha'
        comments['version'] = '20210202'
        _addparaminfo(hdr_params, hdr_info, 'roomname', 'AZUCiosAlpha', 'your identifier of this room')
        _addparaminfo(hdr_params, hdr_info, 'auto_suffix', False, 'add suffix based on detector name or table/wall distinction to results') 

        qc_params = dict(hdr_params)
        qc_info = dict(hdr_info)
        _addparaminfo(qc_params, qc_info, 'pidmm',    36, 'distance between phantom and image detector')
        _addparaminfo(qc_params, qc_info, 'use_mustbeinverted',  False, 'override determination if inversion of pixel values is needed.')
        _addparaminfo(qc_params, qc_info, 'use_mustbemirrored',  True, 'force mirroring of image before analysis.')
        _addparaminfo(qc_params, qc_info, 'artefactborderpx', '5;5;5;5', 'number of pixels to skip on each side (xmin, xmax, ymin, ymax)') 

        # results to show, without action limits (yet)
        _addresult(meta, "artefacts",
                       display_name='artefacts', display_level=2, description='overview of artefacts', units='')
        _addresult(meta, "uniformity",
                       display_name='uniformity', display_level=2, description='overview of uniformity', units='')

        _addparam(meta, 'linepair_type', 'None')



    comments['usage'] = [
        'params that must be set for both actions below:',
        '  "roomname": "Bucky1" # any identifier',
        'and either (to set autodetection of table/wall stand):'
        '  "tablepidmm": 85 # distance [mm] between phantom on table and image detector',
        '  "wallpidmm": 85 # distance [mm] between phantom on wall and image detector',
        'or (to set forced usage of one distance e.g. for portable detectors):',
        '  "pidmm": 0 # distance [mm] between phantom and image detector',
        'optionally stands can be identified by their detectorId:',
        '  "detector_names": "SN1525UU;Tafel|SN152590;Wand"',
        'optionally the source to detector distance can be set manually (with autodetection of table/wall stand)',
        '  "tablesidmm": 1150 # distance [mm] between source and image detector in table',
        '  "wallsidmm": 2000 # distance [mm] between source and image detector in wall',
        'or (to set forced usage of one distance e.g. for portable detectors):',
        '  "sidmm": 1150 # distance [mm] between source and image detector',
        'the following params can be supplied to override automatic determination:',
            '  "use_pixmm": 0.262 # the size of one pixel in mm (on the phantom)',
            '  "use_mustbeinverted": True # higher pixelvalues should mean less attenuation',
            '  "use_mustbemirrored": True # force mirroring of image before analysis',
        'optionally set the following parameters:',
        '  "auto_suffix": true or false [false]  # to set or suppress auto_suffix generation',
        'optionally set the following parameters:',
        '  "artefactborderpx": l;r;t;b [35;35;35;35]  # number of pixels to skip on each side (xmin, xmax, ymin, ymax)',
    ]

    _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config, 'uniformity_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)
    

def writeMGConfig(m_id, tubefilter, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MG/Flatfield for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}
    
    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    _addresult(meta, 'unif_pct',
               display_name='Non-uniformity', display_level=2, description='Non-uniformity of phantom', units='%')
    _addresult(meta, 'snr_hol',
               display_name='SNR', display_level=2, description='mean to noise ratio in ROI in center chestside of phantom', units='')
    _addresult(meta, 'doseratio',
               display_name='dose ratio', display_level=1, description='ratio of calculated to agd dose', units='')
    _addresult(meta, 'art_clusters',
               display_name='artefacts', display_level=2, description='number of clusters of artefacts', units='')
    _addresult(meta, 'expert_inoutoverin',
               display_name='phantom fraction', display_level=1, description='usable fraction of phantom over image', units='')

    _addparam(hdr_params, 'info', 'qc')
    if m_id == 'Selenia':
        comments['description'] += 'Hologic Selenia'
        comments['version'] = '20220118'

        qc_params = hdr_params.copy()
        qc_info = hdr_info.copy()
        _addparaminfo(qc_params, qc_info, 'art_borderpx_lrtb', '0;0;12;12', 'skip this number of pixels in artefact evaluation for left, right, top, bottom of image') 
        _addparaminfo(qc_params, qc_info, 'dc_offset', '50', 'DC offset correction for SNR calculation') 

        _addresult(meta, 'Grid',
                   display_name='grid', display_level=1, description='is grid used', units='', equals='HTC_IN')
        _addresult(meta, 'ProtocolName',
                   display_name='protocol', display_level=1, description='protocol used', units='', equals='Flat Field')

        _addresult(meta, 'unif_pct',  minlowhighmax=[0., 0., 7.5,  10.])
        _addresult(meta, 'snr_hol',   minlowhighmax=[60., 60., 80., 100.])
        _addresult(meta, 'doseratio', valpaccpcrit=[1., 5., 10.])
        _addresult(meta, 'art_clusters',       minlowhighmax=[0, 0,  0, 1])
        _addresult(meta, 'expert_inoutoverin', minlowhighmax=[-.2, -.1, .5, 1.])

    elif m_id == 'MicrodoseL50':
        comments['description'] += 'Philips Microdose L50'
        comments['version'] = '20161219'

        qc_params = hdr_params.copy()
        qc_info = hdr_info.copy()
        _addparaminfo(qc_params, qc_info, 'art_borderpx_lrtb', '0;0;0;0', 'skip this number of pixels in artefact evaluation for left, right, top, bottom of image') 

        _addresult(meta, 'BodyPartThickness',
                   display_name='thickness', display_level=2, description='compressed thickness', units='', equals='42')
        _addresult(meta, 'kVp',
                   display_name='kVp', display_level=2, description='tube voltage', units='', equals='32')

        _addresult(meta, 'unif_pct',  minlowhighmax=[-5.0, -2.5, 2.5, 5.0])
        _addresult(meta, 'snr_hol',   valpaccpcrit=[23.9, 10., 20.])
        _addresult(meta, 'doseratio', valpaccpcrit=[1., 5., 10.])
        _addresult(meta, 'art_clusters',       minlowhighmax=[0, 0,  0, 1])
        _addresult(meta, 'expert_inoutoverin', minlowhighmax=[-.2, -.1, .5, 1.])

    elif m_id == 'Dimensions':
        comments['description'] += 'Hologic Dimensions'
        comments['version'] = '20220118' # increase t/b/r skip

        qc_params = hdr_params.copy()
        qc_info = hdr_info.copy()
        _addparaminfo(qc_params, qc_info, 'art_borderpx_lrtb', '0;2;19;19', 'skip this number of pixels in artefact evaluation for left, right, top, bottom of image') 
        _addparaminfo(qc_params, qc_info, 'dc_offset', '50', 'DC offset correction for SNR calculation') 

        _addresult(meta, 'Grid',
                   display_name='grid', display_level=1, description='is grid used', units='', equals='IN')
        _addresult(meta, 'ProtocolName',
                   display_name='protocol', display_level=1, description='protocol used', units='', equals='Flat Field Conv')

        _addresult(meta, 'unif_pct',  minlowhighmax=[0., 0., 7.5,  10.])
        _addresult(meta, 'snr_hol',   minlowhighmax=[60., 61., 74., 75.])
        _addresult(meta, 'doseratio', valpaccpcrit=[1., 5., 10.])
        _addresult(meta, 'art_clusters',       minlowhighmax=[0, 0,  0, 1])
        _addresult(meta, 'expert_inoutoverin', minlowhighmax=[-.2, -.1, .5, 1.])

    elif m_id == 'AffirmProne':
        comments['description'] += 'Hologic Affirm Prone'
        comments['version'] = '20220118'

        qc_params = hdr_params.copy()
        qc_info = hdr_info.copy()
        _addparaminfo(qc_params, qc_info, 'art_borderpx_lrtb', '0;4;16;16', 'skip this number of pixels in artefact evaluation for left, right, top, bottom of image') 
        _addparaminfo(qc_params, qc_info, 'dc_offset', '50', 'DC offset correction for SNR calculation') 

        _addresult(meta, 'ProtocolName',
                   display_name='protocol', display_level=1, description='protocol used', units='', equals='PMMA 4,5 cm')

        _addresult(meta, 'unif_pct',  minlowhighmax=[0., 0., 7.5,  10.])
        _addresult(meta, 'snr_hol',   minlowhighmax=[80., 81., 99., 100.])
        _addresult(meta, 'doseratio', valpaccpcrit=[1., 5., 10.])
        _addresult(meta, 'art_clusters',       minlowhighmax=[0, 0,  0, 1])
        _addresult(meta, 'expert_inoutoverin', minlowhighmax=[-.2, -.1, .5, 1.])

    _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)

def writeMRConfig(m_id, series, filename=None):
    if filename is None:
        filename = 'config_'+m_id+'.json'

    comments = {
        "description": "MR/PIQT for ",
        "version": __version__,
        "author": "Arnold Schilham, UMCU",
        "creator": "generate_config_json.py version "+str(__version__)
    }

    config = { "cfgformat": CFGFORMAT, "comments": comments }
    meta = { "metaformat": METAFORMAT,}
    hdr_params = {}
    hdr_info = {}
    qc_params = {}
    qc_info = {}
    
    _addaction(config, 'acqdatetime', params={}, filters={}, info={})

    # declare all results here in order of appearance. Details can be added/altered later on
    _addresult(meta, 'AcquisitionDateTime', display_name='DateTime', display_level=2, 
               description='date and time of acquisition', units='', period=14)# every 14 days

    if series == 'QA1':
        range1 = ['3_1_30', '3_2_100']
        range2 = ['2_1_30']
        range3 = ['4_1_30', '4_2_100']
        range4 = ['5_1_30', '5_2_100']
    elif series == 'QA2':
        range1 = ['2_1_15']
        range2 = ['3_1_15']
        range3 = []
        range4 = []
    elif series == 'QA3':
        range1 = ['1_1_50', '1_2_100', '1_3_150']
        range2 = []
        range3 = []
        range4 = []

    if series == 'QA1': # just add to user view, no limits
        _addresult(meta, 'Central_freq_QA1_2_1_30',
                   display_name='Central_freq_QA1_2_1_30', display_level=DISPLAY_USER, description='Central Frequency', units='MHz')

    for model in range1:
        _addresult(meta, 'Patients Name_%s_%s'%(series, model),
                   display_name='patient name_%s'%model, display_level=DISPLAY_KEYUSER, description='patient name', units='', equals='PIQT')
        _addresult(meta, 'S/N (B)_%s_%s'%(series, model),
                   display_name='S/N (B)_%s'%model, display_level=DISPLAY_USER, description='signal to noise ratio', units='%')
        _addresult(meta, 'C-10/C+10_%s_%s'%(series, model),
                   display_name='C-10/C+10_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')
        _addresult(meta, 'Rad 10%%_%s_%s'%(series, model),
                   display_name='Rad 10%%_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')
        _addresult(meta, 'T/C-20_%s_%s'%(series, model),
                   display_name='T/C-20_%s'%model, display_level=DISPLAY_USER, description='uniformity measure', units='%')
        _addresult(meta, 'C-20/C-10_%s_%s'%(series, model),
                   display_name='C-20/C-10_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')

        if series == 'QA1':     
            _addresult(meta, 'T/C-20_%s_%s'%(series, model),
                           display_name='T/C-20_%s'%model, display_level=DISPLAY_USER, description='uniformity measure', units='%')
            _addresult(meta, 'C+10/C+20_%s_%s'%(series, model),
                       display_name='C+10/C+20_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')
        else:
            _addresult(meta, 'T/C-20_%s_%s'%(series, model),
                           display_name='T/C-20_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')
            _addresult(meta, 'C+10/C+20_%s_%s'%(series, model),
                               display_name='C+10/C+20_%s'%model, display_level=DISPLAY_USER, description='uniformity measure', units='%')

        _addresult(meta, 'C+20/Max_%s_%s'%(series, model),
                   display_name='C+20/Max_%s'%model, display_level=DISPLAY_KEYUSER, description='uniformity measure', units='%')
        _addresult(meta, 'FFU__%s_%s'%(series, model),
                   display_name='FFU__%s'%model, display_level=DISPLAY_USER, description='Flood Field Uniformity', units='')

    for model in range2:
        _addresult(meta, 'Patients Name_%s_%s'%(series, model),
                   display_name='patient name_%s'%model, display_level=DISPLAY_KEYUSER, description='patient name', units='', equals='PIQT')
        
        if series == 'QA1':
            _addresult(meta, 'phant_rot_QA1_%s'%model,
                       display_name='phant rot_%s'%model, display_level=DISPLAY_KEYUSER, description='phantom rotation', units='degrees')
            #_addresult(meta, 'm/p_angle_QA1_%s'%model,
            #           display_name='m/p angle_%s'%model, display_level=1, description='m/p angle', units='')
            _addresult(meta, 'size_hor_QA1_%s'%model,
                       display_name='hor size_%s'%model, display_level=DISPLAY_USER, description='horizontal size', units='mm')
            _addresult(meta, 'size_ver_QA1_%s'%model,
                       display_name='ver size_%s'%model, display_level=DISPLAY_USER, description='vertical size', units='mm')

            _addresult(meta, 'hor_int_av_QA1_%s'%model,
                       display_name='hor int av_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='mm')
            _addresult(meta, 'hor_int_dev_QA1_%s'%model,
                       display_name='hor int dev_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='mm')
            _addresult(meta, 'hor_max_right_QA1_%s'%model,
                       display_name='hor max right_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='mm')
            _addresult(meta, 'hor_max_left_QA1_%s'%model,
                       display_name='hor max left_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='mm')
            _addresult(meta, 'hor_diff_av_QA1_%s'%model,
                       display_name='hor diff av_%s'%model, display_level=DISPLAY_USER, description='horizontal geometry measure', units='%')
            _addresult(meta, 'hor_diff_dev_QA1_%s'%model,
                       display_name='hor diff dev_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='%')
            _addresult(meta, 'hor_max_QA1_%s'%model,
                       display_name='hor max_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='%')
            _addresult(meta, 'hor_min_QA1_%s'%model,
                       display_name='hor min_%s'%model, display_level=DISPLAY_KEYUSER, description='horizontal geometry measure', units='%')

            _addresult(meta, 'ver_int_av_QA1_%s'%model,
                       display_name='ver int av_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='mm')
            _addresult(meta, 'ver_int_dev_QA1_%s'%model,
                       display_name='ver int dev_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='mm')
            _addresult(meta, 'ver_max_up_QA1_%s'%model,
                       display_name='ver max up_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='mm')
            _addresult(meta, 'ver_max_down_QA1_%s'%model,
                       display_name='ver max down_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='mm')
            _addresult(meta, 'ver_diff_av_QA1_%s'%model,
                       display_name='ver diff av_%s'%model, display_level=DISPLAY_USER, description='vertical geometry measure', units='%')
            _addresult(meta, 'ver_diff_dev_QA1_%s'%model,
                       display_name='ver diff dev_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='%')
            _addresult(meta, 'ver_max_QA1_%s'%model,
                       display_name='ver max_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='%')
            _addresult(meta, 'ver_min_QA1_%s'%model,
                       display_name='ver min_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='%')
            _addresult(meta, 'lin_NEMA_QA1_%s'%model,
                        display_name='lin_NEMA_%s'%model, display_level=DISPLAY_KEYUSER, description='vertical geometry measure', units='%')
        
            _addresult(meta, 'LIN__%s_%s'%(series, model),
                        display_name='LIN__%s'%model, display_level=DISPLAY_USER, description='Spatial linearity', units='')

        elif series == 'QA2':
            _addresult(meta, 'FWHM_QA2_%s'%model,
                       display_name='FWHM_%s'%model, display_level=DISPLAY_KEYUSER, description='full width half maximum', units='mm')
            _addresult(meta, 'Slice_int_QA2_%s'%model,
                       display_name='Slice_int_%s'%model, display_level=DISPLAY_KEYUSER, description='slice integral', units='mm')
            _addresult(meta, 'SLP__%s_%s'%(series, model),
                       display_name='SLP__%s'%model, display_level=DISPLAY_USER, description='Slice Profile', units='')
            
    for model in range3:
        if series == 'QA1':
            _addresult(meta, 'Patients Name_%s_%s'%(series, model),
                       display_name='patient name_%s'%model, display_level=DISPLAY_KEYUSER, description='patient name', units='', equals='PIQT')

            _addresult(meta, 'FWHM_QA1_%s'%model,
                       display_name='FWHM_%s'%model, display_level=DISPLAY_KEYUSER, description='full width half maximum', units='mm')
            _addresult(meta, 'FWTM_QA1_%s'%model,
                       display_name='FWTM_%s'%model, display_level=DISPLAY_USER, description='full width tenth maximum', units='mm')
            _addresult(meta, 'Slice_int_QA1_%s'%model,
                       display_name='Slice_int_%s'%model, display_level=DISPLAY_KEYUSER, description='slice integral', units='mm')
            #_addresult(meta, 'Phase_Shift_QA1_%s'%model,
            #           display_name='Phase_Shift_%s'%model, display_level=1, description='phase shift', units='')
            _addresult(meta, 'SLP__%s_%s'%(series, model),
                        display_name='SLP__%s'%model, display_level=DISPLAY_USER, description='Slice Profile', units='')
    
    for model in range4:
        if series == 'QA1':
            _addresult(meta, 'Patient Name_%s_%s'%(series, model),
                       display_name='patient name_%s'%model, display_level=DISPLAY_KEYUSER, description='patient name', units='', equals='PIQT')
            _addresult(meta, 'Hor_pxl_size_QA1_%s'%model,
                       display_name='Hor_pxl_size_%s'%model, display_level=DISPLAY_USER, description='horizontal pixel size', units='mm')
            _addresult(meta, 'Ver_pxl_size_QA1_%s'%model,
                       display_name='Ver_pxl_size_%s'%model, display_level=DISPLAY_USER, description='vertical pixel size', units='mm')
            _addresult(meta, 'MTF__%s_%s'%(series, model),
                       display_name='MTF__%s'%model, display_level=DISPLAY_USER, description='Spatial Resolution', units='')


    if m_id == 'Achieva15sHB': # uses receive/transmit ='SENSE-Head-8'/B
        comments['description'] += 'Philips Achieva 1.5T receive/transmit=SENSE-Head-8/B'
        comments['specfiles'] = 'S: t15r3v2l3_gr4_rf0_l01_nt.spec, C: t15r3v2l3_gr4_rf0_l01_nt.cust'
        comments['version'] = '20181105'

        if series == 'QA1':
            # range1
            _addresult(meta, 'S/N (B)_QA1_3_1_30', minlowhighmax=[125., 135., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA1_3_2_100',minlowhighmax=[96., 101., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[61., 71., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[61., 71., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_1_30', minlowhighmax=[45., 60., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_2_100',minlowhighmax=[45., 60., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA1_3_1_30', minlowhighmax=[None, None, 7., 9.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA1_3_2_100',minlowhighmax=[None, None, 7., 9.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[None, None, 9., 13.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[None, None, 9., 13.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[None, None, 15., 20.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[None, None, 15., 20.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_1_30', minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_2_100',minlowhighmax=[None, None, 4., 5.]) # only crit_high given

            # range2
            _addresult(meta, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(meta, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(meta, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5])
            
            _addresult(meta, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_right_QA1_2_1_30',minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_left_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2.0, -1.0, None, None]) # only crit_low given
        
            _addresult(meta, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_down_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(meta, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.0, -1.0, None, None]) # only crit_low given

            # range3
            _addresult(meta, 'FWHM_QA1_4_1_30', minlowhighmax=[4.65, 4.70, 5.10, 5.15]) 
            _addresult(meta, 'FWHM_QA1_4_2_100',minlowhighmax=[4.30, 4.35, 4.75, 4.80]) 
            _addresult(meta, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 7., 8.]) # before 20180418 [0., 0., 4., 8.])
            _addresult(meta, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 7., 8.]) # before 20180418 [0., 0., 4., 8.])
        
            _addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[4.55, 4.65, 4.95, 5.05]) 
            _addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[4.30, 4.35, 4.75, 4.80]) 
        
            #_addresult(meta, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[None, None, 10., 15.]) # only crit_high given
            #_addresult(meta, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[None, None, 15., 20.]) # only crit_high given

            # range4
            _addresult(meta, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given

        elif series == 'QA2':
            # range1
            _addresult(meta, 'S/N (B)_QA2_2_1_15', minlowhighmax=[89., 95., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[61., 71., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA2_2_1_15', minlowhighmax=[45., 60., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA2_2_1_15', minlowhighmax=[None, None, 7., 9.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[None, None, 9., 13.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[None, None, 15., 20.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA2_2_1_15', minlowhighmax=[None, None, 4., 5.]) # only crit_high given

            # range2
            _addresult(meta, 'FWHM_QA2_3_1_15', minlowhighmax=[4.80, 4.85, 5.25, 5.30]) 
            _addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[4.85, 4.95, 5.25, 5.35]) 
            
        elif series == 'QA3':
            # range1
            _addresult(meta, 'S/N (B)_QA3_1_1_50', minlowhighmax=[60., 60., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_2_100',minlowhighmax=[52., 52., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_3_150',minlowhighmax=[42., 42., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_1_50', minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_2_100',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_3_150',minlowhighmax=[86., 92., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA3_1_1_50', minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_2_100',minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_3_150',minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[None, None, 2., 3.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[None, None, 2., 3.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_1_50', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_3_150',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
    
    elif m_id == 'Intera15HB': # uses receive/transmit ='Head'/B
        comments['description'] += 'Philips Intera 1.5T receive/transmit=Head/B'
        comments['specfiles'] = 'S : acsr11v8l1_03_l01_nt.spec C : none'
        comments['version'] = '20181105'

        if series == 'QA1':
            # range1
            _addresult(meta, 'S/N (B)_QA1_3_1_30', minlowhighmax=[92., 97., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA1_3_2_100',minlowhighmax=[64., 68., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_1_30', minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_2_100',minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA1_3_1_30', minlowhighmax=[None, None, 5., 6.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA1_3_2_100',minlowhighmax=[None, None, 5., 6.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_1_30', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given

            # range2
            _addresult(meta, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(meta, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(meta, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.75, 150.25, 150.5]) 
            _addresult(meta, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_right_QA1_2_1_30',minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_left_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2.0, -1.0, None, None]) # only crit_low given
            _addresult(meta, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_down_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(meta, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.0, -1.0, None, None]) # only crit_low given

            # range3
            _addresult(meta, 'FWHM_QA1_4_1_30', minlowhighmax=[4.70, 4.80, 5.10, 5.20]) 
            _addresult(meta, 'FWHM_QA1_4_2_100',minlowhighmax=[4.40, 4.55, 4.75, 4.90]) 
            _addresult(meta, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 4., 8.]) 
            _addresult(meta, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 4., 8.]) 
            _addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[4.60, 4.75, 4.95, 5.10]) 
            _addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[4.40, 4.55, 4.75, 4.90]) 
            #_addresult(meta, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[None, None, 10., 15.]) # only crit_high given
            #_addresult(meta, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[None, None, 10., 15.]) # only crit_high given
        
            # range4
            _addresult(meta, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.0, 1.2]) # only crit_high given
            _addresult(meta, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.0, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.0, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.0, 1.2]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(meta, 'S/N (B)_QA2_2_1_15', minlowhighmax=[60., 64., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA2_2_1_15', minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA2_2_1_15', minlowhighmax=[None, None, 5., 6.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA2_2_1_15', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given

            # range2
            _addresult(meta, 'FWHM_QA2_3_1_15', minlowhighmax=[4.80, 4.85, 5.25, 5.30]) 
            _addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[4.90, 5.05, 5.25, 5.40]) 
        elif series == 'QA3':
            # range1
            _addresult(meta, 'S/N (B)_QA3_1_1_50', minlowhighmax=[60., 62., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_2_100',minlowhighmax=[52., 52., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_3_150',minlowhighmax=[42., 42., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_1_50', minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_2_100',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_3_150',minlowhighmax=[86., 92., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA3_1_1_50', minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_2_100',minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_3_150',minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[None, None, 2., 3.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[None, None, 2., 3.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_1_50', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_3_150',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given

    elif m_id == 'Panorama10sHB': # uses receive/transmit = 'SENSE-Head-8'/B
        comments['description'] += 'Philips Panorama HFO 1.0T receive/transmit=SENSE-Head-8/B'
        comments['specfiles'] = 'S: p10r2v6l3_02_l01_nt.spec, C: p10r2v6l3_02_l13_nt.cust'
        comments['version'] = '20181105'

        if series == 'QA1':
            # range1
            _addresult(meta, 'S/N (B)_QA1_3_1_30', minlowhighmax=[174., 185., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA1_3_2_100',minlowhighmax=[131., 138., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[35., 37., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[35., 37., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_1_30', minlowhighmax=[33., 35., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_2_100',minlowhighmax=[33., 35., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA1_3_1_30', minlowhighmax=[None, None, 11., 12.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA1_3_2_100',minlowhighmax=[None, None, 11., 12.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[None, None,  9., 10.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[None, None,  9., 10.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[None, None, 11., 22.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[None, None, 11., 22.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_1_30', minlowhighmax=[None, None, 25., 28.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_2_100',minlowhighmax=[None, None, 25., 28.]) # only crit_high given

            # range2
            _addresult(meta, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(meta, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(meta, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5]) 
            _addresult(meta, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_right_QA1_2_1_30',minlowhighmax=[None, None, 1.00, 1.5]) # only crit_high given
            _addresult(meta, 'hor_max_left_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -0.5, 0.50, 1.0]) 
            _addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2., -1.0, None, None]) # only crit_low given
            _addresult(meta, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[None, None, 1.50, 2.0]) # only crit_high given
            _addresult(meta, 'ver_max_down_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(meta, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 1.00, 1.5]) # only crit_high given
            _addresult(meta, 'ver_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.50, 2.5]) # only crit_high given
            _addresult(meta, 'ver_min_QA1_2_1_30',      minlowhighmax=[-3.5, -2.5, None, None]) # only crit_low given

            # range3
            _addresult(meta, 'FWHM_QA1_4_1_30', minlowhighmax=[4.55, 4.60, 5.00, 5.05]) 
            _addresult(meta, 'FWHM_QA1_4_2_100',minlowhighmax=[4.25, 4.30, 4.70, 4.75]) 
            _addresult(meta, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 7., 8.]) # before 20180418 [0., 0., 4., 8.]) 
            _addresult(meta, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 7., 8.]) # before 20180418 [0., 0., 4., 8.]) 
            _addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[4.55, 4.65, 4.95, 5.05]) 
            _addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[4.25, 4.35, 4.65, 4.75]) 
            #_addresult(meta, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[None, None, 10., 15.]) # only crit_high given
            #_addresult(meta, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[None, None, 10., 15.]) # only crit_high given

            # range4
            _addresult(meta, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(meta, 'S/N (B)_QA2_2_1_15', minlowhighmax=[125., 135., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[33., 35., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA2_2_1_15', minlowhighmax=[30., 32.,  None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA2_2_1_15', minlowhighmax=[None, None, 11., 12.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[None, None, 10., 11.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[None, None, 21., 22.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA2_2_1_15', minlowhighmax=[None, None, 25., 28.]) # only crit_high given

            # range2
            _addresult(meta, 'FWHM_QA2_3_1_15', minlowhighmax=[4.70, 4.75, 5.15, 5.20]) 
            _addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[4.75, 4.85, 5.15, 5.25]) 
        elif series == 'QA3':
            # range1
            _addresult(meta, 'S/N (B)_QA3_1_1_50', minlowhighmax=[ 49.,  51., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_2_100',minlowhighmax=[ 41.,  43., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 35.,  37., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[85., 87., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[85., 87., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[82., 84., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_1_50', minlowhighmax=[70., 75., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_2_100',minlowhighmax=[60., 65., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_3_150',minlowhighmax=[55., 60., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA3_1_1_50', minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_2_100',minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_3_150',minlowhighmax=[None, None,  5.,  6.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[None, None,  6.,  7.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[None, None,  7.,  8.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[None, None,  9., 10.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[None, None,  4.,  6.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[None, None,  4.,  6.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[None, None,  6.,  7.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_1_50', minlowhighmax=[None, None,  1.,  2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_2_100',minlowhighmax=[None, None,  1.,  2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_3_150',minlowhighmax=[None, None,  1.,  2.]) # only crit_high given

    elif m_id == 'MR7_l01HS': #older version used receive/transmit = 'T/R-head'/S, now uses 'SENSE-Head-8'/B
        comments['description'] += 'Philips Achieva 3.0T receive/transmit=T/R-head/S'
        comments['specfiles'] = 'S: t30r3v2l3_gr5_rf0_l02_nt.spec, C: t30r3v2l3_gr5_rf0_l02_nt.cust'
        comments['version'] = '20181105'

        if series == 'QA1':
            # range1
            _addresult(meta, 'S/N (B)_QA1_3_1_30', minlowhighmax=[106., 107., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 78.,  79., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[92., 94., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_1_30', minlowhighmax=[88., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_2_100',minlowhighmax=[87., 88., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA1_3_1_30', minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA1_3_2_100',minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[None, None,  2.5,  3.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[None, None,  4.5,  5.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_1_30', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given

            # range2
            _addresult(meta, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(meta, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(meta, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.75, 150.25, 150.5]) 
            _addresult(meta, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_right_QA1_2_1_30',minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_left_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -0.5, 0.50, 1.0]) 
            _addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2., -1.0, None, None]) # only crit_low given
            _addresult(meta, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_down_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(meta, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.5, -1.5, None, None]) # only crit_low given

            # range3
            _addresult(meta, 'FWHM_QA1_4_1_30', minlowhighmax=[1.85, 1.94, 1.96, 2.05]) 
            _addresult(meta, 'FWHM_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            _addresult(meta, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 3., 4.]) 
            _addresult(meta, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 3., 4.]) 
            _addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.85, 1.94, 1.96, 2.05]) 
            _addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            #_addresult(meta, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[None, None, 7.5, 10.]) # only crit_high given
            #_addresult(meta, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[None, None, 7.5, 10.]) # only crit_high given
        
            # range4
            _addresult(meta, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.0, 1.2]) # only crit_high given
            _addresult(meta, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.0, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.0, 1.3]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.0, 1.3]) # only crit_high given

        elif series == 'QA2':
            # range1
            _addresult(meta, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 72.,  73., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA2_2_1_15', minlowhighmax=[87., 88., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA2_2_1_15', minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[None, None,  4.5,  5.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA2_2_1_15', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given

            # range2
            _addresult(meta, 'FWHM_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.10]) 
            _addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.10]) 
        elif series == 'QA3':
            # range1
            _addresult(meta, 'S/N (B)_QA3_1_1_50', minlowhighmax=[ 92.,  93., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_2_100',minlowhighmax=[ 81.,  82., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 63.,  64., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[91., 93., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[83., 85., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_1_50', minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_2_100',minlowhighmax=[88., 90., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_3_150',minlowhighmax=[84., 86., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA3_1_1_50', minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_2_100',minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_3_150',minlowhighmax=[None, None, 4., 5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[None, None,  3.5,  4.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[None, None,  4.5,  5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[None, None, 10.0, 12.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_1_50', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_3_150',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given

    elif m_id == 'Achieva30sHB': #newer software uses receive/transmit = 'SENSE-Head-8'/B
        comments['description'] += 'Philips Achieva 3.0T receive/transmit=SENSE-Head-8/B'
        comments['specfiles'] = 'S: t30r3v2l3_gr5_rf0_l02_nt.spec, C: t30r3v2l3_gr5_rf0_l02_nt.cust'
        comments['version'] = '20181105'

        if series == 'QA1':
            # range1
            _addresult(meta, 'S/N (B)_QA1_3_1_30', minlowhighmax=[102., 104., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 77.,  79., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[60., 62., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[60., 62., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 42., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 42., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA1_3_1_30', minlowhighmax=[None, None, 13., 14.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA1_3_2_100',minlowhighmax=[None, None, 13., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[None, None, 16., 18.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[None, None, 16., 18.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[None, None, 11., 12.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[None, None, 11., 12.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_1_30', minlowhighmax=[None, None, 3.0, 4.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_2_100',minlowhighmax=[None, None, 3.0, 4.]) # only crit_high given

            # range2
            _addresult(meta, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(meta, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(meta, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.75, 150.25, 150.5]) 
            _addresult(meta, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_right_QA1_2_1_30',minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_left_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -0.5, 0.50, 1.0]) 
            _addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2., -1.0, None, None]) # only crit_low given
            _addresult(meta, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_down_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(meta, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.5, -1.5, None, None]) # only crit_low given

            # range3
            _addresult(meta, 'FWHM_QA1_4_1_30', minlowhighmax=[1.85, 1.94, 1.96, 2.05]) 
            _addresult(meta, 'FWHM_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            _addresult(meta, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 3., 4.]) 
            _addresult(meta, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 3., 4.]) 
            #_addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.85, 1.94, 1.96, 2.05]) 
            #_addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            #_addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.10]) 
            _addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.8, 1.9, 2.2, 2.2]) 
            _addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.9, 1.9, 2.2, 2.2]) 
            #_addresult(meta, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[None, None, 10., 15.]) # only crit_high given
            #_addresult(meta, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[None, None, 10., 15.]) # only crit_high given

            # range4
            _addresult(meta, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.0, 1.2]) # only crit_high given
            _addresult(meta, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.0, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.0, 1.3]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.0, 1.3]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(meta, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 73.,  75., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[60., 62., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA2_2_1_15', minlowhighmax=[40., 42., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA2_2_1_15', minlowhighmax=[None, None, 13., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[None, None, 16., 18.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[None, None, 11., 12.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA2_2_1_15', minlowhighmax=[None, None, 3.0, 4.]) # only crit_high given

            # range2
            _addresult(meta, 'FWHM_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.10]) 
            _addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[2.2, 2.2, 2.5, 2.5]) 
        elif series == 'QA3':
            # range1
            _addresult(meta, 'S/N (B)_QA3_1_1_50', minlowhighmax=[115., 117., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_2_100',minlowhighmax=[100., 102., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 80.,  82., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_1_50', minlowhighmax=[85., 87., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_2_100',minlowhighmax=[85., 87., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_3_150',minlowhighmax=[70., 72., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA3_1_1_50', minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_2_100',minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_3_150',minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[None, None,  3.,  5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[None, None,  3.,  5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[None, None,  3.,  5.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[None, None, 1.5,  2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[None, None, 1.5,  2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[None, None, 2.0,  3.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_1_50', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_3_150',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given

    elif m_id == 'Achieva30sHS': # receive/transmit ='SENSE-Head-8'/S
        comments['description'] += 'Philips Achieva 3.0T receive/transmit=SENSE-Head-8/S'
        comments['specfiles'] = 'S: t30r3v2l3_gr8_rf1_l02_nt.spec, S: t30r3v2l3_gr8_rf1_l03_nt.spec'
        comments['version'] = '20181105'

        if series == 'QA1':
            # range1
            _addresult(meta, 'S/N (B)_QA1_3_1_30', minlowhighmax=[102., 120., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 77.,  90., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[60., 70., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[60., 70., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 60., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 60., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA1_3_1_30', minlowhighmax=[None, None, 13., 14.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA1_3_2_100',minlowhighmax=[None, None, 13., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[None, None, 15., 18.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[None, None, 15., 18.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[None, None, 10., 12.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[None, None, 10., 12.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_1_30', minlowhighmax=[None, None, 3.0, 4.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_2_100',minlowhighmax=[None, None, 3.0, 4.]) # only crit_high given

            # range2
            _addresult(meta, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(meta, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(meta, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5]) 
            _addresult(meta, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_right_QA1_2_1_30',minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_left_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -0.5, 0.50, 1.0]) 
            _addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2., -1.0, None, None]) # only crit_low given
            _addresult(meta, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_down_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(meta, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.25]) # only crit_high given
            _addresult(meta, 'ver_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.5, -1.5, None, None]) # only crit_low given

            # range3
            _addresult(meta, 'FWHM_QA1_4_1_30', minlowhighmax=[1.85, 1.92, 1.97, 2.05]) 
            _addresult(meta, 'FWHM_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            _addresult(meta, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 3.5, 4.]) # before 20180418 [0., 0., 3., 4.])
            _addresult(meta, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 3.5, 4.]) # before 20180418 [0., 0., 3., 4.])
            _addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.85, 1.92, 1.97, 2.05]) 
            _addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 2.10]) 
            #_addresult(meta, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[None, None, 10., 15.]) # only crit_high given
            #_addresult(meta, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[None, None, 20., 25.]) # only crit_high given

            # range4
            _addresult(meta, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.2, 1.3]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.2, 1.3]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(meta, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 73.,  85., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[60., 70., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA2_2_1_15', minlowhighmax=[40., 60., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA2_2_1_15', minlowhighmax=[None, None, 13., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[None, None, 15., 18.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[None, None, 10., 12.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA2_2_1_15', minlowhighmax=[None, None, 3.0, 4.]) # only crit_high given

            # range2
            _addresult(meta, 'FWHM_QA2_3_1_15', minlowhighmax=[1.85, 1.92, 1.97, 2.10]) 
            _addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.50]) 
        elif series == 'QA3':
            # range1
            _addresult(meta, 'S/N (B)_QA3_1_1_50', minlowhighmax=[115., 115., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_2_100',minlowhighmax=[100., 100., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 65.,  65., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_1_50', minlowhighmax=[85., 90., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_2_100',minlowhighmax=[85., 90., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_3_150',minlowhighmax=[70., 80., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA3_1_1_50', minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_2_100',minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_3_150',minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[None, None,  2.,  3.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[None, None,  2.,  3.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[None, None,  2.,  3.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[None, None, 1.5,  2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[None, None, 1.5,  2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[None, None, 2.5,  3.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_1_50', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_3_150',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given

    elif m_id == 'Achieva70HS': # receive/transmit ='T/R-head'/S
        comments['description'] += 'Philips Achieva 7.0T receive/transmit=T/R-head/S' # copy of Achieva30sHS
        comments['specfiles'] = ''
        comments['version'] = '20181105'

        if series == 'QA1':
            # range1
            _addresult(meta, 'S/N (B)_QA1_3_1_30', minlowhighmax=[102., 120., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 77.,  90., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[60., 70., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[60., 70., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 60., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 60., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA1_3_1_30', minlowhighmax=[None, None, 13., 14.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA1_3_2_100',minlowhighmax=[None, None, 13., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[None, None, 15., 18.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[None, None, 15., 18.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[None, None, 10., 12.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[None, None, 10., 12.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_1_30', minlowhighmax=[None, None, 3.0, 4.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_2_100',minlowhighmax=[None, None, 3.0, 4.]) # only crit_high given

            # range2
            _addresult(meta, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -2.5, 2.5,5.0]) # only crit_high given
            #_addresult(meta, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.5, 90.5, 91.0]) 
            _addresult(meta, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.75, 150.25, 150.5]) 
            _addresult(meta, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_right_QA1_2_1_30',minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_left_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -0.5, 0.50, 1.0]) 
            _addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'hor_min_QA1_2_1_30',      minlowhighmax=[-2., -1.0, None, None]) # only crit_low given
            _addresult(meta, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[None, None, 0.25, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_down_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1.0, -0.5, 0.50, 1.0]) 
            _addresult(meta, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[None, None, 0.50, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_QA1_2_1_30',      minlowhighmax=[None, None, 1.00, 2.0]) # only crit_high given
            _addresult(meta, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2.5, -1.5, None, None]) # only crit_low given

            # range3
            _addresult(meta, 'FWHM_QA1_4_1_30', minlowhighmax=[1.85, 1.92, 1.97, 2.05]) 
            _addresult(meta, 'FWHM_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            _addresult(meta, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 3., 4.]) 
            _addresult(meta, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 3., 4.]) 
            _addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.85, 1.92, 1.97, 2.05]) 
            _addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.75, 1.80, 1.85, 1.90]) 
            #_addresult(meta, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[None, None, 10., 15.]) # only crit_high given
            #_addresult(meta, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[None, None, 20., 25.]) # only crit_high given

            # range4
            _addresult(meta, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.0, 1.2]) # only crit_high given
            _addresult(meta, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.0, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.0, 1.3]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.0, 1.3]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(meta, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 73.,  85., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[60., 70., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA2_2_1_15', minlowhighmax=[40., 60., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA2_2_1_15', minlowhighmax=[None, None, 13., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[None, None, 15., 18.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[None, None, 10., 12.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA2_2_1_15', minlowhighmax=[None, None, 3.0, 4.]) # only crit_high given

            # range2
            _addresult(meta, 'FWHM_QA2_3_1_15', minlowhighmax=[1.85, 1.92, 1.97, 2.05]) 
            _addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.90, 1.97, 2.02, 2.10]) 
        elif series == 'QA3':
            # range1
            _addresult(meta, 'S/N (B)_QA3_1_1_50', minlowhighmax=[115., 120., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_2_100',minlowhighmax=[100., 110., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 80.,  90., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_1_50', minlowhighmax=[85., 90., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_2_100',minlowhighmax=[85., 90., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_3_150',minlowhighmax=[70., 80., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA3_1_1_50', minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_2_100',minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_3_150',minlowhighmax=[None, None,  4.,  5.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[None, None,  2.,  3.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[None, None,  2.,  3.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[None, None,  2.,  3.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[None, None, 1.5,  2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[None, None, 1.5,  2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[None, None, 2.5,  3.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_1_50', minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_2_100',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_3_150',minlowhighmax=[None, None, 1.5, 2.]) # only crit_high given

    elif m_id == 'Ingenia15MB': # receive/transmit=MULTI COIL/B
        comments['description'] += 'Philips Ingenia 1.5T receive/transmit=MULTI COIL/B'
        comments['specfiles'] = 'S: wa15r5v1l7_gr4_rf2_l01_irf.spec, T: wa15r5v1l7_gr4_rf2_l01_irf.typ'
        comments['version'] = '20181105'

        if series == 'QA1':
            # range1
            _addresult(meta, 'S/N (B)_QA1_3_1_30', minlowhighmax=[115., 115., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 87.,  87., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[65., 65., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[65., 65., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 40., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 40., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA1_3_1_30', minlowhighmax=[None, None, 6., 6.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA1_3_2_100',minlowhighmax=[None, None, 6., 6.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[None, None, 4., 4.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[None, None, 4., 4.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[None, None, 20., 20.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[None, None, 20., 20.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_1_30', minlowhighmax=[None, None, 10., 10.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_2_100',minlowhighmax=[None, None, 10., 10.]) # only crit_high given

            # range2
            _addresult(meta, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -5.0, 5.0, 5.0]) # only crit_high given
            #_addresult(meta, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.0, 91.0, 91.0]) 
            _addresult(meta, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5]) 
            _addresult(meta, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            #_addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ None, None, 1.0, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_QA1_2_1_30',      minlowhighmax=[ None, None, 1.0, 1.0]) # only crit_high given
            _addresult(meta, 'hor_min_QA1_2_1_30',      minlowhighmax=[-1., -1., None, None]) # only crit_low given
            _addresult(meta, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ None, None, 1.0, 1.0]) # only crit_high given
            _addresult(meta, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            _addresult(meta, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ None, None, 1.0, 1.15]) # only crit_high given
            _addresult(meta, 'ver_max_QA1_2_1_30',      minlowhighmax=[ None, None, 2.0, 2.0]) # only crit_high given
            _addresult(meta, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2., -2., None, None]) # only crit_low given

            # range3
            _addresult(meta, 'FWHM_QA1_4_1_30', minlowhighmax=[4.65, 4.65, 5.15, 5.25]) 
            _addresult(meta, 'FWHM_QA1_4_2_100',minlowhighmax=[4.40, 4.40, 4.85, 4.95]) 
            _addresult(meta, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 7., 7.]) 
            _addresult(meta, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 7., 7.]) 
            _addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[4.60, 4.60, 5.10, 5.10]) 
            _addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[4.35, 4.35, 4.80, 4.80]) 
            #_addresult(meta, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[None, None, 11., 11.]) # only crit_high given
            #_addresult(meta, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[None, None, 15., 15.]) # only crit_high given

            # range4
            _addresult(meta, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(meta, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 79.,  79., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[65., 65., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA2_2_1_15', minlowhighmax=[40., 40., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA2_2_1_15', minlowhighmax=[None, None, 6., 6.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[None, None, 4., 4.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[None, None, 20., 20.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA2_2_1_15', minlowhighmax=[None, None, 10., 10.]) # only crit_high given

            # range2
            _addresult(meta, 'FWHM_QA2_3_1_15', minlowhighmax=[4.70, 4.70, 5.20, 5.20]) 
            _addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[4.80, 4.80, 5.30, 5.30]) 
        elif series == 'QA3':
            # range1
            _addresult(meta, 'S/N (B)_QA3_1_1_50', minlowhighmax=[ 44.,  44., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_2_100',minlowhighmax=[ 39.,  39., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 33.,  33., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[90., 92., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_1_50', minlowhighmax=[90., 90., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_2_100',minlowhighmax=[90., 90., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_3_150',minlowhighmax=[90., 90., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA3_1_1_50', minlowhighmax=[None, None, 4., 4.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_2_100',minlowhighmax=[None, None, 4., 4.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_3_150',minlowhighmax=[None, None, 4., 4.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[None, None, 2., 2.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[None, None, 2., 2.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[None, None, 2., 2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[None, None,  1.,  1.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[None, None,  1.,  1.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[None, None,  1.,  1.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_1_50', minlowhighmax=[None, None,  1.,  1.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_2_100',minlowhighmax=[None, None,  1.,  1.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_3_150',minlowhighmax=[None, None,  1.,  1.]) # only crit_high given

    elif m_id == 'Ingenia30MS': # receive/transmit=MULTI COIL/S
        comments['description'] += 'Philips Ingenia 3.0T receive/transmit=MULTI COIL/S'
        comments['specfiles'] = 'S: wa30r5v1l7_gr8_rf3_l08_irf.spec, T: wa30r5v1l7_gr8_rf3_l08_irf.typ'
        comments['version'] = '20181105'

        if series == 'QA1':
            # range1
            _addresult(meta, 'S/N (B)_QA1_3_1_30', minlowhighmax=[ 81.,  81., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 59.,  59., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[55., 55., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[55., 55., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 40., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 40., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA1_3_1_30', minlowhighmax=[None, None, 10., 10.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA1_3_2_100',minlowhighmax=[None, None, 10., 10.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[None, None, 14., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[None, None, 14., 14.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[None, None, 20., 20.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[None, None, 20., 20.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_1_30', minlowhighmax=[None, None,  4.,  4.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA1_3_2_100',minlowhighmax=[None, None,  4.,  4.]) # only crit_high given

            # range2
            _addresult(meta, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -5.0, 5.0, 5.0]) # only crit_high given
            #_addresult(meta, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.0, 91.0, 91.0]) 
            _addresult(meta, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5]) 
            _addresult(meta, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            #_addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ None, None, 1.0, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_QA1_2_1_30',      minlowhighmax=[ None, None, 1.0, 1.0]) # only crit_high given
            _addresult(meta, 'hor_min_QA1_2_1_30',      minlowhighmax=[-1., -1., None, None]) # only crit_low given
            _addresult(meta, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ None, None, 1.0, 1.0]) # only crit_high given
            _addresult(meta, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            _addresult(meta, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ None, None, 1.0, 1.0]) # only crit_high given
            _addresult(meta, 'ver_max_QA1_2_1_30',      minlowhighmax=[ None, None, 2.0, 2.0]) # only crit_high given
            _addresult(meta, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2., -2., None, None]) # only crit_low given

            # range3
            _addresult(meta, 'FWHM_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            _addresult(meta, 'FWHM_QA1_4_2_100',minlowhighmax=[1.8, 1.8, 2.0, 2.0]) 
            _addresult(meta, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 4., 4.]) 
            _addresult(meta, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 4., 4.]) 
            #_addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            #_addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.8, 1.8, 2.0, 2.0]) 
            #_addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            _addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.2, 2.2]) 
            _addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.9, 1.9, 2.2, 2.2]) 
            #_addresult(meta, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[None, None, 10., 10.]) # only crit_high given
            #_addresult(meta, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[None, None, 25., 25.]) # only crit_high given

            # range4
            _addresult(meta, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 2.4, 2.4]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.25, 1.25]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 2.5, 2.5]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(meta, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 56.,  56., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[45., 45., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA2_2_1_15', minlowhighmax=[ 0.,  0., 100., 100.]) # only crit_low given # not given!
            _addresult(meta, 'T/C-20_QA2_2_1_15', minlowhighmax=[None, None, 12., 12.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[None, None, 15., 15.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[None, None, 25., 25.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA2_2_1_15', minlowhighmax=[None, None, 12., 12.]) # only crit_high given

            # range2
            _addresult(meta, 'FWHM_QA2_3_1_15', minlowhighmax=[1.9, 1.9, 2.2, 2.2]) 
            _addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[2.2, 2.2, 2.5, 2.5]) 
        elif series == 'QA3':
            # range1
            _addresult(meta, 'S/N (B)_QA3_1_1_50', minlowhighmax=[126., 126., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_2_100',minlowhighmax=[104., 104., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_3_150',minlowhighmax=[ 94.,  94., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[78., 78., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[78., 78., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[78., 78., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_1_50', minlowhighmax=[60., 60., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_2_100',minlowhighmax=[60., 60., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_3_150',minlowhighmax=[60., 60., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA3_1_1_50', minlowhighmax=[None, None,  7.,  7.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_2_100',minlowhighmax=[None, None,  7.,  7.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_3_150',minlowhighmax=[None, None,  7.,  7.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[None, None, 14., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[None, None, 14., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[None, None, 14., 14.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[None, None,  2.,  2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[None, None,  2.,  2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[None, None,  2.,  3.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_1_50', minlowhighmax=[None, None,  2.,  2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_2_100',minlowhighmax=[None, None,  2.,  2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_3_150',minlowhighmax=[None, None,  2.,  2.]) # only crit_high given

    elif m_id == 'Ingenia30MS_YC': # receive/transmit=MULTI COIL/S special for Youth Cohort
        comments['description'] += 'Philips Ingenia 3.0T receive/transmit=MULTI COIL/S'
        comments['specfiles'] = 'S: wa30r5v1l7_gr8_rf3_l08_irf.spec, T: wa30r5v1l7_gr8_rf3_l08_irf.typ, M:20161118'
        comments['version'] = '20181105'

        if series == 'QA1':
            # range1
            _addresult(meta, 'S/N (B)_QA1_3_1_30', minlowhighmax=[ 81.,  81., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA1_3_2_100',minlowhighmax=[ 56.,  59., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_1_30', minlowhighmax=[55., 55., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA1_3_2_100',minlowhighmax=[55., 55., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_1_30', minlowhighmax=[40., 40., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA1_3_2_100',minlowhighmax=[40., 40., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA1_3_1_30', minlowhighmax=[None, None, 10., 10.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA1_3_2_100',minlowhighmax=[None, None, 10., 10.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_1_30', minlowhighmax=[None, None, 14., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA1_3_2_100',minlowhighmax=[None, None, 14., 14.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_1_30', minlowhighmax=[None, None, 20., 20.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA1_3_2_100',minlowhighmax=[None, None, 20., 20.]) # only crit_high given
            #_addresult(meta, 'C+20/Max_QA1_3_1_30', minlowhighmax=[None, None,  4.,  4.]) # only crit_high given
            #_addresult(meta, 'C+20/Max_QA1_3_2_100',minlowhighmax=[None, None,  4.,  4.]) # only crit_high given

            # range2
            _addresult(meta, 'phant_rot_QA1_2_1_30',minlowhighmax=[-5.0, -5.0, 5.0, 5.0]) # only crit_high given
            #_addresult(meta, 'm/p_angle_QA1_2_1_30',minlowhighmax=[89.0, 89.0, 91.0, 91.0]) 
            _addresult(meta, 'size_hor_QA1_2_1_30',minlowhighmax=[149.5, 149.5, 150.5, 150.5]) 
            _addresult(meta, 'hor_int_av_QA1_2_1_30',   minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_int_dev_QA1_2_1_30',  minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_right_QA1_2_1_30',minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_max_left_QA1_2_1_30', minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            #_addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'hor_diff_dev_QA1_2_1_30', minlowhighmax=[ None, None, 1.0, 1.0]) # only crit_high given
            _addresult(meta, 'hor_max_QA1_2_1_30',      minlowhighmax=[ None, None, 1.0, 1.0]) # only crit_high given
            _addresult(meta, 'hor_min_QA1_2_1_30',      minlowhighmax=[-1., -1., None, None]) # only crit_low given
            _addresult(meta, 'ver_int_av_QA1_2_1_30',   minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'ver_int_dev_QA1_2_1_30',  minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_up_QA1_2_1_30',   minlowhighmax=[ None, None, 0.5, 0.5]) # only crit_high given
            _addresult(meta, 'ver_max_down_QA1_2_1_30', minlowhighmax=[ None, None, 1.0, 1.0]) # only crit_high given
            _addresult(meta, 'ver_diff_av_QA1_2_1_30',  minlowhighmax=[-1., -1., 1.0, 1.0]) 
            _addresult(meta, 'ver_diff_dev_QA1_2_1_30', minlowhighmax=[ None, None, 1.0, 1.5]) # only crit_high given
            _addresult(meta, 'ver_max_QA1_2_1_30',      minlowhighmax=[ None, None, 2.0, 2.0]) # only crit_high given
            _addresult(meta, 'ver_min_QA1_2_1_30',      minlowhighmax=[-2., -2., None, None]) # only crit_low given

            # range3
            #_addresult(meta, 'FWHM_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            #_addresult(meta, 'FWHM_QA1_4_2_100',minlowhighmax=[1.8, 1.8, 2.0, 2.0]) 
            _addresult(meta, 'FWTM_QA1_4_1_30', minlowhighmax=[0., 0., 4., 4.]) 
            _addresult(meta, 'FWTM_QA1_4_2_100',minlowhighmax=[0., 0., 4., 4.]) 
            #_addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            #_addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.8, 1.8, 2.0, 2.0]) 
            #_addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[1.9, 1.9, 2.1, 2.1]) 
            _addresult(meta, 'Slice_int_QA1_4_1_30', minlowhighmax=[1.9, 1.9, 2.8, 2.8]) 
            _addresult(meta, 'Slice_int_QA1_4_2_100',minlowhighmax=[1.9, 1.9, 2.8, 2.8]) 
            #_addresult(meta, 'Phase_Shift_QA1_4_1_30', minlowhighmax=[None, None, 10., 10.]) # only crit_high given
            #_addresult(meta, 'Phase_Shift_QA1_4_2_100',minlowhighmax=[None, None, 25., 25.]) # only crit_high given

            # range4
            _addresult(meta, 'Hor_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.2, 1.2]) # only crit_high given
            _addresult(meta, 'Hor_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 2.4, 2.4]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_1_30', minlowhighmax=[None, None, 1.25, 1.25]) # only crit_high given
            _addresult(meta, 'Ver_pxl_size_QA1_5_2_100',minlowhighmax=[None, None, 2.5, 2.5]) # only crit_high given
        elif series == 'QA2':
            # range1
            _addresult(meta, 'S/N (B)_QA2_2_1_15', minlowhighmax=[ 56.,  56., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA2_2_1_15', minlowhighmax=[45., 45., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA2_2_1_15', minlowhighmax=[0., 0., 100., 100.]) # only crit_low given # not given!
            _addresult(meta, 'T/C-20_QA2_2_1_15', minlowhighmax=[None, None, 12., 12.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA2_2_1_15', minlowhighmax=[None, None, 15., 15.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA2_2_1_15', minlowhighmax=[None, None, 25., 25.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA2_2_1_15', minlowhighmax=[None, None, 12., 12.]) # only crit_high given

            # range2
            #_addresult(meta, 'FWHM_QA2_3_1_15', minlowhighmax=[1.9, 1.9, 2.2, 2.2]) 
            _addresult(meta, 'Slice_int_QA2_3_1_15', minlowhighmax=[2.2, 2.2, 2.8, 2.8]) 
        elif series == 'QA3':
            # range1
            _addresult(meta, 'S/N (B)_QA3_1_1_50', minlowhighmax=[55., 55., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_2_100',minlowhighmax=[55., 55., None, None]) # only crit_low given
            _addresult(meta, 'S/N (B)_QA3_1_3_150',minlowhighmax=[40., 40., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_1_50', minlowhighmax=[78., 78., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_2_100',minlowhighmax=[78., 78., None, None]) # only crit_low given
            _addresult(meta, 'C-10/C+10_QA3_1_3_150',minlowhighmax=[78., 78., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_1_50', minlowhighmax=[60., 60., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_2_100',minlowhighmax=[60., 60., None, None]) # only crit_low given
            _addresult(meta, 'Rad 10%_QA3_1_3_150',minlowhighmax=[60., 60., None, None]) # only crit_low given
            _addresult(meta, 'T/C-20_QA3_1_1_50', minlowhighmax=[None, None,  7.,  7.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_2_100',minlowhighmax=[None, None,  7.,  7.]) # only crit_high given
            _addresult(meta, 'T/C-20_QA3_1_3_150',minlowhighmax=[None, None,  7.,  7.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_1_50', minlowhighmax=[None, None, 14., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_2_100',minlowhighmax=[None, None, 14., 14.]) # only crit_high given
            _addresult(meta, 'C-20/C-10_QA3_1_3_150',minlowhighmax=[None, None, 14., 14.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_1_50', minlowhighmax=[None, None,  2.,  2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_2_100',minlowhighmax=[None, None,  2.,  2.]) # only crit_high given
            _addresult(meta, 'C+10/C+20_QA3_1_3_150',minlowhighmax=[None, None,  2.,  3.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_1_50', minlowhighmax=[None, None,  2.,  2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_2_100',minlowhighmax=[None, None,  2.,  2.]) # only crit_high given
            _addresult(meta, 'C+20/Max_QA3_1_3_150',minlowhighmax=[None, None,  2.,  2.]) # only crit_high given

    _addaction(config, 'header_series', params=hdr_params, filters={}, info=hdr_info)
    _addaction(config, 'qc_series', params=qc_params, filters={}, info=qc_info)
    _writeConfig(filename, config, meta)
    

# main
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='WAD Config.json generator version %s'%__version__)
    destfolder = None
    parser.add_argument('-d','--dest',
                        default=destfolder,type=str,
                        help='the destination folder',dest='destfolder')
    
    args = parser.parse_args()
    if args.destfolder is  None:
        parser.print_help()
        sys.exit()

    allMRObjects = [ # machine, outname
        ('Achieva15sHB', 'mr_philips_achieva15_sHB'),
        ('Intera15HB' , 'mr_philips_intera15_HB'),
        ('Panorama10sHB','mr_philips_panorama10_sHB'),
        ('Achieva30sHB', 'mr_philips_achieva30_sHB'),
        ('Achieva30sHS', 'mr_philips_achieva30_sHS'),
        ('Ingenia15MB', 'mr_philips_ingenia15_MB'),
        ('Ingenia30MS', 'mr_philips_ingenia30_MS'),
        ('Ingenia30MS_YC','mr_philips_ingenia30_MS_YC'),
        ('Achieva70HS', 'mr_philips_achieva70_HS'),
    ]

    allObjects = [ # modality, id, outname
        ('CR', 'AZUDIDI','cr_pehamed_philips_didi.json'),
        ('CR', 'WKZFCR1','cr_wellhofer_siemens_fcr1.json'),
        ('CR', 'WKZFCR2','cr_wellhofer_siemens_fcr2.json'),
        ('CR', 'YSIO','cr_wellhofer_siemens_ysio.json'),
        ('CT', 'CT1', 'ct_philips_brilliance64_head.json', 'head'),
        ('CT', 'CT1_iPatient', 'ct_philips_brilliance64ip_head.json', 'head'),
        ('CT', 'CT1_iPatient', 'ct_philips_brilliance64ip_body.json', 'body'),
        ('CT', 'CT2', 'ct_philips_mx8000idt_head.json', 'head'),
        ('CT', 'CT2', 'ct_philips_mx8000idt_body.json', 'body'),
        ('CT', 'CT2leen', 'ct_philips_brilliance64b_head.json', 'head'),
        ('CT', 'CT2_iqon', 'ct_philips_iqon_head.json', 'head'),
        ('CT', 'CT2_iqon', 'ct_philips_iqon_body.json', 'body'),
        ('CT', 'CT2_iqon_MonoE', 'ct_philips_iqon_head_monoe.json', 'head'),
        ('CT', 'CT2_iqon_MonoE', 'ct_philips_iqon_body_monoe.json', 'body'),
        ('CT', 'CT3', 'ct_philips_ict_head.json', 'head'),
        ('CT', 'CT3', 'ct_philips_ict_body.json', 'body'),
        ('CT', 'CT4', 'ct_philips_brilliance16p_head.json', 'head'),
        ('CT', 'CT4F_HeadAa','ct_siemens_force_head_headAa.json', 'head'),
        ('CT', 'CT4F_BodyAa','ct_siemens_force_head_bodyAa.json', 'head'),
        ('CT', 'CT4F_BodyDual','ct_siemens_force_head_dual.json', 'head'),
        ('CT', 'CT4F_BodyPedAa','ct_siemens_force_head_pedAa.json', 'head'),
        ('DX', 'AZUDIDI', 'dx_normi13_philips_umcu.json'),
        ('DX', 'AZUDIDITABLE', 'dx_normi13_philips_umcutable.json'),
        ('DX', 'AZUDIDIWALL', 'dx_normi13_philips_umcuwall.json'),
        ('DX', 'AZUR4DIDITABLE', 'dx_normi13_philipsr4_umcutable.json'),
        ('DX', 'AZUR4DIDIWALL', 'dx_normi13_philipsr4_umcuwall.json'),
        ('DX', 'WKZDIDI1','dx_normi13_philips_wkz1.json'),
        ('DX', 'WKZDIDI2','dx_normi13_philips_wkz2.json'),
        ('DX', 'WKZDIDILOS','dx_normi13_philips_wkzlos.json'),
        ('DX', 'WKZDIDILOS1','dx_normi13_philips_wkzlos1.json'),
        ('DX', 'WKZDIDILOS2','dx_normi13_philips_wkzlos2.json'),
        ('DX', 'WKZDIDITABLE','dx_normi13_philips_wkztable.json'),
        ('DX', 'WKZDIDIWALL', 'dx_normi13_philips_wkzwall.json'),
        ('DXU', 'AZUDIDI', 'dx_uniformity_philips_umcu.json'),
        ('DXU', 'AZUDIDITABLE', 'dx_uniformity_philips_umcutable.json'),
        ('DXU', 'AZUDIDIWALL', 'dx_uniformity_philips_umcuwall.json'),
        ('DXU', 'AZUR4DIDITABLE', 'dx_uniformity_philipsr4_umcutable.json'),
        ('DXU', 'AZUR4DIDIWALL', 'dx_uniformity_philipsr4_umcuwall.json'),
        ('DXU', 'WKZDIDI1','dx_uniformity_philips_wkz1.json'),
        ('DXU', 'WKZDIDI2','dx_uniformity_philips_wkz2.json'),
        ('DXU', 'WKZDIDILOS','dx_uniformity_philips_wkzlos.json'),
        ('DXU', 'WKZDIDILOS1','dx_uniformity_philips_wkzlos1.json'),
        ('DXU', 'WKZDIDILOS2','dx_uniformity_philips_wkzlos2.json'),
        ('DXU', 'WKZDIDITABLE','dx_uniformity_philips_wkztable.json'),
        ('DXU', 'WKZDIDIWALL', 'dx_uniformity_philips_wkzwall.json'),
        ('DX', 'FULL','dx_normi13_uniformity_generic.json'),
        ('DX', 'ElevaAcq', 'rf_normi13_philips_elevaacq.json'),
        ('DX', 'ElevaCine','rf_normi13_philips_elevacine.json'),
        ('DXU', 'ElevaAcq','rf_uniformity_philips_elevaacq.json'),
        ('DX', 'DRXLarge','dx_normi13_carestream_drxlarge.json'),
        ('DX', 'DRXLargeRXT02','dx_normi13_carestream_drxlargerxt02.json'),
        ('DX', 'DRXSmall','dx_normi13_carestream_drxsmall.json'),
        ('DXU', 'DRXLarge','dx_uniformity_carestream_drxlarge.json'),
        ('DXU', 'DRXSmall','dx_uniformity_carestream_drxsmall.json'),
        ('DX', 'FD20', 'xa_normi13_philips_fd20.json'),
        ('DX', 'FD2020_F', 'xa_normi13_philips_fd2020frontal.json'),
        ('DX', 'FD2020_L', 'xa_normi13_philips_fd2020lateral.json'),
        ('DXU', 'FD20', 'xa_uniformity_philips_fd20.json'),
        ('DXU', 'FD2020_F', 'xa_uniformity_philips_fd2020frontal.json'),
        ('DXU', 'FD2020_L', 'xa_uniformity_philips_fd2020lateral.json'),
        ('DX', 'HCK_WKZ_A',  'xa_normi13_siemens_axiom_artisA.json'),
        ('DXU', 'HCK_WKZ_A', 'xa_uniformity_siemens_axiom_artisA.json'),
        ('DXU', 'HCK_WKZ_B', 'xa_uniformity_siemens_axiom_artisB.json'),
        ('DX', 'F7', 'rf_normi13_philips_omnidiagnost.json'),
        ('DXU', 'F7', 'rf_uniformity_philips_omnidiagnost.json'),
        ('DX', 'F7invert', 'rf_normi13_philips_omnidiagnost_invert.json'),
        ('DXU', 'F7invert', 'rf_uniformity_philips_omnidiagnost_invert.json'),
        ('FBIRNQC', 'philips', 'mr_fbirn_qc_philips.json'),
        ('FBIRNB0', 'philips', 'mr_fbirn_b0_philips.json'),
        ('FBIRNB1', 'philips', 'mr_fbirn_b1_philips.json'),
        ('FBIRNSNR', 'philips', 'mr_fbirn_snr_philips.json'),
        ('FBIRNReceive', 'philips', 'mr_fbirn_receive_philips.json'),
        ('MGCD', 'CDMam32','mg_cdmam32_L50.json'),
        ('MG', 'Selenia', 'mg_hologic_selenia_MO.json', 'MO'),
        ('MG', 'Selenia', 'mg_hologic_selenia_RH.json', 'RH'),
        ('MG', 'Dimensions', 'mg_hologic_dimensions_RH.json', 'RH'),
        ('MG', 'Dimensions', 'mg_hologic_dimensions_AG.json', 'AG'),
        ('MG', 'AffirmProne', 'mg_hologic_affirmprone_AG.json', 'AG'),
        ('MG', 'MicrodoseL50', 'mg_philips_L50.json', None),
        ('NM', 'mCT','nm_dailyqc_siemens_mct.json'),
        ('NM', 'spect2','nm_uniformity_siemens_symbia_t2.json'),
        ('OCR', 'epiq', 'ocr_philips_epiq_instance.json'),
        ('OCR', 'Intevo_LEHR', 'ocr_intevo_lehr.json'),
        ('OCR', 'ALOKAG8', 'ocr_alokag8_instance.json'),
        ('OCR', 'Horizon', 'ocr_hologic_horizon_instance.json'),
        ('OCR', 'HorizonHistory', 'ocr_hologic_horizon_hist_instance.json'),
        ('OT', 'input2dicom','input2dicom_example.json'),
        ('RF', 'DDL', 'rf_philips_omni.json'),
        ('US', 'epiq', 'us_philips_epiq_instance.json'),
        ('US', 'epiqeL18_5', 'us_philips_epiqeL18_4_instance.json'),
        ('US', 'epiqL12_3', 'us_philips_epiqL12_3_instance.json'),
        ('US', 'epiqL12_5', 'us_philips_epiqL12_5_instance.json'),
        ('US', 'epiqL15_7io', 'us_philips_epiqL15_7io_instance.json'),
        ('US', 'epiqL17_5', 'us_philips_epiqL17_5_instance.json'),
        ('US', 'iU22', 'us_philips_iu22_instance.json'),
        ('US', 'iU22L9_3', 'us_philips_iu22L9_3_instance.json'),
        ('US', 'iU22L12_5', 'us_philips_iu22L12_5_instance.json'),
        ('US', 'iU22L15_7io', 'us_philips_iu22L15_7io_instance.json'),
        ('US', 'iU22L17_5', 'us_philips_iu22L17_5_instance.json'),
        ('US', 'CX50', 'us_philips_cx50_instance.json'),
        ('US', 'HD11', 'us_philips_hd11_instance.json'),
        ('US', 'VolusonE8', 'us_ge_volusone8_instance.json'),
        ('US', 'ALOKAG8', 'us_aloka_g8_instance.json'),
        ('US2', 'epiq', 'us2_philips_epiq_instance.json'),
        ('US2', 'epiqeL18_5', 'us2_philips_epiqeL18_4_instance.json'),
        ('US2', 'epiqL12_3', 'us2_philips_epiqL12_3_instance.json'),
        ('US2', 'epiqL12_5', 'us2_philips_epiqL12_5_instance.json'),
        ('US2', 'epiqL15_7io', 'us2_philips_epiqL15_7io_instance.json'),
        ('US2', 'epiqL17_5', 'us2_philips_epiqL17_5_instance.json'),
        ('US2', 'epiq_umcu_C5_1', 'us2_umcu_epiq_C5_1_instance.json'),
        ('US2', 'epiq_umcu_C7_2', 'us2_umcu_epiq_C7_2_instance.json'),
        ('US2', 'epiq_umcu_C8_5', 'us2_umcu_epiq_C8_5_instance.json'),
        ('US2', 'epiq_umcu_L12_3', 'us2_umcu_epiq_L12_3_instance.json'),
        ('US2', 'epiq_umcu_L12_5', 'us2_umcu_epiq_L12_5_instance.json'),
        ('US2', 'epiq_umcu_L15_7io', 'us2_umcu_epiq_L15_7io_instance.json'),
        ('US2', 'epiq_umcu_L17_5', 'us2_umcu_epiq_L17_5_instance.json'),
        ('US2', 'epiq_umcu_L12~3', 'us2_umcu_epiq_L12~3_instance.json'),
        ('US2', 'epiq_umcu_eL18_4', 'us2_umcu_epiq_eL18_4_instance.json'),
        ('US2', 'epiq_umcu_mC7_2', 'us2_umcu_epiq_mC7_2_instance.json'),
        ('US2', 'epiq_umcu_XL14_3', 'us2_umcu_epiq_XL14_3_instance.json'),
        ('US2', 'epiq_umcu_X6_1', 'us2_umcu_epiq_X6_1_instance.json'),
        ('US2', 'sequoia', 'us2_siemens_sequoia_instance.json'),
        ('US2', 'sequoia_umcu_10L4', 'us2_umcu_sequoia_10L4_instance.json'),
        ('US2', 'sequoia_umcu_18L6', 'us2_umcu_sequoia_18L6_instance.json'),
        ('US2', 'te7', 'us2_mindray_te7_instance.json'),
        ('US2', 'te7L20_5s', 'us2_mindray_te7_L20_5s_instance.json'),
        
        ('DXHough', 'AZUR4DIDITABLE', 'dx_normi13hough_philipsr4_umcutable.json'),
        ('DXHough', 'AZUR4DIDIWALL',  'dx_normi13hough_philipsr4_umcuwall.json'),
        ('DXHough', 'AZUDIDI',        'dx_normi13hough_philips_umcu.json'),
        ('DXHough', 'AZUDIDITABLE',   'dx_normi13hough_philips_umcutable.json'),
        ('DXHough', 'AZUDIDIWALL',    'dx_normi13hough_philips_umcuwall.json'),
        ('DXHough', 'WKZDIDI1',       'dx_normi13hough_philips_wkz1.json'),
        ('DXHough', 'WKZDIDI2',       'dx_normi13hough_philips_wkz2.json'),
        ('DXHough', 'WKZDIDILOS',     'dx_normi13hough_philips_wkzlos.json'),
        ('DXHough', 'WKZDIDILOS1',    'dx_normi13hough_philips_wkzlos1.json'),
        ('DXHough', 'WKZDIDILOS2',    'dx_normi13hough_philips_wkzlos2.json'),
        ('DXHough', 'WKZDIDITABLE',   'dx_normi13hough_philips_wkztable.json'),
        ('DXHough', 'WKZDIDIWALL',    'dx_normi13hough_philips_wkzwall.json'),
        ('DXHough', 'FULL',           'dx_normi13hough_uniformity_generic.json'),
        ('DXHough', 'ElevaAcq',       'rf_normi13hough_philips_elevaacq.json'),
        ('DXHough', 'ElevaCine',      'rf_normi13hough_philips_elevacine.json'),
        ('DXHough', 'DRXLarge',       'dx_normi13hough_carestream_drxlarge.json'),
        ('DXHough', 'DRXLargeRXT02',  'dx_normi13hough_carestream_drxlargerxt02.json'),
        ('DXHough', 'DRXSmall',       'dx_normi13hough_carestream_drxsmall.json'),
        ('DXHough', 'FD20',           'xa_normi13hough_philips_fd20.json'),
        ('DXHough', 'FD2020_F',       'xa_normi13hough_philips_fd2020frontal.json'),
        ('DXHough', 'FD2020_L',       'xa_normi13hough_philips_fd2020lateral.json'),
        ('DXHough', 'F7',             'rf_normi13hough_philips_omnidiagnost.json'),
        ('DXHough', 'F7invert',       'rf_normi13hough_philips_omnidiagnost_invert.json'),
        ('DXHough', 'CiosAlphaAcq',   'rf_normi13hough_siemens_ciosalpha_acq.json'),
        ('DXHough', 'CiosAlphaCine',  'rf_normi13hough_siemens_ciosalpha_cine.json'),
        ('DXHough', 'CiosAlphaCine',  'rf_normi13hough_siemens_ciosalpha_cine.json'),
        ('DXU',     'CiosAlpha',      'rf_uniformity_siemens_ciosalpha.json'),

        ('CT2', 'PhilipsIQon_Head', 'ct2_philips_iqon_head.json'),
        ('CT2', 'PhilipsIQon_Body', 'ct2_philips_iqon_body.json'),
        ('CT2', 'PhilipsIQon_Head_MonoE', 'ct2_philips_iqon_head_monoe.json'),
        ('CT2', 'PhilipsIQon_Body_MonoE', 'ct2_philips_iqon_body_monoe.json'),
        ('CT2', 'PhilipsiCT_Head', 'ct2_philips_ict_head.json'),
        ('CT2', 'PhilipsiCT_Body', 'ct2_philips_ict_body.json'),
        ('CT2', 'PhilipsB64IP_Head', 'ct2_philips_brilliance64ip_head.json'),
        ('CT2', 'PhilipsB64IP_Body', 'ct2_philips_brilliance64ip_body.json'),
        ('CT2', 'Force_Head_Aa','ct2_siemens_force_head_Aa.json', 'head'),
        ('CT2', 'Force_Body_Aa','ct2_siemens_force_body_Aa.json', 'head'),
        ('CT2', 'Force_Body_Dual','ct2_siemens_force_body_dual.json', 'head'),
        ('CT2', 'Force_Pediatric_Aa','ct2_siemens_force_pediatric_Aa.json', 'head'),

        ('CT2', 'Philips7500_Head', 'ct2_philips_7500_head.json'),
        ('CT2', 'Philips7500_Body', 'ct2_philips_7500_body.json'),
        ('CT2', 'Philips7500_Head_MonoE', 'ct2_philips_7500_head_monoe.json'),
        ('CT2', 'Philips7500_Body_MonoE', 'ct2_philips_7500_body_monoe.json'),

        ('SR', 'Horizon_Summary', 'sr_hologic_horizon_summary.json'),
        ('SR', 'Horizon_History', 'sr_hologic_horizon_history.json'),

    ]
                   
    for machine, fname in allMRObjects:
        for series in ['QA1', 'QA2', 'QA3']:
            allObjects.append( ('MR', machine, fname+'_%s.json'%series, series))

    for obj in allObjects:
        if len(obj) ==3:
            mod, m_id, filename = obj
        else:
            mod, m_id, filename, model = obj
        if args.destfolder:
            try:
                os.makedirs(os.path.join(args.destfolder, 'meta'))
            except OSError as e:
                if e.errno != errno.EEXIST:
                    print(e.errno, e)
                    raise

            fname = os.path.expanduser(os.path.join(args.destfolder,filename))
        else:
            fname = filename
        if mod == 'CT':
            writeCTConfig(m_id, filename=fname, anatomy=model)
        elif mod == 'CR':
            writeCRConfig(m_id, filename=fname)
        elif mod == 'MG':
            writeMGConfig(m_id, filename=fname, tubefilter=model)
        elif mod == 'MGCD':
            writeMGCDConfig(m_id, filename=fname)
        elif mod == 'MR':
            writeMRConfig(m_id, filename=fname, series=model)
        elif mod == 'NM':
            writeNMConfig(m_id, filename=fname)
        elif mod == 'US':
            writeUSConfig(m_id, filename=fname)
        elif mod == 'US2':
            writeUS2Config(m_id, filename=fname)
        elif mod == 'RF':
            writeRFConfig(m_id, filename=fname)
        elif mod == 'OT':
            writeOTConfig(m_id, filename=fname)
        elif mod == 'DX':
            writeDXConfig(m_id, filename=fname)
        elif mod == 'DXHough':
            writeDXHoughConfig(m_id, filename=fname)
        elif mod == 'DXU':
            writeDXUConfig(m_id, filename=fname)
        elif mod == 'OCR':
            writeOCRConfig(m_id, filename=fname)
        elif mod == 'FBIRNQC':
            writeFBIRNQCConfig(m_id, filename=fname)
        elif mod == 'FBIRNB0':
            writeFBIRNB0Config(m_id, filename=fname)
        elif mod == 'FBIRNB1':
            writeFBIRNB1Config(m_id, filename=fname)
        elif mod == 'FBIRNSNR':
            writeFBIRNSNRConfig(m_id, filename=fname)
        elif mod == 'FBIRNReceive':
            writeFBIRNReceiveConfig(m_id, filename=fname)
        elif mod == 'CT2':
            writeCT2Config(m_id, filename=fname)
        elif mod == 'SR':
            writeSRConfig(m_id, filename=fname)
