# Import Form
from flask_wtf import FlaskForm 

# Import Form elements such as StringField and BooleanField (optional)
from wtforms import BooleanField, HiddenField

# Import Form validators
from wtforms.validators import InputRequired, NoneOf

class ConfirmForm(FlaskForm):
    confirm = BooleanField('confirm:')
    extradata = HiddenField('extradata', [])

