# Import Form
from flask_wtf import FlaskForm
from wtforms import HiddenField

# base of dynamic form, other attributes are added on-the-fly
class ManualInputForm(FlaskForm):

    pass