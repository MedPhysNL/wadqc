# Import Form 
from flask_wtf import FlaskForm 

# Import Form elements such as StringField 
from wtforms import HiddenField, BooleanField, FieldList, FormField, StringField, SelectField, IntegerField, RadioField

# Import Form validators
from wtforms.validators import InputRequired, NoneOf

class TagEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    tag_id = HiddenField()
    tag_name = HiddenField()
    tag_selected = BooleanField('include tag')

class UserTagForm(FlaskForm):
    gid = HiddenField('gid', [])
    tags = FieldList(FormField(TagEntryForm))
    include_what = RadioField('make tag visible', choices=[('selection','selection'), ('all','all'), ('none', 'none')])
