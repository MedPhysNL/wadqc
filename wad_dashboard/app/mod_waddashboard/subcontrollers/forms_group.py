# Import Form 
from flask_wtf import FlaskForm 

# Import Form elements such as StringField 
from wtforms import HiddenField, BooleanField, FieldList, FormField, StringField, SelectField, IntegerField

# Import Form validators
from wtforms.validators import InputRequired, NoneOf

class SelectorEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    sid = HiddenField()
    sel_name = HiddenField()
    sel_description = HiddenField()
    sel_isactive = HiddenField()
    sel_selected = BooleanField('include in group')

class SubGroupEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    sub_id = HiddenField()
    sub_name = HiddenField()
    sub_description = HiddenField()
    sub_selected = BooleanField('include in group')

class GroupForm(FlaskForm):
    gid = HiddenField('gid', [])
    section = SelectField('section', coerce=int )
    name = StringField('group name', [InputRequired(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    description = StringField('group description')
    subgroups = FieldList(FormField(SubGroupEntryForm))

class SectionForm(FlaskForm):
    gid = HiddenField('gid', [])
    name = StringField('name', [InputRequired(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    description = StringField('description')
    position = IntegerField('position', [InputRequired(message='Position cannot be empty!')])

class SubGroupForm(FlaskForm):
    gid = HiddenField('gid', [])
    name = StringField('subgroup name', [InputRequired(message='Name cannot be empty!'), NoneOf(['None'],message='Name cannot be None!')])
    description = StringField('subgroup description')
    selectors = FieldList(FormField(SelectorEntryForm))
