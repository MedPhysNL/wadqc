from flask import Blueprint, render_template, Markup, url_for, redirect, request, session
try:
    from app.mod_auth.controllers import login_required
    from app.libs.shared import dbio_connect, getLocalIpAddress
    from app.libs import html_elements
except ImportError:
    from wad_dashboard.app.mod_auth.controllers import login_required
    from wad_dashboard.app.libs.shared import dbio_connect, getLocalIpAddress
    from wad_dashboard.app.libs import html_elements

dbio = dbio_connect()

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# Import modify forms
from .forms_notes import ModifyForm as ModifyFormNote

mod_blueprint = Blueprint('waddashboard_notes', __name__, url_prefix='/waddashboard/notes')

@mod_blueprint.route('/manage', methods=['GET', 'POST'])
@login_required
def manage_note():
    # display and allow handling of DataTags attached to data
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role>0:
        return redirect(url_for('waddashboard.home'))

    try:
        _gid = int(request.args['gid'])
        data = dbio.DBDataSets.get_by_id(_gid)
    except Exception as e:
        logger.error("Invalid DataSet id '{}' ({})".format(request.args.get('gid', None), e))
        
        # go back to overview page
        return(redirect(url_for('waddashboard.home')))

    title = "DataTags for {}".format(data.data_id)

    stuff = data.notes

    table_rows = []

    for note in stuff:
        table_rows.append([note.id, note.data_tag.name, note.description,
                           html_elements.Button(label='delete', href=url_for('.delete_note', gid=note.id)),
                           html_elements.Button(label='edit', href=url_for('.modify_note', gid=note.id)),
                           ])
    table_rows = sorted(table_rows, key=lambda x: x[0])
    table = html_elements.Table(headers=['id', 'tag', 'description'], 
                                rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')


    # add a direct link to open data in PACS
    url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}

    pacs_url = None
    if data.data_source.source_type.name == 'orthanc': # we know how to construct that url
        # determine the outside address of this server
        ip = getLocalIpAddress(data.data_source.host, request.base_url)
        pacs_url = '%s://%s:%s/app/explorer.html'%(data.data_source.protocol,
                                                   ip, 
                                                   data.data_source.port,
                                                   )
    if pacs_url is None:
        data_id = data.data_id
    else:
        url = '%s#%s?uuid=%s'%(pacs_url, url_part[data.data_type.name], data.data_id)
        data_id = html_elements.Link(label=data.data_id, href=url)
    
    # add tag
    newbutton = html_elements.Button(label='New', href=url_for('.modify_note', did=data.id))

    page = data_id + table + newbutton
    
    msg = "The following tags are attached to this dataset. Use the buttons to delete/edit/add tags."

    return render_template("waddashboard/generic.html", title=title, subtitle='', msg='', html=Markup(page),
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg})


@mod_blueprint.route('/delete_note', methods=['GET', 'POST'])
@login_required
def delete_note():
    """
    delete a note from the data
    """
    try:
        _gid = int(request.args['gid'])
        note = dbio.DBNotes.get_by_id(_gid)
    except Exception as e:
        logger.error("Invalid Note id '{}' ({})".format(request.args.get('gid', None), e))
        
        # go back to overview page
        return(redirect(url_for('waddashboard.home')))

    did = note.data_set.id
    note.delete_instance(recursive=True)

    return(redirect(url_for('.manage_note', gid=did)))


@mod_blueprint.route('/modify_note', methods=['GET', 'POST'])
@login_required
def modify_note():
    _gid = int(request.args['gid']) if 'gid' in request.args else None
    _did = int(request.args['did']) if 'did' in request.args else None

    # invalid table request are to be ignored
    # for now only orthanc is supported
    formtitle = 'Modify Note'
    form = ModifyFormNote(None if request.method=="GET" else request.form)
    if not _gid is None:
        src = dbio.DBNotes.get_by_id(_gid)
        form.description.data = src.description
        form.data_tag.data = src.data_tag.id # dropdown
        form.gid.data = _gid
        form.did.data = _did
    elif not _did is None:
        form.did.data = _did
        
    if form.gid is None or form.gid.data in ['', None]: 
        #define here, to avoid wrong label on redisplaying a form with errors
        formtitle = 'New Note'

    # Verify the form
    error = True
    if form.validate_on_submit():
        # the elements of the form
        field_dict = {k:v for k,v in request.form.items()}
        igid = 0
        if 'gid' in field_dict:
            try:
                igid = int(field_dict['gid'])
            except:
                pass
            
        msg = []
        if igid>0: # update, not create
            src = dbio.DBNotes.get_by_id(igid)
            error = False
            src.description = field_dict['description']
            src.data_tag = field_dict['data_tag']
            src.save()
            msg.append('Changed Note {} of DataSet {}.'.format(src.id, src.data_set.id))
        else:
            error = False
            idid = 0
            if 'did' in field_dict:
                try:
                    idid = int(field_dict['did'])
                except:
                    pass
            data_set = dbio.DBDataSets.get_by_id(idid)
            field_dict['data_set'] = data_set
            dbio.DBNotes.create(**field_dict)
            msg.append('Added new Note to DataSet {}.'.format(data_set.id))

        if error:
            inpanel={'type': "panel-danger", 'title': "ERROR", 'content':msg}
        else:
            inpanel={'type': "panel-success", 'title': "Success", 'content':msg}
            
        return render_template("waddashboard/generic.html", title='Notes', subtitle='', msg='', html="",
                               inpanel=inpanel)


    msg = [
        'Fill out the fields and click Submit'
    ]
    return render_template("waddashboard/notes_modify.html", form=form,
                           action=url_for('.modify_note'),
                           title=formtitle, msg=msg,
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg})
        
