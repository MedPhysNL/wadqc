# Import Form 
from flask_wtf import FlaskForm 

# Import Form elements such as StringField 
from wtforms import HiddenField, BooleanField, FieldList, FormField, StringField, SelectField, IntegerField, RadioField

# Import Form validators
from wtforms.validators import InputRequired, NoneOf

class GroupEntryForm(FlaskForm):
    # prefix with cfg_ to prevent no display due to clashes.
    grp_id = HiddenField()
    grp_name = HiddenField()
    grp_description = HiddenField()
    grp_section = HiddenField()
    grp_selected = BooleanField('include group')

class UserGroupForm(FlaskForm):
    gid = HiddenField('gid', [])
    groups = FieldList(FormField(GroupEntryForm))
    include_what = RadioField('make group visible', choices=[('selection','selection'), ('all','all'), ('none', 'none')])

