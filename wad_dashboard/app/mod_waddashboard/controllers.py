# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, session, redirect, url_for, Markup

from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, validators, StringField, SubmitField, IntegerField, FloatField, BooleanField, TimeField
from wtforms.validators import DataRequired, InputRequired
from wtforms.fields import DateTimeLocalField, DateField
from .subcontrollers.forms_manualinput import ManualInputForm

try:
    from app.mod_auth.controllers import login_required
    from app.libs import html_elements
    from app.libs import libgroups, libdisplay
    from .models import WDMainGroups, WDSections, WDSubGroups, WDMainItems, WDUserGroups, WDUsers, WDSubItems, WDUserTags
    from app.libs.shared import dbio_connect, INIFILE, getLocalIpAddress, bytes_as_string, string_as_bytes
except ImportError:
    from wad_dashboard.app.mod_auth.controllers import login_required
    from wad_dashboard.app.libs import html_elements
    from wad_dashboard.app.libs import libgroups, libdisplay
    from wad_dashboard.app.mod_waddashboard.models import WDMainGroups, WDSections, WDSubGroups, WDMainItems, WDUserGroups, WDUsers, WDSubItems, WDUserTags
    from wad_dashboard.app.libs.shared import dbio_connect, INIFILE, getLocalIpAddress, bytes_as_string, string_as_bytes

from wad_qc.connection import bytes_as_string
import json
import jsmin

from wad_qc.connection.pacsio import PACSIO

import datetime
from dateutil.relativedelta import relativedelta

# logging
from werkzeug.local import LocalProxy
from flask import current_app
logger = LocalProxy(lambda: current_app.logger)

# forms
from wad_dashboard.app.mod_waddashboard.subcontrollers.forms_consistency import ConsistencyForm

dbio = dbio_connect()
    
# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_blueprint = Blueprint('waddashboard', __name__, url_prefix='/waddashboard')

def get_visible_groups():
    """
    returns a list of visible MainGroups for the currently logged in user
    """
    if session.get('logged_in'):
        role = session.get('role')
        user = WDUsers.get_by_id(session.get('user_id'))
    else:
        return []
    if role>0:
        usergroups = [ug.maingroup for ug in WDUserGroups.select().where(WDUserGroups.user == user) ]
    else:  
        usergroups = WDMainGroups.select().order_by(WDMainGroups.name)

    return usergroups

def get_visible_tag_ids():
    """
    returns a list of visible Tags for the currently logged in user
    """
    if session.get('logged_in'):
        role = session.get('role')
        user = WDUsers.get_by_id(session.get('user_id'))
    else:
        return [] # not logged in, so nothing visible
    if role>0:
        usertags = [it.tag.id for it in WDUserTags.select().where(WDUserTags.user == user)]
    else:  
        return None # user is admin or key-user, so show all

    return usertags

def is_maingroup_visible(maingroup):
    """
    maingroup is a WDMainGroups instance
    """
    return maingroup in get_visible_groups()

def is_subgroup_visible(subgroup):
    """
    subgroup is a WDSubGroups instance
    """
    if session.get('logged_in'):
        role = session.get('role')
        if role == 0:
            return True

    for group in get_visible_groups():
        for sg in WDMainItems.select().where(WDMainItems.maingroup == group):
            if subgroup == sg.subgroup:
                return True

    return False

def is_selector_visible(selector):
    """
    selector is a DBSelectors instance
    """
    if session.get('logged_in'):
        role = session.get('role')
        if role == 0:
            return True

    for group in get_visible_groups():
        for sg in WDMainItems.select().where(WDMainItems.maingroup == group):
            for sel in WDSubItems.select().where(WDSubItems.subgroup == sg.subgroup):
                if selector == sel.selector:
                    return True

    return False

# Set the route and accepted methods
@mod_blueprint.route('/home', methods=['GET'])
@login_required
def home():
    """
    Just present a menu with choices; should all be require authorization
    """

    usergroups = get_visible_groups()

    menustoprow = []
    menus = [
    ]
    
    totalmenu=[
        { 'label':'QC dashboard', 'href':url_for('.qc') }, 
    ]

    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role == 0:
        totalmenu.append(
            { 'label':'All active selectors', 'href':url_for('.qc', all_selectors=1) }
        )   

    menustoprow.append({'title':'Overview', 'items':totalmenu})
    for section in WDSections.select().order_by(WDSections.position):
        title = section.name
        vizgroups = section.maingroups.order_by(WDMainGroups.name)

        selection = []
        for gr in vizgroups:
            if gr in usergroups:
                selection.append(gr)
        if len(selection) == 0:
            continue
        vizgroups = selection

        items = [ {'label':i.name , 'href':url_for('.qc', group=i.name),
                   'subitems': [{'label': si.subgroup.name, 'href':url_for('.qc',group=i.name, subgroup=si.subgroup.name)} 
                                for si in WDMainItems.select().join(WDSubGroups).where(WDMainItems.maingroup == i).order_by(WDSubGroups.name) ] } 
                  for i in vizgroups ]

        menus.append({'title':title, 'items':items})
        
    return render_template('waddashboard/home.html', menus=menus, menustoprow=menustoprow, title='WAD-QC Dashboard')

def show_this_qc(latest_date, production, latest_status, filters):
    """
    return True or False for filter selected
    """
    # no filters, must show
    if len(filters) == 0:
        return True

    # determine each status and the worst as well
    do_show = True
    worst = -1
    status = {}
    for key,elem in [('date',latest_date), ('production',production), ('status',latest_status)]:
        try:
            if '<div style="background-color:{}"'.format(libdisplay.COLOR_CRITICAL) in elem:
                status[key] = 2
            elif '<div style="background-color:{}"'.format(libdisplay.COLOR_WARNING) in elem:
                status[key] = 1
            elif '<div style="background-color:{}"'.format(libdisplay.COLOR_OK) in elem:
                status[key] = 0
        except TypeError:
            status[key] = 0 # assume 'good' for any elem without a color (status)
            
        worst = max(worst, status[key])
        if status[key] >= filters.get(key, -1):
            do_show = do_show & True
        else:
            do_show = False

    # if 'any' defined, then report only worst
    if 'any' in filters.keys():
        if worst >= filters['any']:
            return True
        else:
            return False
    
    return do_show 




@mod_blueprint.route('/manualinput')
@login_required
def manualinput():    
    # display and allow handling of processes DBProcesses table
    add_refresh = int(request.args.get('refresh', session.get('refresh')))
    msg = "There are no Processes in the queue."
    subtitle=[]
    status_manualinput = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == 'waiting for input')
    stuff = dbio.DBProcesses.select().where(dbio.DBProcesses.process_status==status_manualinput).order_by(dbio.DBProcesses.id.desc())
    table_rows = []
    pacs_url = {}
    url_part =  {'dcm_study':'study', 'dcm_series':'series', 'dcm_instance':'instance'}
    num_status = {}
    for data in stuff:
        if not data.process_status.name in num_status:
            num_status[data.process_status.name] = 0
        num_status[data.process_status.name] += 1

        if not data.data_set.data_source.name in pacs_url:
            pacs_url[data.data_set.data_source.name] = None
            if data.data_set.data_source.source_type.name == 'orthanc': # we know how to construct that url
                # determine the outside address of this server
                ip = data.data_set.data_source.host
                if ip == 'localhost':
                    ip = getLocalIpAddress(data.data_set.data_source.host,request.base_url)
                pacs_url[data.data_set.data_source.name] = '%s://%s:%s/app/explorer.html'%(data.data_set.data_source.protocol,
                                                                                  ip,
                                                                                   data.data_set.data_source.port,
                                                                                  )
        if pacs_url[data.data_set.data_source.name] is None:
            data_id = data.data_id
        else:
            url = '%s#%s?uuid=%s'%(pacs_url[data.data_set.data_source.name], url_part[data.module_config.data_type.name], data.data_set.data_id)
            data_id = html_elements.Link(label=data.data_set.data_id, href=url)

        table_rows.append([data.id, data.selector.name,
                           data.module_config.module.name, data.module_config.name,
                           data.module_config.data_type.name, data.data_set.data_source.name, data_id,
                           data.process_status.name, data.created_time.strftime('%Y-%m-%d %H:%M:%S'),
                           html_elements.Button(label='process', href=url_for('.domanualinput', gid=data.id)),
                           ])
    date_hdr = Markup('created_at'+20*'&nbsp;')
    table = html_elements.Table(headers=['id', 'selector', 'module', 'config', 'datatype', 'source', 'data_id', 
                                         'status', date_hdr], rows=table_rows,
                             _class='tablesorter-wadred', _id='sortTable')

    for key,val in num_status.items():
        subtitle.append('{}: {}'.format(key, val))

    if len(table_rows) >0:
        msg = [
            "Press process to open the manual input form"
        ]

    form = ConsistencyForm(None if request.method=="GET" else request.form)

    return render_template("waddashboard/consistency.html", title="Overview Manual Input processes", subtitle='', msg='', html=Markup(table),
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg},
                           form=form, add_refresh=add_refresh)    

    


@mod_blueprint.route('/manualinput/domanualinput/', methods=['GET','POST'])
@login_required
def domanualinput():
    # TODO:
    #   - pass input data on to the template as defaults when reprocessing

    _gid = int(request.args['gid']) if 'gid' in request.args else None
    
    valid = True
    
    # invalid table requests are to be ignored
    if _gid is None:
        valid = False
    if valid:
        try:
            process = dbio.DBProcesses.get_by_id(_gid)
            if process is None:
                valid = False
        except dbio.DBProcesses.DoesNotExist:
            valid = False

    if not valid:
        logger.error("Need a valid Process id")
        # go back to overview page
        return redirect(url_for('.manualinput')) # TODO return error message

    # init PACS connection
    pacsio = PACSIO(dbio.DBDataSources.get_by_id(process.data_set.data_source).as_dict())

    datatype_name=process.module_config.data_type.name #this tells us if the selector processes at study, series or instance level
    config_id = process.module_config.id #This returns the id of  the stored config file
    modconfig = json.loads(jsmin.jsmin(bytes_as_string(dbio.DBModuleConfigs.get_by_id(config_id).val)))
    manual_input = modconfig.get('manual_input') #this retrieves the manual_input section of the config file

    if datatype_name == 'dcm_study':
        uid=pacsio.getSharedStudyHeaders(process.data_set.data_id)["0020,000d"]["Value"]
    elif datatype_name == 'dcm_series':
        uid=pacsio.getSharedSeriesHeaders(process.data_set.data_id)["0020,000e"]["Value"]
    elif datatype_name == 'dcm_instance':
        uid=pacsio.getInstanceHeaders(process.data_set.data_id)["0008,0018"]["Value"]
    else:
        raise ValueError('[manual_input] Unimplemented datatype: %s'%(datatype_name))

    inputlist = []
    data = {}

    ## retrieve the manual input parameters from the processinput table
    sel_id = process.selector.id
    records = dbio.DBProcessInput.select().where(dbio.DBProcessInput.uid==uid,
                                                 dbio.DBProcessInput.selector_id==sel_id).order_by(dbio.DBProcessInput.name.asc())

    # create temporary class instead of modifying the global class (prevent problems with simultaneous sessions)
    class _ManualInputForm(ManualInputForm):
        pass
                                                 
    # loop over all input parameters and add these to the initialized empty baseclass
    for record in records:
        tmpname = record.name
        tmptype = record.result_type
        tmpdesc = record.desc
        tmpval = record.val
        required = record.required
        logger.debug("input: {} type: {}".format(tmpname,tmptype))
        inputlist.append({'name': tmpname, 'desc': tmpdesc})

        if tmptype == 'datetime':
            from datetime import datetime
            if not tmpval:
                data[tmpname] = datetime.now()
            else:
                datetime_object = datetime.strptime(tmpval, '%Y-%m-%dT%H:%M')
                data[tmpname] = datetime_object
        elif tmptype == 'date':
            from datetime import date, datetime
            if not tmpval:
                data[tmpname] = date.today()
            else:
                date_object = datetime.strptime(tmpval, '%Y-%m-%d').date()
                data[tmpname] = date_object
        elif tmptype == 'time':
            from datetime import datetime
            if tmpval:
                time_object = datetime.strptime(tmpval, '%H:%M:%S').time()
                data[tmpname] = time_object
        elif tmptype == 'bool':
            if tmpval and tmpval.lower() in ['true','y']:
                data[tmpname] = 'true'
        else:
            data[tmpname] = record.val

        validators =[]
        if required:
            validators.append(InputRequired())

        if tmptype == 'float':
            setattr(_ManualInputForm, tmpname, FloatField(tmpname,validators=validators))
        elif tmptype == 'string':
            setattr(_ManualInputForm, tmpname, StringField(tmpname,validators=validators))
        elif tmptype == 'bool':
            setattr(_ManualInputForm, tmpname, BooleanField(tmpname,validators=validators))
        elif tmptype == 'date':
            setattr(_ManualInputForm, tmpname, DateField(tmpname,format='%Y-%m-%d',validators=validators))
        elif tmptype == 'datetime':
            setattr(_ManualInputForm, tmpname, DateTimeLocalField(tmpname,format='%Y-%m-%dT%H:%M',validators=validators))
        elif tmptype == 'time':
            setattr(_ManualInputForm, tmpname, TimeField(tmpname,format='%H:%M:%S',validators=validators))

    #if request.form:
    #    form = ManualInputForm(request.form)
    #else:
    form = _ManualInputForm(data=data)

    if request.method == 'POST' and form.validate():
        logger.info('Sumbitted form: {}'.format(request.form))
        # add inputs to the database
        # change process status to 'new'
        
        ## we insert the manual input values from the form back into the dbprocessinput table
        for input in inputlist:
            input_name = input['name']
            val = request.form.get(input_name,'false')  # checkbox input is not part of the returned form object if unchecked
            sel_id = process.selector.id
            
            record = dbio.DBProcessInput.get(dbio.DBProcessInput.uid==uid,
                                             dbio.DBProcessInput.name==input_name,
                                             dbio.DBProcessInput.selector_id==sel_id)
            record.val = val
            record.save()

        status_new = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == "new")
        record = dbio.DBProcesses.get(dbio.DBProcesses.id == _gid)
        record.process_status = status_new
        record.save()

        return redirect(url_for('waddashboard.manualinput'))

    if form.errors:
        logger.info(form.errors)
        msg = 'Invalid form values. Please correct and retry.'
        inpanel = {'type': "panel-danger", 'title': "ERROR", 'content':msg}
    else:
        msg = 'Please provide the following input data. After submitting the process will be started automatically.'
        inpanel = {'type': "panel-success", 'title': "Info", 'content':msg}

    if request.method == 'POST':
        logger.info(request.form)

    return render_template('waddashboard/manualinput.html', form=form, inputlist=inputlist, inpanel=inpanel)


    
@mod_blueprint.route('/qc')
@login_required
def qc():
    # show a table with selectors:
    #   name, description, latest acq date (red/green), latest qc status (red/green), status production (red/green)

    usergroups = get_visible_groups()

    msg=''
    _group = request.args.get('group', None)
    _subgroup = request.args.get('subgroup', None)
    _separate = 1 if 'separate' in request.args else None
    _all_selectors = (str(request.args.get('all_selectors', None)) == "1")
    role = 2
    if session.get('logged_in'):
        role = session.get('role')
    if role > 0 and _all_selectors: # only for admin
        _all_selectors = False

    filters = {}
    possible_filters = ['date', 'status', 'production', 'any']
    for key in possible_filters:
        if key in request.args:
            try:
                filters[key] = int(request.args[key])
            except:
                pass
    if filters.get('any',-1)> -1: # remove all other filters
        filters = {'any':filters['any']}

    add_refresh = int(request.args.get('refresh', session.get('refresh')))
    
    # status of processes
    num_status = {}
    
    results = []
    stuff = []
    subgroups = []
    if not _subgroup is None:
        grps = [ sg.maingroup for sg in WDMainItems.select().join(WDSubGroups).where(WDSubGroups.name == _subgroup) ]
        for grp in grps:
            if grp in usergroups:
                subgroups = [_subgroup]
                break

    elif not _group is None:
        if WDMainGroups.get_by_name(_group) in usergroups:
            subgroups = sorted([ sg.subgroup.name for sg in WDMainItems.select().join(WDMainGroups).where(WDMainGroups.name == _group) ])
    else:
        # subgroup is None, group is None
        subgroups = set()
        for group in usergroups:
            for sg in WDMainItems.select().where(WDMainItems.maingroup == group):
                subgroups.add( sg.subgroup.name )
        subgroups = sorted(list(subgroups))
        
    if _all_selectors:
        res, stu = groupoverview(filters, None, None, True) 
        results.extend(res)
        stuff.extend(stu)
    else:    
        for sub in subgroups:
            res, stu = groupoverview(filters, _group, sub, not (_separate is None)) 
            results.extend(res)
            stuff.extend(stu)
    
    # find number of jobs still in processes table
    procs = dbio.DBProcesses.select().where(dbio.DBProcesses.selector << list(set(stuff)))
    for data in procs:
        if not data.process_status.name in num_status:
            num_status[data.process_status.name] = 0
        num_status[data.process_status.name] += 1
        
    msg = 'Number of processes still in the queue: {}'.format(len(procs))
    if len(procs)>0:
        msg += '\n\t['
        sep = ''
        for key,val in num_status.items():
            msg += f"{sep}{key}: {val}"
            sep = ',\n\t'
        msg += ']'
    
    table_rows = []
    for res in results:
        if res not in table_rows:
            table_rows.append(res)

    table = html_elements.Table(headers=['name', 'description', 'latest date', 'latest status', 
                                         'latest notes',
                                         'production 6m'],
                                rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')

    # add some status pickers
    stat_vals = [
        ('', -1),
        ('ok', 0),
        ('warning', 1),
        ('critical', 2)
    ]
    pickers = Markup()
    for fil in possible_filters:
        # make a dropdown selector
        linkopt = {'refresh': add_refresh}
        if _all_selectors:
            linkopt['all_selectors']=1
        if _separate:
            linkopt['separate']=1
            
        if not _group is None and not _subgroup is None:
            link = '"{}"'.format(url_for('.qc', group=_group, subgroup=_subgroup, **linkopt))
        elif not _group is None:
            link = '"{}"'.format(url_for('.qc', group=_group, **linkopt))
        elif not _subgroup is None:
            link = '"{}"'.format(url_for('.qc', subgroup=_subgroup, **linkopt))
        else:
            link = '"{}"'.format(url_for('.qc', **linkopt))

        if not fil == 'any': # for 'any' filter, reset other options to None
            for fil2 in possible_filters:
                if not fil2 == 'any': # for not-'any' filters, reset 'any' to None
                    if not fil2 == fil and fil2 in filters.keys():
                        link += '+"&{}="+jQuery("#{}").val()'.format(fil2, 'filter{}'.format(fil2))
        
        pname = 'filter{}'.format(fil)
        idname = [ [val, '{} >= {}'.format(fil, stat)] for stat,val in stat_vals ]
        picker = html_elements.Picker(name=pname, idname=idname, sid=filters.get(fil,-1),
                                      fun=Markup('location.href = {}+"&{}="+jQuery(this).val();'.format(link, fil)))
                
        pickers += picker

    # add a checkbox to ungroup all subgroups; only if there are subgroups in this view
    btnSeparateSelectors = ""
    if not _all_selectors and len(subgroups)>0: 
        btnLabel =  'Join Selectors' if _separate == 1 else 'Separate Selectors'
        btnFlags = {'refresh': add_refresh}
        if not _group is None: btnFlags['group'] = _group 
        if not _subgroup is None: btnFlags['subgroup'] = _subgroup
        if _separate is None: btnFlags['separate'] = 1
        btnSeparateSelectors = html_elements.Button(label=btnLabel, href=url_for('.qc', **btnFlags ))

    page = pickers+btnSeparateSelectors+table
    
    if _group is None and _subgroup is None:
        if _all_selectors:
            subtitle = 'All selectors'
        else:
            subtitle = ''
    elif _subgroup is None:
        subtitle = _group
    elif _group is None:
        subtitle = _subgroup
    else:
        subtitle = "{}/{}".format(_group, _subgroup)

    return render_template('waddashboard/generic.html', title='WAD-QC Status', msg='', subtitle=subtitle, 
                           html=Markup(page), add_refresh=add_refresh,
                           inpanel={'type': "panel-info", 'title': "info", 'content':msg})

def groupoverview(filters, group=None, subgroup=None, separate=False):
    """
    return a status page for a group or subgroup
    optionally the seperate flag is to return all selectors instead of one group result
    """
    subgroups = [] # sets of selectors
    selectors = [] # loose selectors
    if group is None and subgroup is None:
        # no group and no subgroup given, so describe all selectors
        include_inactive=False
        selectors = dbio.DBSelectors.select().order_by(dbio.DBSelectors.name)
        if not include_inactive: 
            selectors = selectors.where(dbio.DBSelectors.isactive == True).order_by(dbio.DBSelectors.name)
        subgroups = WDSubGroups.select()
        
    else:
        if subgroup is None: 
            # group but no subgroup given, so describe all loose selectors in group
            subgroups = [ it.subgroup for it in WDMainItems.select().join(WDMainGroups).where(WDMainGroups.name == group) ]
        else:
            # subgroup given, describe only selectors in sub group
            subgroups = [ WDSubGroups.get_by_name(subgroup) ]
        
        for dasubgroup in subgroups:
            subitems = dasubgroup.subitems
            for subitem in subitems:
                try:
                    selectors.append(dbio.DBSelectors.get_by_id(subitem.selector))
                except dbio.DBSelectors.DoesNotExist: # happens if a selector was deleted in WAD_Admin
                    subitem.delete_instance(recursive=False)
    
    # hide notes not visible to the user
    visible_tag_ids = get_visible_tag_ids()

    # give results for subgroups and loose selectors
    results = []

    stuff = [] # list of all used selectors
    if separate == False:
        # first add results for subgroups
        for dasubgroup in subgroups:
            subitems = dasubgroup.subitems
            sel_ids = []
            for subitem in subitems:
                try:
                    sel_ids.append(subitem.selector)
                except dbio.DBSelectors.DoesNotExist: # happens if a selector was deleted in WAD_Admin
                    subitem.delete_instance(recursive=False)
    
            partstuff = dbio.DBSelectors.select().where(dbio.DBSelectors.id << sel_ids).order_by(dbio.DBSelectors.name)
            stuff.extend(partstuff)
    
            # join results of all selectors into one result
            latest_id, latest_date, production, latest_status, latest_notes = libgroups.getLatestCombinedAcqResults(dbio, partstuff, visible_tag_ids, logger=logger)
            if show_this_qc(latest_date, production, latest_status, filters):
                results.append([html_elements.Link(label=dasubgroup.name, href=url_for('results.showgroupresults', subid=dasubgroup.id, rid=0)),
                                dasubgroup.description,
                                html_elements.Div(label=latest_date),
                                latest_status,
                                latest_notes,
                                production,
                                html_elements.Button(label='timeline', href=url_for('.timeline',subid=dasubgroup.id))
                                ])
            
    else:
        # add results for separate selectors
        selectors = sorted(selectors, key=lambda x: x.name)
        stuff.extend(selectors)
        for sel in selectors:
            latest_id, latest_date, production, latest_status, latest_notes = libgroups.getLatestCombinedAcqResults(dbio, [sel], visible_tag_ids, logger=logger)
            if show_this_qc(latest_date, production, latest_status, filters):
                results.append([html_elements.Link(label=sel.name, href=url_for('results.showresults', rid=latest_id)),
                                   sel.description,
                                   html_elements.Div(label=latest_date),
                                   latest_status,
                                   latest_notes,
                                   production,
                                   html_elements.Button(label='timeline', href=url_for('.timeline',sid=sel.id))
                                   ])

    return results, stuff

@mod_blueprint.route('/timeline')
@login_required
def timeline():
    # show a table: interval, created_at, status,  # in interval
    msg = []
    _sid = int(request.args['sid']) if 'sid' in request.args else None
    _subid = int(request.args['subid']) if 'subid' in request.args else None

    if _sid is None and _subid is None:
        logger.error("Invalid request. Missing parameters sid and subid.")
        return(redirect(url_for('.home')))
    _days = int(request.args['days']) if 'days' in request.args else int(366./2+.5)
    
    # hide notes not visible to the user
    visible_tag_ids = get_visible_tag_ids()

    hdrs = ['start -- end', 'submissions', 'latest status', 'all notes']
    if _subid is None:
        selector = dbio.DBSelectors.get_by_id(_sid)
        if not is_selector_visible(selector):
            logger.error("Invalid request. Selector is not visible to current user.")
            return(redirect(url_for('waddashboard.home')))

        subtitle = '%s: %s'%(selector.name, selector.description)
        selectors = [selector]
    else:
        hdrs = ['start -- end', 'complete submissions', 'latest status', 'all notes']
        subgroup = WDSubGroups.get_by_id(_subid)
        if not is_subgroup_visible(subgroup):
            logger.error("Invalid request. Subgroup is not visible to current user.")
            return(redirect(url_for('waddashboard.home')))
        
        subtitle = '%s: %s'%(subgroup.name, subgroup.description)
        selectors = [ sub.selector for sub in subgroup.subitems ]

    table_rows = []
    stuff, period = libgroups.getTimeLine(dbio, selectors, _days, visible_tag_ids, logger=logger)
    if period is None:
        logger.warn("There is nothing period defined for the timeline.")
    if len(stuff) == 0:
        logger.warn("There is nothing to show in the timeline.")
        #return redirect(url_for('.home'))
    for data in stuff:
        table_rows.append([ "{} -- {}".format(data[0].strftime('%Y-%m-%d'), data[1].strftime('%Y-%m-%d')), data[2], data[3], data[4]])

    table = html_elements.Table(headers=hdrs, rows=table_rows,
                                _class='tablesorter-wadred', _id='sortTable')
        
    if period is None:
        msg.append('There is nothing to show because there is no constraint_period defined.')
    else:
        subtitle += ' period = %d days'%(period)
        if len(stuff) == 0:
            if period>_days:
                msg.append( 'There is nothing to show because the constraint_period is larger than the checked time interval ({}days > {}days).'.format(period, _days) )

    # add messages of processes in the queue
    ##
    # find number of jobs still in processes table
    procnotes = []
    procs = dbio.DBProcesses.select().where(dbio.DBProcesses.selector << selectors)
    num_procstatus = {}
    for proc in procs:
        if not proc.process_status.name in num_procstatus:
            num_procstatus[proc.process_status.name] = 0
        num_procstatus[proc.process_status.name] += 1
        for note in proc.data_set.notes:
            if visible_tag_ids is None or note.data_tag.id in visible_tag_ids: # show all, or only those explicitely defined
                procnotes.append("{}:{}".format(note.data_tag.name, note.description))
        
    if len(procs)>0:
        msg.append('Number of relevant Processes still in the queue: {}'.format(len(procs)))
        for key,val in num_procstatus.items():
            msg.append(Markup(5*"&nbsp;")+"{}: {}".format(key,val))

    if len(procnotes)>0:
        msg.append("These Processes have the following notes:")
        for note in procnotes:
            msg.append(Markup(5*"&nbsp;")+note)
    ##

    # add a picker for reporting interval
    fun = Markup(
        'var qd = {};'\
        'location.search.substr(1).split("&").forEach(function(item) {var s = item.split("="), k = s[0], v = s[1] && decodeURIComponent(s[1]); (k in qd) ? qd[k].push(v) : qd[k] = [v]});' \
        'location.href = %s+"&days="+jQuery(this).val();'%('"?sid="+qd["sid"]' if _subid is None else '"?subid="+qd["subid"]'))
    idname = [ [ds, label] for ds, label in [(int(366./2+.5), 'half a year'), (366, 'one year'), (int(2*366), 'two years')] ]
    picker = html_elements.Picker(name="interval", idname=idname, sid=_days, label="Time interval:",
                                  fun=fun)

    page = picker+table
    
    if len(msg) > 1:
        return render_template('waddashboard/generic.html', title='WAD-QC Timeline', subtitle=subtitle, msg='', html=Markup(page),
                           inpanel={'type': "panel-warning", 'title': "NOTES", 'content':msg})
    else:
        return render_template('waddashboard/generic.html', title='WAD-QC Timeline', subtitle=subtitle, msg=msg, html=Markup(page))
        
def getLatestCreated(selector):
    processes = selector.processes
    results = selector.results
    latest = None
    if len(processes)> 0:
        latest = max([p.created_time for p in processes])
        
    if len(results)> 0:
        latest_result = max([p.created_time for p in results])
        if latest is None:
            latest = latest_result
        else:
            latest = max(latest, latest_result)
    
    return latest
