# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
try:
    from app.libs.shared import dbio_connect
    from app import AUTO_REFRESH
except ImportError:
    from wad_dashboard.app.libs.shared import dbio_connect
    from wad_dashboard.app import AUTO_REFRESH
    
from peewee import CharField, IntegerField, ForeignKeyField
from werkzeug.security import generate_password_hash

"""
Only update the versionnumber if the database (tables, fields) changes.
Use versionnumber as "20160531" or "20171020.1", indicating the date as the major versionnumber,
 and an additional point release if needed.
"""
__version__ = "20200106" # only update this versionnumber if the database (tables, fields) changes and then write upgrade script!

"""
Changelog:
  20200106: added WDUserTags
  20180312: added refresh to User; renamed User to WDUsers; added WDUserGroups;
            added wd_db_version to DBVariables to enable db upgrades; tables moved to wad_db
"""

dbio = dbio_connect()

role_names = {0: 'admin', 1: 'key-user', 2: 'user'}

# top level menu items
class WDSections(dbio.DBModel):
    name         = CharField(max_length=250, unique=True) 
    description  = CharField(max_length=250, null=True)
    position     = IntegerField() # postion on display

# menu items per top level menu; these are groups of groups of selectors
class WDMainGroups(dbio.DBModel):
    name         = CharField(max_length=250, unique=False) 
    description  = CharField(max_length=250, null=True) 
    section      = ForeignKeyField(WDSections, backref='maingroups')
    
# subitems to fill menu items; these are groups of selectors
class WDSubGroups(dbio.DBModel):
    name         = CharField(max_length=250, unique=True) 
    description  = CharField(max_length=250, null=True) 
    
# selectors to fill subitems
class WDSubItems(dbio.DBModel):
    selector  = ForeignKeyField(dbio.DBSelectors, backref='subitems')
    subgroup  = ForeignKeyField(WDSubGroups, backref='subitems')

# translation table linking SubGroups to MainGroups
class WDMainItems(dbio.DBModel):
    maingroup = ForeignKeyField(WDMainGroups, backref='mainitems')
    subgroup  = ForeignKeyField(WDSubGroups, backref='mainitems')

# Define a User model
class WDUsers(dbio.DBModel):
    # User Name
    username = CharField(unique=True) 

    # Identification Data: email & password
    password = CharField() 
    email    = CharField()

    # Authorisation Data: role & status
    role     = IntegerField() # 0 = admin, 1 = keyuser, 2 = technician
    status   = IntegerField()

    refresh  = IntegerField() # auto refresh timeout in seconds

# which groups are visible to the each user
class WDUserGroups(dbio.DBModel):
    user       = ForeignKeyField(WDUsers, backref='usergroups')
    maingroup  = ForeignKeyField(WDMainGroups, backref='usergroups')
    
# which tags are visible to the each user
class WDUserTags(dbio.DBModel):
    user = ForeignKeyField(WDUsers, backref='usertags')
    tag  = ForeignKeyField(dbio.DBDataTags, backref='usertags')

WDTables = [
    WDSections,
    WDMainGroups,
    WDUsers,
    WDUserGroups,
    WDUserTags,
    WDSubGroups,
    WDSubItems,
    WDMainItems,
]

def dbversion_smaller(v1, v2):
    """
    compare a two (major, minor) tuples
    """
    if v1[0]<v2[0]:
        return True
    if v1[0] == v2[0] and v1[1]<v2[1]:
        return True
    return False

def dbversion_equal(v1, v2):
    """
    compare a two (major, minor) tuples
    """
    if v1[0] == v2[0] and v1[1] == v2[1]:
        return True
    return False

def get_dbversion(version):
    """
    Split the string for database version to major, minor. Allow point releases
    """
    version = version.split('.')
    major = int(version[0])
    try:
        minor = int(version[1])
    except:
        minor = 0

    return major, minor


def _dbupgrade_20200106():
    """
    Add table WDDataTags; already done in init_models, but db version not yet set.
    """
    msg = ""
    error = True
    nw_version = "20200106"

    # create table WDUserTags if not exists (it should, but it doesn't hurt to try again)
    dbio.db.create_tables([WDUserTags], safe=True)

    # now updata version information
    version_cur = dbio.DBVariables.get(dbio.DBVariables.name == 'wd_db_version')
    version_cur.val = nw_version
    version_cur.save()

    msg = "waddashboard database upgraded to {}".format(version_cur.val)
    error = False

    return error, msg

def init_models():
    """
    Build the database: this should be run only once on first startup of WAD Dashboard.
    However, using safe=True prevents problems
    """
    
    # first tables need to be initialized!
    for tb in WDTables:
        tb._meta.database = dbio.db

    dbio.db.create_tables(WDTables, safe=True)

    if len(WDUsers.select()) == 0:
        WDUsers.create(username='root', password=generate_password_hash('waddemo'), email='root@localhost', role=0, status=1, refresh=AUTO_REFRESH)

    if len(dbio.DBVariables.select().where(dbio.DBVariables.name == 'wd_db_version')) == 0:
        dbio.DBVariables.create(name='wd_db_version', val=__version__)
 
    # check for required database updates specific for WAD-Dashboard
    current_version = get_dbversion(dbio.DBVariables.get(dbio.DBVariables.name == 'wd_db_version').val)
    latest_version = get_dbversion(__version__)
    error = False
    msgs = []
    while dbversion_smaller(current_version, latest_version):
        if dbversion_smaller(current_version, (20200106,0)):
            error, msg = _dbupgrade_20200106()
        msgs.append(msg.strip())
        if error:
            raise ValueError(msg)

        current_version = get_dbversion(dbio.DBVariables.get(dbio.DBVariables.name == 'wd_db_version').val)
