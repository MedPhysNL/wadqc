# Import flask and template operators
from flask import Flask, render_template, session, url_for, redirect
from flask_wtf import CSRFProtect

# auto refresh timeout in seconds
AUTO_REFRESH = 60

# create the database for WD* models
try:
    from app.mod_waddashboard.models import init_models
except ImportError:
    from wad_dashboard.app.mod_waddashboard.models import init_models
init_models()

# Define the WSGI application object
flask_app = Flask(__name__)
CSRFProtect(flask_app)
# Configurations
try:
    flask_app.config.from_object('config')
except ImportError:
    flask_app.config.from_object('wad_dashboard.config')

# Prevent caching of static imports by appending their modification timestamp to the url
@flask_app.url_defaults
def fix_static_urls(endpoint, values):
    import os
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(flask_app.root_path,
                                 endpoint, filename)
            values['ts'] = int(os.stat(file_path).st_mtime)

# Sample HTTP error handling
@flask_app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

# Import a module / component using its blueprint handler variable (mod_auth)
try:
    from app.mod_auth.controllers import mod_auth as auth_module
    from app.mod_auth.subcontrollers.usermanager import mod_blueprint as auth_users_module
    from app.mod_waddashboard.controllers import mod_blueprint as waddashboard_module
    from app.mod_waddashboard.subcontrollers.results import mod_blueprint as waddashboard_results
    from app.mod_waddashboard.subcontrollers.admin import mod_blueprint as waddashboard_admin
    from app.mod_waddashboard.subcontrollers.display import mod_blueprint as waddashboard_display
    from app.mod_waddashboard.subcontrollers.editor import mod_blueprint as waddashboard_editor
    from app.mod_waddashboard.subcontrollers.notes import mod_blueprint as waddashboard_notes
except ImportError as e:
    from wad_dashboard.app.mod_auth.controllers import mod_auth as auth_module
    from wad_dashboard.app.mod_auth.subcontrollers.usermanager import mod_blueprint as auth_users_module
    from wad_dashboard.app.mod_waddashboard.controllers import mod_blueprint as waddashboard_module
    from wad_dashboard.app.mod_waddashboard.subcontrollers.results import mod_blueprint as waddashboard_results
    from wad_dashboard.app.mod_waddashboard.subcontrollers.admin import mod_blueprint as waddashboard_admin
    from wad_dashboard.app.mod_waddashboard.subcontrollers.display import mod_blueprint as waddashboard_display
    from wad_dashboard.app.mod_waddashboard.subcontrollers.editor import mod_blueprint as waddashboard_editor
    from wad_dashboard.app.mod_waddashboard.subcontrollers.notes import mod_blueprint as waddashboard_notes

# Register blueprint(s)
flask_app.register_blueprint(auth_module)
flask_app.register_blueprint(auth_users_module)
flask_app.register_blueprint(waddashboard_module)
flask_app.register_blueprint(waddashboard_results)
flask_app.register_blueprint(waddashboard_admin)
flask_app.register_blueprint(waddashboard_display)
flask_app.register_blueprint(waddashboard_editor)
flask_app.register_blueprint(waddashboard_notes)
# ..
@flask_app.route('/', defaults={'path': ''})
@flask_app.route('/<path:path>')
def catch_all(path):
    return redirect(url_for('waddashboard.home')) 
