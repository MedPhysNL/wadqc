import os
from wad_qc.connection import dbio
from werkzeug.utils import secure_filename

import tempfile
import socket
from urllib.parse import urlparse

INIFILE = os.path.join(os.environ['WADROOT'], 'WAD_QC', 'wadconfig.ini')

import codecs
def string_as_bytes(x):
    return x.encode("utf-8")
    #return codecs.latin_1_encode(x)[0]
def bytes_as_string(x):
    if isinstance(x, memoryview):
        x = bytes(x)
    return x.decode("utf-8")
    #return codecs.latin_1_decode(x)[0]

isconnected = False
def dbio_connect():
    global isconnected
    if not isconnected:
        dbio.db_connect(INIFILE)
        isconnected = True
    return dbio

def getLocalIpAddress(host, base_url):
    """
    dummy param is just to ensure this function is not available directly from the website
    """
    # determine the outside address of this server
    host_from_base_url = urlparse(base_url).hostname

    # when browser url is of form http:\\localhost:####\ the orthanc servername won't be translated to an ip-address 
    if host_from_base_url != 'localhost':
        # find the ip address of localhost

        if host == 'localhost':
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            try:
                s.connect(('1.1.1.1', 1))
            except socket.error as e:
                # No network, but we are called from a website, so the host address should be localhost
                return 'localhost'
            
            #return s.getsockname()[0]
            return host_from_base_url

    # base_url is localhost
    return host

