from flask import render_template, Markup, url_for, redirect

def Button(label, name=None, type="button", href=None, _class=None):
    return Markup(render_template("elements/button.html", name=name, label=label, type=type, _class=_class, href=href))

def Table(headers=[], rows=[], _class=None, _id=None):
    return Markup(render_template("elements/table.html", headers=headers, rows=rows, _class=_class, _id=_id))

def Link(label, href, target=None):
    return Markup(render_template("elements/link.html", label=label, href=href, target=target))
    
def Image(label, src, width=None):
    return Markup(render_template("elements/image.html", src=src, label=label, width=width))

def Div(label, style=None, labels=None):
    return Markup(render_template("elements/div.html", label=label, style=style, labels=labels))

def Heading(label, type):
    return Markup(render_template("elements/heading.html", label=label, type=type))

def Picker(name="selector", idname=[], sid=None, fun=None, label=None):
    return Markup(render_template("elements/picker.html", name=name, idname=idname, sid=sid, fun=fun, label=label))
