#!/usr/bin/env python
from __future__ import print_function
"""
stand alone test for all functions in pacsio

Changelog:
  20160606: split inifile in dbconfig and setup
  20160530: removed get_pacs_config
  20160513: python3 compatible
  20160425: prefer pacsio init by dict
"""

__version__ = '20160606'

import logging

import os
try: 
    # this will fail unless wad_qc is already installed
    from wad_qc.connection.pacsio import PACSIO
    logging.info("** using installed package wad_qc **")
except ImportError: 
    import sys
    # add parent folder to search path for modules
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    from wad_qc.connection.pacsio import PACSIO
    logging.info("** development mode **")

from wad_qc.connection import dbio
from test_base import reset_logging

import tempfile
import shutil

def test_upload_delete(pacsio):
    # make sure we are in the correct folder
    os.chdir(os.path.abspath(os.path.dirname(__file__)))
    for level in ['instances','series','studies','patients']:
        logging.info('Uploading and deleting files...(testing %s)'%level)
        logs = pacsio.uploadDicomFolder('Some_dicom_data')
        for log in logs:
            logging.info('  Upload of %s: %s'%(log['Filename'],log['Status']))
        
        logging.info('    deleting %s'%level)
        if level == 'instances':
            # 3. delete all instances
            for patid in pacsio.getPatientIds():
                for studid in pacsio.getStudyIds(patid):
                    for serid in pacsio.getSeriesIds(studid):
                        for inid in pacsio.getInstancesIds(serid):
                            pacsio.deleteData(inid, 'dcm_instance')
        elif level == 'series':
            # 3. delete all series
            for patid in pacsio.getPatientIds():
                for studid in pacsio.getStudyIds(patid):
                    for serid in pacsio.getSeriesIds(studid):
                        pacsio.deleteData(serid, 'dcm_series')
        elif level == 'studies':
            # 3. delete all studies
            for patid in pacsio.getPatientIds():
                for studid in pacsio.getStudyIds(patid):
                    pacsio.deleteData(studid, 'dcm_study')
        elif level == 'patients':
            # 3. delete all patients
            for patid in pacsio.getPatientIds():
                pacsio.deleteData(patid, 'dcm_patient')
                            
        logging.info('    after deleting all at level %s, the following is left' % level)
        for patid in pacsio.getPatientIds():
            logging.info('     PatientID',patid)
            for studid in pacsio.getStudyIds(patid):
                logging.info('      StudyID',studid)
                for serid in pacsio.getSeriesIds(studid):
                    logging.info('       SeriesID',serid)
                    for inid in pacsio.getInstancesIds(serid):
                        logging.info('        InstanceID',inid)
    logging.info('')
    

def test_upload_download(pacsio):
    logging.info('Testing download of patients,studies,series,instances')
    pacsio.uploadDicomFolder('Some_dicom_data')
    try:
        tmp_dir = tempfile.mkdtemp()  # create dir
        logging.info(' Will download to %s'%tmp_dir)
        np = nst = nse = ni = 0
        for ip,patid in enumerate(pacsio.getPatientIds()):
            dcmdir = os.path.join(tmp_dir,'patient_%d'%ip)
            os.mkdir(dcmdir)
            pacsio.getData(patid, 'dcm_patient', dcmdir)
            np += 1
            
            for ist,studid in enumerate(pacsio.getStudyIds(patid)):
                dcmdir = os.path.join(tmp_dir,'study_%d_%d'%(ip,ist))
                os.mkdir(dcmdir)
                pacsio.getData(studid, 'dcm_study', dcmdir)
                nst += 1

                for ise,serid in enumerate(pacsio.getSeriesIds(studid)):
                    dcmdir = os.path.join(tmp_dir,'series_%d_%d_%d'%(ip,ist,ise))
                    os.mkdir(dcmdir)
                    pacsio.getData(serid, 'dcm_series', dcmdir)
                    nse += 1

                    for ii,instid in enumerate(pacsio.getInstancesIds(serid)):
                        dcmdir = os.path.join(tmp_dir,'instance_%d_%d_%d_%d'%(ip,ist,ise,ii))
                        os.mkdir(dcmdir)
                        pacsio.getData(instid, 'dcm_instance', dcmdir)
                        ni += 1
        logging.info(' Successfully downloaded:')
        logging.info('  %d complete patients'%np)
        logging.info('  %d complete studies'%nst)
        logging.info('  %d complete series'%nse)
        logging.info('  %d complete instances'%ni)
    finally:
        try:
            logging.info(' Deleting %s'%tmp_dir)
            shutil.rmtree(tmp_dir)  # delete directory
        except OSError as exc:
            if exc.errno != errno.ENOENT:  # ENOENT - no such file or directory
                raise  # re-raise exception
    # delete all patients
    for patid in pacsio.getPatientIds():
        pacsio.deleteData(patid, 'dcm_patient')
    

if __name__ == "__main__":
    """
    1. ensure Othanc is running and accessable with the provided credentials at the provided URL
    2. upload a dicom folder
    3. delete an instance
    """

    rootfolder = os.path.dirname(os.path.abspath(__file__)) # here this folder
    inifile = os.path.join(rootfolder,'wadconfig.ini')
    dbio.db_connect(inifile)
    pacsconfig = dbio.DBDataSources.select()[0].as_dict()
    pacsio = PACSIO(pacsconfig) # for testing, use the test config
    
    reset_logging('INFO')

    logging.info('OK. Orthanc server running at %s...'%pacsio.URL)
    logging.info('')

    # 2. upload and delete a dicom folder
    test_upload_delete(pacsio)

    # 3. check download
    test_upload_download(pacsio)
    
    
    
