WAD-QC 2.0: version 2 of the WAD-QC server written in python
============================================================

An open source implementation in python of the `WAD-QC 
Server <https://github.com/wadqc>`_: a server for automated analysis of 
medical images for quality control, by the `Society for Medical Physics of the Netherlands 
(NVKF) <http://www.nvkf.nl>`_. 

Documentation and instructions are in the `Wiki <https://bitbucket.org/MedPhysNL/wadqc/wiki>`_.