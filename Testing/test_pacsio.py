#!/usr/bin/env python
from __future__ import print_function

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import unittest
import subprocess
import time
import signal

from wad_qc.connection.pacsio import PACSIO

class testPACSIO(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.dcm_folder = "Some_dicom_data"
        self.dcm_file = os.path.join("Some_dicom_data", "Some_study", "some_series_id", "some_instance_id")
        
        config = {
            'name': 'WADQC',
            'typename': 'orthanc',
            'aetitle': 'WADQC',
            'protocol': 'http',
            'host': 'localhost',
            'port': 8055,
            'user':'orthanc',
            'pswd': 'waddemo'
        }
        
        self.proc = subprocess.Popen(['Orthanc', os.path.join('orthanc','test_pacsio.json')], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        time.sleep(0.5)
        try:
            self.pacsio = PACSIO(config)
        except:
            self.tearDownClass()
            raise
            
    @classmethod
    def tearDownClass(self):
        os.kill(self.proc.pid, signal.SIGTERM)

    def testUpload(self):
        log = self.pacsio.uploadDicomFile(self.dcm_file)
        self.assertEqual(log["Filename"], self.dcm_file)
        
        logs = self.pacsio.uploadDicomFolder(self.dcm_folder)
        self.assertEqual(len(logs), 2)
    
        patientIds = self.pacsio.getPatientIds()
        self.assertEqual(len(patientIds), 2)


if __name__ == "__main__":
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    unittest.main()
