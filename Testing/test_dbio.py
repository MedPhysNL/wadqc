#!/usr/bin/env python
from __future__ import print_function

import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

import unittest
import subprocess
import time
import signal
import peewee

from wad_qc.connection import dbio
from wad_qc.connection import get_config_dict_from_inifile, get_wadqc_root, get_data_sources

class testdbioIO(unittest.TestCase):
    some_module = {
        'name':'module_test',
        'uploadfilepath':os.path.join('Some_module', 'TestModule', 'testmodule.py'),
        'filename':'testmodule.py',
        'description':'test module',
    }
    some_config = {
        'modulename': some_module['name'],
        'name':'config_test',
        'description':'test config',
        'val':'<config/>',
    }
    some_selector = {
        'configname': some_config['name'], 
        'configversion': 1,
        'name': 'selector_test', 
        'description': 'test selector',
        'datatypename': 'dcm_series',
        'hidden': True,
        'frequency': 12,
    }
    some_process = {
        'selectorname': some_selector['name'],
        'datasourcename': 'WADQC',
        'data_id': 'not_a_study_id',
        'processstatusname':'new'
    }
    config_dict = get_config_dict_from_inifile('test_wadconfig.ini')
    wadqcroot = get_wadqc_root(config_dict)
    datasources = get_data_sources(config_dict)
        
    @classmethod
    def setUpClass(self):
        # Remove previous DB
        if os.path.exists(self.config_dict["iqc-db"]["DBASE"]):
            os.remove(self.config_dict["iqc-db"]["DBASE"])
            dbio.db_create_only(self.config_dict["iqc-db"], self.wadqcroot, self.datasources)
            
        # Initialize the DB and create tables if needed
        print('Connecting')
        dbio.db_connect(self.config_dict["iqc-db"])
        print('Truncate database')
        dbio.db_truncate(self.wadqcroot, self.datasources)
    
    @classmethod
    def setUp(self):
        # Clear all content from previous tests
        dbio.db_truncate(wadqcroot=self.wadqcroot, sources=self.datasources)
        
    @classmethod
    def tearDownClass(self):
        print('\nClosing database connection')
        dbio.db.close()

    @dbio.db.atomic()
    def testModules(self):
        #Create new entry
        module = dbio.DBModules.create(**self.some_module)
        self.assertIsInstance(module, dbio.DBModules)
        
        #See if we can retrieve the module
        self.assertEqual(module, dbio.DBModules.get(dbio.DBModules.name == self.some_module['name']))
        
        #Raise error when trying to add the same module twice
        self.assertRaises(ValueError, #peewee.IntegrityError,
            dbio.DBModules.create, **self.some_module)
        
        #And delete it again
        self.assertEqual(1, module.delete_instance())
        
        #Should be no modules now
        self.assertEqual(0, len(dbio.DBModules.select()))

    @dbio.db.atomic()
    def testModuleConfig(self):
        ##Add module (required to test moduleConfig)
        dbio.DBModules.create(**self.some_module)

        #Add config
        config = dbio.DBModuleConfigs.create(**self.some_config)
        self.assertIsInstance(config, dbio.DBModuleConfigs)

        #This version should be 1
        self.assertEqual(1, config.versionnumber)
        
        #Is the relation to the module working?
        self.assertEqual(config.module.name, self.some_module['name'])        
        
        #Add 9 more configs
        for i in range(9):
            dbio.DBModuleConfigs.create(**self.some_config)
        
        #Check if classmethod `getConfigsByName` returns configs in descending order of versionnumber
        versions = [c.versionnumber for c in dbio.DBModuleConfigs.getConfigsByName(self.some_config['name'])]
        self.assertEqual(list(range(10,0,-1)), versions)
        
        #Check reverse lookup
        module = dbio.DBModules.get(dbio.DBModules.name == self.some_module['name'])
        self.assertEqual(10, len(list(module.module_configs)))
        
        #Retrieve specific config
        config = dbio.DBModuleConfigs.get(
            dbio.DBModuleConfigs.name == self.some_config['name'], 
            dbio.DBModuleConfigs.versionnumber == 5)
        self.assertEqual(config.name, self.some_config['name'])
        self.assertEqual(config.versionnumber, 5)

    @dbio.db.atomic()
    def testSelector(self):
        #Add module and config
        dbio.DBModules.create(**self.some_module)
        dbio.DBModuleConfigs.create(**self.some_config)
        
        #Add selector
        selector = dbio.DBSelectors.create(**self.some_selector)
        
        #See if we can retrieve the selector
        self.assertEqual(selector, dbio.DBSelectors.get(dbio.DBSelectors.name == self.some_selector['name']))

        #Add a rule to the selector
        rule1 = selector.addRule(dicomtag="Some tag", logicname='ends with', values=["happy", "disaster"])
        
        #Add another rule
        rule2 = selector.addRule(dicomtag="Another tag", logicname='equals', values=["yes", "sure", "meh"])
        
        #Retrieve selector and rules
        self.assertEqual(selector, dbio.DBSelectors.get(dbio.DBSelectors.name == self.some_selector['name']))
        for rule in selector.rules:
            text = " ".join([rule.dicomtag, rule.logic.name, " or ".join([v.val for v in rule.values])])
            if rule == rule1:
                self.assertEqual("Some tag ends with happy or disaster", text)
            else:
                self.assertEqual("Another tag equals yes or sure or meh", text)


    @dbio.db.atomic()    
    def testProcess(self):
        #Add module, config and selector
        module = dbio.DBModules.create(**self.some_module)
        config = dbio.DBModuleConfigs.create(**self.some_config)
        selector = dbio.DBSelectors.create(**self.some_selector)
        
        #Add process
        process = dbio.DBProcesses.create(**self.some_process)
        
        #Add process_log
        process.process_log = 'that was easy'
        
        #Check it
        self.assertEqual('that was easy', process.process_log)
        
        #Check chain of references
        self.assertEqual(module, process.module_config.module)
        
        #Add 9 more processes
        for i in range(9):
            dbio.DBProcesses.create(**self.some_process)
        
        #Get processes with status 'new'
        new = dbio.DBProcesses.getProcessesByStatus('new')
        self.assertEqual(10, len(list(new)))
        
        #Set new to busy
        for proc in new:
            proc.process_status = dbio.DBProcessStatus.get(dbio.DBProcessStatus.name == 'busy')
            proc.save()
        
        #We should have 10 'busy' processes now
        status = [p.process_status.name for p in dbio.DBProcesses.select()]
        self.assertEqual(['busy']*10, status)

    @dbio.db.atomic()
    def testResults(self):
        #Add module, config, selector and process
        dbio.DBModules.create(**self.some_module)
        config = dbio.DBModuleConfigs.create(**self.some_config)
        dbio.DBSelectors.create(**self.some_selector)        
        process = dbio.DBProcesses.create(**self.some_process)
        
        #Add result
        dbio.DBResults.finishedProcess(process)
        
        #Retrieve result
        result = config.results[0]
        
        #Check if they share the same foreign keys and attributes
        fields = [f for f in dbio.DBResults._meta.sorted_field_names if f is not 'id']
        for field in fields:
            self.assertEqual(getattr(result, field), getattr(process, field))

        #Add some actual result values
        dbio.DBResultStrings.create(result=result, name="Some string", val="Hello")
        dbio.DBResultFloats.create( result=result, name="Some float1" , val=1.1)
        dbio.DBResultFloats.create( result=result, name="Some float2" , val=2.2)
        dbio.DBResultFloats.create( result=result, name="Some float3" , val=3.3)
        dbio.DBResultBools.create(  result=result, name="Some bool"  , val=True)
        dbio.DBResultObjects.create(result=result, name="Some object", val="<im a blob/>", filetype="xml")
        
        #Retrieve the results in orderly fasion
        results = result.getResults()
        self.assertEqual(1, len([r.name for r in results if isinstance(r, dbio.DBResultStrings)]))
        self.assertEqual(3, len([r.name for r in results if isinstance(r, dbio.DBResultFloats)]))
        self.assertEqual(1, len([r.name for r in results if isinstance(r, dbio.DBResultBools)]))
        self.assertEqual(1, len([r.name for r in results if isinstance(r, dbio.DBResultObjects)]))
        
        #Try cascaded removal
        result.delete_instance(recursive=True)
        self.assertEqual(0, len(list(dbio.DBResultFloats.select())))
        
        

if __name__ == "__main__":
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    unittest.main()
